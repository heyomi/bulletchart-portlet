package com.realdecoy.portlet.BulletChart.model.edit;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.endeca.portal.actions.model.CommonActionModel;
import com.endeca.portal.data.functions.util.DatePart;
import com.endeca.portal.lql.LQLBuilder;
import com.realdecoy.portlet.BulletChart.BulletChartEnum;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemConfig {
	private String metricKey;
	private LQLBuilder.Function metricAggre;
	private String metricFormatter;
	private String targetMetricKey;
	private LQLBuilder.Function targetMetricAggre;
	private String targetMetricFormatter;
	private String dimensionKey;
	private String dimensionFormatter;
	private List<DatePart> datePartCombo;
	private boolean isTopValue = true;
	private boolean isCustomDisplayName = true;
	private String customDisplayName;
	private boolean isCustomWidth;
	private int customWidth;
	private String description;
	private CommonActionModel action;
	private BulletChartEnum.BulletType featuredMeasure;
	private String customFeaturedMeasure;
	private String target;
	
	public ItemConfig(){
		
	}
	
	public String getMetricKey() {
		return metricKey;
	}
	public void setMetricKey(String metricKey) {
		this.metricKey = metricKey;
	}
	public LQLBuilder.Function getMetricAggre() {
		return metricAggre;
	}
	public void setMetricAggre(LQLBuilder.Function metricAggre) {
		this.metricAggre = metricAggre;
	}
	public String getMetricFormatter() {
		return metricFormatter;
	}
	public void setMetricFormatter(String metricFormatter) {
		this.metricFormatter = metricFormatter;
	}
	public String getTargetMetricKey() {
		return targetMetricKey;
	}
	public void setTargetMetricKey(String targetMetricKey) {
		this.targetMetricKey = targetMetricKey;
	}
	public LQLBuilder.Function getTargetMetricAggre() {
		return targetMetricAggre;
	}
	public void setTargetMetricAggre(LQLBuilder.Function targetMetricAggre) {
		this.targetMetricAggre = targetMetricAggre;
	}
	public String getTargetMetricFormatter() {
		return targetMetricFormatter;
	}
	public void setTargetMetricFormatter(String targetMetricFormatter) {
		this.targetMetricFormatter = targetMetricFormatter;
	}
	public String getDimensionKey() {
		return dimensionKey;
	}
	public void setDimensionKey(String dimensionKey) {
		this.dimensionKey = dimensionKey;
	}
	public String getDimensionFormatter() {
		return dimensionFormatter;
	}
	public void setDimensionFormatter(String dimensionFormatter) {
		this.dimensionFormatter = dimensionFormatter;
	}
	public List<DatePart> getDatePartCombo() {
		return datePartCombo;
	}
	public void setDatePartCombo(List<DatePart> datePartCombo) {
		this.datePartCombo = datePartCombo;
	}
	public boolean getIsTopValue() {
		return isTopValue;
	}
	public void setIsTopValue(boolean isTopValue) {
		this.isTopValue = isTopValue;
	}
	public boolean getIsCustomDisplayName() {
		return isCustomDisplayName;
	}
	public void setIsCustomDisplayName(boolean isCustomDisplayName) {
		this.isCustomDisplayName = isCustomDisplayName;
	}
	public String getCustomDisplayName() {
		return customDisplayName;
	}
	public void setCustomDisplayName(String customDisplayName) {
		this.customDisplayName = customDisplayName;
	}
	public boolean getIsCustomWidth() {
		return isCustomWidth;
	}
	public void setIsCustomWidth(boolean isCustomWidth) {
		this.isCustomWidth = isCustomWidth;
	}
	public int getCustomWidth() {
		return customWidth;
	}
	public void setCustomWidth(int customWidth) {
		this.customWidth = customWidth;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public CommonActionModel getAction() {
		return action;
	}
	public void setAction(CommonActionModel action) {
		this.action = action;
	}

	public BulletChartEnum.BulletType getFeaturedMeasure() {
		return featuredMeasure;
	}

	public void setFeaturedMeasure(BulletChartEnum.BulletType featuredMeasure) {
		this.featuredMeasure = featuredMeasure;
	}

	public String getCustomFeaturedMeasure() {
		return customFeaturedMeasure;
	}

	public void setCustomFeaturedMeasure(String customFeaturedMeasure) {
		this.customFeaturedMeasure = customFeaturedMeasure;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}
	
	
}
