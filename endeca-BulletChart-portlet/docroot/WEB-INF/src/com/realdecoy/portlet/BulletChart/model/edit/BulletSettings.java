package com.realdecoy.portlet.BulletChart.model.edit;

import com.realdecoy.portlet.BulletChart.BulletChartEnum;

public class BulletSettings {
	private int valueTextSize = 28;
	private static int valueTextMaxSize = 48;
	private static int valueTextMinSize = 11;
	private static double valueTextSizeIncrement = 0.5D;
	private int labelTextSize = 12;
	private static int labelTextMaxSize = 48;
	private static int labelTextMinSize = 11;
	private static double labelTextSizeIncrement = 0.5D;
	private BulletChartEnum.NamePositionType namePosition = BulletChartEnum.NamePositionType.BelowValue;
	
	public BulletSettings(){
		this.valueTextSize = 28;
		BulletSettings.valueTextMaxSize = 48;
		BulletSettings.valueTextMinSize = 11;
		BulletSettings.valueTextSizeIncrement = 0.5D;
		this.labelTextSize = 12;
		BulletSettings.labelTextMaxSize = 48;
		BulletSettings.labelTextMinSize = 11;
		BulletSettings.labelTextSizeIncrement = 0.5D;
	}
	
	public int getValueTextSize() {
		return valueTextSize;
	}
	public void setValueTextSize(int valueTextSize) {
		this.valueTextSize = valueTextSize;
	}
	public static int getValueTextMaxSize() {
		return valueTextMaxSize;
	}
	public static void setValueTextMaxSize(int valueTextMaxSize) {
		BulletSettings.valueTextMaxSize = valueTextMaxSize;
	}
	public static int getValueTextMinSize() {
		return valueTextMinSize;
	}
	public static void setValueTextMinSize(int valueTextMinSize) {
		BulletSettings.valueTextMinSize = valueTextMinSize;
	}
	public static double getValueTextSizeIncrement() {
		return valueTextSizeIncrement;
	}
	public static void setValueTextSizeIncrement(double valueTextSizeIncrement) {
		BulletSettings.valueTextSizeIncrement = valueTextSizeIncrement;
	}
	public int getLabelTextSize() {
		return labelTextSize;
	}
	public void setLabelTextSize(int labelTextSize) {
		this.labelTextSize = labelTextSize;
	}
	public static int getLabelTextMaxSize() {
		return labelTextMaxSize;
	}
	public static void setLabelTextMaxSize(int labelTextMaxSize) {
		BulletSettings.labelTextMaxSize = labelTextMaxSize;
	}
	public static int getLabelTextMinSize() {
		return labelTextMinSize;
	}
	public static void setLabelTextMinSize(int labelTextMinSize) {
		BulletSettings.labelTextMinSize = labelTextMinSize;
	}
	public static double getLabelTextSizeIncrement() {
		return labelTextSizeIncrement;
	}
	public static void setLabelTextSizeIncrement(double labelTextSizeIncrement) {
		BulletSettings.labelTextSizeIncrement = labelTextSizeIncrement;
	}

	public BulletChartEnum.NamePositionType getNamePosition() {
		return namePosition;
	}

	public void setNamePosition(BulletChartEnum.NamePositionType namePosition) {
		this.namePosition = namePosition;
	}
	
	
}
