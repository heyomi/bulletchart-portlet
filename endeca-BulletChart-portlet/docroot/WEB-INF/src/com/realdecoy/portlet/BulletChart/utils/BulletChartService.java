package com.realdecoy.portlet.BulletChart.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;

import org.apache.log4j.Logger;

import com.endeca.mdex.conversation.Assignment;
import com.endeca.mdex.conversation.EQL;
import com.endeca.mdex.conversation.Record;
import com.endeca.mdex.conversation.ResultRecords;
import com.endeca.mdex.conversation.Results;
import com.endeca.mdex.eql_parser.types.Query;
import com.endeca.portal.data.DataSource;
import com.endeca.portal.data.DataSourceException;
import com.endeca.portal.data.QueryResults;
import com.endeca.portal.data.QueryState;
import com.endeca.portal.data.functions.LQLQueryConfig;
import com.endeca.portal.data.functions.QueryFunction;
import com.endeca.portal.data.functions.RefinementFilter;
import com.endeca.portal.lql.LQLException;
import com.endeca.portal.mdex.DiscoveryServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.realdecoy.portlet.BulletChart.BulletChartConstants;
import com.realdecoy.portlet.BulletChart.BulletChartPortlet;
import com.realdecoy.portlet.BulletChart.model.edit.BulletConfig;
import com.realdecoy.portlet.BulletChart.model.edit.EditModel;
import com.realdecoy.portlet.BulletChart.model.view.Bullet;
import com.realdecoy.portlet.BulletChart.model.view.BulletModel;
import com.endeca.portlet.EndecaPortlet;
import com.endeca.portlet.util.EndecaPortletUtil;

public class BulletChartService {

    private static final Logger logger = Logger
        .getLogger(BulletChartPortlet.class);

    public List < HashMap < String, String >> getQueries(PortletRequest request,
        EditModel editModel) {
        List < HashMap < String, String >> queryMaps = new ArrayList < HashMap < String, String >> ();
        try {
            if (request != null && editModel != null) {
                int count = 0;
                for (BulletConfig config: editModel.getConfigs()) {
                    if (config.getItemConfig() != null && config.getItemConfig().getMetricKey() != null) {
                        HashMap < String, String > queryMap = EQLUtils
                            .getSummaryQuery(request,
                                config.getSemanticKey(), config);
                        queryMaps.add(queryMap);
                        editModel.getConfigs().get(count).setQueryMap(queryMap);
                    }
                }

            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return queryMaps;

    }

    public List < BulletModel > generate(PortletRequest request,
        EditModel editModel) {
        return loadAndGetBullets(getQueries(request, editModel), request,
            editModel);
    }

    public List < BulletModel > loadAndGetBullets(
        List < HashMap < String, String >> queryMaps, PortletRequest request,
        EditModel editModel) {
        List < BulletModel > bulletModels = new ArrayList < BulletModel > ();

        if (queryMaps.size() > 0) {
            BulletModel bulletModel = new BulletModel();
            int count = 0;

            for (BulletConfig bulletConfig: editModel.getConfigs()) {
                // for (HashMap<String, String> queryMap :
                // bulletConfig.getQueryMap()){
                Bullet bullet = new Bullet();
                
                if(bulletConfig.getQueryMap() != null){
                for (Map.Entry < String, String > entry: bulletConfig
                    .getQueryMap().entrySet()) {
                    if (entry.getKey().toString()
                        .equals(BulletChartConstants.MINIMUM)) {
                        bullet.setMinimum(15.05);
                    } else if (entry.getKey().toString()
                        .equals(BulletChartConstants.MAXIMUM)) {
                        bullet.setMaximum(55.2);
                    } else if (entry.getKey().toString()
                        .equals(BulletChartConstants.MEDIAN)) {
                        bullet.setMedian(35.57);
                    } else if (entry.getKey().toString()
                        .equals(BulletChartConstants.VARIANCE)) {
                        bullet.setVariance(23.05);
                    } else if (entry.getKey().toString()
                        .equals(BulletChartConstants.AVERAGE)) {
                        bullet.setAverage(27.57);
                    } else if (entry.getKey().toString()
                        .equals(BulletChartConstants.STDDEV)) {
                        bullet.setStddev(31.02);
                    }

                    
                    // }
                    
                    if(bullet.getMetric() == null){
                    	bullet.setMetric(bulletConfig.getItemConfig().getMetricKey());
                    }

                }
                
            }
                
                bulletModel.setBullets(bullet);
                if(!bulletModels.contains(bulletModel)){
                	bulletModels.add(bulletModel);
                }
            }
        }

        return bulletModels;
    }

    public void getBullets(PortletRequest request, EditModel editModel)
    throws DataSourceException, LQLException {
        List < HashMap < String, String >> queries = getQueries(request, editModel);

        String LQLQuery = null;
        DataSource dataSource = getDataSource(request);
        Query query = dataSource.parseLQLQuery(LQLQuery, true);
        LQLQueryConfig lqlQueryConfig = new LQLQueryConfig(query);

        List < Map < String, Object >> dataSourceRows = new ArrayList < Map < String, Object >> ();

        // Collect all results
        QueryState all = dataSource.getQueryState();

        // Remove all force element or capability refinement filters
        List < QueryFunction > functionsToRemove = new ArrayList < QueryFunction > ();
        Iterator < QueryFunction > it = all.getFunctions().iterator();
        /*
         * while (it.hasNext()) { QueryFunction function = it.next(); if
         * ((function instanceof RefinementFilter)) { // Check refinement //
         * filters only RefinementFilter filter = (RefinementFilter) function;
         * if (filter.getAttributeKey().equals("Capability") ||
         * filter.getAttributeKey().equals("ForceElement")) {
         * functionsToRemove.add(filter); } } }
         */

        all.removeFunctions(functionsToRemove);
        all.addFunction(lqlQueryConfig);

        // Execute the query
        QueryResults allResults = dataSource.execute(all);
        Results allcsResults = allResults.getDiscoveryServiceResults();

        if (allcsResults != null) {
            EQL allLQL = DiscoveryServiceUtil.getEQL(allcsResults);

            if (allLQL != null) {
                List < ResultRecords > allLQLResultRecords = allLQL
                    .getResultRecords();
                // logger.info("LQL Query number of results: "+
                // allLQLResultRecords.get(0).getRecord().size());

                for (Record lqlRecord: allLQLResultRecords.get(0).getRecord()) {
                    Map < String, Object > dataSourceRow = new HashMap < String, Object > ();

                    List < Assignment > assignmentList = lqlRecord.getAttribute();
                    Iterator < Assignment > iterator = assignmentList.iterator();
                    while (it.hasNext()) {
                        Assignment a = iterator.next();
                        dataSourceRow.put(a.getName(), a.getValue());
                    }
                    dataSourceRows.add(dataSourceRow);
                }
            }
        }

        if (true == false) {
            // Collect the filtered set of results
            QueryState q = dataSource.getQueryState();
            q.addFunction(lqlQueryConfig);

            // Execute the query
            QueryResults queryResults = dataSource.execute(q);
            Results csResults = queryResults.getDiscoveryServiceResults();

            if (csResults != null) {
                EQL lql = DiscoveryServiceUtil.getEQL(csResults);

                if (lql != null) {
                    List < ResultRecords > lqlResultRecords = lql
                        .getResultRecords();

                    // Find any filtered records that match the full set and set
                    // them to be highlighted
                    /*
                     * for (Record lqlRecord :
                     * lqlResultRecords.get(0).getRecord()) { Map<String,
                     * Object> row = new HashMap<String, Object>();
                     *
                     * List<Assignment> assignmentList =
                     * lqlRecord.getAttribute(); Iterator<Assignment> iterator =
                     * assignmentList.iterator(); while (it.hasNext()){
                     * Assignment a = iterator.next(); row.put(a.getName(),
                     * a.getValue()); }
                     *
                     * if (row.get("name") != null){ for (int i=0; i <
                     * dataSourceRows.size(); i++){ Map<String, Object>
                     * dataSourceRow = dataSourceRows.get(i); if
                     * ((dataSourceRow.get("name") != null) &&
                     * (dataSourceRow.get("name").equals(row.get("name"))) &&
                     * (dataSourceRow.get("type") != null) &&
                     * (dataSourceRow.get("type").equals(this.refinementType))){
                     * dataSourceRow.put("highlight", true); break; } } } }
                     */
                }
            }
        }
    }

    // Taken from com.endeca.portlet.util.EndecaPortletUtil
    protected DataSource getDataSource(PortletRequest request)
    throws DataSourceException {
        PortletPreferences preferences = request.getPreferences();
        String dataSourceId = preferences.getValue("dataSource", null);
        if ((dataSourceId == null) || (dataSourceId.equals(""))) {
            String defaultDataSourceId = EndecaPortletUtil
                .getApplicationDataSourceId(PortalUtil
                    .getHttpServletRequest(request));
            if (logger.isDebugEnabled()) {
                logger.debug("No data source configured for portlet: '" + EndecaPortletUtil.getPortletTitle(request) + "'. Defaulting to '" + defaultDataSourceId + "'");
            }

            dataSourceId = defaultDataSourceId;
            try {
                preferences.setValue("dataSource", dataSourceId);
                preferences.store();
            } catch (Exception e) {
                logger.warn("Portlet defaulted to default data source id '" + dataSourceId + "' but could not save preferences.", e);
            }
        }

        return new DataSource(request, dataSourceId);
    }

}