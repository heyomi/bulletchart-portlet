package com.realdecoy.portlet.BulletChart;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.endeca.mdex.conversation.Assignment;
import com.endeca.mdex.conversation.EQL;
import com.endeca.mdex.conversation.Record;
import com.endeca.mdex.conversation.ResultRecords;
import com.endeca.mdex.conversation.Results;
import com.endeca.mdex.eql_parser.types.Query;
import com.endeca.portal.data.DataSource;
import com.endeca.portal.data.DataSourceException;
import com.endeca.portal.data.MDEXState;
import com.endeca.portal.data.QueryResults;
import com.endeca.portal.data.QueryState;
import com.endeca.portal.data.functions.LQLQueryConfig;
import com.endeca.portal.data.functions.QueryFunction;
import com.endeca.portal.data.functions.RefinementFilter;
import com.endeca.portal.data.model.ModelException;
import com.endeca.portal.format.Formatter;
import com.endeca.portal.lql.LQLException;
import com.endeca.portal.mdex.DiscoveryServiceUtil;
import com.endeca.portal.session.UserSession;
import com.endeca.portal.session.UserSessionException;
import com.endeca.portlet.EndecaPortlet;
import com.endeca.portlet.ModelAndView;
import com.endeca.portlet.ProcessResource;
import com.endeca.portlet.util.EndecaPortletUtil;
import com.endeca.portlet.util.LanguageUtils;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.realdecoy.portlet.BulletChart.model.edit.BulletConfig;
import com.realdecoy.portlet.BulletChart.model.edit.EditModel;
import com.realdecoy.portlet.BulletChart.model.view.BulletModel;
import com.realdecoy.portlet.BulletChart.model.view.ViewModel;
import com.realdecoy.portlet.BulletChart.utils.BulletChartService;

/**
 * A portlet class designed to illustrate a basic Latitude Studio
 * component.
 *
 * @author Endeca Technologies, Inc.
 *
 */
public class BulletChartPortlet extends EndecaPortlet {

	public static final String RESOURCE_RECORDDATASTORE = "recordDataStore";
	public static final String RESOURCE_UPDATEEDITMODEL = "updateEditModel";
	public static final String PREFERENCE_EDITMODEL = "editModel";
	public static final String REQUEST_ATTRIBUTE_EDITMODEL = "editModel";
	public static final String REQUEST_ATTRIBUTE_VIEWMODEL = "viewModel";
	public static final String GROUP_ID = "groupId";
	public static final String REFINE_ACTION_NAME = "refine";

	private static final Logger logger = Logger.getLogger( BulletChartPortlet.class );

	private static final ObjectMapper jsonObjectMapper = new ObjectMapper();
	
	private boolean hasRefinements = false;
	private String refinementType = "";

	/* (non-Javadoc)
	 * Method override to handle view-mode runtime requests.
	 * @see com.endeca.portlet.EndecaPortlet#handleViewRenderRequest(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	protected ModelAndView handleViewRenderRequest(RenderRequest request,
			RenderResponse response) throws Exception {
		
		this.hasRefinements = queryStateHasRefinements(request);

		// EndecaPortlets use the ModelAndView object - similar to Spring MVC's - to pass information
		// to the JSP view layer
		ModelAndView mv = new ModelAndView(viewJSP);
		
		// load the edit model
		//PortletPreferences prefs = request.getPreferences();
		//EditModel editModel = jsonObjectMapper.readValue(prefs.getValue(PREFERENCE_EDITMODEL, "{}"), EditModel.class);
		EditModel editModel = getEditModel(request);
		
		ViewModel viewModel = new ViewModel();
		viewModel.setSettingsModel(editModel.getSettingsModel());
		viewModel.setConfigs(editModel.getConfigs());
		
		mv.addObject("viewModel", viewModel);

		// store the view model as a request attribute to pass it to the JSP/JS
		request.setAttribute(REQUEST_ATTRIBUTE_VIEWMODEL, editModel);
		
		
		
		return mv;
	}
	
	private EditModel getEditModel(PortletRequest request)
			     throws IOException, PortletException, DataSourceException
			   {
			     EditModel editModel = null;
			     PortletPreferences prefs = request.getPreferences();
			     String savedEditModel = prefs.getValue("prefsEditModel", "");
			     //SummarizationBarService service = new SummarizationBarService(request, getDataSource(request));
			     if (StringUtils.isNotBlank(savedEditModel)) {
			       try
			       {
			         editModel = (EditModel)jsonObjectMapper.readValue(EndecaPortletUtil.escapeJsonStringForHtml(savedEditModel), EditModel.class);
			       }
			       catch (Exception e)
			       {
			         logger.error("Failed to deserialize saved edit model for guided nav, expected behavior when upgrading.  JSON saved here for restore in event of unexpected corruption, but message should be ignored if this is an upgrade.");
			         logger.error(savedEditModel);
			         
			         prefs.reset("prefsEditModel");
			         prefs.store();
			       }
			     }
			     if (editModel == null) {
			       //editModel = getDefaultEditModel(service, request.getLocale());
			    	 editModel = jsonObjectMapper.readValue(prefs.getValue(PREFERENCE_EDITMODEL, "{}"), EditModel.class);
			     }
			     /*for (SummarizationConfig config : editModel.getConfigs()) {
			       config.setName(service.getSummarizationName(config));
			     }*/
			     editModel.setDefaultCollectionKey(EndecaPortletUtil.getDefaultCollection(request).getKey());
			     
			    //group id
			     editModel.setGroupId(PortalUtil.getScopeGroupId(request));

			    
				return editModel;
			   }
	
	public void doView(RenderRequest request, RenderResponse response) throws PortletException, IOException {
		try {
			Map<String, String> params = new HashMap<String, String>();

			if ("true".equals(request.getAttribute("doViewJavascriptUpdate"))) {
				Set<String> triggeredEvents = getAllPublishedEventNames(request);
				if (triggeredEvents == null) {
					triggeredEvents = Collections.emptySet();
				}
				params.put("asyncTrigger", jsonObjectMapper.writeValueAsString(triggeredEvents));
	
				response.setContentType("application/json; charset=UTF-8");
				response.setProperty("Cache-Control", "no-cache");
	
				jsonObjectMapper.writeValue(response.getWriter(), params);
			} else {
				super.doView(request, response);
			}
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

	/* (non-Javadoc)
	 * Method override to handle view-mode runtime requests.
	 * @see com.endeca.portlet.EndecaPortlet#handleEditRenderRequest(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	protected ModelAndView handleEditRenderRequest(RenderRequest request,
			RenderResponse response) throws Exception {

		// EndecaPortlets use the ModelAndView object - similar to Spring MVC's - to pass information
		// to the JSP view layer
		ModelAndView mv = new ModelAndView(editJSP);

		// load the edit model
		PortletPreferences prefs = request.getPreferences();
		EditModel editModel = jsonObjectMapper.readValue(prefs.getValue(PREFERENCE_EDITMODEL, "{}"), EditModel.class);

		Map<String, MDEXState> mdexStates = getMDEXStates(request);
		List<Map<String, Object>> dataSourceList = EndecaPortletUtil.getDataSourceMap(mdexStates, request);
		List<Map<String, Object>> dataSourceData = EndecaPortletUtil.getDataSourceMap(getMDEXStates(request, getUserSession(request)), request);
		
		mv.addObject("existingDataSourceMap", jsonObjectMapper.writeValueAsString(dataSourceList));
		mv.addObject("dataSourceData", jsonObjectMapper.writeValueAsString(dataSourceData));
		
		String defaultDataSourceId = EndecaPortletUtil.getDefaultDataSourceId();
		mv.addObject("defaultDataSourceId", prefs.getValue("dataSource", defaultDataSourceId));
		
		// store the edit model as a request attribute to pass it to the JSP/JS
		request.setAttribute(REQUEST_ATTRIBUTE_EDITMODEL, editModel);

		return mv;

	}

	/**
	 * Resource handler to update the portlet preferences
	 *
	 * @param request the ResourceRequest
	 * @param response the ResourceResponse
	 * @throws PortletException
	 * @throws IOException
	 */
	@ProcessResource(resourceId="savePrefs")
	public void serveResourceUpdateEditModel(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {

		/*if ( !EndecaPortletUtil.hasUpdatePrivileges(request, getContainer()) )
			return;

		PortletPreferences prefs = request.getPreferences();
		String jsonStringFromClient = IOUtils.toString(request.getPortletInputStream());

		try {
			// parse the request into the edit model to ensure it is valid JSON
			EditModel editModel = jsonObjectMapper.readValue(jsonStringFromClient, EditModel.class);

			prefs.setValue(PREFERENCE_EDITMODEL, jsonObjectMapper.writeValueAsString(editModel));
			prefs.store();

		} catch (IOException e) {
			// IO exceptions related to JSON parsing will be caught.  IO exceptions related to writing response will be thrown
			logger.error(e);
			response.setProperty(ResourceResponse.HTTP_STATUS_CODE, "500");
			response.getWriter().write(e.getMessage());
		}*/
		
		String jsonStringFromClient = IOUtils.toString(request.getPortletInputStream());
		if (logger.isDebugEnabled()) {
			 logger.debug("serveResourceSavePrefs: start");
			     }
			     if (!EndecaPortletUtil.hasUpdatePrivileges(request, getContainer()))
			    {
			     logger.warn("failed to save preferences due to insufficient privilege");
			       return;
			     }
			     String editModelString = getFirstParam(request, "editModel");
			  if (isEmpty(editModelString))
			     {
			      logger.error("failed to save preferences since parameter editModel is empty");
			      throw new PortletException("edit model cannot be empty");
			     }
			    PortletPreferences preferences = request.getPreferences();
			    EditModel editModel = jsonObjectMapper.readValue(editModelString, EditModel.class);
			     if (editModel == null)
			    {
			      logger.error("failed to parse string to edit model");
			      EndecaPortletUtil.writeErrorMessageResponse(LanguageUtils.getMessage(request, "summarizationbar.edit.message.failed-to-save-preferences"), response);
			      
			      return;
			    }
			     preferences.setValue("prefsEditModel", jsonObjectMapper.writeValueAsString(editModel));
			     if ((editModel.getConfigs() != null) && (editModel.getConfigs().get(0) != null)) {
			       preferences.setValue("dataSet", ((BulletConfig)editModel.getConfigs().get(0)).getSemanticKey());
			     }
			    try
			    {
			       preferences.store();
			       EndecaPortletUtil.writeMessageResponse(LanguageUtils.getMessage(request, "df.save-prefs-success"), response);
			     }
			     catch (IOException e)
			    {
			       logger.error("failed to save preferences.", e);
			       EndecaPortletUtil.writeErrorMessageResponse(LanguageUtils.getMessage(request, "summarizationbar.edit.message.failed-to-save-preferences"), response);
			     }
			     if (logger.isDebugEnabled()) {
			       logger.debug("serveResourceSavePrefs: end");
			     }
	}
	
	
	 @ProcessResource(resourceId="getSummarizations")
	   public void serveResourceGetSummarizations(ResourceRequest request, ResourceResponse response)
	     throws PortletException, IOException, DataSourceException
	{
		 EditModel editModel = null;
	 }
	
	@ProcessResource(resourceId = "getEditModel")
	public void serveResourceGetEditModel(ResourceRequest request,
			ResourceResponse response) throws PortletException, IOException,
			DataSourceException, LQLException, ModelException,
			Formatter.FormatParseException {
		if (logger.isDebugEnabled()) {
			logger.debug("serveResourceGetEditModel : start");
		}
		EditModel editModel = getEditModel(request);
		for (BulletConfig config : editModel.getConfigs()) {
			if (!config.getItemConfig().getIsCustomDisplayName()) {
				config.setDataSourceId(getDataSource(request).getId());
			}
		}
		EndecaPortletUtil.writeObjectResponse(editModel, response,
				jsonObjectMapper);
		if (logger.isDebugEnabled()) {
			logger.debug("serveResourceGetEditModel : end");
		}
	}

	/**
	 * Resource handler to act as a proxy for an Ext data source.
	 *
	 * Ext pagination uses request params start= and limit= for the initial rec offset and num per page, respectively.
	 *
	 * @param request the ResourceRequest
	 * @param response the ResourceResponse
	 * @throws PortletException
	 * @throws IOException
	 */
	@ProcessResource(resourceId = RESOURCE_RECORDDATASTORE)
	public void serveResourceRecordDataStore(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {
		PortletPreferences prefs = request.getPreferences();
		EditModel editModel = jsonObjectMapper.readValue(prefs.getValue(PREFERENCE_EDITMODEL, "{}"), EditModel.class);
		
		HashMap<String,Object> dataSourceRows = new HashMap<String,Object>();

		try {
			if(editModel.getConfigs().size() == 0){
				editModel = getEditModel(request);
			}
			
			BulletChartService service = new BulletChartService();
			
			dataSourceRows.put("heb", null);

			response.setContentType(com.endeca.portlet.util.Constants.MIME_TYPE_JSON + "; charset=UTF-8");
			response.setProperty("Cache-Control", "no-cache");

			jsonObjectMapper.writeValue(response.getWriter(), service.generate(request, editModel));

		} catch (Exception e) {
			// exceptions related to data source or JSON parsing will be caught.  IO exceptions related to writing response will be thrown
			logger.error(e);
			response.setProperty(ResourceResponse.HTTP_STATUS_CODE, "500");
			response.getWriter().write(e.getMessage());

		}
	}
	
	@ProcessAction(name=REFINE_ACTION_NAME)
	public void processRefineAction(ActionRequest request, ActionResponse response) throws PortletException, IOException {
		try {
			String dimensionValue = request.getParameter("dimensionValue");
			String propertyName = request.getParameter("propertyName");

			//Dimension name for refinement is found by using the depth and comparing it to the cascade fields
			PortletPreferences prefs = request.getPreferences();
			EditModel editModel = jsonObjectMapper.readValue(prefs.getValue(PREFERENCE_EDITMODEL, "{}"), EditModel.class);

			DataSource ds = getDataSource(request);
			QueryState q = ds.getQueryState();

			try{
				if (!dimensionValue.isEmpty() && !propertyName.isEmpty()) {
					List<QueryFunction> functionsToRemove = new ArrayList<QueryFunction>();

					//Check for the refinement filter with the name we want to remove
					for(QueryFunction function : q.getFunctions()){ 
						if ((function instanceof RefinementFilter)) { //Check refinement filters only
							RefinementFilter filter = (RefinementFilter) function;

							if(filter.getAttributeKey().equals("Capability") || filter.getAttributeKey().equals("ForceElement")){
								functionsToRemove.add(function);
							}
						}
					}

					//Remove post loop
					q.removeFunctions(functionsToRemove);
					
					RefinementFilter newFilter = new RefinementFilter(dimensionValue, propertyName);
					q.addFunction(newFilter);
				} else {
					List<QueryFunction> functionsToRemove = new ArrayList<QueryFunction>();

					//Check for the refinement filter with the name we want to remove
					for(QueryFunction function : q.getFunctions()){ 
						if ((function instanceof RefinementFilter)) { //Check refinement filters only
							RefinementFilter filter = (RefinementFilter) function;

							if(filter.getAttributeKey().equals("Capability") || filter.getAttributeKey().equals("ForceElement")){
								functionsToRemove.add(function);
							}
						}
					}

					//Remove post loop
					q.removeFunctions(functionsToRemove);
				}
			} catch (Exception e) { 
				throw new PortletException("An error occurred while constructing Refinement Filter", e); 
			}


			ds.setQueryState(q);

			request.setAttribute("doViewJavascriptUpdate", "true");       

			logger.info("Refined BulletChart with value: " + dimensionValue + " on property: " + propertyName);
		} catch (DataSourceException e) {
			logger.warn("Exception encountered setting new refinements", e);
		}
	}
	
	private boolean queryStateHasRefinements(RenderRequest request) throws Exception {
		DataSource dataSource = getDataSource(request);
	    
		QueryState state = dataSource.getQueryState();
		
		//Check for a force element or capability refinement filter
		for(QueryFunction function : state.getFunctions()){ 
			if ((function instanceof RefinementFilter)) { //Check refinement filters only
				RefinementFilter filter = (RefinementFilter) function;

				if(filter.getAttributeKey().equals("Capability")) {
					this.refinementType = "Capability"; 
					return true;
				} else if (filter.getAttributeKey().equals("ForceElement")){
					this.refinementType = "ForceElement"; 
					return true;
				}
			}
		}

		return false;
	}
	
	private boolean queryStateHasRefinements(ResourceRequest request) throws Exception {
		DataSource dataSource = getDataSource(request);
    
		QueryState state = dataSource.getQueryState();
		
		//Check for a force element or capability refinement filter
		for(QueryFunction function : state.getFunctions()){ 
			if ((function instanceof RefinementFilter)) { //Check refinement filters only
				RefinementFilter filter = (RefinementFilter) function;

				if(filter.getAttributeKey().equals("Capability")) {
					this.refinementType = "Capability"; 
					return true;
				} else if (filter.getAttributeKey().equals("ForceElement")){
					this.refinementType = "ForceElement"; 
					return true;
				}
			}
		}

		return false;
	}

	private List<Map<String, Object>> getLQLDataSourceRows(String LQLQuery, ResourceRequest request) throws Exception {
		logger.info("LQL Query: " + LQLQuery);
		
		PortletPreferences prefs = request.getPreferences();
		EditModel editModel = jsonObjectMapper.readValue(prefs.getValue(PREFERENCE_EDITMODEL, "{}"), EditModel.class);
    
	    List<Map<String, Object>> dataSourceRows = new ArrayList<Map<String, Object>>();
	    
	    // Check if refinements are enabled
		this.hasRefinements = queryStateHasRefinements(request);

	    DataSource dataSource = getDataSource(request);
		Query query = dataSource.parseLQLQuery(LQLQuery, true);
	    LQLQueryConfig lqlQueryConfig = new LQLQueryConfig(query);
	    
	    // Collect all results
	    QueryState all = dataSource.getQueryState();
	    
	    //Remove all force element or capability refinement filters
	    List<QueryFunction> functionsToRemove = new ArrayList<QueryFunction>();
	    Iterator<QueryFunction> it = all.getFunctions().iterator();
	    while (it.hasNext()) {
	    	QueryFunction function = it.next();
	    	if ((function instanceof RefinementFilter)) { //Check refinement filters only
  				RefinementFilter filter = (RefinementFilter) function;
  				if(filter.getAttributeKey().equals("Capability") || filter.getAttributeKey().equals("ForceElement")){
  					functionsToRemove.add(filter);
  				}
  			}
	    }
	    
	    all.removeFunctions(functionsToRemove);
	    all.addFunction(lqlQueryConfig);

	    // Execute the query
	    QueryResults allResults = dataSource.execute(all);
	    Results allcsResults = allResults.getDiscoveryServiceResults();
	    
	    if (allcsResults != null) {
	    	EQL allLQL = DiscoveryServiceUtil.getEQL(allcsResults);
	    	
	    	if (allLQL != null) {
	    		List<ResultRecords> allLQLResultRecords = allLQL.getResultRecords();
	    		logger.info("LQL Query number of results: " + allLQLResultRecords.get(0).getRecord().size());
	    		
	    		for (Record lqlRecord : allLQLResultRecords.get(0).getRecord()) {
	    			Map<String, Object> dataSourceRow = new HashMap<String, Object>();
	    			
	    			List<Assignment> assignmentList = lqlRecord.getAttribute();
	    			Iterator<Assignment> iterator = assignmentList.iterator();
	    			while (it.hasNext()){
	    				Assignment a = iterator.next();
	    				dataSourceRow.put(a.getName(), a.getValue());
	    			}
	    			dataSourceRows.add(dataSourceRow);
	    		}
	    	}
	    }
	    
	    if (this.hasRefinements){
		    // Collect the filtered set of results
		    QueryState q = dataSource.getQueryState();
		    q.addFunction(lqlQueryConfig);
	
		    // Execute the query
		    QueryResults queryResults = dataSource.execute(q);
		    Results csResults = queryResults.getDiscoveryServiceResults();
		    
		    if (csResults != null) {
		    	EQL lql = DiscoveryServiceUtil.getEQL(csResults);
		    	
		    	if (lql != null) {
		    		List<ResultRecords> lqlResultRecords = lql.getResultRecords();
		    		
		    		// Find any filtered records that match the full set and set them to be highlighted
		    		for (Record lqlRecord : lqlResultRecords.get(0).getRecord()) {
		    			Map<String, Object> row = new HashMap<String, Object>();
		    			
		    			List<Assignment> assignmentList = lqlRecord.getAttribute();
		    			Iterator<Assignment> iterator = assignmentList.iterator();
		    			while (it.hasNext()){
		    				Assignment a = iterator.next();
		    				row.put(a.getName(), a.getValue());
		    			}
		    			
		    			if (row.get("name") != null){
		    				for (int i=0; i < dataSourceRows.size(); i++){
		    					Map<String, Object> dataSourceRow = dataSourceRows.get(i);
		    					if ((dataSourceRow.get("name") != null) && (dataSourceRow.get("name").equals(row.get("name"))) 
		    							&& (dataSourceRow.get("type") != null) && (dataSourceRow.get("type").equals(this.refinementType))){
		    						dataSourceRow.put("highlight", true);
		    						break;
		    					}
		    				}
		    			}
		    		}
		    	}
		    }
	    }
	    
	    return dataSourceRows;
	}
	
	protected Map<String, MDEXState> getMDEXStates(PortletRequest request)
			throws Exception {
		return super.getUserSession(request).getMDEXStatesByApplicationId(
				PortalUtil.getScopeGroupId(request), request);
	}
	
	public Map<String, MDEXState> getMDEXStates(PortletRequest request,
			UserSession session) throws UserSessionException {
		Map<String, MDEXState> dataSourceMap = session
				.getMDEXStatesByApplicationId(
						PortalUtil.getScopeGroupId(request), request);
		if (dataSourceMap == null) {
			dataSourceMap = new HashMap();
		}
		return dataSourceMap;
	}
		
}
