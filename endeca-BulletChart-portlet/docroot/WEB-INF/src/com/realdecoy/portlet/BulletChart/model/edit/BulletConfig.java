package com.realdecoy.portlet.BulletChart.model.edit;


import java.util.HashMap;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.endeca.portal.lql.LQLBuilder;
import com.realdecoy.portlet.BulletChart.BulletChartEnum;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BulletConfig {
	private String id;
	private String name;
	private boolean isCompleted;
	private String dataSourceId;
	private String semanticKey;
	private String semanticName;
	private BulletChartEnum.BulletType type;
	private String metricKey;
	private LQLBuilder.Function metricAggre;
	private String metricFormatter;
	private boolean conditionEnabled = true;
	//private ConditionsConfig conditionsConfig;
	//private InfoConfig infoConfig;
	//private AlertConfig alertConfig;
	private ItemConfig itemConfig;
	private boolean isCustomerizedDesc = false;
	//private BulletChartEnum.BulletType featuredMeasure;
	private HashMap < String, String > queryMap;
	
	public BulletConfig(){
		
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isCompleted() {
		return isCompleted;
	}
	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}
	public String getDataSourceId() {
		return dataSourceId;
	}
	public void setDataSourceId(String dataSourceId) {
		this.dataSourceId = dataSourceId;
	}
	public String getSemanticKey() {
		return semanticKey;
	}
	public void setSemanticKey(String semanticKey) {
		this.semanticKey = semanticKey;
	}
	public String getSemanticName() {
		return semanticName;
	}
	public void setSemanticName(String semanticName) {
		this.semanticName = semanticName;
	}
	public BulletChartEnum.BulletType getType() {
		return type;
	}
	public void setType(BulletChartEnum.BulletType type) {
		this.type = type;
	}
	public String getMetricKey() {
		return metricKey;
	}
	public void setMetricKey(String metricKey) {
		this.metricKey = metricKey;
	}
	public String getMetricFormatter() {
		return metricFormatter;
	}
	public void setMetricFormatter(String metricFormatter) {
		this.metricFormatter = metricFormatter;
	}
	public boolean isConditionEnabled() {
		return conditionEnabled;
	}
	public void setConditionEnabled(boolean conditionEnabled) {
		this.conditionEnabled = conditionEnabled;
	}
	public boolean isCustomerizedDesc() {
		return isCustomerizedDesc;
	}
	public void setCustomerizedDesc(boolean isCustomerizedDesc) {
		this.isCustomerizedDesc = isCustomerizedDesc;
	}
	public LQLBuilder.Function getMetricAggre() {
		return metricAggre;
	}
	public void setMetricAggre(LQLBuilder.Function metricAggre) {
		this.metricAggre = metricAggre;
	}
	public ItemConfig getItemConfig() {
		return itemConfig;
	}
	public void setItemConfig(ItemConfig itemConfig) {
		this.itemConfig = itemConfig;
	}

	public HashMap<String, String> getQueryMap() {
		return queryMap;
	}

	public void setQueryMap(HashMap<String, String> queryMap) {
		this.queryMap = queryMap;
	}	

	/*public BulletChartEnum.BulletType getFeaturedMeasure() {
		return featuredMeasure;
	}

	public void setFeaturedMeasure(BulletChartEnum.BulletType featuredMeasure) {
		this.featuredMeasure = featuredMeasure;
	}*/
	

}