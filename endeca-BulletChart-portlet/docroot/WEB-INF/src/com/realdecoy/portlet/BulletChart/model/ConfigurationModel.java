package com.realdecoy.portlet.BulletChart.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ConfigurationModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String key;
	private String dataSourceId;
	private List<String> semanticKeys;
	private String displayName;
	private int maxGroupNum;
	private String maxGroupRemark;
	private BulletChartModel chart;
	private String tabCd;
	@SuppressWarnings("unused")
	private boolean multiViews;

	public ConfigurationModel() {
		this.key = (new Date().getTime() + "");
		this.maxGroupNum = 1000;
		this.maxGroupRemark = "";
		this.chart = new BulletChartModel();
		this.displayName = this.key;
		this.tabCd = "data";
		this.multiViews = false;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDataSourceId() {
		return dataSourceId;
	}

	public void setDataSourceId(String dataSourceId) {
		this.dataSourceId = dataSourceId;
	}

	public List<String> getSemanticKeys() {
		return semanticKeys;
	}

	public void setSemanticKeys(String semanticKey) {
		 List<String> keys = new ArrayList<String>();
		 keys.add(semanticKey);
		 this.semanticKeys = keys;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public int getMaxGroupNum() {
		return maxGroupNum;
	}

	public void setMaxGroupNum(int maxGroupNum) {
		this.maxGroupNum = maxGroupNum;
	}

	public String getMaxGroupRemark() {
		return maxGroupRemark;
	}

	public void setMaxGroupRemark(String maxGroupRemark) {
		this.maxGroupRemark = maxGroupRemark;
	}

	public BulletChartModel getChart() {
		return chart;
	}

	public void setChart(BulletChartModel chart) {
		this.chart = chart;
	}

	public String getTabCd() {
		return tabCd;
	}

	public void setTabCd(String tabCd) {
		this.tabCd = tabCd;
	}

	public boolean isMultiViews() {
		return getSemanticKeys().size() > 1;
	}

	public void setMultiViews(boolean multiViews) {
		this.multiViews = multiViews;
	}
}
