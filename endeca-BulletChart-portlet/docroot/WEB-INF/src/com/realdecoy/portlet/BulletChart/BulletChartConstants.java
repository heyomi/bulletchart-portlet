package com.realdecoy.portlet.BulletChart;

public abstract interface BulletChartConstants {
	public static final String RESOURCE_GET_EDIT_MODEL = "getEditModel";
	public static final String RESOURCE_SAVE_PREFS = "savePrefs";
	public static final String RESOURCE_GET_BULLETS = "getBullets";
	public static final String RESOURCE_UPDATE_SHOW_CONFIG_MESSAGE = "updateShowConfigMessage";
	public static final String ACTION_REVERT_PREFS = "resetPrefs";
	public static final String ACTION_REFINEMENT = "refinement";
	public static final String ACTION_HYPERLINK = "hyperlink";
	public static final String MAXIMUM = "max";
	public static final String MINIMUM = "min";
	public static final String AVERAGE = "avg";
	public static final String MEDIAN = "median";
	public static final String STDDEV = "std";
	public static final String VARIANCE = "var";
}
