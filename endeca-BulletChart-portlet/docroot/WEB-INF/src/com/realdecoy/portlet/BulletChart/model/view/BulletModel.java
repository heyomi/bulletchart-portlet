package com.realdecoy.portlet.BulletChart.model.view;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

public class BulletModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//private String message;
	//private String key;
	private List<Bullet> bullets;
	
	
	/*public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}*/
	public List<Bullet> getBullets() {
		return bullets;
	}
	public void setBullets(List<Bullet> bullets) {
		this.bullets = bullets;
	}
	
	public void setBullets(Bullet bullet) {
		if(this.bullets == null){
			this.bullets = new ArrayList<Bullet>();
		}
		if(bullet != null){
			this.bullets.add(bullet);
		}
	}
	
	
}
