package com.realdecoy.portlet.BulletChart.model.edit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Representation of the "Edit Model" - the data model used to drive the edit
 * screen and save all preferences.
 * 
 * @author Endeca Technologies, Inc.
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EditModel {

	private BulletSettings settingsModel;
	private String defaultCollectionKey;
	private String groupId;

	private List<BulletConfig> configs = new ArrayList<BulletConfig>();

	public String getDefaultCollectionKey() {
		return defaultCollectionKey;
	}

	public void setDefaultCollectionKey(String defaultCollectionKey) {
		this.defaultCollectionKey = defaultCollectionKey;
	}

	public BulletSettings getSettingsModel() {
		if (this.settingsModel == null) {
			this.settingsModel = new BulletSettings();
		}
		return settingsModel;
	}

	public void setSettingsModel(BulletSettings settingsModel) {
		this.settingsModel = (settingsModel != null) ? settingsModel
				: new BulletSettings();
	}

	public List<BulletConfig> getConfigs() {
		return configs;
	}
	
	public BulletConfig getConfigs(String id) {
		for (BulletConfig cfg : this.configs) {
			if (cfg.getId().equals(id)) {
				return cfg;
			}
		}
		return null;
	}
	
	public BulletConfig getConfigs(int id) {
		for (BulletConfig cfg : this.configs) {
			if (cfg.getId().equals(Integer.toString(id))) {
				return cfg;
			}
		}
		return null;
	}

	public void setConfigs(List<BulletConfig> configs) {
		this.configs = configs;
	}
	
	public String getGroupId() {
		return groupId;
	}
	
	public void setGroupId(long groupId) {
		if(groupId > 0){
			this.groupId = Long.toString(groupId);
		}
	}

	public String toString() {
		ObjectMapper jacksonMapper = new ObjectMapper();
		try {
			return jacksonMapper.writeValueAsString(this);
		} catch (IOException e) {
			return null;
		}
	}

	@JsonIgnore
	public void addConfig(BulletConfig config) {
		this.configs.add(config);
	}
	

}
