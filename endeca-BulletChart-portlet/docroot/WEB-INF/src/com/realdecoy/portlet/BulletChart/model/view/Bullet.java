package com.realdecoy.portlet.BulletChart.model.view;

import com.realdecoy.portlet.BulletChart.BulletChartEnum;
import java.io.Serializable;

public class Bullet implements Serializable{

	private static final long serialVersionUID = 1L;
	private String id;
	private String metric;
	private BulletChartEnum.BulletType type;
	private String value;
	private String formattedValue;
	private String formattedMetricValue;
	private String metricValue;
	private Double average;
	private Double median;
	private Double variance;
	private Double maximum;
	private Double minimum;
	private Double stddev;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = Long.toString(serialVersionUID);
	}
	public String getMetric() {
		return metric;
	}
	public void setMetric(String metric) {
		this.metric = metric;
	}
	public BulletChartEnum.BulletType getType() {
		return type;
	}
	public void setType(BulletChartEnum.BulletType type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getFormattedValue() {
		return formattedValue;
	}
	public void setFormattedValue(String formattedValue) {
		this.formattedValue = formattedValue;
	}
	public String getFormattedMetricValue() {
		return formattedMetricValue;
	}
	public void setFormattedMetricValue(String formattedMetricValue) {
		this.formattedMetricValue = formattedMetricValue;
	}
	public String getMetricValue() {
		return metricValue;
	}
	public void setMetricValue(String metricValue) {
		this.metricValue = metricValue;
	}
	public Double getAverage() {
		return average;
	}
	public void setAverage(Double average) {
		this.average = average;
	}
	public Double getMedian() {
		return median;
	}
	public void setMedian(Double median) {
		this.median = median;
	}
	public Double getVariance() {
		return variance;
	}
	public void setVariance(Double variance) {
		this.variance = variance;
	}
	public Double getMaximum() {
		return maximum;
	}
	public void setMaximum(Double maximum) {
		this.maximum = maximum;
	}
	public Double getMinimum() {
		return minimum;
	}
	public void setMinimum(Double minimum) {
		this.minimum = minimum;
	}
	public Double getStddev() {
		return stddev;
	}
	public void setStddev(Double stddev) {
		this.stddev = stddev;
	}
	
	
}
