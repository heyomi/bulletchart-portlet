package com.realdecoy.portlet.BulletChart;

public class BulletChartBarEnum {
	public BulletChartBarEnum() {
	}

	public enum SummarizationType {
		Average, Median, StandardDeviant, MetricSingle, MetricRatio, MetricPartToWhole, DimensionSingle, DimensionRatio, Alert;

		private SummarizationType() {
		}
	}

	public enum ConditionType {
		SelfValue, ToAnotherMetric, PartToWhole;

		private ConditionType() {
		}
	}

	public static enum ConditionOperator {
		GT, GTEQ, EQ, BTWN, LTEQ, LT;

		private ConditionOperator() {
		}
	}

	public static enum NamePositionType {
		BelowValue, AboveValue;

		private NamePositionType() {
		}
	}
}