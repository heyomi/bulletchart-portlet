package com.realdecoy.portlet.BulletChart.utils;

import java.util.HashMap;
import javax.portlet.PortletRequest;
import com.realdecoy.portlet.BulletChart.model.edit.BulletConfig;
import com.realdecoy.portlet.BulletChart.BulletChartConstants;

public class EQLUtils {
    private static final String MAX_QUERY = "RETURN maximum as SELECT MAX ('METRIC') AS 'max' FROM VIEW GROUP";
    private static final String MIN_QUERY = "RETURN minimum as SELECT MIN ('METRIC') AS 'min' FROM VIEW GROUP";
    private static final String AVG_QUERY = "RETURN average as SELECT AVG ('METRIC') AS 'avg' FROM VIEW GROUP";
    private static final String MEDIAN_QUERY = "RETURN median as SELECT MEDIAN ('METRIC') AS 'med' FROM VIEW GROUP";
    private static final String STDDEV_QUERY = "RETURN stddev as SELECT STDDEV ('METRIC') AS 'std' FROM VIEW GROUP";
    private static final String VARIANCE_QUERY = "RETURN variance as SELECT VARIANCE ('METRIC') AS 'var' FROM VIEW GROUP";

    public static HashMap < String, String > getSummaryQuery(PortletRequest request,
        String view, BulletConfig config) {
        HashMap < String, String > queryMap = new HashMap < String, String > ();

        makeMax(queryMap, view, config);
        makeMin(queryMap, view, config);
        makeMedian(queryMap, view, config);
        makeVariance(queryMap, view, config);
        makeSTDDEV(queryMap, view, config);
        makeAverage(queryMap, view, config);
        //makeFeaturedMeasure(query, view, config);
        
        
        return queryMap;
    }

    private static void makeMax(HashMap < String, String > queryMap, String view,
        BulletConfig config) {
        queryMap.put(BulletChartConstants.MAXIMUM, MAX_QUERY.replaceAll("METRIC", config.getItemConfig().getMetricKey()).replaceAll("VIEW", config.getSemanticKey()));
    }
    private static void makeMin(HashMap < String, String > queryMap, String statementKey,
        BulletConfig config) {
        queryMap.put(BulletChartConstants.MINIMUM, MIN_QUERY.replaceAll("METRIC", config.getItemConfig().getMetricKey()).replaceAll("VIEW", config.getSemanticKey()));
    }
    private static void makeMedian(HashMap < String, String > queryMap, String statementKey,
        BulletConfig config) {
        queryMap.put(BulletChartConstants.MEDIAN, MEDIAN_QUERY.replaceAll("METRIC", config.getItemConfig().getMetricKey()).replaceAll("VIEW", config.getSemanticKey()));
    }
    private static void makeAverage(HashMap < String, String > queryMap, String statementKey,
        BulletConfig config) {
        queryMap.put(BulletChartConstants.AVERAGE, AVG_QUERY.replaceAll("METRIC", config.getItemConfig().getMetricKey()).replaceAll("VIEW", config.getSemanticKey()));
    }
    private static void makeSTDDEV(HashMap < String, String > queryMap, String statementKey,
        BulletConfig config) {
        queryMap.put(BulletChartConstants.STDDEV, STDDEV_QUERY.replaceAll("METRIC", config.getItemConfig().getMetricKey()).replaceAll("VIEW", config.getSemanticKey()));
    }
    private static void makeVariance(HashMap < String, String > queryMap, String statementKey,
        BulletConfig config) {
        queryMap.put(BulletChartConstants.VARIANCE, VARIANCE_QUERY.replaceAll("METRIC", config.getItemConfig().getMetricKey()).replaceAll("VIEW", config.getSemanticKey()));
    }

}