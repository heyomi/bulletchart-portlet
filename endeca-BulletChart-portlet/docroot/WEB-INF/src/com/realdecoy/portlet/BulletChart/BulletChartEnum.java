package com.realdecoy.portlet.BulletChart;

public class BulletChartEnum {
	public BulletChartEnum() {
	}

	public enum BulletType {
		Average, Median, StandardDeviation, MetricSingle, MetricRatio, MetricPartToWhole, DimensionSingle, DimensionRatio, Alert, Custom, Range;

		private BulletType() {
		}
	}

	public enum ConditionType {
		SelfValue, ToAnotherMetric, PartToWhole;

		private ConditionType() {
		}
	}

	public static enum ConditionOperator {
		GT, GTEQ, EQ, BTWN, LTEQ, LT;

		private ConditionOperator() {
		}
	}

	public static enum NamePositionType {
		BelowValue, AboveValue;

		private NamePositionType() {
		}
	}
}