package com.realdecoy.portlet.BulletChart.model;

import java.io.Serializable;

public class BulletChartModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int SELECTOR_INDEX_NONE = -1;
	private String style;
	private String sort;
	private boolean colorPinning;
	private int currentSubgroupIndex;
	private boolean autoConfig;
	
	private String title;
	private String format;
	private String calc;
	private Long maximum;
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getCalc() {
		return calc;
	}

	public void setCalc(String calc) {
		this.calc = calc;
	}

	public Long getMaximum() {
		return maximum;
	}

	public void setMaximum(Long maximum) {
		this.maximum = maximum;
	}

	public Long getMinimum() {
		return minimum;
	}

	public void setMinimum(Long minimum) {
		this.minimum = minimum;
	}

	public Long getCurrent() {
		return current;
	}

	public void setCurrent(Long current) {
		this.current = current;
	}

	public Long getSpecified() {
		return specified;
	}

	public void setSpecified(Long specified) {
		this.specified = specified;
	}

	private Long minimum;
	private Long current;
	private Long specified;

	public BulletChartModel() {
		this.style = "H";
		this.sort = "asc";
		this.colorPinning = true;
		this.autoConfig = false;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public boolean isColorPinning() {
		return colorPinning;
	}

	public void setColorPinning(boolean colorPinning) {
		this.colorPinning = colorPinning;
	}

	public int getCurrentSubgroupIndex() {
		return currentSubgroupIndex;
	}

	public void setCurrentSubgroupIndex(int currentSubgroupIndex) {
		this.currentSubgroupIndex = currentSubgroupIndex;
	}

	public boolean isAutoConfig() {
		return autoConfig;
	}

	public void setAutoConfig(boolean autoConfig) {
		this.autoConfig = autoConfig;
	}
}