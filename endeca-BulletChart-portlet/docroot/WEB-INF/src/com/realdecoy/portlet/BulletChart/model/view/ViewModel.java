package com.realdecoy.portlet.BulletChart.model.view;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.realdecoy.portlet.BulletChart.model.edit.BulletConfig;
import com.realdecoy.portlet.BulletChart.model.edit.BulletSettings;

/**
 * Representation of the "View Model" - the model that is composed during
 * handleViewRenderRequest based on saved preferences ("Edit Model") and other
 * runtime factors
 * 
 * @author Endeca Technologies, Inc.
 * 
 */
public class ViewModel {
	private BulletSettings settingsModel;
	private List<BulletConfig> configs;
	

	public BulletSettings getSettingsModel() {
		return this.settingsModel;
	}

	public void setSettingsModel(BulletSettings settingsModel) {
		this.settingsModel = settingsModel;
	}

	public List<BulletConfig> getConfigs() {
		return configs;
	}

	public void setConfigs(List<BulletConfig> configs) {
		this.configs = configs;
	}

	public String toString() {
		ObjectMapper jacksonMapper = new ObjectMapper();
		try {
			return jacksonMapper.writeValueAsString(this);
		} catch (IOException e) {
			return null;
		}
	}

}
