Ext.ns("Ext.ux.endeca");
Ext.ux.endeca.ConformedDimensionsView = Ext.extend(Ext.BoxComponent, {cls: "eid-conformed",viewKeys: [],itemSelector: "div.eid-conformed-group",conformedData: null,jsController: null,helpText: null,warningIconsMap: {},TIP_TYPE_NONE_CONFORMED: "NONE_CONFORMED",TIP_TYPE_NONE_DATE_SHARED: "NONE_DATE_SHARED",TIP_TYPE_DIFFERENT_DATA_TYPES: "DIFFERENT_DATA_TYPES",TIP_TYPE_COMPLETED: "COMPLETED",TIP_TYPE_NOT_SURE: "NOT_SURE",initComponent: function() {
        Ext.ux.endeca.ConformedDimensionsView.superclass.initComponent.call(this);
        this.addEvents("collapsed", "expand", "edit", "remove", "dimremove", "startdrag", "enddrag", "move", "add", "datachanged");
        this.addDefaultData();
        this.data = Ext.decode(Ext.encode(this.conformedData));
        this.tpl = this.createGroupTpl()
    },addDefaultData: function() {
        if (Ext.isEmpty(this.conformedData)) {
            this.conformedData = [this.getEmptyGroup()]
        } else {
            if (Ext.isArray(this.conformedData) && !this.isEmptyGroup(this.conformedData[this.conformedData.length - 1])) {
                this.conformedData.push(this.getEmptyGroup())
            }
        }
    },isEmptyGroup: function(a) {
        var c = true;
        if (Ext.isArray(a.conformedDimensions)) {
            for (var b = 0; b < a.conformedDimensions.length; b++) {
                if (!Ext.isEmpty(a.conformedDimensions[b].attributeKey)) {
                    c = false;
                    break
                }
            }
        }
        return c
    },isCompletedGroup: function(b) {
        var a = true;
        if (Ext.isArray(b.conformedDimensions)) {
            for (var c = 0; c < b.conformedDimensions.length; c++) {
                if (Ext.isEmpty(b.conformedDimensions[c].attributeKey)) {
                    a = false;
                    break
                }
            }
        }
        return a
    },getEmptyGroup: function() {
        var b = [];
        for (var a = 0; a < this.viewKeys.length; a++) {
            b.push({attributeKey: null,viewKey: this.viewKeys[a]})
        }
        return {conformedDimensions: b}
    },createGroupTpl: function() {
        var b = this;
        var a = new Ext.XTemplate('<tpl for=".">', '<div class="eid-conformed-group {[this.getGroupClass(values)]}">', '<tpl if="!this.isEmptyGroup(values)">', '<div class="eid-conformed-header{[this.getCollapsedCls(values)]}">', '<div class="eid-action-icon eid-icon-remove eid-conformed-header-remove">{[this.localize("chart.iconic.remove-icon-text")]}</div>', '<div class="eid-action-icon eid-icon-edit eid-conformed-header-edit">{[this.localize("chart.iconic.edit-icon-text")]}</div>', '<div class="eid-conformed-warning-icon" style="display:none;"></div>', '<div class="eid-conformed-header-text">{displayName}</div>', '<div class="x-clear"></div>', "</div>", "</tpl>", '<div class="eid-conformed-body">', '<tpl for="conformedDimensions">', '<tpl if="!Ext.isEmpty(values.attributeKey)">', '<div class="eid-slot eid-conformed-slot {[this.getRowClass(xindex, values)]}">', '<div class="eid-action-icon eid-icon-remove eid-conformed-slot-remove">{[this.localize("chart.iconic.remove-icon-text")]}</div>', '<div class="bad-attr-icon" ext:qtip="{[this.localize("chart.edit.config.tooltip.metric-or-dimension-unavailable")]}"></div>', '<div class="eid-conformed-slot-text" ext:qtip="{[this.getTip(values)]}">{[this.getAttrDisplayName(values.attributeKey, values.viewKey)]}</div>', '<div class="x-clear"></div>', "</div>", "</tpl>", '<tpl if="Ext.isEmpty(values.attributeKey)">', '<div class="eid-slot eid-conformed-empty-slot {[this.getRowClass(xindex)]}">{[this.getEmptyText(xindex)]}</div>', "</tpl>", "</tpl>", "</div>", "</div>", "</tpl>", '<tpl if="this.showHelpText(values)">', '<div class="eid-up-arrow"></div>', '<div class="eid-conformed-help-text">{[this.getHelpText()]}</div>', "</tpl>", {compiled: true,disableFormats: true,showHelpText: function(c) {
                return c.length == 1
            },getHelpText: function() {
                return b.helpText
            },getGroupClass: function(d) {
                var c = [];
                if (!b.isEmptyGroup(d)) {
                    c.push(" eid-conformed-dd");
                    if (this.isBadGroup(d)) {
                        c.push(" eid-conformed-bad-group")
                    }
                }
                return c.join(" ")
            },isEmptyGroup: b.isEmptyGroup,getCollapsedCls: function(c) {
                return c.collapsed ? " eid-conformed-collapsed" : ""
            },getViewDisplayName: function(c) {
                return Ext.util.Format.htmlEncode(b.jsController.editModel.getViewDisplayName(c))
            },getAttrDisplayName: function(d, c) {
                return Ext.util.Format.htmlEncode(b.jsController.editModel.getMetadata(null, d, c).dn)
            },getViewDisplayNameByInx: function(c) {
                return this.getViewDisplayName(b.viewKeys[c - 1])
            },getRowClass: function(e, d) {
                var c = [];
                if (e == 1) {
                    c.push("eid-conformed-first-row")
                }
                if (!Ext.isEmpty(d) && this.isBadAttribute(d)) {
                    c.push("eid-bad-attribute")
                }
                return c.join(" ")
            },getTip: function(c) {
                var d = String.format("<div>{0}</div><div class='eid-tip-sep'>{1} {2}</div>", this.getAttrDisplayName(c.attributeKey, c.viewKey), this.localize("chart.edit.conformed-dimensions.tooltip.view-name"), this.getViewDisplayName(c.viewKey));
                return d
            },getEmptyText: function(c) {
                return b.jsController.localize("chart.edit.conformed-dimensions.empty-text", this.getViewDisplayNameByInx(c))
            },localize: function(c) {
                return b.jsController.localize(c)
            },isBadAttribute: function(c) {
                var d = b.jsController.editModel.getMetadata(null, c.attributeKey, c.viewKey);
                return Ext.isEmpty(d) || !d.isDimension
            },isBadGroup: function(c) {
                var e = c.conformedDimensions;
                var g = false;
                for (var d = 0; d < e.length; d++) {
                    var f = e[d];
                    if (!Ext.isEmpty(f.attributeKey)) {
                        if (!this.isBadAttribute(f)) {
                            g = true;
                            break
                        }
                    }
                }
                return !g
            }});
        return a
    },getTemplateTarget: function() {
        return this.el
    },afterRender: function() {
        Ext.ux.endeca.ConformedDimensionsView.superclass.afterRender.call(this);
        this.mon(this.getTemplateTarget(), {click: this.onClick,scope: this});
        this.bindDragAndDrop();
        this.addWarningIcons()
    },onClick: function(d) {
        var b = this.getTemplateTarget();
        var c = d.getTarget(this.itemSelector, b), a;
        if (c) {
            a = this.indexOf(c);
            if (this.onItemClick(c, a, d) !== false) {
                if (d.getTarget(".eid-conformed-header-text", b)) {
                    this.onHeaderClick(c, a, d, d.getTarget(".eid-conformed-header-text", b))
                } else {
                    if (d.getTarget(".eid-conformed-header-remove", b)) {
                        this.onHeaderRemove(c, a, d)
                    } else {
                        if (d.getTarget(".eid-conformed-header-edit", b)) {
                            this.onHeaderEdit(c, a, d)
                        } else {
                            if (d.getTarget(".eid-conformed-slot-remove", b)) {
                                this.onSoltRemove(c, a, d, d.getTarget(".eid-conformed-slot-remove", b))
                            }
                        }
                    }
                }
            }
        }
    },indexOf: function(b) {
        var a = this.getTemplateTarget();
        return a.select(this.itemSelector).indexOf(b)
    },onItemClick: function(a) {
        return true
    },onHeaderClick: function(b, a, c, d) {
        Ext.fly(d).parent().toggleClass("eid-conformed-collapsed");
        this.conformedData[a].collapsed = Ext.fly(d).parent().hasClass("eid-conformed-collapsed");
        this.fireEvent(this.conformedData[a].collapsed ? "collapsed" : "expand", this, a, b, c);
        this.fireEvent("datachanged", this)
    },onHeaderRemove: function(c, a, d) {
        var b = this;
        this.conformedData.splice(a, 1);
        this.fireEvent("remove", this, a, c, d);
        this.refresh()
    },onHeaderEdit: function(c, a, f) {
        var b = this;
        var d = Ext.decode(Ext.encode(this.conformedData[a]));
        this.fireEvent("edit", this, d, a, c, f)
    },onSoltRemove: function(c, a, f, h) {
        var b = this;
        var g = Ext.fly(h).parent(".eid-slot");
        var d = Ext.fly(h).parent(".eid-conformed-body", false).select(".eid-slot").indexOf(g);
        this.conformedData[a].conformedDimensions[d].attributeKey = null;
        if (this.isEmptyGroup(this.conformedData[a])) {
            this.onHeaderRemove(c, a, f)
        } else {
            this.fireEvent("dimremove", this, a, d, c, f);
            this.refresh()
        }
    },bindDragAndDrop: function() {
        var b = this;
        var a = this.getTemplateTarget();
        b.dragZone = new Ext.dd.DragZone(a, {ddGroup: b.ddGroup || "chart-conformed-ddGroup",itemSelector: "div.eid-conformed-group.eid-conformed-dd",highlightValidDragZoneCls: "eid-conformed-dd-dragzone",highlightOrgLocCls: "eid-conformed-dd-orglocation",ddGhostCls: "eid-conformed-dd-ghost",ddGhostInnerCls: "eid-conformed-dd-ghost-inner",getDragData: function(c) {
                var d = c.getTarget(this.itemSelector);
                if (d) {
                    return {ddel: d,repairXY: Ext.fly(d).getXY(),index: b.indexOf(d),srcIndex: b.indexOf(d),view: b,selfMove: true}
                }
            },onInitDrag: function(d, g) {
                var c = this.proxy.getEl();
                var e = this.dragData.ddel;
                this.setPickupStyle(c, e);
                var f = this.getElOffset(e, d, g);
                this.setDelta(Ext.min([200, f.x]), f.y);
                this.highlightDragSource(Ext.get(e));
                b.fireEvent("startdrag", b);
                return true
            },setPickupStyle: function(c, e) {
                c.disableShadow();
                c.select(".x-dd-drop-icon").remove();
                c.select(".x-dd-drag-ghost").setOpacity(0.5).addClass(this.ddGhostCls);
                var d = Ext.get(e.cloneNode(true));
                d.select(".eid-conformed-dd").remove();
                d.select(".eid-action-icon").remove();
                d.addClass(this.ddGhostInnerCls);
                d.setWidth(220);
                this.proxy.update(d.dom)
            },getElOffset: function(f, d, i) {
                var h = Ext.fly(f).getTop();
                var g = Ext.fly(f).getLeft();
                var e = d - g;
                var c = i - h;
                return {x: e,y: c}
            },onEndDrag: function(c) {
                this.cancelHighlightDragSource(Ext.get(this.dragData.ddel));
                b.fireEvent("enddrag", b)
            },highlightDragSource: function(c) {
                if (c) {
                    c.addClass(this.highlightOrgLocCls)
                }
            },cancelHighlightDragSource: function(c) {
                if (c) {
                    c.removeClass(this.highlightOrgLocCls)
                }
            },getRepairXY: function() {
                return this.dragData.repairXY
            }});
        b.dropZone = new Ext.dd.DropZone(a.parent(), {ddGroup: b.ddGroup || "chart-conformed-ddGroup",itemSelector: "div.eid-conformed-group",emptyItemSelector: "div.eid-conformed-empty-slot",getTargetFromEvent: function(c) {
                return this.getTargetBySelector(this.itemSelector, c)
            },getEmptySlotTargetFromEvent: function(h, g) {
                var i = this.getTargetFromEvent(h);
                if (i) {
                    var c = b.el.select("div.eid-conformed-group").indexOf(i);
                    var f = b.viewKeys.indexOf(g.get("viewKey"));
                    var d = b.conformedData[c].conformedDimensions;
                    if (Ext.isEmpty(d[f].attributeKey)) {
                        return Ext.fly(i).select(".eid-slot").item(f)
                    }
                }
            },getTargetBySelector: function(f, h) {
                var c = h.getPageX();
                var i = h.getPageY();
                var d = Ext.select(f, this.el);
                var g = null;
                d.each(function(e) {
                    if ((e.getTop() <= i && e.getBottom() >= i) && (e.getLeft() <= c && e.getRight() >= c)) {
                        g = e.dom;
                        return false
                    }
                });
                return g
            },onContainerOver: function(c, i, h) {
                if (!h.selfMove) {
                    var g = b.viewKeys.indexOf(h.data.get("viewKey"));
                    var k = b.conformedData.length - 1;
                    var f = b.conformedData[k].conformedDimensions;
                    var j = b.el.select("div.eid-conformed-group").item(k);
                    var d = j.select(".eid-slot").item(g);
                    return this.highlightEmptySlot(d, h)
                }
                return this.dropNotAllowed
            },onNodeOver: function(k, p, l, i) {
                if (!Ext.dd.Registry.getTarget(k)) {
                    return this.dropNotAllowed
                }
                var j = Ext.get(k);
                if (j && k.id !== i.ddel.id) {
                    this.clearPlaceHoler();
                    if (i.selfMove) {
                        var m = l.getPageY();
                        var n = j.getTop();
                        var d = j.getBottom();
                        var h = d / 3 + 2 * n / 3;
                        var g = n / 3 + 2 * d / 3;
                        if (n <= m && m < h) {
                            this.moveUp = true
                        } else {
                            if (g < m && m <= d) {
                                this.moveUp = false
                            } else {
                                return this.dropNotAllowed
                            }
                        }
                        var o = i.srcIndex;
                        var c = b.indexOf(k);
                        if (c == b.conformedData.length - 1 && !this.moveUp) {
                            return this.dropNotAllowed
                        }
                        if (b.getId() == i.view.getId()) {
                            if (!this.moveUp) {
                                c++
                            }
                            if (c > o) {
                                c--
                            }
                            if (c == o) {
                                return this.dropNotAllowed
                            }
                        }
                        if (this.moveUp) {
                            Ext.DomHelper.insertBefore(j, this.getRowPlaceHolder())
                        } else {
                            Ext.DomHelper.insertAfter(j, this.getRowPlaceHolder())
                        }
                    } else {
                        var f = this.getEmptySlotTargetFromEvent(l, i.data);
                        return this.highlightEmptySlot(f, i)
                    }
                    return this.dropAllowed
                }
            },notifyOut: function() {
                this.clearPlaceHoler()
            },highlightEmptySlot: function(c, d) {
                if (c) {
                    if (b.allowAdd(d.data, c)) {
                        Ext.fly(c).addClass("eid-conformed-hl-empty-slot");
                        return this.dropAllowed
                    } else {
                        return this.dropNotAllowed
                    }
                } else {
                    return this.dropNotAllowed
                }
            },notifyDrop: function(l, i, g) {
                if (g.selfMove) {
                    var h = b.el.select(".eid-conformed-place-holder").first();
                    if (h) {
                        var j = g.srcIndex;
                        var f = b.el.select("div.eid-conformed-group.eid-conformed-dd, div.eid-conformed-place-holder").indexOf(h);
                        if (f > j && b.getId() == g.view.getId()) {
                            f--
                        }
                        if (b.getId() != g.view.getId() || j !== f) {
                            b.onConformedDimMove(g, j, f);
                            return true
                        }
                    }
                } else {
                    var d = b.el.select("div.eid-conformed-hl-empty-slot").first();
                    if (d) {
                        var f = b.el.select("div.eid-slot").indexOf(d);
                        var k = parseInt(f / b.viewKeys.length);
                        var c = f % b.viewKeys.length;
                        b.onConformedDimAdd(k, c, g.data);
                        return true
                    }
                }
            },getRowPlaceHolder: function() {
                return {cls: "eid-conformed-place-holder",html: "&#160;"}
            },clearPlaceHoler: function() {
                this.el.select(".eid-conformed-place-holder").remove();
                this.el.select(".eid-conformed-hl-empty-slot").removeClass("eid-conformed-hl-empty-slot")
            },registerEntireItems: function() {
                this.drags = this.el.select(this.itemSelector);
                for (var c = 0; c < this.drags.getCount(); c++) {
                    this.registerItem(this.drags.item(c).dom)
                }
            },registerItem: function(c) {
                Ext.dd.Registry.register(c)
            }});
        b.dropZone.registerEntireItems()
    },allowAdd: function(d, c) {
        var f = this;
        var a = f.el.select("div.eid-slot").indexOf(c);
        var g = parseInt(a / f.viewKeys.length);
        var e = a % f.viewKeys.length;
        var b = d.get("isDimension");
        if (!b) {
            return false
        }
        return true
    },getFirstDimInGroup: function(b) {
        var c = {};
        for (var a = 0; a < b.length; a++) {
            if (b[a].attributeKey) {
                return b[a]
            }
        }
        return c
    },onConformedDimMove: function(d, b, a) {
        var c = this;
        var e = d.view.conformedData.splice(b, 1)[0];
        c.conformedData.splice(a, 0, e);
        c.fireEvent("move", c);
        c.refresh();
        if (c.getId() != d.view.getId()) {
            d.view.refresh()
        }
    },onConformedDimAdd: function(f, d, a) {
        var e = this;
        var c = a.get("key");
        e.conformedData[f].conformedDimensions[d].attributeKey = c;
        if (Ext.isEmpty(e.conformedData[f].name)) {
            e.conformedData[f].name = this.genConformedDimKey(c);
            e.conformedData[f].displayName = a.get("dn");
            var h = e.getRelatedFilterRulesByAttrKey(c, e.viewKeys[d]);
            if (h) {
                for (var b = 0; b < e.conformedData[f].conformedDimensions.length; b++) {
                    var g = e.conformedData[f].conformedDimensions[b];
                    if (b != d) {
                        g.attributeKey = h[e.viewKeys[b]]
                    }
                }
                e.conformedData[f].collapsed = true
            }
        }
        e.fireEvent("add", e, f, d, a, e.conformedData[f]);
        e.refresh()
    },getRelatedFilterRulesByAttrKey: function(a, c) {
        var g = this;
        var e = g.jsController.editModel;
        var k = e.dataObj.getFilterRules(e.getCurrentChartConfig().dataSourceId, g.viewKeys);
        var b = {};
        var j = false;
        if (Ext.isArray(k)) {
            for (var d = 0; d < k.length; d++) {
                var h = k[d];
                if (h.isActive && h.sourceCollectionKey == c && h.sourcePropertyKey == a) {
                    if (Ext.isEmpty(b[h.targetCollectionKey])) {
                        j = true;
                        b[h.targetCollectionKey] = h.targetPropertyKey
                    }
                }
            }
            if (j) {
                var f = g.viewKeys;
                for (var d = 0; d < f.length; d++) {
                    if (f[d] !== c && !b[f[d]]) {
                        j = false;
                        break
                    }
                }
            }
        }
        return j ? b : null
    },genConformedDimKey: function(a) {
        var b = this;
        return String.format("CONFORMED_DIM_KEY_{0}_{1}", a, new Date().getTime())
    },addWarningIcons: function() {
        var f = this, g, b = {}, h = [], e = 0, d, c;
        for (e = 0; e < this.conformedData.length - 1; e++) {
            if (this.isCompletedGroup(this.conformedData[e])) {
                d = f.jsController.editModel.getMetadataList(this.conformedData[e]);
                c = f.getKeyForWarningIcon(d);
                g = f.getTipType(d);
                if (g != f.TIP_TYPE_COMPLETED) {
                    if (g == f.TIP_TYPE_NOT_SURE) {
                        if (!f.isDateTypeSame(d)) {
                            f.addWarningIcon(e, c, f.TIP_TYPE_DIFFERENT_DATA_TYPES)
                        } else {
                            if (d[0].datatype == "mdex:dateTime" && !f.isDateSharedGrain(d)) {
                                f.addWarningIcon(e, c, f.TIP_TYPE_NONE_DATE_SHARED)
                            } else {
                                if (d[0].datatype == "mdex:string") {
                                    h.push(this.conformedData[e].conformedDimensions);
                                    b[h.length - 1] = e
                                } else {
                                    f.warningIconsMap[c] = f.TIP_TYPE_COMPLETED
                                }
                            }
                        }
                    } else {
                        f.addWarningIcon(e, c, g)
                    }
                }
            }
        }
        if (h.length > 0) {
            var a = new Ext.LoadMask(this.getTemplateTarget());
            a.show();
            Ext.Ajax.request({url: f.jsController.resourceUrls.getConformanceValueUrl,params: {conformedDims: Ext.encode(h)},success: function(i, k) {
                    var j = Ext.decode(i.responseText);
                    if (!Ext.isEmpty(j)) {
                        for (e = 0; e < j.length; e++) {
                            d = f.jsController.editModel.getMetadataList(f.conformedData[b[e]]);
                            c = f.getKeyForWarningIcon(d);
                            if (!j[e]) {
                                f.addWarningIcon(b[e], c, f.TIP_TYPE_NONE_CONFORMED)
                            } else {
                                f.warningIconsMap[c] = f.TIP_TYPE_COMPLETED
                            }
                        }
                    }
                    a.hide()
                },failure: function(i, j) {
                    a.hide()
                }})
        }
    },isDateTypeSame: function(c) {
        var d = true;
        if (Ext.isArray(c) && c.length > 1) {
            var a = c[0].datatype;
            for (var b = 1; b < c.length; b++) {
                if (a != c[b].datatype) {
                    d = false;
                    break
                }
            }
        }
        return d
    },isDateSharedGrain: function(a) {
        var b = this.jsController.editModel.isDateSharedGrain(a);
        return b
    },getTip: function(a) {
        if (a == this.TIP_TYPE_NONE_CONFORMED) {
            return this.jsController.localize("chart.edit.group-dim.poorly-conformed.info")
        } else {
            if (a == this.TIP_TYPE_NONE_DATE_SHARED) {
                return this.jsController.localize("chart.edit.config.warning.selected-dimensions-none-date-shared")
            } else {
                if (a == this.TIP_TYPE_DIFFERENT_DATA_TYPES) {
                    return this.jsController.localize("chart.edit.group-dim.different-date-types.warning")
                } else {
                    return a
                }
            }
        }
    },getTipType: function(b) {
        var c = this;
        var a = this.getKeyForWarningIcon(b);
        if (this.warningIconsMap[a]) {
            return this.warningIconsMap[a]
        } else {
            return c.TIP_TYPE_NOT_SURE
        }
    },getTipTypeByIdx: function(a) {
        var b = this;
        metadataList = b.jsController.editModel.getMetadataList(this.conformedData[a]);
        key = b.getKeyForWarningIcon(metadataList);
        tipType = b.getTipType(metadataList);
        return tipType
    },getKeyForWarningIcon: function(c) {
        var d = this;
        var b = "";
        if (Ext.isArray(c)) {
            for (var a = 0; a < c.length; a++) {
                b += String.format("+{0}", c[a].key)
            }
        }
        return b
    },addWarningIcon: function(a, b, e) {
        var c = this;
        c.warningIconsMap[b] = e;
        var d = c.getTip(e);
        var f = c.getTemplateTarget().select("div.eid-conformed-warning-icon").item(a);
        if (f) {
            if (e == c.TIP_TYPE_DIFFERENT_DATA_TYPES) {
                f.removeClass("eid-conformed-info-icon")
            } else {
                f.addClass("eid-conformed-info-icon")
            }
            f.setDisplayed(true);
            f.dom.setAttribute("ext:qtip", d)
        }
    },refresh: function(a) {
        if (!Ext.isEmpty(a)) {
            this.conformedData = Ext.decode(Ext.encode(a))
        }
        this.addDefaultData();
        this.data = Ext.decode(Ext.encode(this.conformedData));
        this.update(this.data);
        this.dropZone.registerEntireItems();
        this.addWarningIcons();
        this.fireEvent("datachanged", this)
    }});
