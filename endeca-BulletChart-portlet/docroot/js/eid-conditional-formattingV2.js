Ext.ns("Ext.ux.endeca");
Ext.ux.endeca.ConditionalFormattingPanelv2 = Ext.extend(Ext.Panel, {
    resources: undefined,
    conditionData: undefined,
    enableIndex: false,
    addConditionButton: "conditional-formattingv2.button.add-condition",
    columnDisplayTitle: "conditional-formattingv2.column.title.display",
    columnConditionRuleTitle: "conditional-formattingv2.column.title.rule",
    columnTooltipTitle: "conditional-formattingv2.column.title.tooltip",
    columnRemoveTooltip: "conditional-formattingv2.column.tooltip.remove",
    emptyRulesMessage: "conditional-formattingv2.empty.rules",
    percentageSymbol: "conditional-formattingv2.column.label.symbol.percentage",
    labelStyle: "conditional-formattingv2.label.style",
    conditionBetween: "conditional-formattingv2.combo.display.between",
    conditionGT: "conditional-formattingv2.combo.display.gt",
    conditionGTEQ: "conditional-formattingv2.combo.display.gteq",
    conditionEQ: "conditional-formattingv2.combo.display.eq",
    conditionLT: "conditional-formattingv2.combo.display.lt",
    conditionLTEQ: "conditional-formattingv2.combo.display.lteq",
    selectStyleImageTitle: "conditional-formattingv2.combo.title.select-style-image",
    selectCompareSymbolTitle: "conditional-formattingv2.combo.title.select-compare-symbol",
    inputMinValueTitle: "conditional-formattingv2.input.title.input-min-value",
    inputMaxValueTitle: "conditional-formattingv2.input.title.input-max-value",
    inputTooltipTitle: "conditional-formattingv2.input.title.input-tooltip",
    imageAltStopLight: "conditional-formattingv2.image.alt.stop-light",
    imageAltConditionWarning: "conditional-formattingv2.image.alt.condition-warning",
    imageAltExcalmation: "conditional-formattingv2.image.alt.exclamation",
    imageAltStar: "conditional-formattingv2.image.alt.stars",
    imageAltSentiment1t5: "conditional-formattingv2.image.alt.sentiment1-5",
    imageAltHarveyBalls: "conditional-formattingv2.image.alt.harvey-balls",
    imageAltRank: "conditional-formattingv2.image.alt.rank",
    imageAltArrows: "conditional-formattingv2.image.alt.arrows",
    styleSelectionStore: new Ext.data.ArrayStore({
        fields: ["style", "altResource"],
        data: [
            ["icon_stoplight_sprite", "imageAltStopLight"],
            ["icon_condition_warning_sprite", "imageAltConditionWarning"],
            ["icon_exclamation_sprite", "imageAltExcalmation"],
            ["icon_stars_sprite", "imageAltStar"],
            ["icon_sentiment1-5_sprite", "imageAltSentiment1t5"],
            ["icon_harveyballs_sprite", "imageAltHarveyBalls"],
            ["icon_rank_sprite", "imageAltRank"],
            ["icon_arrows_sprite", "imageAltArrows"]
        ]
    }),
    detailStyleStore: undefined,
    conditionGridStore: undefined,
    tokenLink: undefined,
    isShowPercentageSymbol: false,
    conditionSymbolStore: undefined,
    showCoditionStyleSelection: true,
    gridDragZone: undefined,
    targetCmp: undefined,
    isAddTokenBtnClicked: false,
    getPredefinedColumns: function () {
        var a = this;
        return {
            dragHandler: {
                width: a.enableIndex ? 30 : 15,
                fixed: true,
                resizable: false,
                css: "position: 'relative'; top: '8px';",
                renderer: function (d, c, b, f) {
                    var e = Ext.id();
                    return '<div id="' + e + '" class="eid-icon eid-icon-row-draghandle" style="position: relative; top: 5px; cursor: -moz-grab">' + a.localize("summarizationbar.iconic.drag-icon-text") + "</div>" + (a.enableIndex ? '<span class="row-index">' + (f + 1) + "</span>" : "")
                }
            },
            detailStyleSel: {
                width: 60,
                fixed: true,
                header: a.localize("columnDisplayTitle"),
                dataIndex: "styleId",
                renderer: function (d, c, b) {
                    var e = Ext.id();
                    a.createDetailStyleCombo.defer(100, a, [d, e, b]);
                    return '<div id="' + e + '"></div>'
                }
            },
            conditionRule: {
                header: a.localize("columnConditionRuleTitle"),
                dataIndex: "condition",
                renderer: function (d, c, b) {
                    a.createConditionField.defer(10, a, [d, b]);
                    return '<div id="' + b.id + '_condition_rule_container"></div>'
                },
                id: "conditionRule"
            },
            tooltip: {
                header: a.localize("columnTooltipTitle") + '<button style="margin-left:20px; display:' + (a.tokenLink ? "inline;" : "none;") + '" id="' + a.id + 'add_token_link" class="eid-btn-small" disabled="true">' + (a.tokenLink ? a.tokenLink.linkText : "") + '</button><span class="eid-action-icon eid-icon-help" ext:qprofile="info" style="margin-left:5px; vertical-align:center; display:' + (a.tokenLink ? "inline-block;" : "none;") + '" ' + (a.tokenLink ? 'ext:qtip="' + a.tokenLink.linkTooltip + '"' : "") + ' id="' + a.id + 'add_token_tooltip">' + a.localize("summarizationbar.iconic.info-icon-text") + "</span>",
                dataIndex: "tooltip",
                id: "tooltip",
                renderer: function (d, c, b) {
                    var e = Ext.id();
                    a.createTooltipField.defer(10, a, [d, e, b]);
                    return '<div id="' + e + '"></div>'
                }
            },
            action: {
                xtype: "actioncolumn",
                width: 24,
                fixed: true,
                resizable: false,
                css: "vertical-align: middle;",
                items: [{
                    iconCls: "eid-icon-remove eid-action-icon",
                    altText: a.localize("summarizationbar.iconic.remove-icon-text"),
                    tooltip: a.localize("columnRemoveTooltip"),
                    handler: function (c, e, b) {
                        var d = c.getStore().getAt(e);
                        c.getStore().removeAt(e);
                        a.resetIndex();
                        a.collectStoreData();
                        if (a.afterRecordUpdate) {
                            a.afterRecordUpdate(d, true)
                        }
                    }
                }]
            }
        }
    },
    conditionColumns: ["dragHandler", "detailStyleSel", "conditionRule", "tooltip", "action"],
    cfGridStoreField: ["id", "styleId", "operator", "value1", "value2", "tooltip"],
    cfDefaultGridStoreRecord: {
        id: "",
        styleId: "0",
        operator: "GT",
        value1: "",
        value2: "",
        tooltip: ""
    },
    getGridDragZone: function (b) {
        var a = this;
        if (!a.gridDragZone) {
            a.gridDragZone = new Ext.grid.GridDragZone(b, {
                ddGroup: b.ddGroup || "GridDD",
                onInitDrag: function () {
                    var d = this.dragData.rowIndex;
                    var c = this.dragData.grid.getView().getRow(d);
                    this.proxy.update(c.cloneNode(true))
                }
            })
        }
        Ext.apply(a.gridDragZone.proxy, {
            dropNotAllowed: "eid-summarization-drop-nodrop"
        });
        return a.gridDragZone
    },
    reload: function () {
        var a = this;
        Ext.getCmp(a.id + "codition_grid_panel").reconfigure(a.getConditionGridStore(), a.getConditionGridColumns())
    },
    localize: function (d, c) {
        var a = this;
        if (!a.resources) {
            return d
        }
        var b = a[d] ? a[d] : d;
        if (!c) {
            return a.resources.getMessage(b)
        } else {
            return a.resources.getMessage(b, c)
        }
    },
    hideAddTokenLink: function () {
        Ext.get(this.id + "add_token_link").hide();
        Ext.get(this.id + "add_token_tooltip").hide();
        this.tokenLink = undefined
    },
    showAddTokenLink: function (c) {
        this.tokenLink = c;
        var a = Ext.get(this.id + "add_token_link");
        a.dom.innerHTML = c.linkText;
        a.show();
        var b = Ext.get(this.id + "add_token_tooltip");
        b.set({
            "ext:qtip": c.linkTooltip
        });
        b.show()
    },
    hidePercentageSymbol: function () {
        var a = this;
        var b = Ext.getCmp(a.id + "codition_grid_panel");
        a.isShowPercentageSymbol = false;
        Ext.select(".eid-cf-percentage-symbol").hide()
    },
    showPercentageSymbol: function (d) {
        var a = this;
        var c = Ext.getCmp(a.id + "codition_grid_panel");
        a.isShowPercentageSymbol = true;
        var b = Ext.select(".eid-cf-percentage-symbol");
        Ext.each(b.elements, function (f) {
            f.innerHTML = a.localize("percentageSymbol")
        });
        b.show()
    },
    constructor: function (a) {
        Ext.ux.endeca.ConditionalFormattingPanelv2.superclass.constructor.call(this, a)
    },
    getDetailStyleStore: function () {
        if (!this.detailStyleStore) {
            var a = Ext.get("eid-cf-" + this.conditionData.usedStyleGroupId);
            var c = a.getWidth();
            var d = Math.floor(c / 30);
            var e = new Array(d);
            for (var b = 0; b < d; b++) {
                e[b] = [this.conditionData.usedStyleGroupId, 0 - b * 30 + "px center"]
            }
            this.detailStyleStore = new Ext.data.ArrayStore({
                fields: ["imageName", "position"],
                data: e
            })
        }
        return this.detailStyleStore
    },
    getConditionGridStore: function () {
        if (!this.conditionGridStore) {
            this.conditionGridStore = new Ext.data.JsonStore({
                fields: this.cfGridStoreField,
                data: this.cfDefaultGridStoreRecord
            })
        }
        return this.conditionGridStore
    },
    createDetailStyleCombo: function (c, d, b) {
        var a = this;
        new Ext.form.ComboBox({
            xtype: "combo",
            editable: false,
            tpl: '<tpl for="."><div style="margin-left: 1px; background-image: url(../../html/css/eid-default/images/eid-default-images/portlets/summarization-bar/{imageName}.png); background-position: {position}; width:26px; height:18px" class="x-combo-list-item">&nbsp;</div></tpl>',
            store: this.getDetailStyleStore(),
            forceSelection: true,
            mode: "local",
            selectOnFocus: true,
            width: 48,
            listWidth: 34,
            listeners: {
                select: function (g, f, e) {
                    a.updateGridRecord(b, "styleId", e);
                    g.el.applyStyles({
                        background: "url(../../html/css/eid-default/images/eid-default-images/portlets/summarization-bar/" + a.conditionData.usedStyleGroupId + ".png) " + (-e * (Ext.isIE8 ? 30 : 30)) + "px center",
                        width: "26px",
                        height: "14px",
                        margin: "0px",
                        "background-size": "auto"
                    });
                    g.el.set({
                        title: a.localize("selectStyleImageTitle")
                    })
                },
                afterrender: function (e) {
                    if (!c) {
                        c = 0
                    }
                    e.el.applyStyles({
                        background: "url(../../html/css/eid-default/images/eid-default-images/portlets/summarization-bar/" + a.conditionData.usedStyleGroupId + ".png) " + (-c * (Ext.isIE8 ? 30 : 30)) + "px center",
                        width: "26px",
                        height: "14px",
                        margin: "0px",
                        "background-size": "auto"
                    });
                    e.el.set({
                        title: a.localize("selectStyleImageTitle")
                    })
                }
            }
        }).render(d)
    },
    createConditionField: function (c, b) {
        var d = b.id + "_condition_rule";
        var a = this;
        new Ext.Container({
            cls: "condition-rule-container",
            items: [{
                xtype: "combo",
                fieldClass: "eid-combo-text",
                editable: false,
                width: 150,
                height: 24,
                store: a.conditionSymbolStore,
                displayField: "conditionSymbol",
                valueField: "operator",
                value: b.data.operator,
                forceSelection: true,
                selectOnFocus: true,
                mode: "local",
                triggerAction: "all",
                style: {
                    margin: "0px"
                },
                listeners: {
                    select: function (h, f, e) {
                        a.updateGridRecord(b, "operator", f.data.operator);
                        if (f.data.operator == "LT" || b.data.operator == "LTEQ") {
                            Ext.getCmp(d + "_min_field").hide()
                        } else {
                            var g = Ext.getCmp(d + "_min_field");
                            if (Ext.isEmpty(g.getValue())) {
                                g.setValue(Ext.getCmp(d + "_max_field").getValue())
                            }
                            g.show()
                        } if (f.data.operator == "GT" || f.data.operator == "GTEQ" || f.data.operator == "EQ") {
                            Ext.getCmp(d + "_max_field").hide()
                        } else {
                            var i = Ext.getCmp(d + "_max_field");
                            if (Ext.isEmpty(i.getValue())) {
                                i.setValue(Ext.getCmp(d + "_min_field").getValue())
                            }
                            i.show()
                        } if (f.data.operator != "BTWN" && f.data.operator != "") {
                            Ext.getCmp(d + "_minus_symbol").hide()
                        } else {
                            Ext.getCmp(d + "_minus_symbol").show()
                        }
                    },
                    afterrender: function (e) {
                        e.el.set({
                            title: a.localize("selectCompareSymbolTitle")
                        })
                    }
                }
            }, {
                id: d + "_min_field",
                xtype: "numberfield",
                hideMode: "display",
                value: b.data.value1,
                hidden: b.data.operator == "LT" || b.data.operator == "LTEQ",
                listeners: {
                    change: function (g, f, e) {
                        a.updateGridRecord(b, "value1", f)
                    },
                    afterrender: function (e) {
                        e.el.set({
                            title: a.localize("inputMinValueTitle")
                        })
                    }
                }
            }, {
                id: d + "_minus_symbol",
                xtype: "displayfield",
                hideMode: "display",
                html: "-",
                hidden: b.data.operator != "BTWN" && b.data.operator != ""
            }, {
                id: d + "_max_field",
                xtype: "numberfield",
                hideMode: "display",
                value: b.data.value2,
                hidden: b.data.operator == "GT" || b.data.operator == "GTEQ" || b.data.operator == "EQ",
                listeners: {
                    change: function (g, f, e) {
                        a.updateGridRecord(b, "value2", f)
                    },
                    afterrender: function (e) {
                        e.el.set({
                            title: a.localize("inputMaxValueTitle")
                        })
                    }
                }
            }, {
                id: d + "_percentage_symbol",
                xtype: "displayfield",
                hidden: b.data.type == "SelfValue",
                value: a.localize("percentageSymbol"),
                cls: "eid-cf-percentage-symbol"
            }]
        }).render(b.id + "_condition_rule_container");
        if (!a.isShowPercentageSymbol) {
            Ext.select(".eid-cf-percentage-symbol").hide()
        }
    },
    createTooltipField: function (c, d, b) {
        var a = this;
        new Ext.form.TextField({
            value: c,
            width: "95%",
            listeners: {
                change: function (g, f, e) {
                    a.updateGridRecord(b, "tooltip", f)
                },
                focus: function (e) {
                    if (!a.tokenLink) {
                        return
                    }
                    a.targetCmp = e;
                    Ext.get(a.id + "add_token_link").dom.removeAttribute("disabled")
                },
                blur: function (e) {
                    if (!a.tokenLink) {
                        return
                    }
                    Ext.defer(function () {
                        if (a.targetCmp && a.targetCmp !== e) {} else {
                            if (!a.isAddTokenBtnClicked) {
                                Ext.get(a.id + "add_token_link").dom.setAttribute("disabled", "true");
                                a.targetCmp = undefined
                            }
                            a.isAddTokenBtnClicked = false
                        }
                    }, 200)
                },
                afterrender: function (e) {
                    e.el.set({
                        title: a.localize("inputTooltipTitle")
                    })
                }
            }
        }).render(d)
    },
    getConditionGridColumns: function () {
        var a = this;
        var b = [];
        Ext.each(a.conditionColumns, function (c) {
            if (Ext.isString(c)) {
                b.push(a.getPredefinedColumns()[c])
            } else {
                b.push(c)
            }
        });
        return new Ext.grid.ColumnModel({
            defaults: {
                css: "-moz-user-select: text;"
            },
            columns: b
        })
    },
    updateGridRecord: function (c, b, d) {
        var a = this;
        c.beginEdit();
        c.data[b] = d;
        c.endEdit();
        a.collectStoreData();
        if (a.afterRecordUpdate) {
            a.afterRecordUpdate(c, false)
        }
    },
    collectStoreData: function () {
        var a = this;
        var b = new Array();
        a.getConditionGridStore().each(function (c) {
            b.push(c.data)
        });
        a.conditionData.conditions = b;
        if (a.afterCollect) {
            a.afterCollect(a.conditionData)
        }
    },
    afterCollect: undefined,
    afterRecordUpdate: undefined,
    afterConditionsOrderChange: undefined,
    bindAddMetricLink: function () {
        var a = this;
        var c = Ext.get(a.id + "add_token_link");
        if (!c) {
            return
        }
        try {
            c.purgeAllListeners()
        } catch (b) {}
        c.on("click", function () {
            var e = Ext.getCmp(a.id + "codition_grid_panel");
            if (a.targetCmp) {
                a.isAddTokenBtnClicked = true;
                var d = a.targetCmp.getValue();
                a.targetCmp.setValue(d + "{" + a.tokenLink.token + "}");
                a.targetCmp.focus();
                a.targetCmp.fireEvent("change", a.targetCmp, a.targetCmp.getValue(), d)
            }
        })
    },
    removeUnselectableCls: function (a) {
        a.el.select(".x-unselectable").removeClass("x-unselectable")
    },
    getCoditionStyleSelectionPanel: function () {
        var a = this;
        return {
            id: a.id + "codition_style_selection_panel",
            cls: "eid-summarization-style-selection-panel",
            layout: "table",
            layoutConfig: {
                tableAttrs: {
                    style: {
                        width: "100%"
                    }
                },
                columns: 2
            },
            bodyBorder: false,
            items: [{
                bodyBorder: false,
                layout: "column",
                items: [{
                    xtype: "label",
                    hidden: !a.showCoditionStyleSelection,
                    text: a.localize("labelStyle"),
                    forId: a.id + "style_selection_combo"
                }, {
                    xtype: "combo",
                    hidden: !a.showCoditionStyleSelection,
                    id: a.id + "style_selection_combo",
                    width: 170,
                    lazyInit: false,
                    editable: false,
                    tpl: new Ext.XTemplate('<tpl for=".">', '<div class="x-combo-list-item eid-sbcf-combo-list-item">', '<img id="eid-cf-{style}" {[this.getAltText(values)]} src="../../html/css/eid-default/images/eid-default-images/portlets/summarization-bar/{style}.png"/>', "</div>", "</tpl>", {
                        getAltText: function (b) {
                            return 'alt="' + a.localize(b.altResource) + '"'
                        }
                    }),
                    store: a.styleSelectionStore,
                    forceSelection: true,
                    mode: "local",
                    selectOnFocus: true,
                    listeners: {
                        select: function (d, b, c) {
                            d.el.applyStyles({
                                background: "url(../../html/css/eid-default/images/eid-default-images/portlets/summarization-bar/" + b.data.style + ".png) no-repeat",
                                margin: "0px",
                                "background-size": "auto",
                                "background-position": "0 center"
                            });
                            a.conditionData.usedStyleGroupId = b.data.style;
                            a.detailStyleStore = undefined;
                            a.reload()
                        },
                        afterrender: function (b) {
                            b.el.applyStyles({
                                background: "url(../../html/css/eid-default/images/eid-default-images/portlets/summarization-bar/" + a.conditionData.usedStyleGroupId + ".png) no-repeat",
                                margin: "0px",
                                "background-size": "auto",
                                "background-position": "0 center"
                            })
                        }
                    }
                }]
            }, {
                xtype: "button",
                cls: "x-btn-text-icon eid-btn-add",
                text: a.localize("addConditionButton"),
                hidden: !a.showCoditionStyleSelection,
                handler: function () {
                    a.addCondition()
                }
            }]
        }
    },
    addCondition: function () {
        var a = this;
        var b = a.getConditionGridStore();
        var d = Ext.id();
        var e = Ext.decode(Ext.encode(a.cfDefaultGridStoreRecord));
        e.id = "condition_" + new Date().getTime();
        var c = new b.recordType(e, d);
        b.add(c);
        a.collectStoreData();
        if (a.afterRecordUpdate) {
            a.afterRecordUpdate(c, false)
        }
    },
    resetIndex: function () {
        var b = this;
        if (!b.enableIndex) {
            return
        }
        var a = Ext.select(".row-index", b.id + "codition_grid_panel");
        a.each(function (e, d, c) {
            e.dom.innerHTML = c + 1
        })
    },
    initComponent: function () {
        var a = this;
        if (!a.conditionData.usedStyleGroupId) {
            a.conditionData.usedStyleGroupId = "icon_stoplight_sprite"
        }
        Ext.apply(a, {
            percentageSymbol: a.localize("conditional-formattingv2.column.label.symbol.percentage"),
            items: [a.getCoditionStyleSelectionPanel(), {
                id: a.id + "codition_grid_panel",
                xtype: "grid",
                cls: "eid-condition-grid-panel",
                columnLines: true,
                autoHeight: true,
                enableColumnMove: false,
                store: a.getConditionGridStore(),
                colModel: a.getConditionGridColumns(),
                enableHdMenu: false,
                viewConfig: {
                    emptyText: a.localize("emptyRulesMessage"),
                    scrollOffset: -2,
                    forceFit: true,
                    rowSelectorDepth: 7
                },
                listeners: {
                    render: function (c) {
                        var b = c.getView();
                        b.on({
                            refresh: function (d) {
                                a.bindAddMetricLink();
                                var e = this.grid.getStore();
                                var h;
                                for (var g = 0; g < e.getCount(); g++) {
                                    h = d.getRow(g);
                                    var f = Ext.query(".eid-icon-row-draghandle", h)[0].getAttribute("id");
                                    a.getGridDragZone(c).setHandleElId(f)
                                }
                                a.removeUnselectableCls(c)
                            },
                            rowsinserted: function (d, h, f) {
                                var g = d.getRow(h);
                                var e = Ext.query(".eid-icon-row-draghandle", g)[0].getAttribute("id");
                                a.getGridDragZone(c).setHandleElId(e);
                                a.removeUnselectableCls(c);
                                a.collectStoreData()
                            },
                            rowupdated: function (d, h, f) {
                                var g = d.getRow(h);
                                var e = Ext.query(".eid-icon-row-draghandle", g)[0].getAttribute("id");
                                a.getGridDragZone(c).setHandleElId(e);
                                a.removeUnselectableCls(c)
                            }
                        });
                        new Ext.dd.DropTarget(c.getEl(), {
                            ddGroup: c.ddGroup || "GridDD",
                            grid: c,
                            gridDropTarget: this,
                            dropAllowed: "eid-summarization-drop-ok",
                            notifyDrop: function (n, j, h) {
                                var l = this.grid.getStore();
                                if (l.getCount() <= 1) {
                                    return false
                                }
                                var k = this.grid.getView().findRowIndex(Ext.lib.Event.getTarget(j));
                                var d = new Array();
                                var o = l.data.keys;
                                for (var m in o) {
                                    for (var g = 0; g < h.selections.length; g++) {
                                        if (o[m] == h.selections[g].id) {
                                            if (k == m) {
                                                return false
                                            }
                                            d.push(h.selections[g])
                                        }
                                    }
                                }
                                if (k > h.rowIndex && h.selections.length > 1) {
                                    k = k - (h.selections.length - 1)
                                }
                                if (k == h.rowIndex) {
                                    return false
                                }
                                for (var g = 0; g < h.selections.length; g++) {
                                    l.remove(l.getById(h.selections[g].id))
                                }
                                for (var g = d.length - 1; g >= 0; g--) {
                                    var p = k;
                                    l.insert(p, d[g])
                                }
                                var f = this.grid.getSelectionModel();
                                if (f) {
                                    f.selectRecords(h.selections)
                                }
                                a.resetIndex();
                                if (a.afterConditionsOrderChange) {
                                    a.afterConditionsOrderChange()
                                }
                                return true
                            }
                        })
                    },
                    afterrender: function (b) {
                        Ext.defer(function () {
                            b.getStore().loadData(a.conditionData.conditions)
                        }, 100);
                        b.setAutoScroll(true);
                        a.bindAddMetricLink()
                    }
                }
            }]
        });
        a.conditionSymbolStore = new Ext.data.ArrayStore({
            fields: [{
                name: "conditionSymbol",
                type: "String"
            }, {
                name: "operator",
                type: "String"
            }],
            data: [
                [a.localize("conditionGT"), "GT"],
                [a.localize("conditionGTEQ"), "GTEQ"],
                [a.localize("conditionBetween"), "BTWN"],
                [a.localize("conditionLT"), "LT"],
                [a.localize("conditionLTEQ"), "LTEQ"],
                [a.localize("conditionEQ"), "EQ"]
            ]
        }), Ext.ux.endeca.ConditionalFormattingPanelv2.superclass.initComponent.call(this)
    }
});
Ext.reg("conditionalFormatterPanelV2", Ext.ux.endeca.ConditionalFormattingPanelv2);