Ext.ns('Endeca.Portlets.BulletChartPortlet');

/**
 * @class Endeca.Portlets.BulletChartPortlet.EditModel
 *
 * Constructs an instance of the EditModel JavaScript object, combining the
 * state from the server with the method behavior defined in this file.
 *
 * Will copy each field under the server's root editModel into this object.
 *
 * IMPORTANT: Avoid name conflicts. Ensure that the members in the server's JSON
 * object do not conflict with method names in this JavaScript file.
 *
 * @author Endeca Technologies, Inc.
 *
 */
Endeca.Portlets.BulletChartPortlet.EditModel = function (editModelData,
    defaultDataSourceId) {
	this.constants = new Endeca.Portlets.BulletChartPortlet.Constants();
    for (var member in editModelData) {
        if (editModelData.hasOwnProperty(member)) {
            /* it's a property - copy it */
            this[member] = editModelData[member];
        }
    }
    
    for (var a = 0; a < this.configs.length; a++) {
		Ext.apply(this.configs[a], Endeca.Portlets.BulletChartPortlet.EditModel.ClearDataUtils);
		Ext.apply(this.configs[a], Endeca.Portlets.BulletChartPortlet.EditModel.TypeUtils);
	}
    this.defaultDataSourceId = defaultDataSourceId;
    return this;
};

Endeca.Portlets.BulletChartPortlet.EditModel.DefaultConfigUtils = {
		constants : new Endeca.Portlets.BulletChartPortlet.Constants(),
		getDefaultItemConfig : function() {
			var a = {
				dimensionKey : undefined,
				dimensionFormatter : undefined,
				datePartCombo : undefined,
				metricKey : undefined,
				metricAggre : undefined,
				metricFormatter : undefined,
				targetMetricKey : undefined,
				targetMetricAggre : undefined,
				targetMetricFormatter : undefined,
				isTopValue : true,
				isCustomDisplayName : true,
				customDisplayName : undefined,
				isCustomWidth : undefined,
				customWidth : 350,
				action : undefined,
				description : undefined
			};
			return a
		},
		getDefaultConditionsConfig : function() {
			return {
				type : this.constants.CONDITION_TYPE_SELF_VALUE,
				conditions : [ {
					styleId : "0",
					operator : "GT",
					value1 : "",
					value2 : "",
					tooltip : ""
				} ],
				targetMetricKey : undefined,
				targetMetricAggre : undefined,
				usedStyleGroupId : undefined,
				isMeetDispOnly : false,
				isMetricTextBlack : false
			}
		},
		getDefaultAlertConditionsConfig : function() {
			return {
				id : "condition_" + new Date().getTime(),
				type : this.constants.CONDITION_TYPE_SELF_VALUE,
				metric : null,
				targetMetric : null,
				ratioFormatter : null,
				operator : "GT",
				value1 : "",
				value2 : ""
			}
		},
		getDefaultInfoConfig : function() {
			return {
				customTitle : undefined,
				dimensionKey : undefined,
				isTopValue : true,
				action : null,
				isDisplay : false,
				description : undefined,
				isCustomTitle : false,
				maxNumber : 5,
				isShowMore : false,
				formatter : null
			}
		},
		getDefaultAlertConfig : function() {
			var a = {
				displayName : undefined,
				color : "red",
				description : undefined,
				maxNumber : 10,
				isCustomWidth : false,
				customWidth : 350,
				isOtherPage : false,
				targetPage : "",
				dimensionsList : [],
				conditonsList : [ this.getDefaultAlertConditionsConfig() ],
				displayColumnsList : [ {
					id : "dimensions",
					isDisplay : true,
					isCustomName : false,
					autoGenName : "",
					customName : ""
				} ]
			};
			return a
		}
	};


Endeca.Portlets.BulletChartPortlet.EditModel.prototype = {

    CHART_TYPES: {
        BAR: "Bar",
        BARSTACKED: "BarStacked",
        BARSTACKEDPERCENT: "BarStackedPercent",
        HORIZONTALBAR: "HorizontalBar",
        HORIZONTALBARSTACKED: "HorizontalBarStacked",
        HORIZONTALBARSTACKEDPERCENT: "HorizontalBarStackedPercent",
        LINE: "Line",
        AREA: "Area",
        AREAPRECENT: "AreaPrecent",
        PIE: "Pie",
        BARLINE: "BarLine",
        BARLINEDUAL: "BarLineDual",
        SCATTER: "Scatter",
        BUBBLE: "Bubble"
    },
    CHART_DIRECTION_VERT: "vertical",
    CHART_DIRECTION_HORI: "horizontal",
    STEP_AUTO: "auto",
    STEP_DATA: "data",
    STEP_SELECT_TYPE: "selectChartType",
    STEP_CHART: "chart",
    STEP_CHART_STYLE: "chartStyle",
    METRIC: "metric",
    SECONDARY_METRIC: "secondaryMetric",
    GROUP: "group",
    SYSTEM_COUNT_1: "COUNT_1",
    SYSTEM_COUNT: "COUNT",
    SYSTEM_COUNT_DISTINCT: "COUNTDISTINCT",
    MULTI_BEHAVIOR_DEFAULT: "DEFAULT",
    MULTI_BEHAVIOR_INDIVIDUAL: "INDIVIDUAL",
    MULTI_BEHAVIOR_SET: "SET",
    MDEX_TYPES: ["mdex:int", "mdex:long", "mdex:double", "mdex:string"],
    editModelForPreview: null,
    currentIndex: 0,
    DEFAULT_VARIABLE_LIMIT: 50,
    AGGRE_METHODS: null,
    // FORMATTERCLASS :
    // "com.endeca.portlet.chart.model.format.ChartNumberFormatter",
    separator: ", ",
    /**
     * Convert an array of complex field objects { fieldName:'foo',
     * displayed:true } to an array Ext.tree.TreeNode
     */
    getSemanticKeys : function(){
    	var b = this;
		var a = b.controller.configs[0].semanticKeys;
		return a;
    },
    createConfigurationModel : function(d) {
		var e = this;
		var b = new Date().getTime();
		var a = e.generateDisplayName(d);
		var c = Ext.isEmpty(this.defaultDataSourceId) ? "default"
				: this.defaultDataSourceId;
		var g = e.dataObj.getViewList(c)[0].key;
		var f = {
			key : b,
			displayName : a,
			dataSourceId : c,
			semanticKeys : [ "Base" ],
			maxGroupNum : 1000,
			maxGroupRemark : ""
		};
		return f
	},getCurrentChartConfig : function() {
		var a = this.controller;
		a.currentIndex = 0;
		if (!a.configs[a.currentIndex]) {
			a.configs[a.currentIndex] = {}
		}
		return a.configs[a.currentIndex]
	},
	getViewsMap : function() {
		var f = this;
		var b = {};
		var g = f.getSemanticKeys();
		var c = f.dataObj.getViewList(f.getCurrentChartConfig().dataSourceId);
		for (var e = 0; e < g.length; e++) {
			var a = {};
			for (var d = 0; d < c.length; d++) {
				if (c[d].key == g[e]) {
					a = c[d];
					break
				}
			}
			b[g[e]] = a
		}
		return b
	},getViewDisplayName : function(a) {
		if (Ext.isEmpty(this.getViewsMap()[a])) {
			return ""
		}
		return this.getViewsMap()[a].displayName
	},
    getFieldsAsExtNodeList: function () {
        var extNodeList = [];

        for (var i = 0; i < this.fields.length; i++) {
            extNodeList.push({
                text: this.fields[i].fieldName,
                checked: this.fields[i].displayed,
                leaf: true,
                href: 'javascript:void(0);'
                /*
                 * we don't want links, and
                 * default href='#' doesn't work
                 * in liferay for some reason
                 */
            });
        }

        return extNodeList;

    },
    defaultConfigUtils : Endeca.Portlets.BulletChartPortlet.EditModel.DefaultConfigUtils,
	getDefaultSummarizationConfig : function() {
		var a = {
			id : undefined,
			name : undefined,
			type : this.constants.SUMMARIZATION_TYPE_METRIC_SINGLE,
			itemConfig : this.defaultConfigUtils.getDefaultItemConfig(),
			conditionsConfig : this.defaultConfigUtils
					.getDefaultConditionsConfig(),
			infoConfig : this.defaultConfigUtils.getDefaultInfoConfig(),
			alertConfig : this.defaultConfigUtils.getDefaultAlertConfig(),
			isCompleted : undefined,
			dataSourceId : undefined,
			semanticKey : undefined,
			semanticName : undefined
		};
		Ext
				.apply(
						a,
						Endeca.Portlets.BulletChartPortlet.EditModel.ClearDataUtils);
		Ext.apply(a,
				Endeca.Portlets.BulletChartPortlet.EditModel.TypeUtils);
		return a
	}

};

Endeca.Portlets.BulletChartPortlet.EditModel.ClearDataUtils = {
		defaultConfigUtils : Endeca.Portlets.BulletChartPortlet.EditModel.DefaultConfigUtils,
		clearItemConfig : function() {
			Ext.apply(this.itemConfig, this.defaultConfigUtils
					.getDefaultItemConfig())
		},
		clearConditionsConfig : function() {
			Ext.apply(this.conditionsConfig, this.defaultConfigUtils
					.getDefaultConditionsConfig())
		},
		clearInfoConfig : function() {
			Ext.apply(this.infoConfig, this.defaultConfigUtils
					.getDefaultInfoConfig())
		},
		clearAlertConfig : function() {
			Ext.apply(this.alertConfig, this.defaultConfigUtils
					.getDefaultAlertConfig())
		}
	};
	Endeca.Portlets.BulletChartPortlet.EditModel.TypeUtils = {
		constants : new Endeca.Portlets.BulletChartPortlet.Constants(),
		getType : function() {
			switch (this.type) {
			case this.constants.SUMMARIZATION_TYPE_METRIC_SINGLE:
				return this.constants.SUMMARIZATION_TYPE_METRIC_VALUE;
			case this.constants.SUMMARIZATION_TYPE_METRIC_RATIO:
				return this.constants.SUMMARIZATION_TYPE_METRIC_VALUE;
			case this.constants.SUMMARIZATION_TYPE_METRIC_PART_TO_WHOLE:
				return this.constants.SUMMARIZATION_TYPE_METRIC_VALUE;
			case this.constants.SUMMARIZATION_TYPE_DIMENSION_SINGLE:
				return this.constants.SUMMARIZATION_TYPE_DIMENSION_VALUE;
			case this.constants.SUMMARIZATION_TYPE_DIMENSION_RATIO:
				return this.constants.SUMMARIZATION_TYPE_DIMENSION_VALUE;
			case this.constants.SUMMARIZATION_TYPE_ALERT:
				return this.constants.SUMMARIZATION_TYPE_ALERT_VALUE
			}
		},
		getSubtype : function() {
			switch (this.type) {
			case this.constants.SUMMARIZATION_TYPE_METRIC_SINGLE:
				return this.constants.SUMMARIZATION_SUBTYPE_SINGLE_METRIC;
			case this.constants.SUMMARIZATION_TYPE_METRIC_RATIO:
				return this.constants.SUMMARIZATION_SUBTYPE_METRICS_RATIO;
			case this.constants.SUMMARIZATION_TYPE_METRIC_PART_TO_WHOLE:
				return this.constants.SUMMARIZATION_SUBTYPE_PART_WHOLE_RATIO;
			case this.constants.SUMMARIZATION_TYPE_DIMENSION_SINGLE:
				return this.constants.SUMMARIZATION_SUBTYPE_SINGLE_METRIC;
			case this.constants.SUMMARIZATION_TYPE_DIMENSION_RATIO:
				return this.constants.SUMMARIZATION_SUBTYPE_METRICS_RATIO
			}
		},
		setType : function(b) {
			var a = this;
			if (b == this.constants.SUMMARIZATION_TYPE_METRIC_VALUE) {
				switch (a.type) {
				case this.constants.SUMMARIZATION_TYPE_ALERT:
					a.type = this.constants.SUMMARIZATION_TYPE_METRIC_SINGLE;
					break;
				case this.constants.SUMMARIZATION_TYPE_DIMENSION_SINGLE:
					a.type = this.constants.SUMMARIZATION_TYPE_METRIC_SINGLE;
					break;
				case this.constants.SUMMARIZATION_TYPE_DIMENSION_RATIO:
					a.type = this.constants.SUMMARIZATION_TYPE_METRIC_RATIO;
					break
				}
			} else {
				if (b == this.constants.SUMMARIZATION_TYPE_DIMENSION_VALUE) {
					switch (a.type) {
					case this.constants.SUMMARIZATION_TYPE_ALERT:
						a.type = this.constants.SUMMARIZATION_TYPE_DIMENSION_SINGLE;
						break;
					case this.constants.SUMMARIZATION_TYPE_METRIC_SINGLE:
						a.type = this.constants.SUMMARIZATION_TYPE_DIMENSION_SINGLE;
						break;
					case this.constants.SUMMARIZATION_TYPE_METRIC_RATIO:
						a.type = this.constants.SUMMARIZATION_TYPE_DIMENSION_RATIO;
						break;
					case this.constants.SUMMARIZATION_TYPE_METRIC_PART_TO_WHOLE:
						a.type = this.constants.SUMMARIZATION_TYPE_DIMENSION_SINGLE;
						break
					}
				} else {
					a.type = this.constants.SUMMARIZATION_TYPE_ALERT
				}
			}
		},
		setSubtype : function(b) {
			var a = this;
			if (b == this.constants.SUMMARIZATION_SUBTYPE_SINGLE_METRIC) {
				switch (a.type) {
				case this.constants.SUMMARIZATION_TYPE_METRIC_RATIO:
					a.type = this.constants.SUMMARIZATION_TYPE_METRIC_SINGLE;
					break;
				case this.constants.SUMMARIZATION_TYPE_METRIC_PART_TO_WHOLE:
					a.type = this.constants.SUMMARIZATION_TYPE_METRIC_SINGLE;
					break;
				case this.constants.SUMMARIZATION_TYPE_DIMENSION_RATIO:
					a.type = this.constants.SUMMARIZATION_TYPE_DIMENSION_SINGLE;
					break
				}
			} else {
				if (b == this.constants.SUMMARIZATION_SUBTYPE_METRICS_RATIO) {
					switch (a.type) {
					case this.constants.SUMMARIZATION_TYPE_METRIC_SINGLE:
						a.type = this.constants.SUMMARIZATION_TYPE_METRIC_RATIO;
						break;
					case this.constants.SUMMARIZATION_TYPE_METRIC_PART_TO_WHOLE:
						a.type = this.constants.SUMMARIZATION_TYPE_METRIC_RATIO;
						break;
					case this.constants.SUMMARIZATION_TYPE_DIMENSION_SINGLE:
						a.type = this.constants.SUMMARIZATION_TYPE_DIMENSION_RATIO;
						break
					}
				} else {
					switch (a.type) {
					case this.constants.SUMMARIZATION_TYPE_METRIC_SINGLE:
						a.type = this.constants.SUMMARIZATION_TYPE_METRIC_PART_TO_WHOLE;
						break;
					case this.constants.SUMMARIZATION_TYPE_METRIC_RATIO:
						a.type = this.constants.SUMMARIZATION_TYPE_METRIC_PART_TO_WHOLE;
						break
					}
				}
			}
		}
	};