VisualizationExporter = function(portletId) {
	this.portletId = portletId;
	jQuery('#' + portletId).append('<canvas id="canvas_' + portletId + '" style="height:1px; width:1px; display:none;"/>');
	this.canvas = jQuery('#' + 'canvas_' + portletId);
}

VisualizationExporter.prototype.loadImage = function(svgElem) {
	this.svgElem = svgElem;
	var imgUrl = this.createImageUrl(svgElem);
	this.openInTab(imgUrl);
}

//Move the SVG into a canvas with canvg, then create a PNG data string with the HTML 5 toDataURL function
VisualizationExporter.prototype.createImageUrl = function(svgElem) {
	svgElem = this.formatSvg(svgElem);
	
	canvg(this.canvas.attr('id'), svgElem);

	var canvas = document.getElementById(this.canvas.attr('id'));
	var imgUrl = canvas.toDataURL("image/png");
	return imgUrl;
}

//Reformat the SVG text as necessary
VisualizationExporter.prototype.formatSvg = function(svgElem) {
	//Add spaces between transform functions, otherwise they won't work in canvg
	svgElem = svgElem.replace(/\)rotate\(/g, ') rotate(');
	svgElem = svgElem.replace(/\)translate\(/g, ') translate(');
	svgElem = svgElem.replace(/\)scale\(/g, ') scale(');
	
	return svgElem;
}

//Open the given URL in a new window/tab
VisualizationExporter.prototype.openInTab = function(url) {
	var win = window.open(url, '_blank');
	win.focus();
}