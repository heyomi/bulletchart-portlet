Ext.ns("Ext.ux.endeca");
Ext.ux.endeca.ChartAttributeFormatterPanel = Ext.extend(Ext.ux.endeca.AttributeFormatterPanelV2, {
    generateFormatterObject: function () {
        var a = Ext.ux.endeca.ChartAttributeFormatterPanel.superclass.generateFormatterObject.call(this);
        if (this.dataType === "NUMBER") {
            a.isInteger = this.attrType != "mdex:double";
            a.scaling = this.getValueFrom("scalingSection", "radios");
            a["@class"] = "com.endeca.portlet.chart.model.format.ChartNumberFormatter";
            a.parentFormatSettings = this.parentFormatData
        }
        return a
    },
    initComponent: function () {
        var c = this;
        if (!c.formatData) {
            c.formatData = {
                type: c.dataType,
                "@class": "com.endeca.portlet.chart.model.format.ChartNumberFormatter"
            }
        }
        Ext.ux.endeca.ChartAttributeFormatterPanel.superclass.initComponent.call(c);
        var b = new Ext.Container({
            id: c.id + "scalingSection",
            colspan: 2,
            layout: "column",
            hideMode: "display",
            items: [{
                xtype: "label",
                html: c.messages.getMessage("chart.edit.metric-group.label.auto-number-scaling") + '<div id="' + c.id + 'scalingSectionTitleHelp" class="eid-action-icon eid-icon-help eid-action-icon-eol">' + c.messages.getMessage("chart.iconic.info-icon-text") + "</div>",
                listeners: {
                    afterrender: function () {
                        c.createToolTip({
                            target: c.id + "scalingSectionTitleHelp",
                            html: c.messages.getMessage("chart.edit.metric-group.help.auto-number-scaling")
                        })
                    }
                }
            }, {
                id: c.id + "scalingSectionOn",
                name: c.id + "scalingSection",
                xtype: "radio",
                margins: "0 8px 0 0",
                boxLabel: c.messages.getMessage("chart.edit.metric-group.label.auto-scaling-on"),
                checked: Ext.isEmpty(c.formatData.scaling) || c.formatData.scaling == "YES" ? true : false,
                value: "YES",
                listeners: {
                    check: function (e, d) {
                        if (d === true) {
                            c.formatData.scaling = this.value;
                            c.updateUI()
                        }
                    }
                }
            }, {
                id: c.id + "scalingSectionOff",
                name: c.id + "scalingSection",
                xtype: "radio",
                boxLabel: c.messages.getMessage("chart.edit.metric-group.label.auto-scaling-off"),
                checked: c.formatData.scaling == "NO" ? true : false,
                value: "NO",
                listeners: {
                    check: function (e, d) {
                        if (d === true) {
                            c.formatData.scaling = this.value;
                            c.updateUI()
                        }
                    }
                }
            }]
        });
        var a = Ext.getCmp(c.id + "internalPanel-NUMBER");
        a.insert(9, b);
        c.initChangeListeners(c.findById(c.id + "scalingSection"))
    }
});