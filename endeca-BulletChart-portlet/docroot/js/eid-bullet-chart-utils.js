Ext.ns("Endeca.Portlets.BulletChartPortlet");
Endeca.Portlets.BulletChartPortlet.Utils = function(a, b) {
	this.portletId = a;
	this.resources = b;
	this.constants = new Endeca.Portlets.BulletChartPortlet.Constants();
	return this
};
Endeca.Portlets.BulletChartPortlet.Utils.prototype = {
	localize : function(d, c, b) {
		var a = this;
		if (!a.resources) {
			return d
		}
		if (Ext.isEmpty(c)) {
			return a.resources.getMessage(d)
		} else {
			if (Ext.isEmpty(b)) {
				return a.resources.getMessage(d, c)
			} else {
				return a.resources.getMessage(d, c, b)
			}
		}
	},
	showConfigMessage : function(e, d) {
		var a = this;
		if (e) {
			var b = a.localize("df.view-auto-configured");
			var c = {
				cls : "eid-icon-remove eid-action-icon eid-summarization-bar-view-auto-config-message-remove-icon "
			};
			var b = String.format("{0}{1}", b, Ext.DomHelper.markup(c));
			a.displayInfoMessage(b, false);
			Ext
					.fly(a.portletId + "Info")
					.on(
							"click",
							function(f) {
								if (f
										.getTarget(null, 0, true)
										.hasClass(
												"eid-summarization-bar-view-auto-config-message-remove-icon ")) {
									Ext.fly(a.portletId + "Info").setDisplayed(
											false);
									a.updateShowConfigMessage(d)
								}
							})
		}
	},
	updateShowConfigMessage : function(b) {
		var a = this;
		Ext.Ajax.request({
			url : b,
			success : function(c, d) {
			},
			failure : function(c, d) {
				a.displayErrorMessage(c.responseText)
			}
		})
	},
	displaySuccessMessage : function(b, a) {
		Ext.fly(this.portletId + "Success").update(b);
		Ext.fly(this.portletId + "Success").setVisible(true, true);
		Ext.fly(this.portletId + "Warning").setDisplayed(false);
		Ext.fly(this.portletId + "Error").setDisplayed(false);
		Ext.fly(this.portletId + "Info").setDisplayed(false);
		this.delayHideMsg(5000)
	},
	displayWarnMessage : function(b, a) {
		Ext.fly(this.portletId + "Success").setDisplayed(false);
		Ext.fly(this.portletId + "Warning").update(b);
		Ext.fly(this.portletId + "Warning").setVisible(true, true);
		Ext.fly(this.portletId + "Error").setDisplayed(false);
		Ext.fly(this.portletId + "Info").setDisplayed(false);
		this.delayHideMsg(5000, a)
	},
	displayErrorMessage : function(b, a) {
		Ext.fly(this.portletId + "Success").setDisplayed(false);
		Ext.fly(this.portletId + "Warning").setDisplayed(false);
		Ext.fly(this.portletId + "Error").update(b);
		Ext.fly(this.portletId + "Error").setVisible(true, true);
		Ext.fly(this.portletId + "Info").setDisplayed(false);
		this.delayHideMsg(5000, a)
	},
	displayInfoMessage : function(b, a) {
		Ext.fly(this.portletId + "Success").setDisplayed(false);
		Ext.fly(this.portletId + "Warning").setDisplayed(false);
		Ext.fly(this.portletId + "Error").setDisplayed(false);
		Ext.fly(this.portletId + "Info").update(b);
		Ext.fly(this.portletId + "Info").setVisible(true, true);
		this.delayHideMsg(5000, a)
	},
	hideMessages : function() {
		Ext.fly(this.portletId + "Success").setDisplayed(false);
		Ext.fly(this.portletId + "Warning").setDisplayed(false);
		Ext.fly(this.portletId + "Error").setDisplayed(false);
		Ext.fly(this.portletId + "Info").setDisplayed(false)
	},
	delayHideMsg : function(c, a) {
		var b = this;
		if (typeof a === "undefined") {
			a = true
		}
		if (a) {
			if (b.delayTaskOfHideMsg) {
				clearTimeout(b.delayTaskOfHideMsg)
			}
			b.delayTaskOfHideMsg = b.hideMessages.defer(c, b)
		}
	},
	getAttributeDisplayName : function(a, c, d) {
		if (this.isCountOne(d)) {
			return this.localize("summarizationbar.edit.label.record-count")
		}
		var b = this.getAttributePropertyByKey(a, c, d, "dn");
		return Ext.isEmpty(b) ? d : b
	},
	getAttributeDiscription : function(a, b, c) {
		return this.getAttributePropertyByKey(a, b, c, "description")
	},
	getAttributePropertyByKey : function(a, b, e, d) {
		var c = this.getAttributeByKey(a, b, e);
		if (c) {
			return c[d]
		}
	},
	getAttributeByKey : function(a, d, e) {
		var b = this.getAttributes(a, d);
		for (var c = 0; c < b.length; c++) {
			if (b[c].key == e) {
				return b[c]
			}
		}
	},
	getMetricByKey : function(a, d, f) {
		var b;
		var e = this.getMetrics(a, d);
		for (var c = 0; c < e.length; c++) {
			if (e[c].key == f) {
				b = e[c];
				break
			}
		}
		return b
	},
	getDimensionByKey : function(b, d, a) {
		var f;
		var e = this.getDimensions(b, d);
		for (var c = 0; c < e.length; c++) {
			if (e[c].key == a) {
				f = e[c];
				break
			}
		}
		return f
	},
	getAttributes : function(a, b) {
		return this.dataMgr.getDataObject().getAttributes(a, b)
	},
	getMetrics : function(a, b) {
		return this.dataMgr.getDataObject().getMetrics(a, b)
	},
	getViewName : function(a, b) {
		return this.dataMgr.getDataObject().getViewInfo(a, b)
	},
	hasViewData : function(a, b) {
		return this.dataMgr.getDataObject().hasViewData(a, b)
	},
	getDimensions : function(a, b) {
		return this.dataMgr.getDataObject().getDimensions(a, b)
	},
	getSummarizationName : function(a) {
		var b = this;
		if (a.getType() == b.constants.SUMMARIZATION_TYPE_ALERT_VALUE) {
			return a.alertConfig.displayName
		}
		if (a.itemConfig.isCustomDisplayName) {
			a.name = a.itemConfig.customDisplayName;
			return a.name
		}
		a.name = b.getAutoSummarizationName(a);
		if (Ext.isEmpty(a.name)) {
			a.name = a.itemConfig.customDisplayName
		}
		return a.name
	},
	getAutoSummarizationName : function(c) {
		var e = this;
		var g = e.getFieldValueFromMetric(c.dataSourceId, c.semanticKey,
				c.itemConfig.metricKey, c.itemConfig.metricAggre, true);
		if (c.getType() == e.constants.SUMMARIZATION_TYPE_DIMENSION_VALUE) {
			var f = e.getAttributeByKey(c.dataSourceId, c.semanticKey,
					c.itemConfig.dimensionKey);
			if (!Ext.isEmpty(f)) {
				var b = f.dn;
				if (Ext.isEmpty(b)) {
					b = f.key
				}
				if (!Ext.isEmpty(b)) {
					if (!Ext.isEmpty(c.itemConfig.datePartCombo)) {
						var a = "", h = c.itemConfig.datePartCombo;
						for (var d = 0; d < h.length; d++) {
							a = a + h[d]
						}
						b = b
								+ " ("
								+ EIDJSMessages
										.getMessage("datetime.combonames." + a)
								+ ")"
					}
					if (c.itemConfig.isTopValue) {
						return e
								.localize(
										"summarizationbar.edit.default-summarization-name.dim-value-spotlight.top",
										b, g)
					} else {
						return e
								.localize(
										"summarizationbar.edit.default-summarization-name.dim-value-spotlight.bottom",
										b, g)
					}
				}
			}
		}
		return g
	},
	getFieldValueFromMetric : function(e, f, b, c, i) {
		var h = this;
		if (Ext.isEmpty(b)) {
			return ""
		}
		if (h.isCountOne(b)) {
			return h.localize("summarizationbar.edit.label.record-count")
		}
		var d = h.getAttributeByKey(e, f, b);
		if (Ext.isEmpty(d)) {
			return ""
		}
		var a = d.dn;
		if (Ext.isEmpty(a)) {
			a = d.key
		}
		if (!i) {
			return a
		}
		var g;
		if (!Ext.isEmpty(c)) {
			if (h.isCountOrDistinct(c)) {
				g = h.localize("summarizationbar.edit.aggre-methods."
						+ c.toLowerCase())
			} else {
				g = c.toLowerCase()
			}
		} else {
			if (!Ext.isEmpty(d.defaultAggregation)) {
				g = d.defaultAggregation.toLowerCase()
			}
		}
		if (!Ext.isEmpty(g)) {
			a = a + " (" + g + ")"
		}
		return a
	},
	isCountOrDistinct : function(b) {
		var a = this;
		return b == a.constants.AGGREGATION_FUNCTION_COUNT
				|| b == a.constants.AGGREGATION_FUNCTION_COUNTDISTINCT
	},
	isAnyCount : function(b, a) {
		return this.isCountOrDistinct(a) || this.isCountOne(b)
	},
	isCountOne : function(a) {
		return a == this.constants.COUNT_ONE_KEY
	},
	getAggregatedType : function(b, a) {
		return Ext.ux.endeca.FormattingConstants.Maps.getAggregatedDataType(b,
				a)
	},
	getAttributeAggregatedType : function(d, e, c) {
		var b = this;
		if (b.isCountOne(d)) {
			return Ext.ux.endeca.FormattingConstants.Maps.mdexDataTypeMap.LONG
		} else {
			var a = b.getAttributeByKey(c.dataSourceId, c.semanticKey, d);
			if (a && a.datatype) {
				return b.getAggregatedType(a.datatype, e)
			}
		}
	},
	getAttributeParentFormatter : function(f, g, e) {
		var c = this;
		var b = "{}";
		var d = c.getAttributeAggregatedType(f, g, e);
		var a = c.getAttributeByKey(e.dataSourceId, e.semanticKey, f);
		if (a && a.datatype && a.datatype == d) {
			b = a.formatSettings || "{}"
		}
		return b
	},
	getDefaultRefinementAction : function() {
		return {
			actionType : "refinement",
			"class" : "com.endeca.portal.actions.model.RefinementActionModel"
		}
	},
	replacePlaceHolderToValue : function(b, d, c) {
		var a = new RegExp("{" + d + "}", "g");
		return b.replace(a, c)
	},
	//Replaces (sum), (avg) etc.
	replaceAggregrationType: function(a){
		if(a){
			return a.replace(/ *\([^)]*\) */g, "");
		}
	},
	cleanUpEditView: function (controller) {
        //removes navbar between tabs
		if (jQuery("#" + controller.portletId + "_wizard").length > 0) {
            jQuery("#" + controller.portletId + "_wizard").remove();
        }

        if (jQuery("#" + controller.portletId + "define_item_selected_metric_container").length > 0) {
            jQuery("#" + controller.portletId + "define_item_selected_metric_container").eq(2).hide();
            var selectedId = jQuery("#" + controller.portletId + "define_item_selected_metric_container input").attr("id");
            
            if(selectedId != undefined){
            	var selectedText = Ext.get(selectedId).dom.value;
            	Ext.get(selectedId).dom.value = this.replaceAggregrationType(selectedText);
            }
            
        }


        /*jQuery("#" + this.portletId + "define_item_metric_attr_config_fieldconfigure_dimension_window").on('DOMNodeInserted',function(e){
    		if(jQuery("#" + this.portletId + "define_item_metric_attr_config_fieldconfigure_dimension_window").length > 0){
        		jQuery("#" + this.portletId + "define_item_metric_attr_config_fieldconfigure_dimension_window eid-attribute-select-field-configuration-combox").hide();
            }
        	
    	});*/
        
        //unused
       /* if (jQuery(".endeca-attribute-reservoir").length > 0) {
            var attributes = jQuery(".endeca-attribute-reservoir .x-grid3-body .x-grid3-row .column-display-name");
            $.each(attributes, function (i, v) {
                if ((v.text() == "Number of Records" || v.text() == "Number of Records with values") || v.text() == "Number of unique values") {
                    v.parent().parent().parent().parent().parent().parent().hide();
                }
            });
        }*/

        

 		//Hide down arrow
        /*if (jQuery("#" + controller.portletId + "define_item_selected_metric_container .x-form-twin-triggers").length > 0) {
            var img = jQuery("#" + controller.portletId + "define_item_selected_metric_container .x-form-twin-triggers");
            img.hide();
            img.find('.eid-icon-remove').show();
        }*/

        //
        if (jQuery("#x-form-el-" + controller.portletId + "define_item_selected_metric_container").length > 0) {
            jQuery("#x-form-el-" + controller.portletId + "define_item_selected_metric_container .x-form-twin-triggers img").eq(2).hide();
        }

    }
};