Ext.ns('Endeca.Portlets.BulletChartPortlet');

/**
 * @class Endeca.Portlets.BulletChartPortlet.Controller
 *
 * Constructs an instance of the Portlet JavaScript Controller, providing the portletId.
 *
 * The portletId is needed so that the JavaScript code can locate its HTML divs
 *
 * @author Endeca Technologies, Inc.
 */
Endeca.Portlets.BulletChartPortlet.Controller = function (portletId) {
    this.portletId = portletId;
    return this;
};

var _tab;

Endeca.Portlets.BulletChartPortlet.Controller.prototype = {
    /**
     * Initialize the View component hierarchy, and render it to the div with id=(portletId + 'View')
     */
    initializeView: function () {
    	var jsController = this;
    	
      	 Ext.Ajax.request({
             url: jsController.resourceUrls.recordDataStore,
             success: function(c) {
            	 var data = JSON.parse(c.responseText);
                 if(data != "" && data[0].bullets != undefined){
                    jsController.setBullets(data[0].bullets);
                    jsController.renderBulletLayout(jsController.viewModel);
                    jsController.loadBullets(jsController.viewModel,data[0].bullets);
                 }else{
                    jsController.displayErrorMessage("No data");
                 }
             },
             failure: function(c) {
            	 jsController.displayErrorMessage(c.responseText)
             },
             params: {}
         });
    },
    getNumberofBullets: function (viewModel) {
        var count = 0;
        for (var i = viewModel.bullets.length - 1; i >= 0; i--) {
            if(viewModel.bullets[i].metric != null) {
                count++;
            }
        };
        return count;
    },
    setBullets : function (data) {
        var jsController = this;
        jsController.viewModel.bullets = data;
    },
    loadBullets: function (viewModel, bullets) {
        var jsController = this;
        var data = [];
        var i = 0;
        var bullets = jsController.getNumberofBullets(viewModel);

        for (var i = viewModel.bullets.length - 1; i >= 0; i--) {
            if(viewModel.bullets[i].metric != null) {
                data.push(jsController.loadData(viewModel, i));
            }
        };

        if (bullets > 0) {
            jQuery.each(data, function (i, val) {
                jsController.addBullet(i, data[i])
            });
        }else{
            jsController.displayErrorMessage("c.responseText");
        }
    },
    loadData: function (data, i) {
        return {
            "title": data.configs[i].name,
            "subtitle": "US$, in thousands", //sub-label for bullet chart
            "ranges": [data.bullets[i].minimum, data.bullets[i].median, data.bullets[i].maximum], //Minimum, mean and maximum values.
            "measures": [Math.floor((Math.random() * 100) + 1)], //Value representing current measurement (the thick blue line in the example)
            "markers": [Math.floor((Math.random() * 100) + 1)] //Place a marker on the chart (the white triangle marker)
        };
    },
    /* Utilizes nv.addGraph function*/
    addBullet: function (i, data) {
        nv.addGraph(function () {
            var chart = nv.models.bulletChart();
            d3.select('#bulletChart-container svg._bullet_' + i).datum(data).transition()
                .duration(1000)
                .attr("class", "bullet")
                .call(chart);

            return chart;
        });
    },
    getBullets: function (a) {
        var b = this;
        
        //Set Featured Measure
        if (b.summarizationIndex >= 0) {
        	b.editModel.configs[b.summarizationIndex].itemConfig["featuredMeasure"] = jQuery("#" + b.portletId + "featured_measure").val().replace(/\s/g, '');
        }
        
        Ext.Ajax.request({
            url: b.resourceUrls.getBulletsUrl,
            success: function (c) {
                
                if (a) {
                    
                }
            },
            failure: function (c) {
               
            }
        })
    },
    renderBulletLayout: function (viewModel) {
        var controller = this;
        var bullets = controller.getNumberofBullets(viewModel);

        if (bullets > 0) {
            if (jQuery("#bulletChart-container").length > 0) {
                for (var i = 0; i < bullets; i++) {
                    jQuery("#bulletChart-container").append('<svg class="_bullet_' + i + '"></svg>');
                }
            }
        }else{
            jsController.displayErrorMessage("c.responseText");
        }

    },
    initDataManager: function () {
        var jsController = this;
        if (!jsController.dataMgr) {
        	jsController.dataMgr = new Ext.ux.endeca.ViewDataManager({
                resourceUrl: jsController.resourceUrls.getViewDataUrl,
                hasSet: false,
                hasArb: false
            });
        }
    },
    initEditModel: function (e) {
        var b = this;
        b.editModel.controller = b;
        //b.editModel.onDataUpdate = b.dataUpdateAction.createDelegate(b);
        b.editModel.dataObj = b.dataMgr.getDataObject();
        if (b.dataSourceData.length != 'undefined') {
            for (var c = 0; c < b.dataSourceData.length; c++) {
                var d = b.dataSourceData[c];
                var a = d.views || d.viewList;
                if (a) {
                    b.editModel.dataObj.addViewList(d.id, a)
                }
            }
        }
    }
    ,loadEditModelAndCreateUI: function() {
        var a = this;
        var b = new Ext.LoadMask(Ext.get(a.portletId + "bullet_chart_edit").parent(".portlet-content", false));
        b.show();
        Ext.Ajax.request({url: a.resourceUrls.getEditModelUrl,success: function(c) {
                var d = Ext.decode(c.responseText);
                if (d) {
                    a.uiProvider.editModel = new Endeca.Portlets.BulletChartPortlet.EditModel(d);
                    a.uiProvider.createEditUI().render(a.portletId + "bullet_chart_edit");
                    a.uiProvider.mediator.asyncObject = a.asyncObject;
                    a.uiProvider.mediator.lastSemanticKey = a.uiProvider.editModel.defaultCollectionKey
                } else {
                    a.utils.displayErrorMessage(a.utils.localize("summarizationbar.edit.label.failed-to-get-edit-model"))
                }
                b.hide()
            },failure: function(c) {
                a.utils.displayErrorMessage(a.utils.localize("summarizationbar.edit.label.failed-to-get-edit-model"));
                b.hide()
            }})
    },
    initAggMethodsResource: function () {
        var a = this;
        a.editModel.AGGRE_METHODS = {
        	// Only 
            //SUM: a.resources.getMessage("chart.edit.config.aggType.sum"),
            AVG: a.resources.getMessage("chart.edit.config.aggType.avg"),
            MEDIAN: a.resources.getMessage("chart.edit.config.aggType.median"),
            //MIN: a.resources.getMessage("chart.edit.config.aggType.min"),
            // MAX: a.resources.getMessage("chart.edit.config.aggType.max"),
            //COUNT_1: a.resources.getMessage("chart.edit.config.aggType.count_1"),
            //COUNT: a.resources.getMessage("chart.edit.config.aggType.count"),
            //COUNTDISTINCT: a.resources.getMessage("chart.edit.config.aggType.countdistinct"),
            VARIANCE: a.resources.getMessage("chart.edit.config.aggType.variance"),
            STDDEV: a.resources.getMessage("chart.edit.config.aggType.stddev")
        }
    },
    initExitListener: function () {
        var a = this;
        //  Ext.select("a.portlet-icon-back").on("click", a.handleExit, a)
    },
    syncPreviewPanelSize: function () {
        var c = this;
        var e = c.editModel.getChartStyle();
        var b = 400;
        var d = this.getPreviewWidth();
        if (e.comSize) {
            b = e.comSize.height
        }
        this.lastPreviewHeight = b;
        this.lastPreviewWidth = d;
        var a = Math.floor((890 - d) / 2);
        Ext.getCmp(c.portletId + "-previewPanel").setHeight(b);
        Ext.getCmp(c.portletId + "-previewPanel-west").setWidth(a);
        Ext.getCmp(c.portletId + "-previewPanel-east").setWidth(a);
        Ext.getCmp(c.portletId + "-previewPanel").doLayout()
    },
    synchronizeAllViewData2Model: function () {
        var a = this;
        var b = Ext.getCmp(a.portletId + "topTabPanel");
        b.items.each(function (i, e, h) {
            var f = i.id;
            var d = i.key;
            var g = i.findById(f + "StepTabPanel");
            if (g) {
                var c = g.getComponent(a.editModel.STEP_CHART_STYLE);
                if (c.items && c.items.getCount() > 0) {
                    a.synchronizeChartViewData2Model(f, d)
                }
            }
        })
    },
    localize: function (c, b) {
        var a = this;
        if (!a.resources) {
            return c
        }
        if (!b) {
            return a.resources.getMessage(c)
        } else {
            return a.resources.getMessage(c, b)
        }
    },
    createViewBannerItems: function (e, d) {
        var a = this,
            i = [],
            c = a.editModel.getCurrentChartConfig(),
            f = c.semanticKeys;
        if (f.length > 1) {
            var h = Ext.getCmp(d + e + "MultiViewsInfo");
            if (h) {
                h.show()
            }
            var b = new Ext.BoxComponent({
                xtype: "label",
                cls: "eid-multi-views-banner-item",
                html: a.localize("chart.edit.multi-view-select-info-label")
            });
            i.push(b);
            for (var g = 0; g < f.length; g++) {
                b = new Ext.Container({
                    cls: "eid-multi-views-banner-item",
                    items: [{
                        xtype: "label",
                        cls: "eid-views-select-info",
                        text: a.editModel.getViewDisplayName(f[g])
                    }]
                });
                if (e === a.editModel.STEP_DATA) {
                    if (g !== f.length - 1) {
                        b.add({
                            xtype: "label",
                            text: a.editModel.separator
                        })
                    }
                } else {
                    if ((e === a.editModel.STEP_CHART) || (e === a.editModel.STEP_SELECT_TYPE) || (e === a.editModel.STEP_CHART_STYLE)) {
                        b.add({
                            xtype: "box",
                            cls: "eid-action-icon eid-icon-remove",
                            viewKey: f[g],
                            autoEl: {
                                html: Ext.util.Format.htmlEncode(Ext.util.Format.htmlEncode(a.localize("chart.edit.remove-multi-view-qtip"))),
                                "ext:qtip": a.localize("chart.edit.remove-multi-view-qtip")
                            },
                            listeners: {
                                afterrender: function (j) {
                                    j.el.on("click", function (k) {
                                        a.showRemoveViewWarningWin(d, j.viewKey)
                                    })
                                }
                            }
                        })
                    }
                }
                i.push(b)
            }
        } else {
            if (f.length === 1) {
                var h = Ext.getCmp(d + e + "MultiViewsInfo");
                if (h) {
                    h.hide()
                }
            }
        }
        return i
    },
    getChartEditMask: function () {
        var a = this;
        if (!a.chartEidtMask) {
            var c = new Ext.LoadMask(Ext.get(a.portletId + "EditPanel"));
            // var b = new Ext.LoadMask(Ext.get(a.portletId + "-buttons"));
            // c.show = c.show.createSequence(b.show.createDelegate(b));
            //c.hide = c.hide.createSequence(b.hide.createDelegate(b));
            a.chartEidtMask = c;
            // a.chartButtonsMask = b
        }
        return a.chartEidtMask
    },
    refine: function (dimValue, propertyName) {
        var jsController = this;

        //Current dimension name will be mapped to the depth based on the preferences and the nav state
        //If the value is an empty string, we will remove this refinement
        var params = {
            dimensionValue: dimValue,
            propertyName: propertyName
        }

        /* post the new navigation refinement to the server */
        Ext.Ajax.request({
            url: jsController.refineUrl,
            params: params,
            loadMask: true,
            success: function (response) {
                var result = Ext.decode(response.responseText);

                var trigger = Ext.decode(result.asyncTrigger);

                //When we refine, we have special behavior on scatter plot portlets, they won't refresh from a portlet refresh but with JS Code instead to avoid flickering
                //Everything else will refresh normally
                for (var i in Endeca.Async.instances) {
                    if (Endeca.Async.instances[i].portletId.indexOf("BulletChartPortlet") !== -1 && Endeca.Async.instances[i].currentPortletMode == "view") {
                        Endeca.Async.instances[i].currentPortletMode = "refreshDisabled"; //Putting in edit mode will skip the refresh on this portlet
                    }
                }

                for (var i = 0; i < trigger.length; i++) {
                    Endeca.Async.triggerEvent(trigger[i]);
                }

                //Put back these portlets in view mode
                for (var i in Endeca.Async.instances) {
                    if (Endeca.Async.instances[i].portletId.indexOf("BulletChartPortlet") !== -1 && Endeca.Async.instances[i].currentPortletMode == "refreshDisabled") {
                        Endeca.Async.instances[i].currentPortletMode = "view";
                    }
                }
            },
            failure: function (response) {
                jsController.displayErrorMessage(response.responseText);
            }
        });
    },

    /**
     * Initialize the Edit component hierarchy, and render it to the div with id=(portletId + 'Edit')
     */
    initializeEdit: function () {
        /*var jsController = this;

        jsController.initDataManager();
        //jsController.initEditModel();
        jsController.editModel.dataSourceData = jsController.dataSourceData;
        //jsController.med = new Endeca.Portlets.ChartEditPagePortlet.Mediator(jsController);
        jsController.initAggMethodsResource();
        jsController.initExitListener();
        if (jsController.containerPanel) {
            jsController.containerPanel.destroy()
        }

        var f = jsController.getChartConfigs("Chart View");
        var c = jsController.portletId + f[0].key;
        jsController.containerPanel = new Ext.Container({
            id: jsController.portletId + "containerPanel",
            renderTo: jsController.portletId + "EditPanel",
            cls: "edit-panel-wrapper",
            defaultListenerScope: true,
            border: false,
            items: [jsController.createTabPanel(c), {
                xtype: "button",
                text: jsController.localize("df.button-save-preferences"),
                cls: "eid-btn-action",
                handler: function () {
                    jsController.postEditModel()
                }
            }]
        });*/

var a = this;
        if (!a.uiProvider) {
            a.uiProvider = new Endeca.Portlets.BulletChartPortlet.UIProvider(a.portletId, false, {resources: a.resources,resourceUrls: a.resourceUrls,formatterPanelResource: a.formatterPanelResource})
        }
        if (!a.utils) {
           // a.utils = new Endeca.Portlets.BulletChartPortlet.Utils(a.portletId, a.resources)
        }
        a.uiProvider.dataSourceData = a.dataSourceData;
        a.uiProvider.isRevert = false;
        a.uiProvider.conditionalStyleMap = a.conditionalStyleMap;
        a.initAggMethodsResource();
        a.loadEditModelAndCreateUI();

        //jsController.containerPanel.doLayout();
    },
    getViewDataStore: function () {
        var jsController = this;
        var data = [];
        var endecaServerConnection = Ext.isEmpty(jsController.editModel.defaultDataSourceId) ? "default" : jsController.editModel.defaultDataSourceId;
        jsController.editModel.dataObj = jsController.dataMgr.getDataObject();
        var datasources = jsController.editModel.dataObj.getViewList(endecaServerConnection);

        jQuery.each(datasources, function (key, value) {
            data.push([value.key, value.displayName, value.description]);
        });

        return data;

    },
    createTabPanel: function (c) {
        var currentTab = 0;
        var jsController = this;
        var tabs = new Ext.Container({
            id: jsController.portletId + "inner-containerPanel",
            cls: "inner-edit-panel-wrapper",
            defaultListenerScope: true,
            border: false,
            items: [
                    
{
    
	xtype : 'box',
	autoEl: {
		tag : 'p',
		html : "Data Source Settings" 
	}},
                    
                    {
                xtype: 'combo',
                id: jsController.portletId + 'ds-selector',
                forceSelection: true,
                allowBlank: false,
                fieldLabel: 'Endeca Server Connection: ',
                hiddenName: 'Endeca Server Connection',
                triggerAction: 'all',
                mode: 'local',
                displayField: 'displayName',
                valueField: 'key',
                cls: "data-selection-panel",
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'key',
                        'displayName',
                        'description'
                    ],
                    data: jsController.getViewDataStore()
                })
            }]
        });
        return tabs
    },
    setTabNavState: function (tabPanel, tab) {
        //this.editModel.activeTab = tab.getItemId() != 'undefined' ? tab.getItemId() : 0;

        if (tab.getItemId() != 'undefined') {
            if (tab.getItemId() == 'data')
                this.editModel.activeTab = 0;
            //tabPanel.setActiveTab(0);
            if (tab.getItemId() == 'selectChartType')
                this.editModel.activeTab = 1;
            //tabPanel.setActiveTab(1);
            if (tab.getItemId() == 'chart')
                this.editModel.activeTab = 2;
            //tabPanel.setActiveTab(2);
            if (tab.getItemId() == 'chartStyle')
                this.editModel.activeTab = 3;
            //tabPanel.setActiveTab(3);
        } else
            this.editModel.activeTab = 0;

        _tab = this.editModel.activeTab
    },
    getTabNavState: function () {
        return this.editModel.activeTab || 0;
    },
    showConfigPanel: function (d, e, f) {
        var a = this;
        var g = Ext.getCmp(d + "StepTabPanel");
        var b;
        // var c = a.editModel.getCurrentChartConfig();
        //var h = a.getValidViews();
        //if (h.length !== c.semanticKeys.length) {
        //    a.editModel.syncEditModelWhenViewChanged(h)
        // }
        if (e === a.editModel.STEP_AUTO) {
            e = a.editModel.STEP_DATA
        }
        b = g.getComponent(e);
        var i;
        if (b.items.getCount() === 0) {
            if (e === a.editModel.STEP_SELECT_TYPE) {
                //i = a.createSelectTypePanel(d, e);
                //g.remove(1);
                //g.insert(1, i)
                a.editModel.activeTab = 1;
            } else {
                if (e === a.editModel.STEP_CHART) {
                    // i = a.createChartConfigPanel(d, e);
                    //g.remove(2);
                    //g.insert(2, i)
                    a.editModel.activeTab = 2;
                } else {
                    if (e === a.editModel.STEP_CHART_STYLE) {
                        //i = a.createChartStylePanel(d, e);
                        // g.remove(3);
                        //g.insert(3, i)
                        a.editModel.activeTab = 3;
                    } else {

                        /*var ds = new Ext.Panel({
                    		 id: a.portletId + "dsStepTabPanel",
                             cls: "dsstep-tabPanel",
                             width: "99%",
                             plain: true,
                           // renderTo: Ext.getCmp(d + "StepTabPanel"),
                            title: '3 Ways to render HTML inside of a ExtJS container',
                            items: [{
                                html: "<a href='#'>1. HTML property of a panel</a>",
                                xtype: "panel"},
                            {
                                xtype: "panel",
                                html: new Ext.XTemplate("<a href='#'>{value}").apply({
                                    value: '2. HTML property of a panel generated by an XTemplate'
                                })}]
                        });*/
                        a.editModel.activeTab = 0;
                        g.setActiveTab(0);
                        //i = a.createDataSelectionPanel(d, e);
                        //g.remove(0);
                        //g.insert(0, i);
                    }
                }
            }
            g.setActiveTab(e)
        } else if (e === a.editModel.STEP_DATA) {
            // i = a.createChartConfigPanel(d, e);
            //g.remove(2);
            g.setActiveTab(0);
        }
        delete a.step
    },
    createDataSelectionPanel: function (c, d) {
        var jsController = this;

        /* var states = new Ext.data.Store({
            fields: ['abbr', 'name'],
            data: [{
                "abbr": "AL",
                "name": "Alabama"
            }, {
                "abbr": "AK",
                "name": "Alaska"
            }, {
                "abbr": "AZ",
                "name": "Arizona"
            }
            //...
            ]
        });*/
        // Create the combo box, attached to the states data store
        var combo = new Ext.form.ComboBox({
            fieldLabel: 'Choose State',
            store: new Ext.data.ArrayStore({
                id: 0,
                fields: [
                    'abbr',
                    'name'
                ],
                data: [
                    [1, 'item1'],
                    [2, 'item2']
                ]
            }),
            queryMode: 'local',
            mode: 'local',
            displayField: 'name',
            valueField: 'abbr',
            id: jsController.portletId + "dataPerspectivePanel",
            cls: "data-selection-panel",
            tabCls: "ext-numbered-tab-1",
            renderTo: jsController.portletId + "StepTabPanel"
        });

        return combo;

    },
    getChartConfigs: function (a) {
        var b = this;
        if (!b.configs) {
            b.configs = new Array();
            var c = b.createConfigurationModel("Chart View");
            b.configs.push(c);
            b.currentIndex = 0
        }
        return b.configs
    },
    createConfigurationModel: function (d) {
        var e = this;
        var b = new Date().getTime();
        var a = b;
        var c = Ext.isEmpty(this.editModel.defaultDataSourceId) ? "default" : this.editModel.defaultDataSourceId;
        e.editModel.dataObj = e.dataMgr.getDataObject();
        var g = e.editModel.dataObj.getViewList(c)[0].key;
        var f = {
            key: b,
            displayName: a,
            dataSourceId: c,
            semanticKeys: ["Base"],
            maxGroupNum: 1000,
            maxGroupRemark: ""
        };
        return f
    },
    getChartConfigs: function (a) {
        var b = this;
        if (!b.configs) {
            b.configs = new Array();
            var c = b.createConfigurationModel(a);
            b.configs.push(c);
            b.currentIndex = 0
        }
        return b.configs
    },
    /**
     * Post the editModel to the server
     */
    postEditModel: function () {
        var jsController = this;

        /* forceElementEqlQuery */
        var forceElementEqlQuery = Ext.getCmp('forceElementEqlQuery' + this.portletId).getValue();
        if (!Ext.isEmpty(forceElementEqlQuery)) {
            jsController.editModel.forceElementEqlQuery = forceElementEqlQuery;
        }

        /* capabilityEqlQuery */
        var capabilityEqlQuery = Ext.getCmp('capabilityEqlQuery' + this.portletId).getValue();
        if (!Ext.isEmpty(capabilityEqlQuery)) {
            jsController.editModel.capabilityEqlQuery = capabilityEqlQuery;
        }

        /* post the new edit model to the server */
        Ext.Ajax.request({
            url: jsController.resourceUrls['updateEditModel'],
            method: 'POST',
            jsonData: jsController.editModel,
            success: function (response) {
                jsController.displaySuccessMessage(jsController.resources.getMessage('success-settings-saved'));
            },
            failure: function (response) {
                if (Ext.isEmpty(response) || Ext.isEmpty(response.responseText)) {
                    jsController.displayErrorMessage(jsController.resources.getMessage('error-server-unavailable'));
                } else {
                    jsController.displayErrorMessage(response.responseText);
                }
            }
        });

    },

    /**
     * Display a message in a success div (green), hiding all other messages
     */
    displaySuccessMessage: function (message) {
        var jsController = this;
        Ext.fly(jsController.portletId + 'Success').update(message);
        Ext.fly(jsController.portletId + 'Success').setVisible(true, true);
        Ext.fly(jsController.portletId + 'Warning').setDisplayed(false);
        Ext.fly(jsController.portletId + 'Error').setDisplayed(false);
        this.delayHideMsg(5000)
    },

    /**
     * Display a message in a warning div (yellow), hiding all other messages
     */
    displayWarnMessage: function (message) {
        var jsController = this;
        Ext.fly(jsController.portletId + 'Success').setDisplayed(false);
        Ext.fly(jsController.portletId + 'Warning').update(message);
        Ext.fly(jsController.portletId + 'Warning').setVisible(true, true);
        Ext.fly(jsController.portletId + 'Error').setDisplayed(false);
        this.delayHideMsg(5000)
    },

    /**
     * Display a message in an error div (red), hiding all other messages
     */
    displayErrorMessage: function (message) {
        var jsController = this;
        Ext.fly(jsController.portletId + 'Success').setDisplayed(false);
        Ext.fly(jsController.portletId + 'Warning').setDisplayed(false);
        Ext.fly(jsController.portletId + 'Error').update(message);
        Ext.fly(jsController.portletId + 'Error').setVisible(true, true);
        this.delayHideMsg(5000)
    },

    /**
     * Hide all message divs
     */
    hideMessages: function () {
        var jsController = this;
        Ext.fly(jsController.portletId + 'Success').setDisplayed(false);
        Ext.fly(jsController.portletId + 'Warning').setDisplayed(false);
        Ext.fly(jsController.portletId + 'Error').setDisplayed(false);
    },
    delayHideMsg : function(c, a) {
		var b = this;
		if (typeof a === "undefined") {
			a = true
		}
		if (a) {
			if (b.delayTaskOfHideMsg) {
				clearTimeout(b.delayTaskOfHideMsg)
			}
			b.delayTaskOfHideMsg = b.hideMessages.defer(c, b)
		}
	},

    //Alter the SVG to be more suitable for a PNG export
    prepareExport: function (jsController) {
        jq14('#' + jsController.portletId + 'ViewPanel .altered-container').remove();
        jq14('#canvas_' + jsController.portletId).remove();

        var exporter = new VisualizationExporter(jsController.portletId + 'ViewPanel');

        jq14('#' + jsController.portletId + 'ViewPanel').append('<div class="altered-container" style="display:none;">' + jq14('#' + jsController.portletId + 'ViewPanel .heb-container div').html() + '</div>');

        d3.selectAll('#' + jsController.portletId + 'ViewPanel .altered-container path.link')
            .style("fill", "none")
            .style("stroke", "#1f77b4");

        d3.selectAll('#' + jsController.portletId + 'ViewPanel .altered-container g.source-highlight')
            .style("fill", "#1f77b4");

        d3.selectAll('#' + jsController.portletId + 'ViewPanel .altered-container g.target-highlight')
            .style("fill", "#1f77b4");

        exporter.loadImage(jq14('#' + jsController.portletId + 'ViewPanel .altered-container').html());
    }

};