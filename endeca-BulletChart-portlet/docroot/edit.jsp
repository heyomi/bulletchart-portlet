<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://endeca.com/discovery" prefix="edisc"%>
<portlet:defineObjects /><%-- establish the renderRequest, renderResponse, and portletConfig for use in the JSP --%>

<%-- We can't assume that liferay-display.xml imported our JavaScript successfully - call import-javascript-dependencies as a backup --%>
<%@ include file="import-edit-js-dependencies.jspf" %>

<edisc:getStaticProperty var="getEditModel" className="com.realdecoy.portlet.BulletChart.BulletChartConstants" property="RESOURCE_GET_EDIT_MODEL" />
<edisc:getStaticProperty var="postEditModelName" className="com.realdecoy.portlet.BulletChart.BulletChartConstants" property="RESOURCE_SAVE_PREFS"/>
<edisc:getStaticProperty var="revertEditViewName" className="com.realdecoy.portlet.BulletChart.BulletChartConstants" property="ACTION_REVERT_PREFS"/>
<edisc:getStaticProperty var="getSummarizations" className="com.realdecoy.portlet.BulletChart.BulletChartConstants" property="RESOURCE_GET_SUMMARIZATIONS"/>
<edisc:getStaticProperty var="refinementActionName" className="com.realdecoy.portlet.BulletChart.BulletChartConstants" property="ACTION_REFINEMENT"/>
<edisc:getStaticProperty var="hyperlinkActionName" className="com.realdecoy.portlet.BulletChart.BulletChartConstants" property="ACTION_HYPERLINK"/>

<edisc:getStaticProperty var="getViewData" className="com.endeca.portlet.util.Constants" property="RESOURCE_GET_SEMANTIC_VIEW_DATA" />
<edisc:getStaticProperty var="formattingPreview" className="com.endeca.portlet.util.Constants" property="RESOURCE_FORMATTING_PREVIEW" />
<edisc:getStaticProperty var="formattingGetCurrencyList" className="com.endeca.portlet.util.Constants" property="RESOURCE_FORMATTING_GET_CURRENCY_LIST" />

                             


<%-- We can't assume that liferay-display.xml imported our JavaScript successfully - call import-javascript-dependencies as a backup 
<%@ include file="import-javascript-dependencies.jspf" %> --%>

<%-- prepare divs - 3 types of messaging divs, and one for rendering of the edit panel --%>
<div id="<portlet:namespace />Error" class="portlet-msg-error" style="display:none"></div>
<div id="<portlet:namespace />Warning" class="portlet-msg-alert" style="display:none"></div>
<div id="<portlet:namespace />Success" class="portlet-msg-success" style="display:none"></div>
<div id="<portlet:namespace />Info" class="portlet-msg-info" style="display:none"></div>
<div id="<portlet:namespace />bullet_chart_edit" class="endeca-bullet-chart-portlet-edit"></div>

<script type="text/javascript">

	<%-- instantiate the javascript object  --%>
	var e<portlet:namespace/>BulletChartPortlet = new Endeca.Portlets.BulletChartPortlet.Controller('<portlet:namespace/>');

	<%-- load the resource strings and urls --%>  
	e<portlet:namespace/>BulletChartPortlet.resources = <edisc:initJSLanguageUtils/>;
	e<portlet:namespace/>BulletChartPortlet.resourceUrls = {
			<%-- getCfgListUrl: '${getCfgList}',  --%> 
			getViewDataUrl : '<portlet:resourceURL escapeXml="false" id="${getViewData}"/>',
			updateEditModel: '<portlet:resourceURL escapeXml="false" id="updateEditModel"/>',
			getEditModelUrl : '<portlet:resourceURL id="${getEditModel}" />',
			postEditModel: '<portlet:resourceURL escapeXml="false" id="${postEditModelName}" />',
			revertEditView : '<portlet:actionURL name="${revertEditViewName}" windowState="EXCLUSIVE"/>',
			getViewDataUrl : '<portlet:resourceURL escapeXml="false" id="${getViewData}"/>',
			getSummarizationsUrl: '<portlet:resourceURL escapeXml="false" id="${getSummarizations}"/>',
			formatterPreviewUrl : '<portlet:resourceURL id="${formattingPreview}" />',
			formatterGetCurrencyListUrl : '<portlet:resourceURL id="${formattingGetCurrencyList}" />',
			refinementActionUrl : '<portlet:actionURL name="${refinementActionName}" windowState="EXCLUSIVE"/>',
			hyperlinkActionUrl : '<portlet:actionURL name="${hyperlinkActionName}" windowState="EXCLUSIVE"/>'
	};

	<%-- load the editModel: passing state from JSP to JS, adding behavior via JavaScript constructor --%>
	<c:if test="${not empty editModel}">
		e<portlet:namespace/>BulletChartPortlet.editModel = new Endeca.Portlets.BulletChartPortlet.EditModel(${editModel},"${defaultDataSourceId}");
		<%-- e<portlet:namespace/>BulletChartPortlet.dataSourceData = ${existingDataSourceMap}; --%>
		e<portlet:namespace/>BulletChartPortlet.dataSourceData = ${dataSourceData};
		<%-- e<portlet:namespace/>BulletChartPortlet.editModel.groupId = ${groupId}; --%>
	</c:if>

	<%-- tell Ext to call initializeEdit() when the portlet is ready --%>
	Ext.onReady(function() {
		e<portlet:namespace/>BulletChartPortlet.initializeEdit();
	});
</script>
<input id="_bulletchartid" type="hidden" value="<portlet:namespace/>"/>

