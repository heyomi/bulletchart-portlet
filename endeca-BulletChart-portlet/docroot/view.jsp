<%--
/**
 * view.jsp
 *
 * Server-side view template for the "view" mode, with the following responsibilities:
 *
 * 1.) ensure that javascript dependencies are imported
 * 2.) instantiate the Portlet JavaScript Object
 * 3.) pass data from the server to the Portlet JavaScript Object, including:
 *     - the portlet id (via <portlet:namespace/>)
 *     - localized string resources (via <edisc:initJSLanguageUtils/>)
 *     - resource URLs
 *     - the view model
 * 4.) provide two divs for ExtJS code to render - one for the root panel, one for error messages
 * 5.) invoke .initializeView() on the Portlet JavaScript Object - transferring control to JavaScript
 *
--%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%-- TODO REVIEW remove temptation of c/fmt/fn? --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://endeca.com/discovery" prefix="edisc"%>
<portlet:defineObjects /><%-- establish the renderRequest, renderResponse, and portletConfig for use in the JSP --%>

<%-- We can't assume that liferay-display.xml imported our JavaScript successfully - call import-javascript-dependencies as a backup --%>
<%@ include file="import-javascript-dependencies.jspf" %>

<edisc:getStaticProperty var="refineActionName" className="com.endeca.portlet.BulletChart.BulletChartPortlet" property="REFINE_ACTION_NAME"/>
<portlet:actionURL var="refineActionUrl" name="${refineActionName}"/>
<edisc:getStaticProperty var="getBullets" className="com.realdecoy.portlet.BulletChart.BulletChartConstants" property="RESOURCE_GET_BULLETS"/>

<%-- prepare divs - 3 types of messaging divs, and one for rendering of the view panel --%>
<div id="<portlet:namespace />Error" class="portlet-msg-error" style="display:none"></div>
<div id="<portlet:namespace />Warning" class="portlet-msg-alert" style="display:none"></div>
<div id="<portlet:namespace />Success" class="portlet-msg-success" style="display:none"></div>
<div id="<portlet:namespace />ViewPanel" class="endecaBulletChartPortletView" style="position:relative;">
	<div id="bulletChart-container"></div>
</div>

<script type="text/javascript">
	
	<%-- instantiate our portlet javascript object --%>
	var e<portlet:namespace/>BulletChartPortletJs = new Endeca.Portlets.BulletChartPortlet.Controller('<portlet:namespace/>');
	
	<%-- load the resource strings and urls --%>  
	e<portlet:namespace/>BulletChartPortletJs.resources = <edisc:initJSLanguageUtils/>;
	e<portlet:namespace/>BulletChartPortletJs.resourceUrls = {
			recordDataStore: '<portlet:resourceURL escapeXml="false" id="recordDataStore"/>',
			getBulletsUrl: '<portlet:resourceURL escapeXml="false" id="${getBullets}"/>'
	};

	<%-- load the viewModel: passing it from JSP to JS, adding behavior via JavaScript constructor --%> 
	<c:if test="${not empty viewModel}">
		e<portlet:namespace/>BulletChartPortletJs.viewModel = new Endeca.Portlets.BulletChartPortlet.ViewModel(${viewModel});
	</c:if>
	
	e<portlet:namespace/>BulletChartPortletJs.refineUrl = "<c:out value="${refineActionUrl}" escapeXml="false" />";

	<%-- tell Ext to call initializeView() when the portlet is ready --%>
	Ext.onReady(function() {
		e<portlet:namespace/>BulletChartPortletJs.initializeView();
	});
</script>


