/*  1:   */ package com.endeca.portlet.summarizationbar;
/*  2:   */ 
/*  3:   */ import java.util.HashMap;
/*  4:   */ import java.util.Map;
/*  5:   */ 
/*  6:   */ public abstract interface SummarizationBarConstants
/*  7:   */ {
/*  8:   */   public static final String RESOURCE_SAVE_PREFS = "savePrefs";
/*  9:   */   public static final String RESOURCE_GET_SUMMARIZATIONS = "getSummarizations";
/* 10:   */   public static final String RESOURCE_UPDATE_SHOW_CONFIG_MESSAGE = "updateShowConfigMessage";
/* 11:   */   public static final String RESOURCE_GET_EDIT_MODEL = "getEditModel";
/* 12:   */   public static final String ACTION_REVERT_PREFS = "resetPrefs";
/* 13:   */   public static final String ACTION_REFINEMENT = "refinement";
/* 14:   */   public static final String ACTION_HYPERLINK = "hyperlink";
/* 15:   */   public static final String PARAM_EDIT_MODEL = "editModel";
/* 16:   */   public static final String PARAM_IS_UPDATE_PREVIEW = "isUpdatePreview";
/* 17:   */   public static final String PARAM_SUMMARIZATION_ID = "summarizationId";
/* 18:   */   public static final String PARAM_HYPER_LINK_URL = "hyperLinkURL";
/* 19:   */   public static final String PARAM_DIMENSION_KEYS = "dimensionKeys";
/* 20:   */   public static final String PARAM_DIMENSION_VALUES = "dimensionValues";
/* 21:   */   public static final String PREFS_EDIT_MODEL = "prefsEditModel";
/* 22:   */   public static final String PREFS_SHOW_CONFIG_MESSAGE = "showConfigMessage";
/* 23:   */   public static final String DEFAULT_USED_STYLE_GROUP_ID = "icon_stoplight_sprite";
/* 24:   */   public static final String DEFAULT_ALERT_COLOR = "red";
/* 25:   */   public static final String SESSION_IS_REVERT_ACTION = "isRevert";
/* 26:   */   public static final String REQUEST_ATTRIBUTE_IS_REVERT_ACTION = "isRevert";
/* 27:   */   public static final String REQUEST_ATTRIBUTE_SHOW_CONFIG_MESSAGE = "showConfigMessage";
/* 28:   */   public static final String REQUEST_ATTRIBUTE_EDIT_MODEL = "editModel";
/* 29:   */   public static final String REQUEST_ATTRIBUTE_VIEW_MODEL = "viewModel";
/* 30:   */   public static final String REQUEST_ATTRIBUTE_CONDITIONAL_STYLE = "conditionalStyle";
/* 31:   */   public static final String REQUEST_ATTRIBUTE_DATA_SOURCE_DATA = "dataSourceData";
/* 32:   */   public static final String REQUEST_ATTRIBUTE_CUSTOM_ACTION_JSON_RESPONSE = "customActionJsonResponse";
/* 33:   */   public static final String COUNT_ONE_KEY = "system-metrics-count_1";
/* 34:   */   public static final String COUNT_ONE = "COUNT_1";
/* 35:   */   public static final String STATEMENT_SUMMARIZATION = "_$SUMMARIZATION_";
/* 36:   */   public static final String STATEMENT_SUMMARIZATION_BASE = "_$SUMMARIZATION_BASE_";
/* 37:   */   public static final String SELECT_METRIC = "_$METRIC_";
/* 38:   */   public static final String SELECT_DATEPART = "_&DATEPART_";
/* 39:   */   public static final String GROUP_BY_ALIAS = "_$GROUP_BY_ALIAS_";
/* 40:   */   public static final String SELECT_CONDITION_VALUE = "_$CONDITION_VALUE";
/* 41:   */   public static final String SELECT_CONDITION_TARTET_VALUE = "_$CONDITION_TARTET_VALUE";
/* 42:62 */   public static final Map<String, String> CONDITIONAL_STYLE_MAP = new HashMap() {};
/* 43:   */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.SummarizationBarConstants
 * JD-Core Version:    0.7.0.1
 */