/* 1:  */ package com.endeca.portlet.summarizationbar;
/* 2:  */ 
/* 3:  */ public class SummarizationBarEnum
/* 4:  */ {
/* 5:  */   public static enum SummarizationType
/* 6:  */   {
/* 7:4 */     MetricSingle,  MetricRatio,  MetricPartToWhole,  DimensionSingle,  DimensionRatio,  Alert;
/* 8:  */     
/* 9:  */     private SummarizationType() {}
/* ::  */   }
/* ;:  */   
/* <:  */   public static enum ConditionType
/* =:  */   {
/* >:5 */     SelfValue,  ToAnotherMetric,  PartToWhole;
/* ?:  */     
/* @:  */     private ConditionType() {}
/* A:  */   }
/* B:  */   
/* C:  */   public static enum ConditionOperator
/* D:  */   {
/* E:6 */     GT,  GTEQ,  EQ,  BTWN,  LTEQ,  LT;
/* F:  */     
/* G:  */     private ConditionOperator() {}
/* H:  */   }
/* I:  */   
/* J:  */   public static enum NamePositionType
/* K:  */   {
/* L:7 */     BelowValue,  AboveValue;
/* M:  */     
/* N:  */     private NamePositionType() {}
/* O:  */   }
/* P:  */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.SummarizationBarEnum
 * JD-Core Version:    0.7.0.1
 */