/*  1:   */ package com.endeca.portlet.summarizationbar.model.edit;
/*  2:   */ 
/*  3:   */ import com.endeca.portlet.summarizationbar.SummarizationBarObjectMapper;
/*  4:   */ import java.util.ArrayList;
/*  5:   */ import java.util.List;
/*  6:   */ import org.codehaus.jackson.annotate.JsonIgnore;
/*  7:   */ import org.codehaus.jackson.annotate.JsonIgnoreProperties;
/*  8:   */ 
/*  9:   */ @JsonIgnoreProperties(ignoreUnknown=true)
/* 10:   */ public class EditModel
/* 11:   */ {
/* 12:   */   private GeneralSettingsModel settingsModel;
/* 13:   */   private String defaultCollectionKey;
/* 14:17 */   private List<SummarizationConfig> configs = new ArrayList();
/* 15:   */   
/* 16:   */   public GeneralSettingsModel getSettingsModel()
/* 17:   */   {
/* 18:23 */     if (this.settingsModel == null) {
/* 19:24 */       this.settingsModel = new GeneralSettingsModel();
/* 20:   */     }
/* 21:27 */     return this.settingsModel;
/* 22:   */   }
/* 23:   */   
/* 24:   */   public void setSettingsModel(GeneralSettingsModel settingsModel)
/* 25:   */   {
/* 26:34 */     this.settingsModel = settingsModel;
/* 27:   */   }
/* 28:   */   
/* 29:   */   public List<SummarizationConfig> getConfigs()
/* 30:   */   {
/* 31:42 */     return this.configs;
/* 32:   */   }
/* 33:   */   
/* 34:   */   public void setConfigs(List<SummarizationConfig> configs)
/* 35:   */   {
/* 36:49 */     this.configs = configs;
/* 37:   */   }
/* 38:   */   
/* 39:   */   public String toString()
/* 40:   */   {
/* 41:56 */     return SummarizationBarObjectMapper.toString(this);
/* 42:   */   }
/* 43:   */   
/* 44:   */   public SummarizationConfig getConfig(String id)
/* 45:   */   {
/* 46:60 */     for (SummarizationConfig cfg : this.configs) {
/* 47:61 */       if (cfg.getId().equals(id)) {
/* 48:62 */         return cfg;
/* 49:   */       }
/* 50:   */     }
/* 51:65 */     return null;
/* 52:   */   }
/* 53:   */   
/* 54:   */   @JsonIgnore
/* 55:   */   public void addConfig(SummarizationConfig config)
/* 56:   */   {
/* 57:70 */     this.configs.add(config);
/* 58:   */   }
/* 59:   */   
/* 60:   */   public String getDefaultCollectionKey()
/* 61:   */   {
/* 62:77 */     return this.defaultCollectionKey;
/* 63:   */   }
/* 64:   */   
/* 65:   */   public void setDefaultCollectionKey(String defaultCollectionKey)
/* 66:   */   {
/* 67:84 */     this.defaultCollectionKey = defaultCollectionKey;
/* 68:   */   }
/* 69:   */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.edit.EditModel
 * JD-Core Version:    0.7.0.1
 */