 package com.endeca.portlet.summarizationbar.model.edit;
 
 import com.endeca.portal.format.Formatter;
 import com.endeca.portlet.summarizationbar.SummarizationBarEnum.ConditionOperator;
 import com.endeca.portlet.summarizationbar.SummarizationBarEnum.ConditionType;
 import com.endeca.portlet.summarizationbar.model.view.Summarization;
 import org.apache.commons.lang.StringUtils;
 import org.codehaus.jackson.annotate.JsonIgnore;
 
/*  10:    */ public class ConditionEntity
/*  11:    */ {
/*  12:    */   private String id;
/*  13:    */   private String styleId;
/*  14:    */   private SummarizationBarEnum.ConditionOperator operator;
/*  15:    */   private String value1;
/*  16:    */   private String value2;
/*  17:    */   private String tooltip;
/*  18:    */   public static final String CONDITIONID_PREFIX = "condition_";
/*  19:    */   
/*  20:    */   public String getId()
/*  21:    */   {
/*  22: 26 */     return this.id;
/*  23:    */   }
/*  24:    */   
/*  25:    */   public void setId(String id)
/*  26:    */   {
/*  27: 33 */     this.id = id;
/*  28:    */   }
/*  29:    */   
/*  30:    */   public String getStyleId()
/*  31:    */   {
/*  32: 40 */     return this.styleId;
/*  33:    */   }
/*  34:    */   
/*  35:    */   public void setStyleId(String styleId)
/*  36:    */   {
/*  37: 47 */     this.styleId = styleId;
/*  38:    */   }
/*  39:    */   
/*  40:    */   public SummarizationBarEnum.ConditionOperator getOperator()
/*  41:    */   {
/*  42: 54 */     return this.operator;
/*  43:    */   }
/*  44:    */   
/*  45:    */   public void setOperator(SummarizationBarEnum.ConditionOperator operator)
/*  46:    */   {
/*  47: 61 */     this.operator = operator;
/*  48:    */   }
/*  49:    */   
/*  50:    */   public String getValue1()
/*  51:    */   {
/*  52: 68 */     return this.value1;
/*  53:    */   }
/*  54:    */   
/*  55:    */   public void setValue1(String value1)
/*  56:    */   {
/*  57: 75 */     this.value1 = value1;
/*  58:    */   }
/*  59:    */   
/*  60:    */   public String getValue2()
/*  61:    */   {
/*  62: 82 */     return this.value2;
/*  63:    */   }
/*  64:    */   
/*  65:    */   public void setValue2(String value2)
/*  66:    */   {
/*  67: 89 */     this.value2 = value2;
/*  68:    */   }
/*  69:    */   
/*  70:    */   public String getTooltip()
/*  71:    */   {
/*  72: 96 */     return this.tooltip;
/*  73:    */   }
/*  74:    */   
/*  75:    */   public void setTooltip(String tooltip)
/*  76:    */   {
/*  77:103 */     this.tooltip = tooltip;
/*  78:    */   }
/*  79:    */   
/*  80:    */   @JsonIgnore
/*  81:    */   public boolean isValid(SummarizationBarEnum.ConditionType type, String metricKey, String targetMetricKey)
/*  82:    */   {
/*  83:110 */     switch (1.$SwitchMap$com$endeca$portlet$summarizationbar$SummarizationBarEnum$ConditionType[type.ordinal()])
/*  84:    */     {
/*  85:    */     case 1: 
/*  86:112 */       if (StringUtils.isBlank(targetMetricKey)) {
/*  87:113 */         return false;
/*  88:    */       }
/*  89:    */     case 2: 
/*  90:    */     case 3: 
/*  91:117 */       if (StringUtils.isBlank(metricKey)) {
/*  92:118 */         return false;
/*  93:    */       }
/*  94:    */       break;
/*  95:    */     }
/*  96:123 */     if ((SummarizationBarEnum.ConditionOperator.EQ.equals(getOperator())) || (SummarizationBarEnum.ConditionOperator.GT.equals(getOperator())) || (SummarizationBarEnum.ConditionOperator.GTEQ.equals(getOperator()))) {
/*  97:125 */       return !StringUtils.isEmpty(getValue1());
/*  98:    */     }
/*  99:127 */     if (SummarizationBarEnum.ConditionOperator.BTWN.equals(getOperator()))
/* 100:    */     {
/* 101:129 */       if ((StringUtils.isEmpty(getValue1())) || (StringUtils.isEmpty(getValue2()))) {
/* 102:130 */         return false;
/* 103:    */       }
/* 104:132 */       double v1 = Double.parseDouble(getValue1());
/* 105:133 */       double v2 = Double.parseDouble(getValue2());
/* 106:134 */       return v1 <= v2;
/* 107:    */     }
/* 108:136 */     if ((SummarizationBarEnum.ConditionOperator.LT.equals(getOperator())) || (SummarizationBarEnum.ConditionOperator.LTEQ.equals(getOperator()))) {
/* 109:137 */       return !StringUtils.isEmpty(getValue2());
/* 110:    */     }
/* 111:140 */     return false;
/* 112:    */   }
/* 113:    */   
/* 114:    */   public boolean checkMeetsCondition(Summarization summarization, ConditionsConfig conditionsConfig, double target)
/* 115:    */   {
/* 116:146 */     if (SummarizationBarEnum.ConditionOperator.EQ.equals(getOperator())) {
/* 117:147 */       return (target == Double.parseDouble(getValue1())) || (compareEqualsCondition(summarization, conditionsConfig, target));
/* 118:    */     }
/* 119:149 */     if (SummarizationBarEnum.ConditionOperator.BTWN.equals(getOperator()))
/* 120:    */     {
/* 121:150 */       double v1 = Double.parseDouble(getValue1());
/* 122:151 */       double v2 = Double.parseDouble(getValue2());
/* 123:152 */       return ((target >= v1) && (target <= v2)) || (compareEqualsCondition(summarization, conditionsConfig, target));
/* 124:    */     }
/* 125:154 */     if (SummarizationBarEnum.ConditionOperator.GT.equals(getOperator())) {
/* 126:155 */       return (target > Double.parseDouble(getValue1())) && (!compareEqualsCondition(summarization, conditionsConfig, target));
/* 127:    */     }
/* 128:157 */     if (SummarizationBarEnum.ConditionOperator.GTEQ.equals(getOperator())) {
/* 129:158 */       return (target >= Double.parseDouble(getValue1())) || (compareEqualsCondition(summarization, conditionsConfig, target));
/* 130:    */     }
/* 131:160 */     if (SummarizationBarEnum.ConditionOperator.LT.equals(getOperator())) {
/* 132:161 */       return (target < Double.parseDouble(getValue2())) && (!compareEqualsCondition(summarization, conditionsConfig, target));
/* 133:    */     }
/* 134:163 */     if (SummarizationBarEnum.ConditionOperator.LTEQ.equals(getOperator())) {
/* 135:164 */       return (target <= Double.parseDouble(getValue2())) || (compareEqualsCondition(summarization, conditionsConfig, target));
/* 136:    */     }
/* 137:167 */     return false;
/* 138:    */   }
/* 139:    */   
/* 140:    */   private boolean compareEqualsCondition(Summarization summarization, ConditionsConfig conditionsConfig, double target)
/* 141:    */   {
/* 142:172 */     if (!conditionsConfig.getType().equals(SummarizationBarEnum.ConditionType.SelfValue)) {
/* 143:173 */       return false;
/* 144:    */     }
/* 145:175 */     Formatter formatter = summarization.getMetricFormatter();
/* 146:    */     try
/* 147:    */     {
/* 148:177 */       String targetFormattedValue = formatter.format(String.valueOf(target));
/* 149:178 */       String formattedValue1 = null;
/* 150:179 */       String formattedValue2 = null;
/* 151:180 */       if (StringUtils.isNotBlank(getValue1())) {
/* 152:181 */         formattedValue1 = formatter.format(getValue1());
/* 153:    */       }
/* 154:183 */       if (StringUtils.isNotBlank(getValue2())) {
/* 155:184 */         formattedValue2 = formatter.format(getValue2());
/* 156:    */       }
/* 157:186 */       return (targetFormattedValue.equals(formattedValue1)) || (targetFormattedValue.equals(formattedValue2));
/* 158:    */     }
/* 159:    */     catch (Exception e) {}
/* 160:188 */     return false;
/* 161:    */   }
/* 162:    */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.edit.ConditionEntity
 * JD-Core Version:    0.7.0.1
 */