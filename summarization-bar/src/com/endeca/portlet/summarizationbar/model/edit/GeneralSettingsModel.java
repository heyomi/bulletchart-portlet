/*  1:   */ package com.endeca.portlet.summarizationbar.model.edit;
/*  2:   */ 
/*  3:   */ import com.endeca.portlet.summarizationbar.SummarizationBarEnum.NamePositionType;
/*  4:   */ import org.codehaus.jackson.annotate.JsonIgnoreProperties;
/*  5:   */ 
/*  6:   */ @JsonIgnoreProperties(ignoreUnknown=true)
/*  7:   */ public class GeneralSettingsModel
/*  8:   */ {
/*  9:10 */   private int valueTextSize = 28;
/* 10:11 */   private static int valueTextMaxSize = 48;
/* 11:12 */   private static int valueTextMinSize = 11;
/* 12:13 */   private static double valueTextSizeIncrement = 0.5D;
/* 13:16 */   private int labelTextSize = 12;
/* 14:17 */   private static int labelTextMaxSize = 48;
/* 15:18 */   private static int labelTextMinSize = 11;
/* 16:19 */   private static double labelTextSizeIncrement = 0.5D;
/* 17:21 */   private SummarizationBarEnum.NamePositionType namePosition = SummarizationBarEnum.NamePositionType.BelowValue;
/* 18:   */   
/* 19:   */   public int getValueTextSize()
/* 20:   */   {
/* 21:27 */     return this.valueTextSize;
/* 22:   */   }
/* 23:   */   
/* 24:   */   public void setValueTextSize(int valueTextSize)
/* 25:   */   {
/* 26:33 */     this.valueTextSize = valueTextSize;
/* 27:   */   }
/* 28:   */   
/* 29:   */   public int getLabelTextSize()
/* 30:   */   {
/* 31:39 */     return this.labelTextSize;
/* 32:   */   }
/* 33:   */   
/* 34:   */   public void setLabelTextSize(int labelTextSize)
/* 35:   */   {
/* 36:45 */     this.labelTextSize = labelTextSize;
/* 37:   */   }
/* 38:   */   
/* 39:   */   public SummarizationBarEnum.NamePositionType getNamePosition()
/* 40:   */   {
/* 41:51 */     return this.namePosition;
/* 42:   */   }
/* 43:   */   
/* 44:   */   public void setNamePosition(SummarizationBarEnum.NamePositionType namePosition)
/* 45:   */   {
/* 46:57 */     this.namePosition = namePosition;
/* 47:   */   }
/* 48:   */   
/* 49:   */   public int getValueTextMaxSize()
/* 50:   */   {
/* 51:62 */     return valueTextMaxSize;
/* 52:   */   }
/* 53:   */   
/* 54:   */   public int getValueTextMinSize()
/* 55:   */   {
/* 56:66 */     return valueTextMinSize;
/* 57:   */   }
/* 58:   */   
/* 59:   */   public double getValueTextSizeIncrement()
/* 60:   */   {
/* 61:69 */     return valueTextSizeIncrement;
/* 62:   */   }
/* 63:   */   
/* 64:   */   public int getLabelTextMaxSize()
/* 65:   */   {
/* 66:73 */     return labelTextMaxSize;
/* 67:   */   }
/* 68:   */   
/* 69:   */   public int getLabelTextMinSize()
/* 70:   */   {
/* 71:77 */     return labelTextMinSize;
/* 72:   */   }
/* 73:   */   
/* 74:   */   public double getLabelTextSizeIncrement()
/* 75:   */   {
/* 76:81 */     return labelTextSizeIncrement;
/* 77:   */   }
/* 78:   */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.edit.GeneralSettingsModel
 * JD-Core Version:    0.7.0.1
 */