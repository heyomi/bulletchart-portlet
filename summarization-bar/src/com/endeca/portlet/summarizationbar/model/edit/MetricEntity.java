/*  1:   */ package com.endeca.portlet.summarizationbar.model.edit;
/*  2:   */ 
/*  3:   */ import com.endeca.portal.lql.LQLBuilder.Function;
/*  4:   */ 
/*  5:   */ public class MetricEntity
/*  6:   */   extends DimensionEntity
/*  7:   */ {
/*  8:   */   private LQLBuilder.Function aggregation;
/*  9:   */   
/* 10:   */   public LQLBuilder.Function getAggregation()
/* 11:   */   {
/* 12:13 */     return this.aggregation;
/* 13:   */   }
/* 14:   */   
/* 15:   */   public void setAggregation(LQLBuilder.Function aggregation)
/* 16:   */   {
/* 17:20 */     this.aggregation = aggregation;
/* 18:   */   }
/* 19:   */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.edit.MetricEntity
 * JD-Core Version:    0.7.0.1
 */