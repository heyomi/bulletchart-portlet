/*  1:   */ package com.endeca.portlet.summarizationbar.model.edit;
/*  2:   */ 
/*  3:   */ import org.codehaus.jackson.annotate.JsonIgnore;
/*  4:   */ 
/*  5:   */ public class AlertDisplayColumnEntity
/*  6:   */ {
/*  7:   */   private String id;
/*  8: 8 */   private boolean isDisplay = true;
/*  9: 9 */   private boolean isCustomName = false;
/* 10:   */   private String autoGenName;
/* 11:   */   private String customName;
/* 12:   */   public static final String DIMENSIONS_COLUMN_ID = "dimensions";
/* 13:   */   
/* 14:   */   public String getId()
/* 15:   */   {
/* 16:19 */     return this.id;
/* 17:   */   }
/* 18:   */   
/* 19:   */   public void setId(String id)
/* 20:   */   {
/* 21:25 */     this.id = id;
/* 22:   */   }
/* 23:   */   
/* 24:   */   public boolean getIsDisplay()
/* 25:   */   {
/* 26:32 */     return this.isDisplay;
/* 27:   */   }
/* 28:   */   
/* 29:   */   public void setIsDisplay(boolean isDisplay)
/* 30:   */   {
/* 31:38 */     this.isDisplay = isDisplay;
/* 32:   */   }
/* 33:   */   
/* 34:   */   public boolean getIsCustomName()
/* 35:   */   {
/* 36:46 */     return this.isCustomName;
/* 37:   */   }
/* 38:   */   
/* 39:   */   public void setIsCustomName(boolean isCustomName)
/* 40:   */   {
/* 41:52 */     this.isCustomName = isCustomName;
/* 42:   */   }
/* 43:   */   
/* 44:   */   public String getAutoGenName()
/* 45:   */   {
/* 46:58 */     return this.autoGenName;
/* 47:   */   }
/* 48:   */   
/* 49:   */   public void setAutoGenName(String autoGenName)
/* 50:   */   {
/* 51:64 */     this.autoGenName = autoGenName;
/* 52:   */   }
/* 53:   */   
/* 54:   */   public String getCustomName()
/* 55:   */   {
/* 56:70 */     return this.customName;
/* 57:   */   }
/* 58:   */   
/* 59:   */   public void setCustomName(String customName)
/* 60:   */   {
/* 61:76 */     this.customName = customName;
/* 62:   */   }
/* 63:   */   
/* 64:   */   @JsonIgnore
/* 65:   */   public String getName()
/* 66:   */   {
/* 67:84 */     return getIsCustomName() ? this.customName : this.autoGenName;
/* 68:   */   }
/* 69:   */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.edit.AlertDisplayColumnEntity
 * JD-Core Version:    0.7.0.1
 */