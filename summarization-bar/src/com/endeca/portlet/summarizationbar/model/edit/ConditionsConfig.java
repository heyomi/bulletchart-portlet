 package com.endeca.portlet.summarizationbar.model.edit;
 
 import com.endeca.portal.lql.LQLBuilder.Function;
 import com.endeca.portlet.summarizationbar.SummarizationBarEnum.ConditionType;
 import java.util.ArrayList;
 import java.util.List;
 import org.codehaus.jackson.annotate.JsonIgnore;
 
 public class ConditionsConfig
/*  10:    */ {
/*  11:    */   private SummarizationBarEnum.ConditionType type;
/*  12:    */   private String usedStyleGroupId;
/*  13:    */   private boolean isMeetDispOnly;
/*  14:    */   private boolean isMetricTextBlack;
/*  15:    */   private String targetMetricKey;
/*  16:    */   private String targetMetricFormatter;
/*  17:    */   private LQLBuilder.Function targetMetricAggre;
/*  18:    */   private List<ConditionEntity> conditions;
/*  19:    */   private String styleImage;
/*  20:    */   
/*  21:    */   public SummarizationBarEnum.ConditionType getType()
/*  22:    */   {
/*  23: 27 */     return this.type;
/*  24:    */   }
/*  25:    */   
/*  26:    */   public void setType(SummarizationBarEnum.ConditionType type)
/*  27:    */   {
/*  28: 34 */     this.type = type;
/*  29:    */   }
/*  30:    */   
/*  31:    */   public String getUsedStyleGroupId()
/*  32:    */   {
/*  33: 41 */     return this.usedStyleGroupId;
/*  34:    */   }
/*  35:    */   
/*  36:    */   public void setUsedStyleGroupId(String usedStyleGroupId)
/*  37:    */   {
/*  38: 48 */     this.usedStyleGroupId = usedStyleGroupId;
/*  39:    */   }
/*  40:    */   
/*  41:    */   public boolean getIsMeetDispOnly()
/*  42:    */   {
/*  43: 55 */     return this.isMeetDispOnly;
/*  44:    */   }
/*  45:    */   
/*  46:    */   public void setIsMeetDispOnly(boolean isMeetDispOnly)
/*  47:    */   {
/*  48: 62 */     this.isMeetDispOnly = isMeetDispOnly;
/*  49:    */   }
/*  50:    */   
/*  51:    */   public boolean getIsMetricTextBlack()
/*  52:    */   {
/*  53: 69 */     return this.isMetricTextBlack;
/*  54:    */   }
/*  55:    */   
/*  56:    */   public void setIsMetricTextBlack(boolean isMetricTextBlack)
/*  57:    */   {
/*  58: 76 */     this.isMetricTextBlack = isMetricTextBlack;
/*  59:    */   }
/*  60:    */   
/*  61:    */   public String getTargetMetricKey()
/*  62:    */   {
/*  63: 83 */     return this.targetMetricKey;
/*  64:    */   }
/*  65:    */   
/*  66:    */   public void setTargetMetricKey(String targetMetricKey)
/*  67:    */   {
/*  68: 90 */     this.targetMetricKey = targetMetricKey;
/*  69:    */   }
/*  70:    */   
/*  71:    */   public LQLBuilder.Function getTargetMetricAggre()
/*  72:    */   {
/*  73: 97 */     return this.targetMetricAggre;
/*  74:    */   }
/*  75:    */   
/*  76:    */   public void setTargetMetricAggre(LQLBuilder.Function targetMetricAggre)
/*  77:    */   {
/*  78:104 */     this.targetMetricAggre = targetMetricAggre;
/*  79:    */   }
/*  80:    */   
/*  81:    */   public List<ConditionEntity> getConditions()
/*  82:    */   {
/*  83:111 */     return this.conditions;
/*  84:    */   }
/*  85:    */   
/*  86:    */   public void setConditions(List<ConditionEntity> conditions)
/*  87:    */   {
/*  88:118 */     this.conditions = conditions;
/*  89:    */   }
/*  90:    */   
/*  91:    */   public String getStyleImage()
/*  92:    */   {
/*  93:125 */     return this.styleImage;
/*  94:    */   }
/*  95:    */   
/*  96:    */   public void setStyleImage(String styleImage)
/*  97:    */   {
/*  98:132 */     this.styleImage = styleImage;
/*  99:    */   }
/* 100:    */   
/* 101:    */   public String getTargetMetricFormatter()
/* 102:    */   {
/* 103:139 */     return this.targetMetricFormatter;
/* 104:    */   }
/* 105:    */   
/* 106:    */   public void setTargetMetricFormatter(String targetMetricFormatter)
/* 107:    */   {
/* 108:146 */     this.targetMetricFormatter = targetMetricFormatter;
/* 109:    */   }
/* 110:    */   
/* 111:    */   @JsonIgnore
/* 112:    */   public List<ConditionEntity> getValidConditions(String metricKey)
/* 113:    */   {
/* 114:151 */     List<ConditionEntity> result = new ArrayList();
/* 115:152 */     for (ConditionEntity condition : this.conditions) {
/* 116:153 */       if (condition.isValid(getType(), metricKey, getTargetMetricKey())) {
/* 117:154 */         result.add(condition);
/* 118:    */       }
/* 119:    */     }
/* 120:157 */     return result;
/* 121:    */   }
/* 122:    */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.edit.ConditionsConfig
 * JD-Core Version:    0.7.0.1
 */