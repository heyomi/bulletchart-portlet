 package com.endeca.portlet.summarizationbar.model.edit;
 
 import java.util.ArrayList;
 import java.util.List;
 import org.codehaus.jackson.annotate.JsonIgnoreProperties;
 
 @JsonIgnoreProperties(ignoreUnknown=true)
 public class AlertConfig
 {
/*  10:    */   private String displayName;
/*  11:    */   private String color;
/*  12:    */   private String description;
/*  13: 15 */   private int maxNumber = 10;
/*  14: 17 */   private boolean isCustomWidth = false;
/*  15: 18 */   private int customWidth = 350;
/*  16:    */   private boolean isOtherPage;
/*  17:    */   private String targetPage;
/*  18: 23 */   private List<DimensionEntity> dimensionsList = new ArrayList();
/*  19: 24 */   private List<AlertConditionEntity> conditonsList = new ArrayList();
/*  20: 25 */   private List<AlertDisplayColumnEntity> displayColumnsList = new ArrayList();
/*  21:    */   
/*  22:    */   public String getDisplayName()
/*  23:    */   {
/*  24: 31 */     return this.displayName;
/*  25:    */   }
/*  26:    */   
/*  27:    */   public void setDisplayName(String displayName)
/*  28:    */   {
/*  29: 37 */     this.displayName = displayName;
/*  30:    */   }
/*  31:    */   
/*  32:    */   public String getDescription()
/*  33:    */   {
/*  34: 43 */     return this.description;
/*  35:    */   }
/*  36:    */   
/*  37:    */   public void setDescription(String description)
/*  38:    */   {
/*  39: 49 */     this.description = description;
/*  40:    */   }
/*  41:    */   
/*  42:    */   public String getColor()
/*  43:    */   {
/*  44: 55 */     return this.color;
/*  45:    */   }
/*  46:    */   
/*  47:    */   public void setColor(String color)
/*  48:    */   {
/*  49: 61 */     this.color = color;
/*  50:    */   }
/*  51:    */   
/*  52:    */   public boolean getIsCustomWidth()
/*  53:    */   {
/*  54: 67 */     return this.isCustomWidth;
/*  55:    */   }
/*  56:    */   
/*  57:    */   public void setIsCustomWidth(boolean isCustomWidth)
/*  58:    */   {
/*  59: 73 */     this.isCustomWidth = isCustomWidth;
/*  60:    */   }
/*  61:    */   
/*  62:    */   public int getCustomWidth()
/*  63:    */   {
/*  64: 79 */     return this.customWidth;
/*  65:    */   }
/*  66:    */   
/*  67:    */   public void setCustomWidth(int customWidth)
/*  68:    */   {
/*  69: 85 */     this.customWidth = customWidth;
/*  70:    */   }
/*  71:    */   
/*  72:    */   public boolean getIsOtherPage()
/*  73:    */   {
/*  74: 91 */     return this.isOtherPage;
/*  75:    */   }
/*  76:    */   
/*  77:    */   public void setIsOtherPage(boolean isOtherPage)
/*  78:    */   {
/*  79: 97 */     this.isOtherPage = isOtherPage;
/*  80:    */   }
/*  81:    */   
/*  82:    */   public String getTargetPage()
/*  83:    */   {
/*  84:103 */     return this.targetPage;
/*  85:    */   }
/*  86:    */   
/*  87:    */   public void setTargetPage(String targetPage)
/*  88:    */   {
/*  89:109 */     this.targetPage = targetPage;
/*  90:    */   }
/*  91:    */   
/*  92:    */   public int getMaxNumber()
/*  93:    */   {
/*  94:115 */     return this.maxNumber;
/*  95:    */   }
/*  96:    */   
/*  97:    */   public void setMaxNumber(int maxNumber)
/*  98:    */   {
/*  99:121 */     this.maxNumber = maxNumber;
/* 100:    */   }
/* 101:    */   
/* 102:    */   public List<DimensionEntity> getDimensionsList()
/* 103:    */   {
/* 104:127 */     return this.dimensionsList;
/* 105:    */   }
/* 106:    */   
/* 107:    */   public void setDimensionsList(List<DimensionEntity> dimensionsList)
/* 108:    */   {
/* 109:133 */     this.dimensionsList = dimensionsList;
/* 110:    */   }
/* 111:    */   
/* 112:    */   public List<AlertConditionEntity> getConditonsList()
/* 113:    */   {
/* 114:139 */     return this.conditonsList;
/* 115:    */   }
/* 116:    */   
/* 117:    */   public void setConditonsList(List<AlertConditionEntity> conditonsList)
/* 118:    */   {
/* 119:145 */     this.conditonsList = conditonsList;
/* 120:    */   }
/* 121:    */   
/* 122:    */   public List<AlertDisplayColumnEntity> getDisplayColumnsList()
/* 123:    */   {
/* 124:151 */     return this.displayColumnsList;
/* 125:    */   }
/* 126:    */   
/* 127:    */   public void setDisplayColumnsList(List<AlertDisplayColumnEntity> displayColumnsList)
/* 128:    */   {
/* 129:158 */     this.displayColumnsList = displayColumnsList;
/* 130:    */   }
/* 131:    */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.edit.AlertConfig
 * JD-Core Version:    0.7.0.1
 */