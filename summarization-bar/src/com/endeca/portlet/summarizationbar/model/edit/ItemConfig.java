 package com.endeca.portlet.summarizationbar.model.edit;
 
 import com.endeca.portal.actions.model.CommonActionModel;
 import com.endeca.portal.data.functions.util.DatePart;
 import com.endeca.portal.lql.LQLBuilder.Function;
 import java.util.List;
 import org.codehaus.jackson.annotate.JsonIgnoreProperties;
 
 @JsonIgnoreProperties(ignoreUnknown=true)
/*  10:    */ public class ItemConfig
/*  11:    */ {
/*  12:    */   private String metricKey;
/*  13:    */   private LQLBuilder.Function metricAggre;
/*  14:    */   private String metricFormatter;
/*  15:    */   private String targetMetricKey;
/*  16:    */   private LQLBuilder.Function targetMetricAggre;
/*  17:    */   private String targetMetricFormatter;
/*  18:    */   private String dimensionKey;
/*  19:    */   private String dimensionFormatter;
/*  20:    */   private List<DatePart> datePartCombo;
/*  21: 24 */   private boolean isTopValue = true;
/*  22: 25 */   private boolean isCustomDisplayName = true;
/*  23:    */   private String customDisplayName;
/*  24:    */   private boolean isCustomWidth;
/*  25:    */   private int customWidth;
/*  26:    */   private String description;
/*  27:    */   private CommonActionModel action;
/*  28:    */   
/*  29:    */   public void setAction(CommonActionModel action)
/*  30:    */   {
/*  31: 33 */     this.action = action;
/*  32:    */   }
/*  33:    */   
/*  34:    */   public String getMetricKey()
/*  35:    */   {
/*  36: 40 */     return this.metricKey;
/*  37:    */   }
/*  38:    */   
/*  39:    */   public void setMetricKey(String metricKey)
/*  40:    */   {
/*  41: 47 */     this.metricKey = metricKey;
/*  42:    */   }
/*  43:    */   
/*  44:    */   public LQLBuilder.Function getMetricAggre()
/*  45:    */   {
/*  46: 54 */     return this.metricAggre;
/*  47:    */   }
/*  48:    */   
/*  49:    */   public void setMetricAggre(LQLBuilder.Function metricAggre)
/*  50:    */   {
/*  51: 61 */     this.metricAggre = metricAggre;
/*  52:    */   }
/*  53:    */   
/*  54:    */   public String getMetricFormatter()
/*  55:    */   {
/*  56: 68 */     return this.metricFormatter;
/*  57:    */   }
/*  58:    */   
/*  59:    */   public void setMetricFormatter(String metricFormatter)
/*  60:    */   {
/*  61: 75 */     this.metricFormatter = metricFormatter;
/*  62:    */   }
/*  63:    */   
/*  64:    */   public String getTargetMetricKey()
/*  65:    */   {
/*  66: 82 */     return this.targetMetricKey;
/*  67:    */   }
/*  68:    */   
/*  69:    */   public void setTargetMetricKey(String targetMetricKey)
/*  70:    */   {
/*  71: 89 */     this.targetMetricKey = targetMetricKey;
/*  72:    */   }
/*  73:    */   
/*  74:    */   public LQLBuilder.Function getTargetMetricAggre()
/*  75:    */   {
/*  76: 96 */     return this.targetMetricAggre;
/*  77:    */   }
/*  78:    */   
/*  79:    */   public void setTargetMetricAggre(LQLBuilder.Function targetMetricAggre)
/*  80:    */   {
/*  81:103 */     this.targetMetricAggre = targetMetricAggre;
/*  82:    */   }
/*  83:    */   
/*  84:    */   public String getTargetMetricFormatter()
/*  85:    */   {
/*  86:110 */     return this.targetMetricFormatter;
/*  87:    */   }
/*  88:    */   
/*  89:    */   public void setTargetMetricFormatter(String targetMetricFormatter)
/*  90:    */   {
/*  91:117 */     this.targetMetricFormatter = targetMetricFormatter;
/*  92:    */   }
/*  93:    */   
/*  94:    */   public String getDimensionKey()
/*  95:    */   {
/*  96:124 */     return this.dimensionKey;
/*  97:    */   }
/*  98:    */   
/*  99:    */   public void setDimensionKey(String dimensionKey)
/* 100:    */   {
/* 101:131 */     this.dimensionKey = dimensionKey;
/* 102:    */   }
/* 103:    */   
/* 104:    */   public String getDimensionFormatter()
/* 105:    */   {
/* 106:138 */     return this.dimensionFormatter;
/* 107:    */   }
/* 108:    */   
/* 109:    */   public void setDimensionFormatter(String dimensionFormatter)
/* 110:    */   {
/* 111:145 */     this.dimensionFormatter = dimensionFormatter;
/* 112:    */   }
/* 113:    */   
/* 114:    */   public List<DatePart> getDatePartCombo()
/* 115:    */   {
/* 116:152 */     return this.datePartCombo;
/* 117:    */   }
/* 118:    */   
/* 119:    */   public void setDatePartCombo(List<DatePart> datePartCombo)
/* 120:    */   {
/* 121:159 */     this.datePartCombo = datePartCombo;
/* 122:    */   }
/* 123:    */   
/* 124:    */   public boolean getIsTopValue()
/* 125:    */   {
/* 126:166 */     return this.isTopValue;
/* 127:    */   }
/* 128:    */   
/* 129:    */   public void setIsTopValue(boolean isTopValue)
/* 130:    */   {
/* 131:173 */     this.isTopValue = isTopValue;
/* 132:    */   }
/* 133:    */   
/* 134:    */   public boolean getIsCustomDisplayName()
/* 135:    */   {
/* 136:180 */     return this.isCustomDisplayName;
/* 137:    */   }
/* 138:    */   
/* 139:    */   public void setIsCustomDisplayName(boolean isCustomDisplayName)
/* 140:    */   {
/* 141:187 */     this.isCustomDisplayName = isCustomDisplayName;
/* 142:    */   }
/* 143:    */   
/* 144:    */   public String getCustomDisplayName()
/* 145:    */   {
/* 146:194 */     return this.customDisplayName;
/* 147:    */   }
/* 148:    */   
/* 149:    */   public void setCustomDisplayName(String customDisplayName)
/* 150:    */   {
/* 151:201 */     this.customDisplayName = customDisplayName;
/* 152:    */   }
/* 153:    */   
/* 154:    */   public boolean getIsCustomWidth()
/* 155:    */   {
/* 156:208 */     return this.isCustomWidth;
/* 157:    */   }
/* 158:    */   
/* 159:    */   public void setIsCustomWidth(boolean isCustomWidth)
/* 160:    */   {
/* 161:215 */     this.isCustomWidth = isCustomWidth;
/* 162:    */   }
/* 163:    */   
/* 164:    */   public int getCustomWidth()
/* 165:    */   {
/* 166:222 */     return this.customWidth;
/* 167:    */   }
/* 168:    */   
/* 169:    */   public void setCustomWidth(int customWidth)
/* 170:    */   {
/* 171:229 */     this.customWidth = customWidth;
/* 172:    */   }
/* 173:    */   
/* 174:    */   public String getDescription()
/* 175:    */   {
/* 176:236 */     return this.description;
/* 177:    */   }
/* 178:    */   
/* 179:    */   public void setDescription(String description)
/* 180:    */   {
/* 181:243 */     this.description = description;
/* 182:    */   }
/* 183:    */   
/* 184:    */   public CommonActionModel getAction()
/* 185:    */   {
/* 186:247 */     return this.action;
/* 187:    */   }
/* 188:    */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.edit.ItemConfig
 * JD-Core Version:    0.7.0.1
 */