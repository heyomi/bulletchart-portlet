/*  1:   */ package com.endeca.portlet.summarizationbar.model.edit;
/*  2:   */ 
/*  3:   */ import com.endeca.portal.data.functions.util.DatePart;
/*  4:   */ import java.util.List;
/*  5:   */ 
/*  6:   */ public class DimensionEntity
/*  7:   */ {
/*  8:   */   private String key;
/*  9:   */   private String formatter;
/* 10:   */   private List<DatePart> datePartCombo;
/* 11:   */   
/* 12:   */   public String getKey()
/* 13:   */   {
/* 14:20 */     return this.key;
/* 15:   */   }
/* 16:   */   
/* 17:   */   public void setKey(String key)
/* 18:   */   {
/* 19:26 */     this.key = key;
/* 20:   */   }
/* 21:   */   
/* 22:   */   public String getFormatter()
/* 23:   */   {
/* 24:32 */     return this.formatter;
/* 25:   */   }
/* 26:   */   
/* 27:   */   public void setFormatter(String formatter)
/* 28:   */   {
/* 29:38 */     this.formatter = formatter;
/* 30:   */   }
/* 31:   */   
/* 32:   */   public List<DatePart> getDatePartCombo()
/* 33:   */   {
/* 34:45 */     return this.datePartCombo;
/* 35:   */   }
/* 36:   */   
/* 37:   */   public void setDatePartCombo(List<DatePart> datePartCombo)
/* 38:   */   {
/* 39:51 */     this.datePartCombo = datePartCombo;
/* 40:   */   }
/* 41:   */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.edit.DimensionEntity
 * JD-Core Version:    0.7.0.1
 */