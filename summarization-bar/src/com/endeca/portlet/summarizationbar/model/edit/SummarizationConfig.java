 package com.endeca.portlet.summarizationbar.model.edit;
 
 import com.endeca.portlet.summarizationbar.SummarizationBarEnum;
import com.endeca.portlet.summarizationbar.SummarizationBarEnum.SummarizationType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
 
 @JsonIgnoreProperties(ignoreUnknown=true)
 public class SummarizationConfig
 {
   private String id;
/*  10:    */   private String name;
/*  11:    */   private boolean isCompleted;
/*  12:    */   private String dataSourceId;
/*  13:    */   private String semanticKey;
/*  14:    */   private String semanticName;
/*  15:    */   private SummarizationBarEnum.SummarizationType type;
/*  16:    */   private ItemConfig itemConfig;
/*  17: 19 */   private boolean conditionEnabled = true;
/*  18:    */   private ConditionsConfig conditionsConfig;
/*  19:    */   private InfoConfig infoConfig;
/*  20:    */   private AlertConfig alertConfig;
/*  21: 25 */   private boolean isCustomerizedDesc = false;
/*  22:    */   
/*  23:    */   public String getId()
/*  24:    */   {
/*  25: 31 */     return this.id;
/*  26:    */   }
/*  27:    */   
/*  28:    */   public void setId(String id)
/*  29:    */   {
/*  30: 38 */     this.id = id;
/*  31:    */   }
/*  32:    */   
/*  33:    */   public String getName()
/*  34:    */   {
/*  35: 45 */     return this.name;
/*  36:    */   }
/*  37:    */   
/*  38:    */   public void setName(String name)
/*  39:    */   {
/*  40: 52 */     this.name = name;
/*  41:    */   }
/*  42:    */   
/*  43:    */   public boolean getIsCompleted()
/*  44:    */   {
/*  45: 59 */     return this.isCompleted;
/*  46:    */   }
/*  47:    */   
/*  48:    */   public void setIsCompleted(boolean isCompleted)
/*  49:    */   {
/*  50: 66 */     this.isCompleted = isCompleted;
/*  51:    */   }
/*  52:    */   
/*  53:    */   public String getDataSourceId()
/*  54:    */   {
/*  55: 73 */     return this.dataSourceId;
/*  56:    */   }
/*  57:    */   
/*  58:    */   public void setDataSourceId(String dataSourceId)
/*  59:    */   {
/*  60: 80 */     this.dataSourceId = dataSourceId;
/*  61:    */   }
/*  62:    */   
/*  63:    */   public String getSemanticKey()
/*  64:    */   {
/*  65: 87 */     return this.semanticKey;
/*  66:    */   }
/*  67:    */   
/*  68:    */   public void setSemanticKey(String semanticKey)
/*  69:    */   {
/*  70: 94 */     this.semanticKey = semanticKey;
/*  71:    */   }
/*  72:    */   
/*  73:    */   public String getSemanticName()
/*  74:    */   {
/*  75: 98 */     return this.semanticName;
/*  76:    */   }
/*  77:    */   
/*  78:    */   public void setSemanticName(String semanticName)
/*  79:    */   {
/*  80:102 */     this.semanticName = semanticName;
/*  81:    */   }
/*  82:    */   
/*  83:    */   public SummarizationBarEnum.SummarizationType getType()
/*  84:    */   {
/*  85:109 */     return this.type;
/*  86:    */   }
/*  87:    */   
/*  88:    */   public void setType(SummarizationBarEnum.SummarizationType type)
/*  89:    */   {
/*  90:116 */     this.type = type;
/*  91:    */   }
/*  92:    */   
/*  93:    */   public ItemConfig getItemConfig()
/*  94:    */   {
/*  95:123 */     return this.itemConfig;
/*  96:    */   }
/*  97:    */   
/*  98:    */   public void setItemConfig(ItemConfig itemConfig)
/*  99:    */   {
/* 100:130 */     this.itemConfig = itemConfig;
/* 101:    */   }
/* 102:    */   
/* 103:    */   public ConditionsConfig getConditionsConfig()
/* 104:    */   {
/* 105:137 */     return this.conditionsConfig;
/* 106:    */   }
/* 107:    */   
/* 108:    */   public void setConditionsConfig(ConditionsConfig conditionsConfig)
/* 109:    */   {
/* 110:144 */     this.conditionsConfig = conditionsConfig;
/* 111:    */   }
/* 112:    */   
/* 113:    */   public InfoConfig getInfoConfig()
/* 114:    */   {
/* 115:151 */     return this.infoConfig;
/* 116:    */   }
/* 117:    */   
/* 118:    */   public void setInfoConfig(InfoConfig infoConfig)
/* 119:    */   {
/* 120:158 */     this.infoConfig = infoConfig;
/* 121:    */   }
/* 122:    */   
/* 123:    */   public boolean getConditionEnabled()
/* 124:    */   {
/* 125:162 */     return this.conditionEnabled;
/* 126:    */   }
/* 127:    */   
/* 128:    */   public void setConditionEnabled(boolean conditionEnabled)
/* 129:    */   {
/* 130:166 */     this.conditionEnabled = conditionEnabled;
/* 131:    */   }
/* 132:    */   
/* 133:    */   public AlertConfig getAlertConfig()
/* 134:    */   {
/* 135:172 */     return this.alertConfig;
/* 136:    */   }
/* 137:    */   
/* 138:    */   public void setAlertConfig(AlertConfig alertConfig)
/* 139:    */   {
/* 140:178 */     this.alertConfig = alertConfig;
/* 141:    */   }
/* 142:    */   
/* 143:    */   public boolean getIsCustomerizedDesc()
/* 144:    */   {
/* 145:182 */     return this.isCustomerizedDesc;
/* 146:    */   }
/* 147:    */   
/* 148:    */   public void setIsCustomerizedDesc(boolean isCustomerizedDesc)
/* 149:    */   {
/* 150:186 */     this.isCustomerizedDesc = isCustomerizedDesc;
/* 151:    */   }
/* 152:    */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.edit.SummarizationConfig
 * JD-Core Version:    0.7.0.1
 */