/*  1:   */ package com.endeca.portlet.summarizationbar.model.edit;
/*  2:   */ 
/*  3:   */ import com.endeca.portlet.summarizationbar.SummarizationBarEnum.ConditionType;
/*  4:   */ import org.codehaus.jackson.annotate.JsonIgnore;
/*  5:   */ 
/*  6:   */ public class AlertConditionEntity
/*  7:   */   extends ConditionEntity
/*  8:   */ {
/*  9:   */   private SummarizationBarEnum.ConditionType type;
/* 10:   */   private MetricEntity metric;
/* 11:   */   private MetricEntity targetMetric;
/* 12:   */   private String ratioFormatter;
/* 13:   */   
/* 14:   */   public SummarizationBarEnum.ConditionType getType()
/* 15:   */   {
/* 16:18 */     return this.type;
/* 17:   */   }
/* 18:   */   
/* 19:   */   public void setType(SummarizationBarEnum.ConditionType type)
/* 20:   */   {
/* 21:24 */     this.type = type;
/* 22:   */   }
/* 23:   */   
/* 24:   */   public MetricEntity getMetric()
/* 25:   */   {
/* 26:30 */     return this.metric;
/* 27:   */   }
/* 28:   */   
/* 29:   */   public void setMetric(MetricEntity metric)
/* 30:   */   {
/* 31:36 */     this.metric = metric;
/* 32:   */   }
/* 33:   */   
/* 34:   */   public MetricEntity getTargetMetric()
/* 35:   */   {
/* 36:42 */     return this.targetMetric;
/* 37:   */   }
/* 38:   */   
/* 39:   */   public void setTargetMetric(MetricEntity targetMetric)
/* 40:   */   {
/* 41:48 */     this.targetMetric = targetMetric;
/* 42:   */   }
/* 43:   */   
/* 44:   */   public String getRatioFormatter()
/* 45:   */   {
/* 46:54 */     return this.ratioFormatter;
/* 47:   */   }
/* 48:   */   
/* 49:   */   public void setRatioFormatter(String ratioFormatter)
/* 50:   */   {
/* 51:60 */     this.ratioFormatter = ratioFormatter;
/* 52:   */   }
/* 53:   */   
/* 54:   */   @JsonIgnore
/* 55:   */   public boolean isValid(SummarizationBarEnum.ConditionType type, MetricEntity metric, MetricEntity targetMetric)
/* 56:   */   {
/* 57:64 */     String metricKey = "";String targetMetricKey = "";
/* 58:65 */     switch (1.$SwitchMap$com$endeca$portlet$summarizationbar$SummarizationBarEnum$ConditionType[type.ordinal()])
/* 59:   */     {
/* 60:   */     case 1: 
/* 61:67 */       if (targetMetric == null) {
/* 62:68 */         return false;
/* 63:   */       }
/* 64:70 */       targetMetricKey = targetMetric.getKey();
/* 65:   */     case 2: 
/* 66:   */     case 3: 
/* 67:74 */       if (metric == null) {
/* 68:75 */         return false;
/* 69:   */       }
/* 70:77 */       metricKey = metric.getKey();
/* 71:   */     }
/* 72:80 */     return super.isValid(type, metricKey, targetMetricKey);
/* 73:   */   }
/* 74:   */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.edit.AlertConditionEntity
 * JD-Core Version:    0.7.0.1
 */