 package com.endeca.portlet.summarizationbar.model.edit;
 
 public class InfoConfig
 {
   private boolean isDisplay;
   private boolean isCustomTitle;
   private String customTitle;
   private int maxNumber;
   private boolean isShowMore;
/*  10:    */   private String dimensionKey;
/*  11:    */   private boolean isTopValue;
/*  12:    */   private String formatter;
/*  13:    */   private String action;
/*  14:    */   
/*  15:    */   public boolean getIsDisplay()
/*  16:    */   {
/*  17: 19 */     return this.isDisplay;
/*  18:    */   }
/*  19:    */   
/*  20:    */   public void setIsDisplay(boolean isDisplay)
/*  21:    */   {
/*  22: 25 */     this.isDisplay = isDisplay;
/*  23:    */   }
/*  24:    */   
/*  25:    */   public boolean getIsCustomTitle()
/*  26:    */   {
/*  27: 31 */     return this.isCustomTitle;
/*  28:    */   }
/*  29:    */   
/*  30:    */   public void setIsCustomTitle(boolean isCustomTitle)
/*  31:    */   {
/*  32: 37 */     this.isCustomTitle = isCustomTitle;
/*  33:    */   }
/*  34:    */   
/*  35:    */   public String getCustomTitle()
/*  36:    */   {
/*  37: 43 */     return this.customTitle;
/*  38:    */   }
/*  39:    */   
/*  40:    */   public void setCustomTitle(String customTitle)
/*  41:    */   {
/*  42: 49 */     this.customTitle = customTitle;
/*  43:    */   }
/*  44:    */   
/*  45:    */   public int getMaxNumber()
/*  46:    */   {
/*  47: 55 */     return this.maxNumber;
/*  48:    */   }
/*  49:    */   
/*  50:    */   public void setMaxNumber(int maxNumber)
/*  51:    */   {
/*  52: 61 */     this.maxNumber = maxNumber;
/*  53:    */   }
/*  54:    */   
/*  55:    */   public boolean getIsShowMore()
/*  56:    */   {
/*  57: 67 */     return this.isShowMore;
/*  58:    */   }
/*  59:    */   
/*  60:    */   public void setIsShowMore(boolean isShowMore)
/*  61:    */   {
/*  62: 73 */     this.isShowMore = isShowMore;
/*  63:    */   }
/*  64:    */   
/*  65:    */   public String getDimensionKey()
/*  66:    */   {
/*  67: 79 */     return this.dimensionKey;
/*  68:    */   }
/*  69:    */   
/*  70:    */   public void setDimensionKey(String dimensionKey)
/*  71:    */   {
/*  72: 85 */     this.dimensionKey = dimensionKey;
/*  73:    */   }
/*  74:    */   
/*  75:    */   public boolean getIsTopValue()
/*  76:    */   {
/*  77: 91 */     return this.isTopValue;
/*  78:    */   }
/*  79:    */   
/*  80:    */   public void setIsTopValue(boolean isTopValue)
/*  81:    */   {
/*  82: 97 */     this.isTopValue = isTopValue;
/*  83:    */   }
/*  84:    */   
/*  85:    */   public String getFormatter()
/*  86:    */   {
/*  87:103 */     return this.formatter;
/*  88:    */   }
/*  89:    */   
/*  90:    */   public void setFormatter(String formatter)
/*  91:    */   {
/*  92:109 */     this.formatter = formatter;
/*  93:    */   }
/*  94:    */   
/*  95:    */   public String getAction()
/*  96:    */   {
/*  97:115 */     return this.action;
/*  98:    */   }
/*  99:    */   
/* 100:    */   public void setAction(String action)
/* 101:    */   {
/* 102:121 */     this.action = action;
/* 103:    */   }
/* 104:    */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.edit.InfoConfig
 * JD-Core Version:    0.7.0.1
 */