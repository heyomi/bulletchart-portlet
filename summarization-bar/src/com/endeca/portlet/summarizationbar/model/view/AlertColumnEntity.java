/*  1:   */ package com.endeca.portlet.summarizationbar.model.view;
/*  2:   */ 
/*  3:   */ public class AlertColumnEntity
/*  4:   */ {
/*  5:   */   private String id;
/*  6:   */   private String name;
/*  7:   */   
/*  8:   */   public String getId()
/*  9:   */   {
/* 10:15 */     return this.id;
/* 11:   */   }
/* 12:   */   
/* 13:   */   public void setId(String id)
/* 14:   */   {
/* 15:22 */     this.id = id;
/* 16:   */   }
/* 17:   */   
/* 18:   */   public String getName()
/* 19:   */   {
/* 20:29 */     return this.name;
/* 21:   */   }
/* 22:   */   
/* 23:   */   public void setName(String name)
/* 24:   */   {
/* 25:36 */     this.name = name;
/* 26:   */   }
/* 27:   */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.view.AlertColumnEntity
 * JD-Core Version:    0.7.0.1
 */