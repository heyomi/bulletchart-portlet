/*  1:   */ package com.endeca.portlet.summarizationbar.model.view;
/*  2:   */ 
/*  3:   */ import java.util.Set;
/*  4:   */ import org.codehaus.jackson.annotate.JsonProperty;
/*  5:   */ 
/*  6:   */ public class CustomActionResponse
/*  7:   */ {
/*  8:   */   private Set<String> events;
/*  9:   */   private String redirectUrl;
/* 10:   */   private String popupUrl;
/* 11:   */   
/* 12:   */   public CustomActionResponse() {}
/* 13:   */   
/* 14:   */   public CustomActionResponse(Set<String> events, String redirectUrl)
/* 15:   */   {
/* 16:15 */     this.events = events;
/* 17:16 */     this.redirectUrl = redirectUrl;
/* 18:   */   }
/* 19:   */   
/* 20:   */   @JsonProperty("triggeredEvents")
/* 21:   */   public Set<String> getEvents()
/* 22:   */   {
/* 23:21 */     return this.events;
/* 24:   */   }
/* 25:   */   
/* 26:   */   @JsonProperty("triggeredEvents")
/* 27:   */   public void setEvents(Set<String> events)
/* 28:   */   {
/* 29:26 */     this.events = events;
/* 30:   */   }
/* 31:   */   
/* 32:   */   public String getRedirectUrl()
/* 33:   */   {
/* 34:30 */     return this.redirectUrl;
/* 35:   */   }
/* 36:   */   
/* 37:   */   public void setRedirectUrl(String redirectUrl)
/* 38:   */   {
/* 39:34 */     this.redirectUrl = redirectUrl;
/* 40:   */   }
/* 41:   */   
/* 42:   */   public String getPopupUrl()
/* 43:   */   {
/* 44:38 */     return this.popupUrl;
/* 45:   */   }
/* 46:   */   
/* 47:   */   public void setPopupUrl(String popupUrl)
/* 48:   */   {
/* 49:42 */     this.popupUrl = popupUrl;
/* 50:   */   }
/* 51:   */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.view.CustomActionResponse
 * JD-Core Version:    0.7.0.1
 */