/*  1:   */ package com.endeca.portlet.summarizationbar.model.view;
/*  2:   */ 
/*  3:   */ import java.util.List;
/*  4:   */ 
/*  5:   */ public class SummarizationModel
/*  6:   */ {
/*  7:   */   private List<Summarization> summarizations;
/*  8:   */   private String message;
/*  9:   */   private String messageKey;
/* 10:   */   
/* 11:   */   public List<Summarization> getSummarizations()
/* 12:   */   {
/* 13:18 */     return this.summarizations;
/* 14:   */   }
/* 15:   */   
/* 16:   */   public void setSummarizations(List<Summarization> summarizations)
/* 17:   */   {
/* 18:25 */     this.summarizations = summarizations;
/* 19:   */   }
/* 20:   */   
/* 21:   */   public String getMessage()
/* 22:   */   {
/* 23:32 */     return this.message;
/* 24:   */   }
/* 25:   */   
/* 26:   */   public void setMessage(String message)
/* 27:   */   {
/* 28:39 */     this.message = message;
/* 29:   */   }
/* 30:   */   
/* 31:   */   public String getMessageKey()
/* 32:   */   {
/* 33:46 */     return this.messageKey;
/* 34:   */   }
/* 35:   */   
/* 36:   */   public void setMessageKey(String messageKey)
/* 37:   */   {
/* 38:53 */     this.messageKey = messageKey;
/* 39:   */   }
/* 40:   */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.view.SummarizationModel
 * JD-Core Version:    0.7.0.1
 */