 package com.endeca.portlet.summarizationbar.model.view;
 
 import com.endeca.portal.actions.model.CommonActionModel;
 import com.endeca.portal.format.Formatter;
 import com.endeca.portlet.summarizationbar.SummarizationBarEnum.SummarizationType;
 import java.util.List;
 
 public class Summarization
 {
/*  10:    */   private String id;
/*  11:    */   private String name;
/*  12:    */   private SummarizationBarEnum.SummarizationType type;
/*  13:    */   private String value;
/*  14:    */   private String formattedValue;
/*  15:    */   private String formattedMetricValue;
/*  16:    */   private String metricValue;
/*  17: 22 */   private int width = 350;
/*  18:    */   private String description;
/*  19:    */   private String styleGroupId;
/*  20:    */   private String styleId;
/*  21:    */   private Boolean isMetricTextBlack;
/*  22: 29 */   private boolean isDisplay = true;
/*  23:    */   private String conditionType;
/*  24:    */   private String conditionTooltip;
/*  25:    */   private String conditionTargetValue;
/*  26:    */   private String conditionValue;
/*  27:    */   private String infoTitle;
/*  28:    */   private List<InfoEntity> infoList;
/*  29:    */   private String alertColor;
/*  30:    */   private List<AlertColumnEntity> alertConditionColumns;
/*  31:    */   private List<AlertValueEntity> alertConditionValues;
/*  32:    */   private List<AlertColumnEntity> alertDimensionColumns;
/*  33:    */   private List<AlertValueEntity> alertDimensionValues;
/*  34:    */   private String dimensionKey;
/*  35:    */   private CommonActionModel action;
/*  36:    */   private Formatter metricFormatter;
/*  37:    */   
/*  38:    */   public Formatter getMetricFormatter()
/*  39:    */   {
/*  40: 56 */     return this.metricFormatter;
/*  41:    */   }
/*  42:    */   
/*  43:    */   public void setMetricFormatter(Formatter metricFormatter)
/*  44:    */   {
/*  45: 60 */     this.metricFormatter = metricFormatter;
/*  46:    */   }
/*  47:    */   
/*  48:    */   public CommonActionModel getAction()
/*  49:    */   {
/*  50: 64 */     return this.action;
/*  51:    */   }
/*  52:    */   
/*  53:    */   public void setAction(CommonActionModel action)
/*  54:    */   {
/*  55: 68 */     this.action = action;
/*  56:    */   }
/*  57:    */   
/*  58:    */   public String getDimensionKey()
/*  59:    */   {
/*  60: 75 */     return this.dimensionKey;
/*  61:    */   }
/*  62:    */   
/*  63:    */   public void setDimensionKey(String dimensionKey)
/*  64:    */   {
/*  65: 82 */     this.dimensionKey = dimensionKey;
/*  66:    */   }
/*  67:    */   
/*  68:    */   public String getId()
/*  69:    */   {
/*  70: 89 */     return this.id;
/*  71:    */   }
/*  72:    */   
/*  73:    */   public void setId(String id)
/*  74:    */   {
/*  75: 96 */     this.id = id;
/*  76:    */   }
/*  77:    */   
/*  78:    */   public String getName()
/*  79:    */   {
/*  80:103 */     return this.name;
/*  81:    */   }
/*  82:    */   
/*  83:    */   public void setName(String name)
/*  84:    */   {
/*  85:110 */     this.name = name;
/*  86:    */   }
/*  87:    */   
/*  88:    */   public SummarizationBarEnum.SummarizationType getType()
/*  89:    */   {
/*  90:117 */     return this.type;
/*  91:    */   }
/*  92:    */   
/*  93:    */   public void setType(SummarizationBarEnum.SummarizationType type)
/*  94:    */   {
/*  95:124 */     this.type = type;
/*  96:    */   }
/*  97:    */   
/*  98:    */   public String getValue()
/*  99:    */   {
/* 100:131 */     return this.value;
/* 101:    */   }
/* 102:    */   
/* 103:    */   public void setValue(String value)
/* 104:    */   {
/* 105:138 */     this.value = value;
/* 106:    */   }
/* 107:    */   
/* 108:    */   public String getFormattedValue()
/* 109:    */   {
/* 110:145 */     return this.formattedValue;
/* 111:    */   }
/* 112:    */   
/* 113:    */   public void setFormattedValue(String formattedValue)
/* 114:    */   {
/* 115:152 */     this.formattedValue = formattedValue;
/* 116:    */   }
/* 117:    */   
/* 118:    */   public String getMetricValue()
/* 119:    */   {
/* 120:159 */     return this.metricValue;
/* 121:    */   }
/* 122:    */   
/* 123:    */   public void setMetricValue(String metricValue)
/* 124:    */   {
/* 125:166 */     this.metricValue = metricValue;
/* 126:    */   }
/* 127:    */   
/* 128:    */   public int getWidth()
/* 129:    */   {
/* 130:173 */     return this.width;
/* 131:    */   }
/* 132:    */   
/* 133:    */   public void setWidth(int width)
/* 134:    */   {
/* 135:180 */     this.width = width;
/* 136:    */   }
/* 137:    */   
/* 138:    */   public String getDescription()
/* 139:    */   {
/* 140:187 */     return this.description;
/* 141:    */   }
/* 142:    */   
/* 143:    */   public void setDescription(String description)
/* 144:    */   {
/* 145:194 */     this.description = description;
/* 146:    */   }
/* 147:    */   
/* 148:    */   public String getStyleGroupId()
/* 149:    */   {
/* 150:201 */     return this.styleGroupId;
/* 151:    */   }
/* 152:    */   
/* 153:    */   public void setStyleGroupId(String styleGroupId)
/* 154:    */   {
/* 155:208 */     this.styleGroupId = styleGroupId;
/* 156:    */   }
/* 157:    */   
/* 158:    */   public String getStyleId()
/* 159:    */   {
/* 160:215 */     return this.styleId;
/* 161:    */   }
/* 162:    */   
/* 163:    */   public void setStyleId(String styleId)
/* 164:    */   {
/* 165:222 */     this.styleId = styleId;
/* 166:    */   }
/* 167:    */   
/* 168:    */   public Boolean getIsMetricTextBlack()
/* 169:    */   {
/* 170:229 */     return this.isMetricTextBlack;
/* 171:    */   }
/* 172:    */   
/* 173:    */   public void setIsMetricTextBlack(Boolean isMetricTextBlack)
/* 174:    */   {
/* 175:236 */     this.isMetricTextBlack = isMetricTextBlack;
/* 176:    */   }
/* 177:    */   
/* 178:    */   public boolean getIsDisplay()
/* 179:    */   {
/* 180:243 */     return this.isDisplay;
/* 181:    */   }
/* 182:    */   
/* 183:    */   public void setIsDisplay(boolean isDisplay)
/* 184:    */   {
/* 185:250 */     this.isDisplay = isDisplay;
/* 186:    */   }
/* 187:    */   
/* 188:    */   public String getConditionType()
/* 189:    */   {
/* 190:257 */     return this.conditionType;
/* 191:    */   }
/* 192:    */   
/* 193:    */   public void setConditionType(String conditionType)
/* 194:    */   {
/* 195:264 */     this.conditionType = conditionType;
/* 196:    */   }
/* 197:    */   
/* 198:    */   public String getConditionTooltip()
/* 199:    */   {
/* 200:271 */     return this.conditionTooltip;
/* 201:    */   }
/* 202:    */   
/* 203:    */   public void setConditionTooltip(String conditionTooltip)
/* 204:    */   {
/* 205:278 */     this.conditionTooltip = conditionTooltip;
/* 206:    */   }
/* 207:    */   
/* 208:    */   public String getConditionTargetValue()
/* 209:    */   {
/* 210:285 */     return this.conditionTargetValue;
/* 211:    */   }
/* 212:    */   
/* 213:    */   public void setConditionTargetValue(String conditionTargetValue)
/* 214:    */   {
/* 215:292 */     this.conditionTargetValue = conditionTargetValue;
/* 216:    */   }
/* 217:    */   
/* 218:    */   public String getConditionValue()
/* 219:    */   {
/* 220:299 */     return this.conditionValue;
/* 221:    */   }
/* 222:    */   
/* 223:    */   public void setConditionValue(String conditionValue)
/* 224:    */   {
/* 225:306 */     this.conditionValue = conditionValue;
/* 226:    */   }
/* 227:    */   
/* 228:    */   public String getInfoTitle()
/* 229:    */   {
/* 230:313 */     return this.infoTitle;
/* 231:    */   }
/* 232:    */   
/* 233:    */   public void setInfoTitle(String infoTitle)
/* 234:    */   {
/* 235:320 */     this.infoTitle = infoTitle;
/* 236:    */   }
/* 237:    */   
/* 238:    */   public List<InfoEntity> getInfoList()
/* 239:    */   {
/* 240:327 */     return this.infoList;
/* 241:    */   }
/* 242:    */   
/* 243:    */   public void setInfoList(List<InfoEntity> infoList)
/* 244:    */   {
/* 245:334 */     this.infoList = infoList;
/* 246:    */   }
/* 247:    */   
/* 248:    */   public String getAlertColor()
/* 249:    */   {
/* 250:341 */     return this.alertColor;
/* 251:    */   }
/* 252:    */   
/* 253:    */   public void setAlertColor(String alertColor)
/* 254:    */   {
/* 255:348 */     this.alertColor = alertColor;
/* 256:    */   }
/* 257:    */   
/* 258:    */   public List<AlertColumnEntity> getAlertConditionColumns()
/* 259:    */   {
/* 260:356 */     return this.alertConditionColumns;
/* 261:    */   }
/* 262:    */   
/* 263:    */   public void setAlertConditionColumns(List<AlertColumnEntity> alertConditionColumns)
/* 264:    */   {
/* 265:364 */     this.alertConditionColumns = alertConditionColumns;
/* 266:    */   }
/* 267:    */   
/* 268:    */   public List<AlertColumnEntity> getAlertDimensionColumns()
/* 269:    */   {
/* 270:373 */     return this.alertDimensionColumns;
/* 271:    */   }
/* 272:    */   
/* 273:    */   public void setAlertDimensionColumns(List<AlertColumnEntity> alertDimensionColumns)
/* 274:    */   {
/* 275:381 */     this.alertDimensionColumns = alertDimensionColumns;
/* 276:    */   }
/* 277:    */   
/* 278:    */   public List<AlertValueEntity> getAlertConditionValues()
/* 279:    */   {
/* 280:388 */     return this.alertConditionValues;
/* 281:    */   }
/* 282:    */   
/* 283:    */   public void setAlertConditionValues(List<AlertValueEntity> alertConditionValues)
/* 284:    */   {
/* 285:395 */     this.alertConditionValues = alertConditionValues;
/* 286:    */   }
/* 287:    */   
/* 288:    */   public List<AlertValueEntity> getAlertDimensionValues()
/* 289:    */   {
/* 290:403 */     return this.alertDimensionValues;
/* 291:    */   }
/* 292:    */   
/* 293:    */   public void setAlertDimensionValues(List<AlertValueEntity> alertDimensionValues)
/* 294:    */   {
/* 295:410 */     this.alertDimensionValues = alertDimensionValues;
/* 296:    */   }
/* 297:    */   
/* 298:    */   public String getFormattedMetricValue()
/* 299:    */   {
/* 300:417 */     return this.formattedMetricValue;
/* 301:    */   }
/* 302:    */   
/* 303:    */   public void setFormattedMetricValue(String formattedMetricValue)
/* 304:    */   {
/* 305:424 */     this.formattedMetricValue = formattedMetricValue;
/* 306:    */   }
/* 307:    */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.view.Summarization
 * JD-Core Version:    0.7.0.1
 */