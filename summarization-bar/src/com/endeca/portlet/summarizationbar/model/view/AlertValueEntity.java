/*  1:   */ package com.endeca.portlet.summarizationbar.model.view;
/*  2:   */ 
/*  3:   */ import java.util.List;
/*  4:   */ 
/*  5:   */ public class AlertValueEntity
/*  6:   */ {
/*  7:   */   private List<String> ids;
/*  8:   */   private List<String> attributeKeys;
/*  9:   */   private List<String> values;
/* 10:   */   private List<String> formattedValues;
/* 11:   */   
/* 12:   */   public List<String> getIds()
/* 13:   */   {
/* 14:20 */     return this.ids;
/* 15:   */   }
/* 16:   */   
/* 17:   */   public void setIds(List<String> ids)
/* 18:   */   {
/* 19:27 */     this.ids = ids;
/* 20:   */   }
/* 21:   */   
/* 22:   */   public List<String> getAttributeKeys()
/* 23:   */   {
/* 24:34 */     return this.attributeKeys;
/* 25:   */   }
/* 26:   */   
/* 27:   */   public void setAttributeKeys(List<String> attributeKeys)
/* 28:   */   {
/* 29:41 */     this.attributeKeys = attributeKeys;
/* 30:   */   }
/* 31:   */   
/* 32:   */   public List<String> getValues()
/* 33:   */   {
/* 34:48 */     return this.values;
/* 35:   */   }
/* 36:   */   
/* 37:   */   public void setValues(List<String> values)
/* 38:   */   {
/* 39:55 */     this.values = values;
/* 40:   */   }
/* 41:   */   
/* 42:   */   public List<String> getFormattedValues()
/* 43:   */   {
/* 44:62 */     return this.formattedValues;
/* 45:   */   }
/* 46:   */   
/* 47:   */   public void setFormattedValues(List<String> formattedValues)
/* 48:   */   {
/* 49:69 */     this.formattedValues = formattedValues;
/* 50:   */   }
/* 51:   */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.view.AlertValueEntity
 * JD-Core Version:    0.7.0.1
 */