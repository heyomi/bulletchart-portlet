/*  1:   */ package com.endeca.portlet.summarizationbar.model.view;
/*  2:   */ 
/*  3:   */ import com.endeca.portlet.summarizationbar.SummarizationBarObjectMapper;
/*  4:   */ import com.endeca.portlet.summarizationbar.model.edit.GeneralSettingsModel;
/*  5:   */ 
/*  6:   */ public class ViewModel
/*  7:   */ {
/*  8:   */   private GeneralSettingsModel settingsModel;
/*  9:   */   
/* 10:   */   public GeneralSettingsModel getSettingsModel()
/* 11:   */   {
/* 12:15 */     return this.settingsModel;
/* 13:   */   }
/* 14:   */   
/* 15:   */   public void setSettingsModel(GeneralSettingsModel settingsModel)
/* 16:   */   {
/* 17:23 */     this.settingsModel = settingsModel;
/* 18:   */   }
/* 19:   */   
/* 20:   */   public String toString()
/* 21:   */   {
/* 22:31 */     return SummarizationBarObjectMapper.toString(this);
/* 23:   */   }
/* 24:   */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.view.ViewModel
 * JD-Core Version:    0.7.0.1
 */