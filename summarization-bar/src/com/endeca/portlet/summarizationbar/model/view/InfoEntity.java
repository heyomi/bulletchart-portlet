/*  1:   */ package com.endeca.portlet.summarizationbar.model.view;
/*  2:   */ 
/*  3:   */ public class InfoEntity
/*  4:   */ {
/*  5:   */   private String dimensionSpec;
/*  6:   */   private String dimensionValue;
/*  7:   */   private String aggregatedValue;
/*  8:   */   
/*  9:   */   public String getDimensionSpec()
/* 10:   */   {
/* 11:12 */     return this.dimensionSpec;
/* 12:   */   }
/* 13:   */   
/* 14:   */   public void setDimensionSpec(String dimensionSpec)
/* 15:   */   {
/* 16:18 */     this.dimensionSpec = dimensionSpec;
/* 17:   */   }
/* 18:   */   
/* 19:   */   public String getDimensionValue()
/* 20:   */   {
/* 21:24 */     return this.dimensionValue;
/* 22:   */   }
/* 23:   */   
/* 24:   */   public void setDimensionValue(String dimensionValue)
/* 25:   */   {
/* 26:30 */     this.dimensionValue = dimensionValue;
/* 27:   */   }
/* 28:   */   
/* 29:   */   public String getAggregatedValue()
/* 30:   */   {
/* 31:36 */     return this.aggregatedValue;
/* 32:   */   }
/* 33:   */   
/* 34:   */   public void setAggregatedValue(String aggregatedValue)
/* 35:   */   {
/* 36:42 */     this.aggregatedValue = aggregatedValue;
/* 37:   */   }
/* 38:   */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.model.view.InfoEntity
 * JD-Core Version:    0.7.0.1
 */