 package com.endeca.portlet.summarizationbar;
 
 import com.endeca.portal.actions.model.PassParameterActionModel;
 import com.endeca.portal.actions.model.RefinementActionModel;
 import com.endeca.portal.attributes.model.PropertyType;
 import com.endeca.portal.attributes.viewmodel.Attribute;
 import com.endeca.portal.attributes.viewmodel.PredefinedMetric;
 import com.endeca.portal.data.CollectionBaseView;
 import com.endeca.portal.data.DataSource;
/*  10:    */ import com.endeca.portal.data.DataSourceException;
/*  11:    */ import com.endeca.portal.data.KeyNotFoundException;
/*  12:    */ import com.endeca.portal.data.QueryState;
/*  13:    */ import com.endeca.portal.data.SemanticView;
/*  14:    */ import com.endeca.portal.data.functions.QueryFunction;
/*  15:    */ import com.endeca.portal.data.functions.RefinementFilter.MultiSelect;
/*  16:    */ import com.endeca.portal.data.functions.util.DateFilterUtil;
/*  17:    */ import com.endeca.portal.data.functions.util.DatePart;
/*  18:    */ import com.endeca.portal.data.functions.util.QueryFunctionUtils;
/*  19:    */ import com.endeca.portal.data.model.ModelException;
/*  20:    */ import com.endeca.portal.format.Formatter.FormatParseException;
/*  21:    */ import com.endeca.portal.lql.LQLBuilder.Function;
/*  22:    */ import com.endeca.portal.lql.LQLException;
/*  23:    */ import com.endeca.portlet.EndecaPortlet;
/*  24:    */ import com.endeca.portlet.ModelAndView;
/*  25:    */ import com.endeca.portlet.ProcessResource;
/*  26:    */ import com.endeca.portlet.summarizationbar.model.edit.AlertConditionEntity;
/*  27:    */ import com.endeca.portlet.summarizationbar.model.edit.AlertConfig;
/*  28:    */ import com.endeca.portlet.summarizationbar.model.edit.ConditionsConfig;
/*  29:    */ import com.endeca.portlet.summarizationbar.model.edit.DimensionEntity;
/*  30:    */ import com.endeca.portlet.summarizationbar.model.edit.EditModel;
/*  31:    */ import com.endeca.portlet.summarizationbar.model.edit.ItemConfig;
/*  32:    */ import com.endeca.portlet.summarizationbar.model.edit.MetricEntity;
/*  33:    */ import com.endeca.portlet.summarizationbar.model.edit.SummarizationConfig;
/*  34:    */ import com.endeca.portlet.summarizationbar.model.view.CustomActionResponse;
/*  35:    */ import com.endeca.portlet.summarizationbar.model.view.SummarizationModel;
/*  36:    */ import com.endeca.portlet.summarizationbar.model.view.ViewModel;
/*  37:    */ import com.endeca.portlet.summarizationbar.service.SummarizationBarService;
/*  38:    */ import com.endeca.portlet.summarizationbar.service.SummarizationBarUtils;
/*  39:    */ import com.endeca.portlet.util.Constants;
/*  40:    */ import com.endeca.portlet.util.EndecaPortletUtil;
/*  41:    */ import com.endeca.portlet.util.LanguageUtils;
/*  42:    */ import java.io.IOException;
/*  43:    */ import java.util.Collections;
/*  44:    */ import java.util.HashMap;
/*  45:    */ import java.util.HashSet;
/*  46:    */ import java.util.Iterator;
/*  47:    */ import java.util.LinkedHashMap;
/*  48:    */ import java.util.List;
/*  49:    */ import java.util.Locale;
/*  50:    */ import java.util.Map;
/*  51:    */ import java.util.Set;
/*  52:    */ import javax.portlet.ActionRequest;
/*  53:    */ import javax.portlet.ActionResponse;
/*  54:    */ import javax.portlet.PortletException;
/*  55:    */ import javax.portlet.PortletPreferences;
/*  56:    */ import javax.portlet.PortletRequest;
/*  57:    */ import javax.portlet.PortletResponse;
/*  58:    */ import javax.portlet.PortletSession;
/*  59:    */ import javax.portlet.ProcessAction;
/*  60:    */ import javax.portlet.RenderRequest;
/*  61:    */ import javax.portlet.RenderResponse;
/*  62:    */ import javax.portlet.ResourceRequest;
/*  63:    */ import javax.portlet.ResourceResponse;
/*  64:    */ import org.apache.commons.lang.StringUtils;
/*  65:    */ import org.apache.log4j.Logger;
/*  66:    */ import org.codehaus.jackson.map.ObjectMapper;
/*  67:    */ import org.json.JSONArray;
/*  68:    */ 
/*  69:    */ public class SummarizationBarPortlet
/*  70:    */   extends EndecaPortlet
/*  71:    */ {
/*  72: 73 */   private static final ObjectMapper jsonObjectMapper = new ObjectMapper();
/*  73: 75 */   private static final Logger logger = Logger.getLogger(SummarizationBarPortlet.class);
/*  74:    */   
/*  75:    */   protected ModelAndView handleViewRenderRequest(RenderRequest request, RenderResponse response)
/*  76:    */     throws Exception
/*  77:    */   {
/*  78: 80 */     ModelAndView mv = new ModelAndView(this.viewJSP);
/*  79: 81 */     EditModel editModel = getEditModel(request);
/*  80: 82 */     ViewModel viewModel = new ViewModel();
/*  81: 83 */     viewModel.setSettingsModel(editModel.getSettingsModel());
/*  82:    */     
/*  83: 85 */     mv.addObject("showConfigMessage", isShowAutoConfigMessage(request));
/*  84: 86 */     mv.addObject("viewModel", viewModel);
/*  85: 87 */     mv.addObject("conditionalStyle", SummarizationBarObjectMapper.toString(SummarizationBarConstants.CONDITIONAL_STYLE_MAP));
/*  86:    */     
/*  87: 89 */     return mv;
/*  88:    */   }
/*  89:    */   
/*  90:    */   protected ModelAndView handleEditRenderRequest(RenderRequest request, RenderResponse response)
/*  91:    */     throws Exception
/*  92:    */   {
/*  93: 95 */     if (logger.isDebugEnabled()) {
/*  94: 96 */       logger.debug("handleEditRenderRequest: start");
/*  95:    */     }
/*  96: 99 */     ModelAndView mv = new ModelAndView(this.editJSP);
/*  97:    */     
/*  98:    */ 
/*  99:102 */     PortletSession portletSession = request.getPortletSession();
/* 100:103 */     if (portletSession.getAttribute("isRevert") != null)
/* 101:    */     {
/* 102:104 */       mv.addObject("isRevert", Boolean.valueOf(true));
/* 103:105 */       portletSession.removeAttribute("isRevert");
/* 104:    */     }
/* 105:    */     else
/* 106:    */     {
/* 107:107 */       mv.addObject("isRevert", Boolean.valueOf(false));
/* 108:    */     }
/* 109:110 */     updateShowAutoConfigMessage(request.getPreferences(), Constants.FALSE_STRING);
/* 110:    */     
/* 111:    */ 
/* 112:113 */     SummarizationBarService service = new SummarizationBarService(request, getDataSource(request));
/* 113:114 */     List<Map<String, Object>> dataSourceData = EndecaPortletUtil.getDataSourceMap(service.getMDEXStates(request, getUserSession(request)), request);
/* 114:    */     
/* 115:116 */     mv.addObject("dataSourceData", SummarizationBarObjectMapper.toString(dataSourceData));
/* 116:117 */     mv.addObject("conditionalStyle", SummarizationBarObjectMapper.toString(SummarizationBarConstants.CONDITIONAL_STYLE_MAP));
/* 117:119 */     if (logger.isDebugEnabled()) {
/* 118:120 */       logger.debug("handleEditRenderRequest: end");
/* 119:    */     }
/* 120:122 */     return mv;
/* 121:    */   }
/* 122:    */   
/* 123:    */   public void handleActionViewRenderRequest(RenderRequest request, RenderResponse response)
/* 124:    */   {
/* 125:    */     try
/* 126:    */     {
/* 127:127 */       CustomActionResponse customActionJSONResponse = (CustomActionResponse)request.getAttribute("customActionJsonResponse");
/* 128:130 */       if (customActionJSONResponse == null)
/* 129:    */       {
/* 130:131 */         EndecaPortletUtil.writeObjectResponse("", response);
/* 131:132 */         return;
/* 132:    */       }
/* 133:134 */       Set<String> resultEvents = new HashSet();
/* 134:135 */       Set<String> customEvents = customActionJSONResponse.getEvents();
/* 135:136 */       Set<String> allEvents = getAllPublishedEventNames(request);
/* 136:137 */       if (customEvents != null) {
/* 137:138 */         resultEvents.addAll(customEvents);
/* 138:    */       }
/* 139:140 */       if (allEvents != null) {
/* 140:141 */         resultEvents.addAll(allEvents);
/* 141:    */       }
/* 142:143 */       customActionJSONResponse.setEvents(resultEvents);
/* 143:144 */       EndecaPortletUtil.writeObjectResponse(customActionJSONResponse, response);
/* 144:    */     }
/* 145:    */     catch (Exception e)
/* 146:    */     {
/* 147:146 */       e.printStackTrace();
/* 148:    */     }
/* 149:    */   }
/* 150:    */   
/* 151:    */   @ProcessResource(resourceId="savePrefs")
/* 152:    */   public void serveResourceSavePrefs(ResourceRequest request, ResourceResponse response)
/* 153:    */     throws PortletException, IOException
/* 154:    */   {
/* 155:160 */     if (logger.isDebugEnabled()) {
/* 156:161 */       logger.debug("serveResourceSavePrefs: start");
/* 157:    */     }
/* 158:164 */     if (!EndecaPortletUtil.hasUpdatePrivileges(request, getContainer()))
/* 159:    */     {
/* 160:165 */       logger.warn("failed to save preferences due to insufficient privilege");
/* 161:166 */       return;
/* 162:    */     }
/* 163:170 */     String editModelString = getFirstParam(request, "editModel");
/* 164:172 */     if (isEmpty(editModelString))
/* 165:    */     {
/* 166:173 */       logger.error("failed to save preferences since parameter editModel is empty");
/* 167:174 */       throw new PortletException("edit model cannot be empty");
/* 168:    */     }
/* 169:177 */     PortletPreferences preferences = request.getPreferences();
/* 170:178 */     EditModel editModel = (EditModel)SummarizationBarObjectMapper.fromString(editModelString, EditModel.class);
/* 171:180 */     if (editModel == null)
/* 172:    */     {
/* 173:181 */       logger.error("failed to parse string to edit model");
/* 174:182 */       EndecaPortletUtil.writeErrorMessageResponse(LanguageUtils.getMessage(request, "summarizationbar.edit.message.failed-to-save-preferences"), response);
/* 175:    */       
/* 176:184 */       return;
/* 177:    */     }
/* 178:187 */     preferences.setValue("prefsEditModel", SummarizationBarObjectMapper.toString(editModel));
/* 179:188 */     if ((editModel.getConfigs() != null) && (editModel.getConfigs().get(0) != null)) {
/* 180:189 */       preferences.setValue("dataSet", ((SummarizationConfig)editModel.getConfigs().get(0)).getSemanticKey());
/* 181:    */     }
/* 182:    */     try
/* 183:    */     {
/* 184:191 */       preferences.store();
/* 185:192 */       EndecaPortletUtil.writeMessageResponse(LanguageUtils.getMessage(request, "df.save-prefs-success"), response);
/* 186:    */     }
/* 187:    */     catch (IOException e)
/* 188:    */     {
/* 189:194 */       logger.error("failed to save preferences.", e);
/* 190:195 */       EndecaPortletUtil.writeErrorMessageResponse(LanguageUtils.getMessage(request, "summarizationbar.edit.message.failed-to-save-preferences"), response);
/* 191:    */     }
/* 192:199 */     if (logger.isDebugEnabled()) {
/* 193:200 */       logger.debug("serveResourceSavePrefs: end");
/* 194:    */     }
/* 195:    */   }
/* 196:    */   
/* 197:    */   @ProcessResource(resourceId="updateShowConfigMessage")
/* 198:    */   public void serveResourceUpdateShowAutoConfigMessage(ResourceRequest request, ResourceResponse response)
/* 199:    */     throws PortletException
/* 200:    */   {
/* 201:207 */     PortletPreferences prefs = request.getPreferences();
/* 202:208 */     updateShowAutoConfigMessage(prefs, Constants.FALSE_STRING);
/* 203:    */   }
/* 204:    */   
/* 205:    */   @ProcessResource(resourceId="getSummarizations")
/* 206:    */   public void serveResourceGetSummarizations(ResourceRequest request, ResourceResponse response)
/* 207:    */     throws PortletException, IOException, DataSourceException
/* 208:    */   {
/* 209:215 */     EditModel editModel = null;
/* 210:    */     
/* 211:217 */     boolean isUpdatePreview = false;
/* 212:218 */     String isUpdatePreviewString = getFirstParam(request, "isUpdatePreview");
/* 213:220 */     if (StringUtils.isNotBlank(isUpdatePreviewString)) {
/* 214:221 */       isUpdatePreview = Boolean.parseBoolean(isUpdatePreviewString);
/* 215:    */     }
/* 216:223 */     if (isUpdatePreview)
/* 217:    */     {
/* 218:224 */       String editModelString = getFirstParam(request, "editModel");
/* 219:225 */       editModel = (EditModel)SummarizationBarObjectMapper.fromString(EndecaPortletUtil.escapeJsonStringForHtml(editModelString), EditModel.class);
/* 220:226 */       if (editModel == null)
/* 221:    */       {
/* 222:227 */         logger.error("failed to parse string to edit model");
/* 223:228 */         EndecaPortletUtil.writeErrorMessageResponse(LanguageUtils.getMessage(request, "summarizationbar.edit.message.failed-to-load-summarizations"), response);
/* 224:    */         
/* 225:230 */         return;
/* 226:    */       }
/* 227:    */     }
/* 228:    */     else
/* 229:    */     {
/* 230:    */       try
/* 231:    */       {
/* 232:234 */         editModel = getEditModel(request);
/* 233:    */       }
/* 234:    */       catch (IOException e)
/* 235:    */       {
/* 236:236 */         logger.error("failed to load editmodel.", e);
/* 237:237 */         EndecaPortletUtil.writeErrorMessageResponse(LanguageUtils.getMessage(request, "summarizationbar.edit.message.failed-to-load-summarizations"), response);
/* 238:    */         
/* 239:239 */         return;
/* 240:    */       }
/* 241:    */     }
/* 242:    */     try
/* 243:    */     {
/* 244:245 */       SummarizationBarService service = new SummarizationBarService(request, getDataSource(request));
/* 245:246 */       SummarizationModel summarizationModel = new SummarizationModel();
/* 246:247 */       summarizationModel.setSummarizations(service.getSummarizationList(editModel));
/* 247:    */       
/* 248:249 */       PortletPreferences prefs = request.getPreferences();
/* 249:250 */       String savedEditModel = prefs.getValue("prefsEditModel", "");
/* 250:252 */       if ((StringUtils.isNotBlank(savedEditModel)) && (!service.hasCompletedConfig(editModel.getConfigs())))
/* 251:    */       {
/* 252:253 */         logger.error("no configured summarizations.");
/* 253:254 */         summarizationModel.setMessage(LanguageUtils.getMessage(request, "df.view-unconfigured"));
/* 254:    */         
/* 255:256 */         summarizationModel.setMessageKey("108");
/* 256:257 */         EndecaPortletUtil.writeObjectResponse(summarizationModel, response, jsonObjectMapper);
/* 257:258 */         return;
/* 258:    */       }
/* 259:261 */       if (summarizationModel.getSummarizations().size() == 0)
/* 260:    */       {
/* 261:262 */         logger.error("no summary items to display currently.");
/* 262:263 */         summarizationModel.setMessage(LanguageUtils.getMessage(request, "summarizationbar.edit.message.no-summary-items"));
/* 263:264 */         summarizationModel.setMessageKey("110");
/* 264:    */       }
/* 265:267 */       EndecaPortletUtil.writeObjectResponse(summarizationModel, response, jsonObjectMapper);
/* 266:    */     }
/* 267:    */     catch (Exception e)
/* 268:    */     {
/* 269:270 */       logger.error("failed to get summarizations.", e);
/* 270:271 */       EndecaPortletUtil.writeErrorMessageResponse(LanguageUtils.getMessage(request, "summarizationbar.edit.message.failed-to-load-summarizations"), response);
/* 271:    */     }
/* 272:    */   }
/* 273:    */   
/* 274:    */   @ProcessResource(resourceId="getEditModel")
/* 275:    */   public void serveResourceGetEditModel(ResourceRequest request, ResourceResponse response)
/* 276:    */     throws PortletException, IOException, DataSourceException, LQLException, ModelException, Formatter.FormatParseException
/* 277:    */   {
/* 278:291 */     if (logger.isDebugEnabled()) {
/* 279:292 */       logger.debug("serveResourceGetEditModel : start");
/* 280:    */     }
/* 281:295 */     EditModel editModel = getEditModel(request);
/* 282:303 */     for (SummarizationConfig config : editModel.getConfigs()) {
/* 283:304 */       if (!config.getItemConfig().getIsCustomDisplayName()) {
/* 284:306 */         config.setDataSourceId(getDataSource(request).getId());
/* 285:    */       }
/* 286:    */     }
/* 287:310 */     EndecaPortletUtil.writeObjectResponse(editModel, response, jsonObjectMapper);
/* 288:312 */     if (logger.isDebugEnabled()) {
/* 289:313 */       logger.debug("serveResourceGetEditModel : end");
/* 290:    */     }
/* 291:    */   }
/* 292:    */   
/* 293:    */   @ProcessAction(name="resetPrefs")
/* 294:    */   public void processActionRevertPrefs(ActionRequest request, ActionResponse response)
/* 295:    */     throws PortletException
/* 296:    */   {
/* 297:326 */     if (logger.isDebugEnabled()) {
/* 298:327 */       logger.debug("processActionRevertPrefs: start.");
/* 299:    */     }
/* 300:329 */     request.getPortletSession().setAttribute("isRevert", new Boolean(true));
/* 301:330 */     if (logger.isDebugEnabled()) {
/* 302:331 */       logger.debug("processActionRevertPrefs: end.");
/* 303:    */     }
/* 304:    */   }
/* 305:    */   
/* 306:    */   @ProcessAction(name="refinement")
/* 307:    */   public void processActionRefinement(ActionRequest request, ActionResponse response)
/* 308:    */     throws PortletException
/* 309:    */   {
/* 310:    */     try
/* 311:    */     {
/* 312:345 */       String summarizationId = getFirstParam(request, "summarizationId");
/* 313:346 */       String dimensionKeys = getFirstParam(request, "dimensionKeys");
/* 314:347 */       String dimensionValues = getFirstParam(request, "dimensionValues");
/* 315:348 */       if ((StringUtils.isEmpty(summarizationId)) || (StringUtils.isEmpty(dimensionKeys)) || (StringUtils.isEmpty(dimensionValues))) {
/* 316:349 */         throw new Exception("refinements parameter is missing or empty");
/* 317:    */       }
/* 318:351 */       JSONArray dimensionKeysArr = new JSONArray(dimensionKeys);
/* 319:352 */       JSONArray dimensionValuesArr = new JSONArray(dimensionValues);
/* 320:353 */       if (dimensionKeysArr.length() != dimensionValuesArr.length()) {
/* 321:354 */         throw new Exception("refinements parameter is missing or empty");
/* 322:    */       }
/* 323:358 */       SummarizationBarService service = new SummarizationBarService();
/* 324:359 */       SummarizationConfig summarizationConfig = service.getSummarizationConfig(getEditModel(request), summarizationId);
/* 325:360 */       DataSource dataSource = getDataSource(request);
/* 326:361 */       SemanticView semanticView = dataSource.getCollectionOrSemanticView(summarizationConfig.getSemanticKey(), request.getLocale());
/* 327:362 */       QueryState queryState = dataSource.getQueryState();
/* 328:365 */       for (int i = 0; i < dimensionKeysArr.length(); i++)
/* 329:    */       {
/* 330:366 */         String dimensionKey = dimensionKeysArr.getString(i);
/* 331:367 */         Attribute attr = (Attribute)semanticView.getAllAttributes().get(dimensionKey);
/* 332:368 */         if (attr != null)
/* 333:    */         {
/* 334:369 */           QueryFunction queryFunction = null;
/* 335:370 */           if (PropertyType.DATETIME.equals(attr.getType()))
/* 336:    */           {
/* 337:371 */             List<DatePart> dateParts = DateFilterUtil.getDefaultCascadeDateParts(attr);
/* 338:372 */             queryFunction = QueryFunctionUtils.getQueryFunction(attr, dimensionValuesArr.getString(i), RefinementFilter.MultiSelect.OR, true, dateParts);
/* 339:    */           }
/* 340:    */           else
/* 341:    */           {
/* 342:374 */             queryFunction = QueryFunctionUtils.getQueryFunction(semanticView, dimensionKeysArr.getString(i), dimensionValuesArr.getString(i));
/* 343:    */           }
/* 344:376 */           queryState.addFunction(queryFunction, semanticView, request.getLocale());
/* 345:    */         }
/* 346:    */       }
/* 347:379 */       dataSource.setQueryState(queryState);
/* 348:    */       
/* 349:381 */       CustomActionResponse actionResponse = new CustomActionResponse(Collections.emptySet(), null);
/* 350:382 */       String targetPage = "";
/* 351:383 */       if (SummarizationBarUtils.isDimensionSummarizationType(summarizationConfig.getType()))
/* 352:    */       {
/* 353:384 */         RefinementActionModel refinementAction = (RefinementActionModel)service.getActionModel(summarizationConfig, RefinementActionModel.class);
/* 354:385 */         if ((refinementAction != null) && (!refinementAction.getIsCurrentPage())) {
/* 355:386 */           targetPage = refinementAction.getTargetPage();
/* 356:    */         }
/* 357:    */       }
/* 358:388 */       else if ((SummarizationBarUtils.isAlertSummarizationType(summarizationConfig.getType())) && 
/* 359:389 */         (summarizationConfig.getAlertConfig().getIsOtherPage()))
/* 360:    */       {
/* 361:390 */         targetPage = summarizationConfig.getAlertConfig().getTargetPage();
/* 362:    */       }
/* 363:393 */       if (!StringUtils.isEmpty(targetPage))
/* 364:    */       {
/* 365:394 */         Map<String, String[]> params = new HashMap();
/* 366:395 */         String redirectUrl = handlePageTransition(request, targetPage, params);
/* 367:396 */         actionResponse.setRedirectUrl(redirectUrl);
/* 368:    */       }
/* 369:398 */       request.setAttribute("customActionJsonResponse", actionResponse);
/* 370:    */     }
/* 371:    */     catch (Exception e)
/* 372:    */     {
/* 373:400 */       throw new PortletException(e);
/* 374:    */     }
/* 375:    */   }
/* 376:    */   
/* 377:    */   @ProcessAction(name="hyperlink")
/* 378:    */   public void processActionHyperlink(ActionRequest request, ActionResponse response)
/* 379:    */     throws PortletException
/* 380:    */   {
/* 381:    */     try
/* 382:    */     {
/* 383:407 */       SummarizationBarService service = new SummarizationBarService();
/* 384:408 */       SummarizationConfig summarizationConfig = service.getSummarizationConfig(getEditModel(request), getFirstParam(request, "summarizationId"));
/* 385:410 */       if (summarizationConfig == null) {
/* 386:411 */         return;
/* 387:    */       }
/* 388:414 */       PassParameterActionModel passParameterAction = (PassParameterActionModel)service.getActionModel(summarizationConfig, PassParameterActionModel.class);
/* 389:415 */       if (passParameterAction == null) {
/* 390:416 */         return;
/* 391:    */       }
/* 392:418 */       String url = getFirstParam(request, "hyperLinkURL");
/* 393:    */       
/* 394:420 */       CustomActionResponse actionResponse = new CustomActionResponse();
/* 395:421 */       if (passParameterAction.getOpenWindow()) {
/* 396:422 */         actionResponse.setPopupUrl(url);
/* 397:    */       } else {
/* 398:424 */         actionResponse.setRedirectUrl(url);
/* 399:    */       }
/* 400:426 */       request.setAttribute("customActionJsonResponse", actionResponse);
/* 401:    */     }
/* 402:    */     catch (Exception e) {}
/* 403:    */   }
/* 404:    */   
/* 405:    */   protected String handlePageTransition(ActionRequest request, String viewTransitionTarget, Map<String, String[]> params)
/* 406:    */     throws PortletException
/* 407:    */   {
/* 408:    */     try
/* 409:    */     {
/* 410:435 */       handlePageTransitionParameters(request, viewTransitionTarget, params);
/* 411:436 */       return EndecaPortletUtil.getFullyQualifiedTransitionContext(request, viewTransitionTarget);
/* 412:    */     }
/* 413:    */     catch (Exception e)
/* 414:    */     {
/* 415:438 */       throw new PortletException(e);
/* 416:    */     }
/* 417:    */   }
/* 418:    */   
/* 419:    */   public void resetPortletStateMaintainPortletPrefs(PortletRequest request, PortletResponse response)
/* 420:    */     throws PortletException, IOException
/* 421:    */   {
/* 422:445 */     EditModel editModel = null;
/* 423:446 */     DataSource dataSource = null;
/* 424:447 */     SemanticView semanticView = null;
/* 425:    */     try
/* 426:    */     {
/* 427:450 */       editModel = getEditModel(request);
/* 428:451 */       dataSource = getDataSource(request);
/* 429:    */       
/* 430:453 */       String viewKey = EndecaPortletUtil.getConfiguredViewKey(request);
/* 431:454 */       DataSource ds = EndecaPortletUtil.getDataSource(request);
/* 432:455 */       semanticView = ds.getCollectionOrSemanticView(viewKey, request.getLocale());
/* 433:    */     }
/* 434:    */     catch (DataSourceException e)
/* 435:    */     {
/* 436:457 */       logger.error("resetPortletStateMaintainPortletPrefs: Failed to reset the portlet state, cannot open the data source.");
/* 437:458 */       throw new PortletException(e);
/* 438:    */     }
/* 439:    */     catch (KeyNotFoundException e)
/* 440:    */     {
/* 441:460 */       logger.error("resetPortletStateMaintainPortletPrefs: Failed to reset the portlet state, cannot get the data set.");
/* 442:461 */       throw new PortletException(e);
/* 443:    */     }
/* 444:464 */     for (SummarizationConfig config : editModel.getConfigs())
/* 445:    */     {
/* 446:465 */       config.setDataSourceId(dataSource.getId());
/* 447:466 */       config.setSemanticKey(semanticView.getKey());
/* 448:    */       
/* 449:468 */       resetPortletStateMaintainAttributes(config, semanticView);
/* 450:    */     }
/* 451:471 */     PortletPreferences prefs = request.getPreferences();
/* 452:472 */     prefs.setValue("prefsEditModel", SummarizationBarObjectMapper.toString(editModel));
/* 453:473 */     prefs.store();
/* 454:    */   }
/* 455:    */   
/* 456:    */   private void resetPortletStateMaintainAttributes(SummarizationConfig config, SemanticView semanticView)
/* 457:    */   {
/* 458:477 */     boolean isCompleted = true;
/* 459:479 */     if (SummarizationBarUtils.isAlertSummarizationType(config.getType()))
/* 460:    */     {
/* 461:480 */       List<DimensionEntity> dimensionsList = config.getAlertConfig().getDimensionsList();
/* 462:481 */       Iterator<DimensionEntity> iterator = dimensionsList.iterator();
/* 463:482 */       while (iterator.hasNext()) {
/* 464:483 */         if (!isValidDimension(semanticView, ((DimensionEntity)iterator.next()).getKey())) {
/* 465:484 */           iterator.remove();
/* 466:    */         }
/* 467:    */       }
/* 468:487 */       if (dimensionsList.size() == 0) {
/* 469:488 */         isCompleted = false;
/* 470:    */       }
/* 471:491 */       List<AlertConditionEntity> conditionList = config.getAlertConfig().getConditonsList();
/* 472:492 */       for (AlertConditionEntity condition : conditionList)
/* 473:    */       {
/* 474:493 */         if ((condition.getMetric() != null) && (!isValidMetric(semanticView, condition.getMetric().getKey(), condition.getMetric().getAggregation())))
/* 475:    */         {
/* 476:495 */           condition.setMetric(null);
/* 477:496 */           isCompleted = false;
/* 478:    */         }
/* 479:498 */         if ((SummarizationBarEnum.ConditionType.ToAnotherMetric.equals(condition.getType())) && (condition.getTargetMetric() != null) && (!isValidMetric(semanticView, condition.getTargetMetric().getKey(), condition.getTargetMetric().getAggregation())))
/* 480:    */         {
/* 481:500 */           condition.setTargetMetric(null);
/* 482:501 */           isCompleted = false;
/* 483:    */         }
/* 484:    */       }
/* 485:    */     }
/* 486:    */     else
/* 487:    */     {
/* 488:506 */       boolean setAsCustomName = false;
/* 489:507 */       ItemConfig itemConfig = config.getItemConfig();
/* 490:508 */       if (!isValidMetric(semanticView, itemConfig.getMetricKey(), itemConfig.getMetricAggre()))
/* 491:    */       {
/* 492:509 */         itemConfig.setMetricKey(null);
/* 493:510 */         itemConfig.setMetricAggre(null);
/* 494:511 */         itemConfig.setMetricFormatter(null);
/* 495:512 */         isCompleted = false;
/* 496:513 */         setAsCustomName = true;
/* 497:    */       }
/* 498:516 */       if ((SummarizationBarUtils.isDimensionSummarizationType(config.getType())) && (!isValidDimension(semanticView, itemConfig.getDimensionKey())))
/* 499:    */       {
/* 500:518 */         itemConfig.setDimensionKey(null);
/* 501:519 */         itemConfig.setDimensionFormatter(null);
/* 502:520 */         isCompleted = false;
/* 503:521 */         setAsCustomName = true;
/* 504:    */       }
/* 505:524 */       ConditionsConfig conditionConfig = config.getConditionsConfig();
/* 506:525 */       if ((conditionConfig != null) && (SummarizationBarEnum.ConditionType.ToAnotherMetric.equals(conditionConfig.getType())) && (!isValidMetric(semanticView, conditionConfig.getTargetMetricKey(), conditionConfig.getTargetMetricAggre())))
/* 507:    */       {
/* 508:527 */         conditionConfig.setTargetMetricKey(null);
/* 509:528 */         conditionConfig.setTargetMetricAggre(null);
/* 510:529 */         conditionConfig.setTargetMetricFormatter(null);
/* 511:530 */         isCompleted = false;
/* 512:    */       }
/* 513:533 */       if (setAsCustomName)
/* 514:    */       {
/* 515:534 */         itemConfig.setIsCustomDisplayName(true);
/* 516:535 */         config.setName(itemConfig.getCustomDisplayName());
/* 517:    */       }
/* 518:    */     }
/* 519:540 */     config.setIsCompleted(isCompleted);
/* 520:    */   }
/* 521:    */   
/* 522:    */   public void resetPortletState(PortletRequest request, PortletResponse response)
/* 523:    */     throws PortletException, IOException
/* 524:    */   {
/* 525:    */     try
/* 526:    */     {
/* 527:546 */       PortletPreferences prefs = request.getPreferences();
/* 528:547 */       SummarizationBarService service = new SummarizationBarService(request, getDataSource(request));
/* 529:548 */       EditModel editModel = getDefaultEditModel(service, request.getLocale());
/* 530:549 */       prefs.setValue("prefsEditModel", SummarizationBarObjectMapper.toString(editModel));
/* 531:550 */       prefs.store();
/* 532:    */     }
/* 533:    */     catch (Exception e)
/* 534:    */     {
/* 535:553 */       throw new PortletException(e);
/* 536:    */     }
/* 537:    */   }
/* 538:    */   
/* 539:    */   private EditModel getEditModel(PortletRequest request)
/* 540:    */     throws IOException, PortletException, DataSourceException
/* 541:    */   {
/* 542:559 */     EditModel editModel = null;
/* 543:560 */     PortletPreferences prefs = request.getPreferences();
/* 544:561 */     String savedEditModel = prefs.getValue("prefsEditModel", "");
/* 545:562 */     SummarizationBarService service = new SummarizationBarService(request, getDataSource(request));
/* 546:564 */     if (StringUtils.isNotBlank(savedEditModel)) {
/* 547:    */       try
/* 548:    */       {
/* 549:566 */         editModel = (EditModel)jsonObjectMapper.readValue(EndecaPortletUtil.escapeJsonStringForHtml(savedEditModel), EditModel.class);
/* 550:    */       }
/* 551:    */       catch (Exception e)
/* 552:    */       {
/* 553:568 */         logger.error("Failed to deserialize saved edit model for guided nav, expected behavior when upgrading.  JSON saved here for restore in event of unexpected corruption, but message should be ignored if this is an upgrade.");
/* 554:569 */         logger.error(savedEditModel);
/* 555:    */         
/* 556:571 */         prefs.reset("prefsEditModel");
/* 557:572 */         prefs.store();
/* 558:    */       }
/* 559:    */     }
/* 560:575 */     if (editModel == null) {
/* 561:576 */       editModel = getDefaultEditModel(service, request.getLocale());
/* 562:    */     }
/* 563:580 */     for (SummarizationConfig config : editModel.getConfigs()) {
/* 564:581 */       config.setName(service.getSummarizationName(config));
/* 565:    */     }
/* 566:584 */     editModel.setDefaultCollectionKey(EndecaPortletUtil.getDefaultCollection(request).getKey());
/* 567:585 */     return editModel;
/* 568:    */   }
/* 569:    */   
/* 570:    */   private EditModel getDefaultEditModel(SummarizationBarService service, Locale locale)
/* 571:    */     throws PortletException, DataSourceException, IOException
/* 572:    */   {
/* 573:591 */     EditModel editModel = new EditModel();
/* 574:    */     
/* 575:593 */     editModel.addConfig(service.getCountOneSummarizationConfig());
/* 576:    */     
/* 577:595 */     return editModel;
/* 578:    */   }
/* 579:    */   
/* 580:    */   private String isShowAutoConfigMessage(PortletRequest request)
/* 581:    */   {
/* 582:599 */     String isShowMessage = Constants.FALSE_STRING;
/* 583:600 */     if (EndecaPortletUtil.hasUpdatePrivileges(request, getContainer()))
/* 584:    */     {
/* 585:601 */       PortletPreferences prefs = request.getPreferences();
/* 586:602 */       isShowMessage = prefs.getValue("showConfigMessage", Constants.TRUE_STRING);
/* 587:    */     }
/* 588:604 */     return isShowMessage;
/* 589:    */   }
/* 590:    */   
/* 591:    */   private void updateShowAutoConfigMessage(PortletPreferences prefs, String showMessage)
/* 592:    */     throws PortletException
/* 593:    */   {
/* 594:    */     try
/* 595:    */     {
/* 596:610 */       prefs.setValue("showConfigMessage", showMessage);
/* 597:611 */       prefs.store();
/* 598:    */     }
/* 599:    */     catch (Exception e)
/* 600:    */     {
/* 601:613 */       throw new PortletException(e);
/* 602:    */     }
/* 603:    */   }
/* 604:    */   
/* 605:    */   private boolean isValidMetric(SemanticView semanticView, String metricKey, LQLBuilder.Function metricAggre)
/* 606:    */   {
/* 607:619 */     if ((semanticView == null) || (StringUtils.isEmpty(metricKey))) {
/* 608:620 */       return false;
/* 609:    */     }
/* 610:623 */     if ("system-metrics-count_1".equals(metricKey)) {
/* 611:624 */       return true;
/* 612:    */     }
/* 613:627 */     Map<String, Attribute> attributes = semanticView.getAllAttributes();
/* 614:628 */     Attribute attribute = (Attribute)attributes.get(metricKey);
/* 615:629 */     if ((attribute != null) && (metricAggre != null) && (StringUtils.contains(attribute.getAvailableAggregations(), metricAggre.name()))) {
/* 616:630 */       return true;
/* 617:    */     }
/* 618:633 */     LinkedHashMap<String, PredefinedMetric> predefinedMetrics = semanticView.getPredefinedMetrics();
/* 619:634 */     if (predefinedMetrics.get(metricKey) != null) {
/* 620:635 */       return true;
/* 621:    */     }
/* 622:638 */     return false;
/* 623:    */   }
/* 624:    */   
/* 625:    */   private boolean isValidDimension(SemanticView semanticView, String dimensionKey)
/* 626:    */   {
/* 627:641 */     if ((semanticView == null) || (StringUtils.isEmpty(dimensionKey))) {
/* 628:642 */       return false;
/* 629:    */     }
/* 630:645 */     Map<String, Attribute> attributes = semanticView.getAllAttributes();
/* 631:646 */     if (attributes.get(dimensionKey) != null) {
/* 632:647 */       return true;
/* 633:    */     }
/* 634:650 */     return false;
/* 635:    */   }
/* 636:    */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.SummarizationBarPortlet
 * JD-Core Version:    0.7.0.1
 */