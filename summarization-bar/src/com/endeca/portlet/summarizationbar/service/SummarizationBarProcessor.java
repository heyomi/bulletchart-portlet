 package com.endeca.portlet.summarizationbar.service;
 
 import com.endeca.mdex.conversation.EQL;
 import com.endeca.mdex.conversation.ResultRecords;
 import com.endeca.mdex.conversation.Results;
 import com.endeca.mdex.eql_parser.types.Query;
 import com.endeca.portal.data.DataSource;
 import com.endeca.portal.data.DataSourceException;
 import com.endeca.portal.data.KeyNotFoundException;
/*  10:    */ import com.endeca.portal.data.QueryResults;
/*  11:    */ import com.endeca.portal.data.QueryState;
/*  12:    */ import com.endeca.portal.data.SemanticView;
/*  13:    */ import com.endeca.portal.data.functions.LQLQueryConfig;
/*  14:    */ import com.endeca.portal.data.model.Assignment;
/*  15:    */ import com.endeca.portal.data.model.Assignments;
/*  16:    */ import com.endeca.portal.data.model.ModelException;
/*  17:    */ import com.endeca.portal.data.model.ModelUtil;
/*  18:    */ import com.endeca.portal.data.model.SimpleRecord;
/*  19:    */ import com.endeca.portal.lql.LQLException;
/*  20:    */ import com.endeca.portal.mdex.DiscoveryServiceUtil;
/*  21:    */ import com.endeca.portlet.summarizationbar.model.edit.SummarizationConfig;
/*  22:    */ import java.util.ArrayList;
/*  23:    */ import java.util.List;
/*  24:    */ import javax.portlet.PortletException;
/*  25:    */ import javax.portlet.PortletRequest;
/*  26:    */ import org.apache.log4j.Logger;
/*  27:    */ 
/*  28:    */ public class SummarizationBarProcessor
/*  29:    */ {
/*  30: 35 */   private static final Logger logger = Logger.getLogger(SummarizationBarProcessor.class);
/*  31:    */   private DataSource dataSource;
/*  32:    */   
/*  33:    */   public SummarizationBarProcessor(DataSource dataSource)
/*  34:    */   {
/*  35: 40 */     this.dataSource = dataSource;
/*  36:    */   }
/*  37:    */   
/*  38:    */   public List<com.endeca.portal.data.model.Record> getStateRecords(PortletRequest request, SummarizationConfig config)
/*  39:    */     throws KeyNotFoundException, DataSourceException, LQLException, PortletException
/*  40:    */   {
/*  41: 46 */     QueryState queryState = this.dataSource.getQueryState();
/*  42:    */     
/*  43: 48 */     SemanticView view = this.dataSource.getCollectionOrSemanticView(config.getSemanticKey(), request.getLocale());
/*  44:    */     
/*  45: 50 */     Query lqlQuery = EQLUtils.getSummaryQuery(request, view, config);
/*  46:    */     
/*  47: 52 */     EQL eql = executeQuery(request, view, lqlQuery, queryState);
/*  48:    */     
/*  49: 54 */     return parseEQL(eql, view);
/*  50:    */   }
/*  51:    */   
/*  52:    */   public long getAlertsRecordCount(PortletRequest request, SummarizationConfig config)
/*  53:    */     throws KeyNotFoundException, DataSourceException, LQLException, PortletException, ModelException
/*  54:    */   {
/*  55: 59 */     long recordCount = 0L;
/*  56: 60 */     QueryState queryState = this.dataSource.getQueryState();
/*  57:    */     
/*  58: 62 */     SemanticView view = this.dataSource.getCollectionOrSemanticView(config.getSemanticKey(), request.getLocale());
/*  59:    */     
/*  60: 64 */     Query lqlQuery = EQLUtils.getAlertsRecordCountQuery(request, view, config);
/*  61:    */     
/*  62: 66 */     EQL eql = executeQuery(request, view, lqlQuery, queryState);
/*  63:    */     
/*  64: 68 */     List<com.endeca.portal.data.model.Record> recordList = parseEQL(eql, view);
/*  65: 69 */     if (recordList.size() > 0)
/*  66:    */     {
/*  67: 70 */       com.endeca.portal.data.model.Record record = (com.endeca.portal.data.model.Record)recordList.get(0);
/*  68: 71 */       if (record != null)
/*  69:    */       {
/*  70: 72 */         Assignments assignments = record.getAssignments("_$METRIC_system-metrics-count_1");
/*  71: 73 */         if ((assignments != null) && (assignments.getSoleAssignment() != null)) {
/*  72: 74 */           recordCount = Long.parseLong(assignments.getSoleAssignment().getValue());
/*  73:    */         }
/*  74:    */       }
/*  75:    */     }
/*  76: 79 */     return recordCount;
/*  77:    */   }
/*  78:    */   
/*  79:    */   private EQL executeQuery(PortletRequest request, SemanticView view, Query eqlQuery, QueryState queryState)
/*  80:    */     throws DataSourceException
/*  81:    */   {
/*  82: 92 */     queryState.addFunction(new LQLQueryConfig(eqlQuery), view, request.getLocale());
/*  83: 93 */     if (logger.isDebugEnabled()) {
/*  84:    */       try
/*  85:    */       {
/*  86: 95 */         logger.debug(DiscoveryServiceUtil.toSoap(this.dataSource.createDiscoveryServiceQuery(queryState)));
/*  87:    */       }
/*  88:    */       catch (Exception e)
/*  89:    */       {
/*  90: 97 */         logger.error(e);
/*  91:    */       }
/*  92:    */     }
/*  93:100 */     QueryResults results = this.dataSource.execute(queryState);
/*  94:101 */     Results result = results.getDiscoveryServiceResults();
/*  95:    */     
/*  96:103 */     return DiscoveryServiceUtil.getEQL(result);
/*  97:    */   }
/*  98:    */   
/*  99:    */   public List<com.endeca.portal.data.model.Record> parseEQL(EQL eql, SemanticView view)
/* 100:    */   {
/* 101:114 */     List<com.endeca.portal.data.model.Record> recordList = new ArrayList();
/* 102:    */     
/* 103:116 */     List<ResultRecords> resultRecordsList = eql.getResultRecords();
/* 104:117 */     if ((resultRecordsList == null) || (resultRecordsList.isEmpty())) {
/* 105:118 */       return recordList;
/* 106:    */     }
/* 107:121 */     for (int i = 0; i < resultRecordsList.size(); i++)
/* 108:    */     {
/* 109:122 */       ResultRecords records = (ResultRecords)resultRecordsList.get(i);
/* 110:123 */       for (com.endeca.mdex.conversation.Record record : records.getRecord())
/* 111:    */       {
/* 112:124 */         SimpleRecord simpleRecord = (SimpleRecord)ModelUtil.convert(record, view);
/* 113:125 */         recordList.add(simpleRecord);
/* 114:    */       }
/* 115:    */     }
/* 116:128 */     return recordList;
/* 117:    */   }
/* 118:    */   
/* 119:    */   public void setDataSource(DataSource dataSource)
/* 120:    */   {
/* 121:132 */     this.dataSource = dataSource;
/* 122:    */   }
/* 123:    */   
/* 124:    */   public DataSource getDataSource()
/* 125:    */     throws DataSourceException
/* 126:    */   {
/* 127:136 */     return this.dataSource;
/* 128:    */   }
/* 129:    */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.service.SummarizationBarProcessor
 * JD-Core Version:    0.7.0.1
 */