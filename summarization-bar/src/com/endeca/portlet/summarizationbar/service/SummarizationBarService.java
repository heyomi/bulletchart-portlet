 package com.endeca.portlet.summarizationbar.service;
 
 import com.endeca.portal.attributes.model.PropertyType;
 import com.endeca.portal.attributes.viewmodel.Attribute;
 import com.endeca.portal.data.DataSource;
 import com.endeca.portal.data.DataSourceException;
 import com.endeca.portal.data.KeyNotFoundException;
 import com.endeca.portal.data.MDEXState;
 import com.endeca.portal.data.SemanticView;
/*  10:    */ import com.endeca.portal.data.functions.util.DateFilterSpec;
/*  11:    */ import com.endeca.portal.data.functions.util.DateFilterUtil;
/*  12:    */ import com.endeca.portal.data.functions.util.DatePart;
/*  13:    */ import com.endeca.portal.data.model.Assignment;
/*  14:    */ import com.endeca.portal.data.model.Assignments;
/*  15:    */ import com.endeca.portal.data.model.ModelException;
/*  16:    */ import com.endeca.portal.data.model.Record;
/*  17:    */ import com.endeca.portal.format.DateTimeFormatter;
/*  18:    */ import com.endeca.portal.format.FormatSettings.Type;
/*  19:    */ import com.endeca.portal.format.Formatter;
/*  20:    */ import com.endeca.portal.format.Formatter.FormatParseException;
/*  21:    */ import com.endeca.portal.format.FormattingUtils;
/*  22:    */ import com.endeca.portal.lql.LQLBuilder.Function;
/*  23:    */ import com.endeca.portal.lql.LQLException;
/*  24:    */ import com.endeca.portal.session.UserSession;
/*  25:    */ import com.endeca.portal.session.UserSessionException;
/*  26:    */ import com.endeca.portlet.summarizationbar.SummarizationBarEnum.ConditionOperator;
/*  27:    */ import com.endeca.portlet.summarizationbar.SummarizationBarEnum.ConditionType;
/*  28:    */ import com.endeca.portlet.summarizationbar.SummarizationBarEnum.SummarizationType;
/*  29:    */ import com.endeca.portlet.summarizationbar.SummarizationBarObjectMapper;
/*  30:    */ import com.endeca.portlet.summarizationbar.model.edit.AlertConditionEntity;
/*  31:    */ import com.endeca.portlet.summarizationbar.model.edit.AlertConfig;
/*  32:    */ import com.endeca.portlet.summarizationbar.model.edit.AlertDisplayColumnEntity;
/*  33:    */ import com.endeca.portlet.summarizationbar.model.edit.ConditionEntity;
/*  34:    */ import com.endeca.portlet.summarizationbar.model.edit.ConditionsConfig;
/*  35:    */ import com.endeca.portlet.summarizationbar.model.edit.DimensionEntity;
/*  36:    */ import com.endeca.portlet.summarizationbar.model.edit.EditModel;
/*  37:    */ import com.endeca.portlet.summarizationbar.model.edit.InfoConfig;
/*  38:    */ import com.endeca.portlet.summarizationbar.model.edit.ItemConfig;
/*  39:    */ import com.endeca.portlet.summarizationbar.model.edit.MetricEntity;
/*  40:    */ import com.endeca.portlet.summarizationbar.model.edit.SummarizationConfig;
/*  41:    */ import com.endeca.portlet.summarizationbar.model.view.AlertColumnEntity;
/*  42:    */ import com.endeca.portlet.summarizationbar.model.view.AlertValueEntity;
/*  43:    */ import com.endeca.portlet.summarizationbar.model.view.Summarization;
/*  44:    */ import com.endeca.portlet.util.EndecaPortletUtil;
/*  45:    */ import com.endeca.portlet.util.LanguageUtils;
/*  46:    */ import com.liferay.portal.util.PortalUtil;
/*  47:    */ import java.io.IOException;
/*  48:    */ import java.util.ArrayList;
/*  49:    */ import java.util.Arrays;
/*  50:    */ import java.util.Date;
/*  51:    */ import java.util.HashMap;
/*  52:    */ import java.util.Iterator;
/*  53:    */ import java.util.List;
/*  54:    */ import java.util.Map;
/*  55:    */ import javax.portlet.PortletException;
/*  56:    */ import javax.portlet.PortletRequest;
/*  57:    */ import org.apache.commons.lang.StringUtils;
/*  58:    */ import org.apache.log4j.Logger;
/*  59:    */ 
/*  60:    */ public class SummarizationBarService
/*  61:    */ {
/*  62:    */   private PortletRequest request;
/*  63:    */   private DataSource dataSource;
/*  64:    */   private SummarizationBarProcessor processor;
/*  65: 66 */   private static final Logger logger = Logger.getLogger(SummarizationBarService.class);
/*  66:    */   
/*  67:    */   public SummarizationBarService() {}
/*  68:    */   
/*  69:    */   public SummarizationBarService(PortletRequest request, DataSource dataSource)
/*  70:    */   {
/*  71: 71 */     this.request = request;
/*  72: 72 */     this.dataSource = dataSource;
/*  73: 73 */     this.processor = new SummarizationBarProcessor(dataSource);
/*  74:    */   }
/*  75:    */   
/*  76:    */   public List<Summarization> getSummarizationList(EditModel editModel)
/*  77:    */     throws IOException, PortletException, DataSourceException, LQLException, ModelException, UserSessionException, Formatter.FormatParseException, KeyNotFoundException
/*  78:    */   {
/*  79: 78 */     List<Summarization> summarizations = new ArrayList();
/*  80: 80 */     for (SummarizationConfig config : editModel.getConfigs()) {
/*  81: 81 */       if (config.getIsCompleted())
/*  82:    */       {
/*  83: 85 */         Summarization summarization = getSummarization(config);
/*  84: 86 */         if ((summarization != null) && (summarization.getValue() != null)) {
/*  85: 87 */           summarizations.add(summarization);
/*  86:    */         }
/*  87:    */       }
/*  88:    */     }
/*  89: 91 */     return summarizations;
/*  90:    */   }
/*  91:    */   
/*  92:    */   public Summarization getSummarizationInstance(SummarizationConfig config)
/*  93:    */   {
/*  94: 95 */     Summarization summarization = new Summarization();
/*  95: 96 */     ItemConfig itemConfig = config.getItemConfig();
/*  96: 97 */     AlertConfig alertConfig = config.getAlertConfig();
/*  97: 98 */     summarization.setId(config.getId());
/*  98: 99 */     summarization.setType(config.getType());
/*  99:102 */     if (SummarizationBarUtils.isAlertSummarizationType(config.getType()))
/* 100:    */     {
/* 101:103 */       if (alertConfig.getIsCustomWidth()) {
/* 102:104 */         summarization.setWidth(alertConfig.getCustomWidth());
/* 103:    */       }
/* 104:106 */       summarization.setDescription(alertConfig.getDescription());
/* 105:107 */       summarization.setAlertColor(alertConfig.getColor());
/* 106:108 */       summarization.setName(alertConfig.getDisplayName());
/* 107:    */     }
/* 108:    */     else
/* 109:    */     {
/* 110:111 */       if (SummarizationBarUtils.isDimensionSummarizationType(config.getType())) {
/* 111:112 */         summarization.setDimensionKey(itemConfig.getDimensionKey());
/* 112:    */       }
/* 113:114 */       summarization.setName(getSummarizationName(config));
/* 114:115 */       summarization.setAction(itemConfig.getAction());
/* 115:116 */       if (itemConfig.getIsCustomWidth()) {
/* 116:117 */         summarization.setWidth(itemConfig.getCustomWidth());
/* 117:    */       }
/* 118:119 */       summarization.setDescription(itemConfig.getDescription());
/* 119:    */     }
/* 120:122 */     return summarization;
/* 121:    */   }
/* 122:    */   
/* 123:    */   public Summarization getSummarization(SummarizationConfig config)
/* 124:    */     throws DataSourceException, LQLException, PortletException, ModelException, Formatter.FormatParseException, KeyNotFoundException
/* 125:    */   {
/* 126:126 */     if (SummarizationBarUtils.isAlertSummarizationType(config.getType()))
/* 127:    */     {
/* 128:127 */       long alertsRecordCount = this.processor.getAlertsRecordCount(this.request, config);
/* 129:128 */       if (alertsRecordCount == 0L) {
/* 130:129 */         return null;
/* 131:    */       }
/* 132:132 */       List<Record> records = this.processor.getStateRecords(this.request, config);
/* 133:133 */       Summarization summarization = getSummarizationInstance(config);
/* 134:134 */       mergeAlertValues(config, summarization, records, alertsRecordCount);
/* 135:135 */       return summarization;
/* 136:    */     }
/* 137:138 */     List<Record> records = this.processor.getStateRecords(this.request, config);
/* 138:139 */     if (records.size() == 0) {
/* 139:140 */       return null;
/* 140:    */     }
/* 141:142 */     Summarization summarization = getSummarizationInstance(config);
/* 142:143 */     mergeSummaryValues(config, summarization, records);
/* 143:144 */     applySummaryCondition(summarization, config);
/* 144:    */     
/* 145:146 */     return summarization;
/* 146:    */   }
/* 147:    */   
/* 148:    */   private void mergeAlertValues(SummarizationConfig config, Summarization summarization, List<Record> records, long alertsRecordCount)
/* 149:    */     throws ModelException, KeyNotFoundException, DataSourceException, LQLException, PortletException, Formatter.FormatParseException
/* 150:    */   {
/* 151:152 */     AlertConfig alertConfig = config.getAlertConfig();
/* 152:153 */     SemanticView semanticView = this.dataSource.getCollectionOrSemanticView(config.getSemanticKey(), this.request.getLocale());
/* 153:    */     
/* 154:    */ 
/* 155:156 */     AlertDisplayColumnEntity alertDisplayColumnEntity = (AlertDisplayColumnEntity)alertConfig.getDisplayColumnsList().get(0);
/* 156:157 */     AlertColumnEntity alertColumnEntity = new AlertColumnEntity();
/* 157:    */     
/* 158:159 */     List<String> dimensionKeys = new ArrayList();
/* 159:160 */     List<String> dimensionFormatters = new ArrayList();
/* 160:161 */     for (DimensionEntity dimensionEntity : alertConfig.getDimensionsList())
/* 161:    */     {
/* 162:162 */       dimensionKeys.add(dimensionEntity.getKey());
/* 163:163 */       dimensionFormatters.add(dimensionEntity.getFormatter());
/* 164:    */     }
/* 165:166 */     List<AlertColumnEntity> alertDimensionColumns = new ArrayList();
/* 166:167 */     alertColumnEntity.setId(alertDisplayColumnEntity.getId());
/* 167:168 */     alertColumnEntity.setName(alertDisplayColumnEntity.getName());
/* 168:169 */     alertDimensionColumns.add(alertColumnEntity);
/* 169:    */     
/* 170:    */ 
/* 171:    */ 
/* 172:173 */     List<AlertColumnEntity> alertConditionColumns = new ArrayList();
/* 173:174 */     List<String> conditioalKeyColumns = new ArrayList();
/* 174:175 */     List<String> conditioalKeyPrefixColumns = new ArrayList();
/* 175:176 */     List<String> conditioalFormaterColumns = new ArrayList();
/* 176:177 */     AlertConditionEntity alertConditionEntity = null;
/* 177:178 */     for (int i = 1; i < alertConfig.getDisplayColumnsList().size(); i++)
/* 178:    */     {
/* 179:179 */       alertDisplayColumnEntity = (AlertDisplayColumnEntity)alertConfig.getDisplayColumnsList().get(i);
/* 180:180 */       alertConditionEntity = (AlertConditionEntity)alertConfig.getConditonsList().get(i - 1);
/* 181:181 */       if ((alertDisplayColumnEntity.getIsDisplay()) && (alertConditionEntity.isValid(alertConditionEntity.getType(), alertConditionEntity.getMetric(), alertConditionEntity.getTargetMetric())))
/* 182:    */       {
/* 183:182 */         conditioalKeyColumns.add("_$METRIC_" + alertConditionEntity.getId() + alertConditionEntity.getMetric().getKey());
/* 184:183 */         conditioalKeyPrefixColumns.add("_$METRIC_" + alertConditionEntity.getId());
/* 185:184 */         if (SummarizationBarEnum.ConditionType.SelfValue.equals(alertConditionEntity.getType())) {
/* 186:185 */           conditioalFormaterColumns.add(alertConditionEntity.getMetric().getFormatter());
/* 187:    */         } else {
/* 188:187 */           conditioalFormaterColumns.add(StringUtils.isBlank(alertConditionEntity.getRatioFormatter()) ? FormatSettings.Type.PERCENTAGE.toString() : alertConditionEntity.getRatioFormatter());
/* 189:    */         }
/* 190:191 */         alertColumnEntity = new AlertColumnEntity();
/* 191:192 */         alertColumnEntity.setId(alertDisplayColumnEntity.getId());
/* 192:193 */         alertColumnEntity.setName(alertDisplayColumnEntity.getName());
/* 193:194 */         alertConditionColumns.add(alertColumnEntity);
/* 194:    */       }
/* 195:    */     }
/* 196:199 */     Formatter formatter = null;
/* 197:    */     
/* 198:201 */     List<AlertValueEntity> alertDimensionValues = new ArrayList();
/* 199:202 */     List<AlertValueEntity> alertConditionValues = new ArrayList();
/* 200:    */     
/* 201:204 */     List<String> conditionKeyList = null;
/* 202:205 */     List<String> conditionIdList = null;
/* 203:206 */     List<String> conditionValueList = null;
/* 204:207 */     List<String> conditionFormatValueList = null;
/* 205:    */     
/* 206:209 */     List<String> dimensionKeyList = null;
/* 207:210 */     List<String> dimensionValueList = null;
/* 208:211 */     List<String> dimensionFormatValueList = null;
/* 209:213 */     for (Record record : records)
/* 210:    */     {
/* 211:215 */       conditionKeyList = Arrays.asList(new String[conditioalKeyColumns.size()]);
/* 212:216 */       conditionIdList = Arrays.asList(new String[conditioalKeyColumns.size()]);
/* 213:217 */       conditionValueList = Arrays.asList(new String[conditioalKeyColumns.size()]);
/* 214:218 */       conditionFormatValueList = Arrays.asList(new String[conditioalKeyColumns.size()]);
/* 215:    */       
/* 216:220 */       dimensionKeyList = Arrays.asList(new String[dimensionKeys.size()]);
/* 217:221 */       dimensionValueList = Arrays.asList(new String[dimensionKeys.size()]);
/* 218:222 */       dimensionFormatValueList = Arrays.asList(new String[dimensionKeys.size()]);
/* 219:    */       
/* 220:224 */       Assignment assignment = null;
/* 221:225 */       String attributeKey = null;
/* 222:226 */       String conditionId = null;
/* 223:227 */       for (String attrKey : record.getAttributeKeys())
/* 224:    */       {
/* 225:228 */         assignment = record.getAssignments(attrKey).getSoleAssignment();
/* 226:230 */         if (attrKey.startsWith("_$METRIC_"))
/* 227:    */         {
/* 228:231 */           int keyIndex = conditioalKeyColumns.indexOf(attrKey);
/* 229:232 */           if (keyIndex != -1)
/* 230:    */           {
/* 231:233 */             attributeKey = attrKey.replace((CharSequence)conditioalKeyPrefixColumns.get(keyIndex), "");
/* 232:234 */             conditionId = attrKey.replace((CharSequence)conditioalKeyPrefixColumns.get(keyIndex), "_$METRIC_");
/* 233:235 */             conditionKeyList.set(keyIndex, attributeKey);
/* 234:236 */             conditionIdList.set(keyIndex, conditionId);
/* 235:237 */             conditionValueList.set(keyIndex, assignment.getValue());
/* 236:238 */             if (FormatSettings.Type.PERCENTAGE.toString().equals(conditioalFormaterColumns.get(keyIndex)))
/* 237:    */             {
/* 238:239 */               formatter = FormattingUtils.createFormatter(this.request, assignment.getType(), (String)null);
/* 239:240 */               formatter.setType(FormatSettings.Type.PERCENTAGE);
/* 240:    */             }
/* 241:    */             else
/* 242:    */             {
/* 243:242 */               formatter = FormattingUtils.createFormatter(this.request, assignment.getType(), (String)conditioalFormaterColumns.get(keyIndex));
/* 244:    */             }
/* 245:244 */             conditionFormatValueList.set(keyIndex, formatter.format(assignment.getValue()));
/* 246:    */           }
/* 247:    */         }
/* 248:247 */         else if (attrKey.contains("_&DATEPART_"))
/* 249:    */         {
/* 250:248 */           attributeKey = attrKey.split("_&DATEPART_")[0];
/* 251:249 */           int dimIndex = dimensionKeys.indexOf(attributeKey);
/* 252:251 */           if (!StringUtils.isNotBlank((String)dimensionKeyList.get(dimIndex)))
/* 253:    */           {
/* 254:254 */             DateFilterSpec dateFilterSpec = new DateFilterSpec();
/* 255:255 */             List<DatePart> dateParts = SummarizationBarUtils.getDateParts(config, attributeKey);
/* 256:256 */             if (dateParts == null)
/* 257:    */             {
/* 258:257 */               Attribute attribute = (Attribute)semanticView.getAllAttributes().get(attributeKey);
/* 259:258 */               dateParts = DateFilterUtil.getDefaultCascadeDateParts(attribute);
/* 260:    */             }
/* 261:260 */             for (Iterator i$ = dateParts.iterator(); i$.hasNext();)
/* 262:    */             {
/* 263:260 */               datePart = (DatePart)i$.next();
/* 264:261 */               for (Assignment a : record.getAssignments(attributeKey + "_&DATEPART_" + datePart.getDatePart())) {
/* 265:262 */                 dateFilterSpec.setDatePartValue(datePart, Integer.parseInt(a.getValue()));
/* 266:    */               }
/* 267:    */             }
/* 268:    */             DatePart datePart;
/* 269:266 */             dimensionKeyList.set(dimIndex, attributeKey);
/* 270:267 */             dimensionValueList.set(dimIndex, SummarizationBarObjectMapper.toString(dateFilterSpec));
/* 271:268 */             DateTimeFormatter dateTimeformatter = (DateTimeFormatter)FormattingUtils.createFormatter(this.request, PropertyType.DATETIME, (String)dimensionFormatters.get(dimensionKeys.indexOf(attributeKey)));
/* 272:269 */             dimensionFormatValueList.set(dimIndex, dateTimeformatter.format(dateFilterSpec));
/* 273:    */           }
/* 274:    */         }
/* 275:271 */         else if ((attrKey.startsWith("_$GROUP_BY_ALIAS_")) || (SummarizationBarUtils.getGroupBys(null, config, semanticView).contains(attrKey)))
/* 276:    */         {
/* 277:272 */           attributeKey = attrKey.replace("_$GROUP_BY_ALIAS_", "");
/* 278:273 */           if (dimensionKeys.contains(attributeKey))
/* 279:    */           {
/* 280:274 */             int dimIndex = dimensionKeys.indexOf(attributeKey);
/* 281:275 */             dimensionKeyList.set(dimIndex, attributeKey);
/* 282:276 */             dimensionValueList.set(dimIndex, assignment.getNavigableValue());
/* 283:277 */             formatter = FormattingUtils.createFormatter(this.request, assignment.getType(), (String)dimensionFormatters.get(dimIndex));
/* 284:278 */             dimensionFormatValueList.set(dimIndex, formatter.format(assignment.getValue()));
/* 285:    */           }
/* 286:    */         }
/* 287:    */       }
/* 288:283 */       AlertValueEntity alertCoditionalEntity = new AlertValueEntity();
/* 289:284 */       alertCoditionalEntity.setIds(conditionIdList);
/* 290:285 */       alertCoditionalEntity.setAttributeKeys(conditionKeyList);
/* 291:286 */       alertCoditionalEntity.setValues(conditionValueList);
/* 292:287 */       alertCoditionalEntity.setFormattedValues(conditionFormatValueList);
/* 293:    */       
/* 294:    */ 
/* 295:290 */       AlertValueEntity alertDimensionEntity = new AlertValueEntity();
/* 296:291 */       alertDimensionEntity.setAttributeKeys(dimensionKeyList);
/* 297:292 */       alertDimensionEntity.setValues(dimensionValueList);
/* 298:293 */       alertDimensionEntity.setFormattedValues(dimensionFormatValueList);
/* 299:    */       
/* 300:295 */       alertConditionValues.add(alertCoditionalEntity);
/* 301:296 */       alertDimensionValues.add(alertDimensionEntity);
/* 302:    */     }
/* 303:300 */     summarization.setValue(String.valueOf(alertsRecordCount));
/* 304:301 */     formatter = FormattingUtils.createFormatter(this.request, PropertyType.INT, (String)null);
/* 305:302 */     summarization.setFormattedValue(formatter.format(String.valueOf(alertsRecordCount)));
/* 306:    */     
/* 307:304 */     summarization.setAlertConditionColumns(alertConditionColumns);
/* 308:305 */     summarization.setAlertConditionValues(alertConditionValues);
/* 309:    */     
/* 310:307 */     summarization.setAlertDimensionColumns(alertDimensionColumns);
/* 311:308 */     summarization.setAlertDimensionValues(alertDimensionValues);
/* 312:    */   }
/* 313:    */   
/* 314:    */   private void mergeSummaryValues(SummarizationConfig config, Summarization summarization, List<Record> records)
/* 315:    */     throws ModelException, Formatter.FormatParseException, KeyNotFoundException, DataSourceException, LQLException, PortletException
/* 316:    */   {
/* 317:315 */     Record record = (Record)records.get(0);
/* 318:316 */     ItemConfig itemConfig = config.getItemConfig();
/* 319:318 */     for (String attrKey : record.getAttributeKeys())
/* 320:    */     {
/* 321:319 */       Assignment assignment = record.getAssignments(attrKey).getSoleAssignment();
/* 322:321 */       if (attrKey.equals("_$METRIC__$CONDITION_TARTET_VALUE"))
/* 323:    */       {
/* 324:322 */         Formatter formatter = FormattingUtils.createFormatter(this.request, assignment.getType(), config.getConditionsConfig().getTargetMetricFormatter());
/* 325:323 */         summarization.setConditionTargetValue(formatter.format(assignment.getValue()));
/* 326:    */       }
/* 327:325 */       else if (attrKey.equals("_$METRIC__$CONDITION_VALUE"))
/* 328:    */       {
/* 329:326 */         summarization.setConditionValue(assignment.getValue());
/* 330:    */       }
/* 331:328 */       else if (attrKey.startsWith("_$METRIC_"))
/* 332:    */       {
/* 333:329 */         Formatter formatter = FormattingUtils.createFormatter(this.request, assignment.getType(), itemConfig.getMetricFormatter());
/* 334:330 */         if (!SummarizationBarUtils.isDimensionSummarizationType(config.getType()))
/* 335:    */         {
/* 336:331 */           summarization.setFormattedValue(formatter.format(assignment.getValue()));
/* 337:332 */           summarization.setValue(assignment.getValue());
/* 338:333 */           summarization.setMetricValue(assignment.getValue());
/* 339:    */         }
/* 340:    */         else
/* 341:    */         {
/* 342:335 */           summarization.setFormattedMetricValue(formatter.format(assignment.getValue()));
/* 343:336 */           summarization.setMetricValue(assignment.getValue());
/* 344:    */         }
/* 345:339 */         if (SummarizationBarEnum.ConditionType.SelfValue.equals(SummarizationBarUtils.getConditionType(config.getConditionsConfig()))) {
/* 346:340 */           summarization.setConditionValue(assignment.getValue());
/* 347:    */         }
/* 348:    */       }
/* 349:342 */       else if (SummarizationBarUtils.isDimensionSummarizationType(config.getType()))
/* 350:    */       {
/* 351:343 */         if (attrKey.contains("_&DATEPART_"))
/* 352:    */         {
/* 353:344 */           if (!StringUtils.isNotBlank(summarization.getValue()))
/* 354:    */           {
/* 355:347 */             String attributeKey = attrKey.split("_&DATEPART_")[0];
/* 356:348 */             DateFilterSpec dateFilterSpec = new DateFilterSpec();
/* 357:349 */             List<DatePart> dateParts = SummarizationBarUtils.getDateParts(config, attributeKey);
/* 358:350 */             if (dateParts == null)
/* 359:    */             {
/* 360:351 */               SemanticView semanticView = this.dataSource.getCollectionOrSemanticView(config.getSemanticKey(), this.request.getLocale());
/* 361:352 */               Attribute attribute = (Attribute)semanticView.getAllAttributes().get(attributeKey);
/* 362:353 */               dateParts = DateFilterUtil.getDefaultCascadeDateParts(attribute);
/* 363:    */             }
/* 364:355 */             for (Iterator i$ = dateParts.iterator(); i$.hasNext();)
/* 365:    */             {
/* 366:355 */               datePart = (DatePart)i$.next();
/* 367:356 */               for (Assignment a : record.getAssignments(attributeKey + "_&DATEPART_" + datePart.getDatePart())) {
/* 368:357 */                 dateFilterSpec.setDatePartValue(datePart, Integer.parseInt(a.getValue()));
/* 369:    */               }
/* 370:    */             }
/* 371:    */             DatePart datePart;
/* 372:361 */             summarization.setValue(SummarizationBarObjectMapper.toString(dateFilterSpec));
/* 373:362 */             DateTimeFormatter dateTimeformatter = (DateTimeFormatter)FormattingUtils.createFormatter(this.request, PropertyType.DATETIME, itemConfig.getDimensionFormatter());
/* 374:363 */             summarization.setFormattedValue(dateTimeformatter.format(dateFilterSpec));
/* 375:    */           }
/* 376:    */         }
/* 377:365 */         else if ((attrKey.startsWith("_$GROUP_BY_ALIAS_")) || (attrKey.equals(itemConfig.getDimensionKey())))
/* 378:    */         {
/* 379:366 */           summarization.setValue(assignment.getNavigableValue());
/* 380:367 */           Formatter formatter = FormattingUtils.createFormatter(this.request, assignment.getType(), itemConfig.getDimensionFormatter());
/* 381:368 */           summarization.setFormattedValue(formatter.format(assignment.getValue()));
/* 382:    */         }
/* 383:    */       }
/* 384:    */     }
/* 385:    */   }
/* 386:    */   
/* 387:    */   public <T> T getActionModel(SummarizationConfig summarizationConfig, Class<T> clazz)
/* 388:    */   {
/* 389:375 */     if ((summarizationConfig == null) || (summarizationConfig.getItemConfig().getAction() == null)) {
/* 390:376 */       return null;
/* 391:    */     }
/* 392:378 */     return clazz.cast(summarizationConfig.getItemConfig().getAction());
/* 393:    */   }
/* 394:    */   
/* 395:    */   private void applySummaryCondition(Summarization summarization, SummarizationConfig config)
/* 396:    */     throws DataSourceException
/* 397:    */   {
/* 398:383 */     ConditionsConfig conditionsCfg = config.getConditionsConfig();
/* 399:    */     
/* 400:385 */     String styleGroupId = conditionsCfg.getUsedStyleGroupId();
/* 401:    */     
/* 402:387 */     summarization.setIsMetricTextBlack(Boolean.valueOf(conditionsCfg.getIsMetricTextBlack()));
/* 403:    */     
/* 404:    */ 
/* 405:    */ 
/* 406:    */ 
/* 407:392 */     List<ConditionEntity> validConditions = conditionsCfg.getValidConditions(config.getItemConfig().getMetricKey());
/* 408:393 */     if (validConditions.size() == 0) {
/* 409:394 */       return;
/* 410:    */     }
/* 411:401 */     boolean isMeetsCondition = false;
/* 412:    */     
/* 413:403 */     double target = Double.parseDouble(summarization.getConditionValue());
/* 414:406 */     for (ConditionEntity entity : validConditions)
/* 415:    */     {
/* 416:408 */       isMeetsCondition = entity.checkMeetsCondition(summarization, conditionsCfg, target);
/* 417:409 */       if (isMeetsCondition)
/* 418:    */       {
/* 419:410 */         summarization.setConditionType(conditionsCfg.getType().toString());
/* 420:411 */         summarization.setStyleGroupId(styleGroupId);
/* 421:412 */         summarization.setStyleId(entity.getStyleId());
/* 422:413 */         summarization.setConditionTooltip(entity.getTooltip());
/* 423:414 */         break;
/* 424:    */       }
/* 425:    */     }
/* 426:421 */     if ((conditionsCfg.getIsMeetDispOnly()) && (!isMeetsCondition)) {
/* 427:422 */       summarization.setIsDisplay(false);
/* 428:    */     }
/* 429:    */   }
/* 430:    */   
/* 431:    */   public SummarizationConfig getCountOneSummarizationConfig()
/* 432:    */     throws PortletException, DataSourceException, IOException
/* 433:    */   {
/* 434:428 */     SummarizationConfig config = new SummarizationConfig();
/* 435:429 */     config.setId(String.valueOf(System.currentTimeMillis()));
/* 436:430 */     config.setDataSourceId(this.dataSource.getId());
/* 437:431 */     config.setSemanticKey(EndecaPortletUtil.getConfiguredViewKey(this.request));
/* 438:    */     
/* 439:433 */     config.setIsCompleted(true);
/* 440:434 */     config.setType(SummarizationBarEnum.SummarizationType.MetricSingle);
/* 441:    */     
/* 442:436 */     ItemConfig itemConfig = new ItemConfig();
/* 443:437 */     itemConfig.setCustomDisplayName(LanguageUtils.getMessage(this.request, "summarizationbar.edit.aggre-methods.count"));
/* 444:438 */     itemConfig.setMetricKey("system-metrics-count_1");
/* 445:    */     
/* 446:440 */     config.setName(LanguageUtils.getMessage(this.request, "summarizationbar.edit.aggre-methods.count"));
/* 447:441 */     config.setItemConfig(itemConfig);
/* 448:442 */     config.setInfoConfig(new InfoConfig());
/* 449:    */     
/* 450:444 */     ConditionsConfig conditionsConfig = new ConditionsConfig();
/* 451:445 */     conditionsConfig.setType(SummarizationBarEnum.ConditionType.SelfValue);
/* 452:446 */     conditionsConfig.setUsedStyleGroupId("icon_stoplight_sprite");
/* 453:447 */     List<ConditionEntity> conditionEntityList = new ArrayList();
/* 454:448 */     conditionEntityList.add(getDefaultConditionEntity());
/* 455:449 */     conditionsConfig.setConditions(conditionEntityList);
/* 456:450 */     config.setConditionsConfig(conditionsConfig);
/* 457:    */     
/* 458:452 */     config.setAlertConfig(new AlertConfig());
/* 459:453 */     config.getAlertConfig().setColor("red");
/* 460:    */     
/* 461:455 */     AlertDisplayColumnEntity dimensionsColumn = new AlertDisplayColumnEntity();
/* 462:456 */     dimensionsColumn.setId("dimensions");
/* 463:457 */     config.getAlertConfig().getDisplayColumnsList().add(dimensionsColumn);
/* 464:    */     
/* 465:459 */     AlertConditionEntity defaultCondition = new AlertConditionEntity();
/* 466:460 */     defaultCondition.setId("condition_" + new Date().getTime());
/* 467:461 */     defaultCondition.setType(SummarizationBarEnum.ConditionType.SelfValue);
/* 468:462 */     defaultCondition.setOperator(SummarizationBarEnum.ConditionOperator.GT);
/* 469:463 */     config.getAlertConfig().getConditonsList().add(defaultCondition);
/* 470:    */     
/* 471:465 */     return config;
/* 472:    */   }
/* 473:    */   
/* 474:    */   public ConditionEntity getDefaultConditionEntity()
/* 475:    */   {
/* 476:469 */     ConditionEntity entity = new ConditionEntity();
/* 477:470 */     entity.setStyleId(String.valueOf(0));
/* 478:471 */     entity.setOperator(SummarizationBarEnum.ConditionOperator.GT);
/* 479:472 */     entity.setTooltip("");
/* 480:473 */     return entity;
/* 481:    */   }
/* 482:    */   
/* 483:    */   public SummarizationConfig getSummarizationConfig(EditModel editModel, String summarizationId)
/* 484:    */     throws PortletException, IOException
/* 485:    */   {
/* 486:478 */     if (editModel == null) {
/* 487:479 */       return null;
/* 488:    */     }
/* 489:481 */     List<SummarizationConfig> summarizationConfigs = editModel.getConfigs();
/* 490:482 */     if (summarizationConfigs.isEmpty()) {
/* 491:483 */       return null;
/* 492:    */     }
/* 493:485 */     SummarizationConfig summarizationConfig = null;
/* 494:487 */     for (SummarizationConfig config : summarizationConfigs) {
/* 495:488 */       if (config.getId().equals(summarizationId))
/* 496:    */       {
/* 497:489 */         summarizationConfig = config;
/* 498:490 */         break;
/* 499:    */       }
/* 500:    */     }
/* 501:493 */     return summarizationConfig;
/* 502:    */   }
/* 503:    */   
/* 504:    */   public boolean hasCompletedConfig(List<SummarizationConfig> configs)
/* 505:    */   {
/* 506:497 */     for (SummarizationConfig config : configs) {
/* 507:498 */       if (config.getIsCompleted()) {
/* 508:499 */         return true;
/* 509:    */       }
/* 510:    */     }
/* 511:502 */     return false;
/* 512:    */   }
/* 513:    */   
/* 514:    */   public String getSummarizationName(SummarizationConfig config)
/* 515:    */   {
/* 516:506 */     if ((SummarizationBarUtils.isAlertSummarizationType(config.getType())) || (config.getItemConfig().getIsCustomDisplayName())) {
/* 517:508 */       return config.getName();
/* 518:    */     }
/* 519:511 */     HashMap<String, Attribute> attributes = null;
/* 520:    */     try
/* 521:    */     {
/* 522:514 */       attributes = this.dataSource.getCollectionOrSemanticView(config.getSemanticKey(), this.request.getLocale()).getAttributes();
/* 523:    */     }
/* 524:    */     catch (KeyNotFoundException e)
/* 525:    */     {
/* 526:516 */       logger.error("getSummarizationName : KeyNotFoundException, failed to get attributes.");
/* 527:    */     }
/* 528:    */     catch (DataSourceException e)
/* 529:    */     {
/* 530:518 */       logger.error("getSummarizationName : DataSourceException, failed to get attributes.");
/* 531:    */     }
/* 532:521 */     String summarizationName = "";
/* 533:522 */     String metricName = "";
/* 534:523 */     String dimensionName = "";
/* 535:525 */     if (attributes != null)
/* 536:    */     {
/* 537:526 */       ItemConfig itemConfig = config.getItemConfig();
/* 538:527 */       Attribute metric = (Attribute)attributes.get(itemConfig.getMetricKey());
/* 539:528 */       if (metric != null) {
/* 540:529 */         metricName = StringUtils.isNotEmpty(metric.getDisplayName()) ? metric.getDisplayName() : metric.getKey();
/* 541:    */       }
/* 542:531 */       metricName = getDisplayNameWithAggre(metricName, itemConfig.getMetricKey(), itemConfig.getMetricAggre());
/* 543:533 */       if (SummarizationBarEnum.SummarizationType.MetricSingle.equals(config.getType()))
/* 544:    */       {
/* 545:534 */         summarizationName = metricName;
/* 546:    */       }
/* 547:536 */       else if (SummarizationBarEnum.SummarizationType.DimensionSingle.equals(config.getType()))
/* 548:    */       {
/* 549:537 */         if (StringUtils.isNotEmpty(itemConfig.getDimensionKey()))
/* 550:    */         {
/* 551:538 */           Attribute dimension = (Attribute)attributes.get(itemConfig.getDimensionKey());
/* 552:539 */           if (dimension != null)
/* 553:    */           {
/* 554:540 */             dimensionName = StringUtils.isNotEmpty(dimension.getDisplayName()) ? dimension.getDisplayName() : dimension.getKey();
/* 555:541 */             if (PropertyType.DATETIME.equals(dimension.getType()))
/* 556:    */             {
/* 557:542 */               String dateComboStringName = "";
/* 558:543 */               List<DatePart> dateParts = SummarizationBarUtils.getDateParts(config, itemConfig.getDimensionKey());
/* 559:544 */               if (dateParts == null) {
/* 560:545 */                 dateParts = DateFilterUtil.getDefaultCascadeDateParts(dimension);
/* 561:    */               }
/* 562:547 */               for (DatePart datePart : dateParts) {
/* 563:548 */                 dateComboStringName = dateComboStringName + datePart.getDatePart();
/* 564:    */               }
/* 565:550 */               dimensionName = dimensionName + " (" + LanguageUtils.getMessage(this.request, new StringBuilder().append("summarizationbar.datetime.").append(dateComboStringName).toString()) + ")";
/* 566:    */             }
/* 567:    */           }
/* 568:    */         }
/* 569:554 */         if (StringUtils.isNotEmpty(dimensionName))
/* 570:    */         {
/* 571:555 */           Object[] parameters = new Object[2];
/* 572:556 */           parameters[0] = dimensionName;
/* 573:557 */           parameters[1] = metricName;
/* 574:558 */           if (itemConfig.getIsTopValue()) {
/* 575:559 */             summarizationName = LanguageUtils.getMessage(this.request, "summarizationbar.edit.default-summarization-name.dim-value-spotlight.top", parameters);
/* 576:    */           } else {
/* 577:561 */             summarizationName = LanguageUtils.getMessage(this.request, "summarizationbar.edit.default-summarization-name.dim-value-spotlight.bottom", parameters);
/* 578:    */           }
/* 579:    */         }
/* 580:    */         else
/* 581:    */         {
/* 582:564 */           summarizationName = metricName;
/* 583:    */         }
/* 584:    */       }
/* 585:568 */       if (StringUtils.isEmpty(summarizationName)) {
/* 586:569 */         summarizationName = config.getItemConfig().getCustomDisplayName();
/* 587:    */       }
/* 588:    */     }
/* 589:573 */     return summarizationName;
/* 590:    */   }
/* 591:    */   
/* 592:    */   public String getDisplayNameWithAggre(String displayName, String metricKey, LQLBuilder.Function aggre)
/* 593:    */   {
/* 594:577 */     String summarizationName = "";
/* 595:578 */     if ("system-metrics-count_1".equals(metricKey)) {
/* 596:579 */       summarizationName = displayName + LanguageUtils.getMessage(this.request, "summarizationbar.edit.label.record-count");
/* 597:580 */     } else if (LQLBuilder.Function.COUNT.equals(aggre)) {
/* 598:581 */       summarizationName = displayName + " (" + LanguageUtils.getMessage(this.request, "summarizationbar.edit.aggre-methods.count") + ")";
/* 599:582 */     } else if (LQLBuilder.Function.COUNTDISTINCT.equals(aggre)) {
/* 600:583 */       summarizationName = displayName + " (" + LanguageUtils.getMessage(this.request, "summarizationbar.edit.aggre-methods.countdistinct") + ")";
/* 601:584 */     } else if (aggre != null) {
/* 602:585 */       summarizationName = displayName + " (" + aggre.toString().toLowerCase() + ")";
/* 603:    */     }
/* 604:587 */     return summarizationName;
/* 605:    */   }
/* 606:    */   
/* 607:    */   public Map<String, MDEXState> getMDEXStates(PortletRequest request, UserSession session)
/* 608:    */     throws UserSessionException
/* 609:    */   {
/* 610:592 */     Map<String, MDEXState> dataSourceMap = session.getMDEXStatesByApplicationId(PortalUtil.getScopeGroupId(request), request);
/* 611:593 */     if (dataSourceMap == null) {
/* 612:594 */       dataSourceMap = new HashMap();
/* 613:    */     }
/* 614:596 */     return dataSourceMap;
/* 615:    */   }
/* 616:    */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.service.SummarizationBarService
 * JD-Core Version:    0.7.0.1
 */