 package com.endeca.portlet.summarizationbar.service;
 
 import com.endeca.mdex.eql_parser.types.AttributeRefExpression;
 import com.endeca.mdex.eql_parser.types.ExpressionBase;
 import com.endeca.mdex.eql_parser.types.Select;
 import com.endeca.mdex.eql_parser.types.Statement;
 import com.endeca.portal.attributes.model.PropertyType;
 import com.endeca.portal.attributes.viewmodel.Attribute;
 import com.endeca.portal.data.SemanticView;
/*  10:    */ import com.endeca.portal.data.functions.util.DateFilterUtil;
/*  11:    */ import com.endeca.portal.data.functions.util.DatePart;
/*  12:    */ import com.endeca.portal.lql.LQLBuilder;
/*  13:    */ import com.endeca.portal.lql.LQLBuilder.Function;
/*  14:    */ import com.endeca.portlet.summarizationbar.SummarizationBarEnum.ConditionType;
/*  15:    */ import com.endeca.portlet.summarizationbar.SummarizationBarEnum.SummarizationType;
/*  16:    */ import com.endeca.portlet.summarizationbar.model.edit.AlertConfig;
/*  17:    */ import com.endeca.portlet.summarizationbar.model.edit.ConditionsConfig;
/*  18:    */ import com.endeca.portlet.summarizationbar.model.edit.DimensionEntity;
/*  19:    */ import com.endeca.portlet.summarizationbar.model.edit.ItemConfig;
/*  20:    */ import com.endeca.portlet.summarizationbar.model.edit.SummarizationConfig;
/*  21:    */ import java.util.ArrayList;
/*  22:    */ import java.util.HashMap;
/*  23:    */ import java.util.List;
/*  24:    */ import org.apache.commons.lang.StringUtils;
/*  25:    */ 
/*  26:    */ public class SummarizationBarUtils
/*  27:    */ {
/*  28:    */   public static boolean isMetricSummarizationType(SummarizationBarEnum.SummarizationType type)
/*  29:    */   {
/*  30: 31 */     return (SummarizationBarEnum.SummarizationType.MetricSingle.equals(type)) || (SummarizationBarEnum.SummarizationType.MetricRatio.equals(type)) || (SummarizationBarEnum.SummarizationType.MetricPartToWhole.equals(type));
/*  31:    */   }
/*  32:    */   
/*  33:    */   public static boolean isDimensionSummarizationType(SummarizationBarEnum.SummarizationType type)
/*  34:    */   {
/*  35: 37 */     return (SummarizationBarEnum.SummarizationType.DimensionSingle.equals(type)) || (SummarizationBarEnum.SummarizationType.DimensionRatio.equals(type));
/*  36:    */   }
/*  37:    */   
/*  38:    */   public static boolean isAlertSummarizationType(SummarizationBarEnum.SummarizationType type)
/*  39:    */   {
/*  40: 42 */     return SummarizationBarEnum.SummarizationType.Alert.equals(type);
/*  41:    */   }
/*  42:    */   
/*  43:    */   public static boolean isCountOne(String metricKey)
/*  44:    */   {
/*  45: 46 */     if ((StringUtils.isNotEmpty(metricKey)) && ("system-metrics-count_1".equals(metricKey))) {
/*  46: 47 */       return true;
/*  47:    */     }
/*  48: 49 */     return false;
/*  49:    */   }
/*  50:    */   
/*  51:    */   public static List<String> getGroupBys(Statement stmt, SummarizationConfig config, SemanticView semanticView)
/*  52:    */   {
/*  53: 53 */     List<String> groupBys = new ArrayList();
/*  54: 54 */     if (isDimensionSummarizationType(config.getType()))
/*  55:    */     {
/*  56: 55 */       String dimensionKey = config.getItemConfig().getDimensionKey();
/*  57: 56 */       if (StringUtils.isNotEmpty(dimensionKey))
/*  58:    */       {
/*  59: 57 */         Attribute attr = (Attribute)semanticView.getAllAttributes().get(dimensionKey);
/*  60:    */         AttributeRefExpression attrRefExp;
/*  61: 58 */         if ((attr != null) && (PropertyType.DATETIME.equals(attr.getType())))
/*  62:    */         {
/*  63: 60 */           if (stmt != null)
/*  64:    */           {
/*  65: 61 */             attrRefExp = LQLBuilder.makeAttributeRef(dimensionKey);
/*  66: 62 */             List<DatePart> dateParts = getDateParts(config, dimensionKey);
/*  67: 63 */             if (dateParts == null) {
/*  68: 64 */               dateParts = DateFilterUtil.getDefaultCascadeDateParts(attr);
/*  69:    */             }
/*  70: 66 */             for (DatePart datePart : dateParts)
/*  71:    */             {
/*  72: 68 */               ExpressionBase property = LQLBuilder.makeFunctionCallExpression(LQLBuilder.Function.EXTRACT, new ExpressionBase[] { attrRefExp, DatePart.valueOf(datePart.getDatePart()).getDatePartConstantKeyword() });
/*  73: 69 */               Select select = LQLBuilder.makeSelect(dimensionKey + "_&DATEPART_" + datePart.getDatePart(), property);
/*  74: 70 */               stmt.getSelects().add(select);
/*  75:    */               
/*  76: 72 */               groupBys.add(dimensionKey + "_&DATEPART_" + datePart.getDatePart());
/*  77:    */             }
/*  78:    */           }
/*  79:    */         }
/*  80:    */         else {
/*  81: 76 */           groupBys.add(dimensionKey);
/*  82:    */         }
/*  83:    */       }
/*  84:    */     }
/*  85: 79 */     else if (isAlertSummarizationType(config.getType()))
/*  86:    */     {
/*  87: 80 */       for (DimensionEntity dim : config.getAlertConfig().getDimensionsList()) {
/*  88: 81 */         if (StringUtils.isNotEmpty(dim.getKey()))
/*  89:    */         {
/*  90: 82 */           Attribute attr = (Attribute)semanticView.getAllAttributes().get(dim.getKey());
/*  91:    */           AttributeRefExpression key;
/*  92: 83 */           if ((attr != null) && (PropertyType.DATETIME.equals(attr.getType())))
/*  93:    */           {
/*  94: 85 */             if (stmt != null)
/*  95:    */             {
/*  96: 86 */               key = LQLBuilder.makeAttributeRef(dim.getKey());
/*  97: 87 */               List<DatePart> dateParts = dim.getDatePartCombo();
/*  98: 88 */               if (dateParts == null) {
/*  99: 89 */                 dateParts = DateFilterUtil.getDefaultCascadeDateParts(attr);
/* 100:    */               }
/* 101: 91 */               for (DatePart datePart : dateParts)
/* 102:    */               {
/* 103: 93 */                 ExpressionBase property = LQLBuilder.makeFunctionCallExpression(LQLBuilder.Function.EXTRACT, new ExpressionBase[] { key, DatePart.valueOf(datePart.getDatePart()).getDatePartConstantKeyword() });
/* 104: 94 */                 Select select = LQLBuilder.makeSelect(dim.getKey() + "_&DATEPART_" + datePart.getDatePart(), property);
/* 105: 95 */                 stmt.getSelects().add(select);
/* 106:    */                 
/* 107: 97 */                 groupBys.add(dim.getKey() + "_&DATEPART_" + datePart.getDatePart());
/* 108:    */               }
/* 109:    */             }
/* 110:    */           }
/* 111:    */           else {
/* 112:101 */             groupBys.add(dim.getKey());
/* 113:    */           }
/* 114:    */         }
/* 115:    */       }
/* 116:    */     }
/* 117:106 */     return groupBys;
/* 118:    */   }
/* 119:    */   
/* 120:    */   public static List<DatePart> getDateParts(SummarizationConfig config, String attrKey)
/* 121:    */   {
/* 122:110 */     if (isAlertSummarizationType(config.getType())) {
/* 123:111 */       for (DimensionEntity dim : config.getAlertConfig().getDimensionsList()) {
/* 124:112 */         if (dim.getKey().equals(attrKey)) {
/* 125:113 */           return dim.getDatePartCombo();
/* 126:    */         }
/* 127:    */       }
/* 128:    */     } else {
/* 129:117 */       return config.getItemConfig().getDatePartCombo();
/* 130:    */     }
/* 131:119 */     return null;
/* 132:    */   }
/* 133:    */   
/* 134:    */   public static SummarizationBarEnum.ConditionType getConditionType(ConditionsConfig conditionsConfig)
/* 135:    */   {
/* 136:123 */     if (conditionsConfig != null) {
/* 137:124 */       return conditionsConfig.getType();
/* 138:    */     }
/* 139:126 */     return SummarizationBarEnum.ConditionType.SelfValue;
/* 140:    */   }
/* 141:    */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.service.SummarizationBarUtils
 * JD-Core Version:    0.7.0.1
 */