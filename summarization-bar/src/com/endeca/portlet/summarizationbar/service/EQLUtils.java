 package com.endeca.portlet.summarizationbar.service;
 
 import com.endeca.mdex.eql_parser.types.AttributeRefExpression;
 import com.endeca.mdex.eql_parser.types.BetweenExpression;
 import com.endeca.mdex.eql_parser.types.DoubleLiteral;
 import com.endeca.mdex.eql_parser.types.ExpressionBase;
 import com.endeca.mdex.eql_parser.types.From;
 import com.endeca.mdex.eql_parser.types.FunctionCallExpression;
 import com.endeca.mdex.eql_parser.types.Having;
/*  10:    */ import com.endeca.mdex.eql_parser.types.IntegerLiteral;
/*  11:    */ import com.endeca.mdex.eql_parser.types.MultiplicativeExpression;
/*  12:    */ import com.endeca.mdex.eql_parser.types.OrderByList;
/*  13:    */ import com.endeca.mdex.eql_parser.types.Query;
/*  14:    */ import com.endeca.mdex.eql_parser.types.Statement;
/*  15:    */ import com.endeca.mdex.eql_parser.types.StatementSource;
/*  16:    */ import com.endeca.portal.attributes.viewmodel.PredefinedMetric;
/*  17:    */ import com.endeca.portal.data.DataSourceException;
/*  18:    */ import com.endeca.portal.data.KeyNotFoundException;
/*  19:    */ import com.endeca.portal.data.SemanticView;
/*  20:    */ import com.endeca.portal.data.SemanticViewUtils;
/*  21:    */ import com.endeca.portal.lql.LQLBuilder;
/*  22:    */ import com.endeca.portal.lql.LQLBuilder.CompareOp;
/*  23:    */ import com.endeca.portal.lql.LQLBuilder.Function;
/*  24:    */ import com.endeca.portal.lql.LQLBuilder.MultiplicativeOp;
/*  25:    */ import com.endeca.portal.lql.LQLException;
/*  26:    */ import com.endeca.portlet.summarizationbar.SummarizationBarEnum.ConditionOperator;
/*  27:    */ import com.endeca.portlet.summarizationbar.SummarizationBarEnum.ConditionType;
/*  28:    */ import com.endeca.portlet.summarizationbar.SummarizationBarEnum.SummarizationType;
/*  29:    */ import com.endeca.portlet.summarizationbar.model.edit.AlertConditionEntity;
/*  30:    */ import com.endeca.portlet.summarizationbar.model.edit.AlertConfig;
/*  31:    */ import com.endeca.portlet.summarizationbar.model.edit.ConditionsConfig;
/*  32:    */ import com.endeca.portlet.summarizationbar.model.edit.DimensionEntity;
/*  33:    */ import com.endeca.portlet.summarizationbar.model.edit.ItemConfig;
/*  34:    */ import com.endeca.portlet.summarizationbar.model.edit.MetricEntity;
/*  35:    */ import com.endeca.portlet.summarizationbar.model.edit.SummarizationConfig;
/*  36:    */ import java.math.BigInteger;
/*  37:    */ import java.util.Date;
/*  38:    */ import java.util.LinkedHashMap;
/*  39:    */ import java.util.List;
/*  40:    */ import java.util.Locale;
/*  41:    */ import javax.portlet.PortletException;
/*  42:    */ import javax.portlet.PortletRequest;
/*  43:    */ 
/*  44:    */ public class EQLUtils
/*  45:    */ {
/*  46:    */   public static Query getSummaryQuery(PortletRequest request, SemanticView view, SummarizationConfig config)
/*  47:    */     throws KeyNotFoundException, DataSourceException, LQLException, PortletException
/*  48:    */   {
/*  49: 51 */     Query query = new Query();
/*  50:    */     
/*  51: 53 */     Statement stmt = makeStatement("_$SUMMARIZATION_", config.getSemanticKey());
/*  52:    */     
/*  53: 55 */     makeSelect(view, stmt, config, request.getLocale());
/*  54:    */     
/*  55: 57 */     makeWhere(view, stmt, config);
/*  56:    */     
/*  57: 59 */     makeGroup(view, stmt, config, request.getLocale());
/*  58:    */     
/*  59: 61 */     makeHaving(stmt, config, view, request.getLocale());
/*  60:    */     
/*  61: 63 */     makeOrderBy(stmt, config, view, request.getLocale());
/*  62:    */     
/*  63: 65 */     makePaging(stmt, config);
/*  64:    */     
/*  65: 67 */     query.getStatements().add(stmt);
/*  66:    */     
/*  67: 69 */     return query;
/*  68:    */   }
/*  69:    */   
/*  70:    */   public static Query getAlertsRecordCountQuery(PortletRequest request, SemanticView view, SummarizationConfig config)
/*  71:    */     throws KeyNotFoundException, DataSourceException, LQLException, PortletException
/*  72:    */   {
/*  73: 74 */     Query query = new Query();
/*  74: 75 */     Locale locale = request.getLocale();
/*  75: 76 */     Statement baseStmt = makeStatement("_$SUMMARIZATION_BASE_", config.getSemanticKey());
/*  76: 77 */     baseStmt.setReturnTable(false);
/*  77: 78 */     makeSelect(view, baseStmt, config, locale);
/*  78: 79 */     makeWhere(view, baseStmt, config);
/*  79: 80 */     makeGroup(view, baseStmt, config, locale);
/*  80: 81 */     makeHaving(baseStmt, config, view, locale);
/*  81: 82 */     query.getStatements().add(baseStmt);
/*  82:    */     
/*  83: 84 */     Statement stmt = makeStatement("_$SUMMARIZATION_", config.getSemanticKey());
/*  84: 85 */     stmt.setFrom(LQLBuilder.makeFrom(baseStmt.getStatementKey()));
/*  85: 86 */     makeCountOneSelect(stmt, "_$METRIC_", view, locale);
/*  86: 87 */     stmt.setGroup(LQLBuilder.makeGroupAllGroup());
/*  87: 88 */     query.getStatements().add(stmt);
/*  88:    */     
/*  89: 90 */     return query;
/*  90:    */   }
/*  91:    */   
/*  92:    */   private static Statement makeStatement(String summarizationStatement, String statementKey)
/*  93:    */   {
/*  94:101 */     Statement stmt = LQLBuilder.makeStatement(summarizationStatement + new Date().getTime(), true);
/*  95:102 */     makeFrom(stmt, statementKey);
/*  96:103 */     return stmt;
/*  97:    */   }
/*  98:    */   
/*  99:    */   private static void makeFrom(Statement stmt, String statementKey)
/* 100:    */   {
/* 101:113 */     StatementSource stmtSource = LQLBuilder.makeStatementSource(statementKey);
/* 102:114 */     From from = LQLBuilder.makeFrom(stmtSource);
/* 103:115 */     stmt.setFrom(from);
/* 104:    */   }
/* 105:    */   
/* 106:    */   private static void makeGroup(SemanticView view, Statement stmt, SummarizationConfig config, Locale locale)
/* 107:    */   {
/* 108:119 */     List<String> groupByList = SummarizationBarUtils.getGroupBys(stmt, config, view);
/* 109:120 */     String[] groupBys = (String[])groupByList.toArray(new String[groupByList.size()]);
/* 110:122 */     if (groupBys.length > 0) {
/* 111:123 */       stmt.setGroup(LQLBuilder.makeSimpleGroupByMembers("_$GROUP_BY_ALIAS_", view, locale, groupBys));
/* 112:    */     } else {
/* 113:125 */       stmt.setGroup(LQLBuilder.makeGroupAllGroup());
/* 114:    */     }
/* 115:    */   }
/* 116:    */   
/* 117:    */   private static void makeHaving(Statement stmt, SummarizationConfig config, SemanticView view, Locale locale)
/* 118:    */   {
/* 119:130 */     ExpressionBase filter = null;
/* 120:131 */     ExpressionBase having = null;
/* 121:132 */     if (SummarizationBarUtils.isAlertSummarizationType(config.getType())) {
/* 122:133 */       for (AlertConditionEntity acEntity : config.getAlertConfig().getConditonsList()) {
/* 123:134 */         if (acEntity.isValid(acEntity.getType(), acEntity.getMetric(), acEntity.getTargetMetric()))
/* 124:    */         {
/* 125:135 */           having = getConditionHaving(acEntity, view, locale);
/* 126:136 */           if (SummarizationBarEnum.ConditionType.ToAnotherMetric.equals(acEntity.getType())) {
/* 127:137 */             having = getInfHaving(having, "_$CONDITION_VALUE_$METRIC_" + acEntity.getId() + acEntity.getMetric().getKey(), acEntity.getType(), view, locale);
/* 128:    */           } else {
/* 129:142 */             having = getInfHaving(having, "_$METRIC_" + acEntity.getId() + acEntity.getMetric().getKey(), acEntity.getType(), view, locale);
/* 130:    */           }
/* 131:147 */           if (having != null) {
/* 132:148 */             if (filter != null) {
/* 133:149 */               filter = LQLBuilder.makeAnd(filter, having);
/* 134:    */             } else {
/* 135:151 */               filter = having;
/* 136:    */             }
/* 137:    */           }
/* 138:    */         }
/* 139:    */       }
/* 140:157 */     } else if ((config.getConditionsConfig() != null) && (config.getConditionEnabled())) {
/* 141:158 */       filter = getInfHaving(having, "_$METRIC__$CONDITION_VALUE", config.getConditionsConfig().getType(), view, locale);
/* 142:    */     }
/* 143:162 */     if (filter != null)
/* 144:    */     {
/* 145:163 */       ExpressionBase existing = null;
/* 146:164 */       if ((stmt.getHaving() != null) && (stmt.getHaving().getFilter() != null)) {
/* 147:165 */         existing = stmt.getHaving().getFilter();
/* 148:    */       }
/* 149:167 */       if (existing != null) {
/* 150:168 */         filter = LQLBuilder.makeAnd(existing, filter);
/* 151:    */       }
/* 152:170 */       stmt.setHaving(LQLBuilder.makeHaving(filter));
/* 153:    */     }
/* 154:    */   }
/* 155:    */   
/* 156:    */   private static ExpressionBase getInfHaving(ExpressionBase filter, String key, SummarizationBarEnum.ConditionType type, SemanticView view, Locale locale)
/* 157:    */   {
/* 158:176 */     if (SummarizationBarEnum.ConditionType.ToAnotherMetric.equals(type))
/* 159:    */     {
/* 160:177 */       AttributeRefExpression attrRefExpre = LQLBuilder.makeAttributeRef(key, view, locale);
/* 161:178 */       MultiplicativeExpression infExpr = LQLBuilder.makeMultiplicativeExpression(LQLBuilder.makeIntegerLiteral(BigInteger.ONE), LQLBuilder.makeIntegerLiteral(BigInteger.ZERO), LQLBuilder.MultiplicativeOp.DIV);
/* 162:179 */       MultiplicativeExpression negInfExpr = LQLBuilder.makeMultiplicativeExpression(LQLBuilder.makeIntegerLiteral(BigInteger.valueOf(-1L)), LQLBuilder.makeIntegerLiteral(BigInteger.ZERO), LQLBuilder.MultiplicativeOp.DIV);
/* 163:180 */       if (filter == null) {
/* 164:181 */         filter = LQLBuilder.makeComparisonExpression(attrRefExpre, infExpr, LQLBuilder.CompareOp.NEQ);
/* 165:    */       } else {
/* 166:183 */         filter = LQLBuilder.makeAnd(filter, LQLBuilder.makeComparisonExpression(attrRefExpre, infExpr, LQLBuilder.CompareOp.NEQ));
/* 167:    */       }
/* 168:186 */       filter = LQLBuilder.makeAnd(filter, LQLBuilder.makeComparisonExpression(attrRefExpre, negInfExpr, LQLBuilder.CompareOp.NEQ));
/* 169:    */     }
/* 170:188 */     return filter;
/* 171:    */   }
/* 172:    */   
/* 173:    */   private static ExpressionBase getConditionHaving(AlertConditionEntity acEntity, SemanticView view, Locale locale)
/* 174:    */   {
/* 175:193 */     ExpressionBase having = null;
/* 176:    */     
/* 177:195 */     DoubleLiteral value1 = null;
/* 178:196 */     DoubleLiteral value2 = null;
/* 179:197 */     AttributeRefExpression attrRefExpre = null;
/* 180:199 */     if (SummarizationBarEnum.ConditionType.ToAnotherMetric.equals(acEntity.getType())) {
/* 181:200 */       attrRefExpre = LQLBuilder.makeAttributeRef("_$CONDITION_VALUE_$METRIC_" + acEntity.getId() + acEntity.getMetric().getKey(), view, locale);
/* 182:    */     } else {
/* 183:202 */       attrRefExpre = LQLBuilder.makeAttributeRef("_$METRIC_" + acEntity.getId() + acEntity.getMetric().getKey(), view, locale);
/* 184:    */     }
/* 185:205 */     switch (1.$SwitchMap$com$endeca$portlet$summarizationbar$SummarizationBarEnum$ConditionOperator[acEntity.getOperator().ordinal()])
/* 186:    */     {
/* 187:    */     case 1: 
/* 188:207 */       value1 = LQLBuilder.makeDoubleLiteral(acEntity.getValue1());
/* 189:208 */       having = LQLBuilder.makeComparisonExpression(attrRefExpre, value1, LQLBuilder.CompareOp.GT);
/* 190:209 */       break;
/* 191:    */     case 2: 
/* 192:212 */       value1 = LQLBuilder.makeDoubleLiteral(acEntity.getValue1());
/* 193:213 */       having = LQLBuilder.makeComparisonExpression(attrRefExpre, value1, LQLBuilder.CompareOp.GTEQ);
/* 194:214 */       break;
/* 195:    */     case 3: 
/* 196:217 */       value1 = LQLBuilder.makeDoubleLiteral(acEntity.getValue1());
/* 197:218 */       having = LQLBuilder.makeComparisonExpression(attrRefExpre, value1, LQLBuilder.CompareOp.EQ);
/* 198:219 */       break;
/* 199:    */     case 4: 
/* 200:222 */       value1 = LQLBuilder.makeDoubleLiteral(acEntity.getValue1());
/* 201:223 */       value2 = LQLBuilder.makeDoubleLiteral(acEntity.getValue2());
/* 202:224 */       BetweenExpression betweenExpression = new BetweenExpression();
/* 203:225 */       betweenExpression.setOperand(attrRefExpre);
/* 204:226 */       betweenExpression.setLowerBoundNotStrict(value1);
/* 205:227 */       betweenExpression.setUpperBoundStrict(value2);
/* 206:228 */       having = betweenExpression;
/* 207:229 */       break;
/* 208:    */     case 5: 
/* 209:232 */       value2 = LQLBuilder.makeDoubleLiteral(acEntity.getValue2());
/* 210:233 */       having = LQLBuilder.makeComparisonExpression(attrRefExpre, value2, LQLBuilder.CompareOp.LTEQ);
/* 211:234 */       break;
/* 212:    */     case 6: 
/* 213:237 */       value2 = LQLBuilder.makeDoubleLiteral(acEntity.getValue2());
/* 214:238 */       having = LQLBuilder.makeComparisonExpression(attrRefExpre, value2, LQLBuilder.CompareOp.LT);
/* 215:    */     }
/* 216:242 */     return having;
/* 217:    */   }
/* 218:    */   
/* 219:    */   private static void makeOrderBy(Statement stmt, SummarizationConfig config, SemanticView view, Locale locale)
/* 220:    */   {
/* 221:247 */     StringBuilder orderByKey = new StringBuilder("_$METRIC_");
/* 222:248 */     if (SummarizationBarUtils.isDimensionSummarizationType(config.getType()))
/* 223:    */     {
/* 224:249 */       ItemConfig itemConfig = config.getItemConfig();
/* 225:250 */       if (SummarizationBarUtils.isCountOne(itemConfig.getMetricKey())) {
/* 226:251 */         orderByKey.append("system-metrics-count_1");
/* 227:    */       } else {
/* 228:253 */         orderByKey.append(itemConfig.getMetricKey());
/* 229:    */       }
/* 230:255 */       stmt.setOrder(LQLBuilder.makeOrderByList(orderByKey.toString(), !itemConfig.getIsTopValue(), view, locale));
/* 231:    */     }
/* 232:257 */     else if (SummarizationBarUtils.isAlertSummarizationType(config.getType()))
/* 233:    */     {
/* 234:258 */       OrderByList orderByList = new OrderByList();
/* 235:259 */       for (AlertConditionEntity condition : config.getAlertConfig().getConditonsList())
/* 236:    */       {
/* 237:260 */         orderByKey = new StringBuilder("_$METRIC_");
/* 238:261 */         if (condition.isValid(condition.getType(), condition.getMetric(), condition.getTargetMetric()))
/* 239:    */         {
/* 240:262 */           orderByKey.append(condition.getId());
/* 241:263 */           if (SummarizationBarUtils.isCountOne(condition.getMetric().getKey())) {
/* 242:264 */             orderByKey.append("system-metrics-count_1");
/* 243:    */           } else {
/* 244:266 */             orderByKey.append(condition.getMetric().getKey());
/* 245:    */           }
/* 246:269 */           boolean isAscending = true;
/* 247:270 */           if ((SummarizationBarEnum.ConditionOperator.GT.equals(condition.getOperator())) || (SummarizationBarEnum.ConditionOperator.GTEQ.equals(condition.getOperator()))) {
/* 248:271 */             isAscending = false;
/* 249:    */           }
/* 250:273 */           orderByList.getOrderBys().add(LQLBuilder.makeOrderBy(orderByKey.toString(), isAscending, view, locale));
/* 251:    */         }
/* 252:    */       }
/* 253:276 */       stmt.setOrder(orderByList);
/* 254:    */     }
/* 255:    */   }
/* 256:    */   
/* 257:    */   private static void makePaging(Statement stmt, SummarizationConfig config)
/* 258:    */   {
/* 259:281 */     if ((SummarizationBarUtils.isAlertSummarizationType(config.getType())) && (config.getAlertConfig() != null)) {
/* 260:282 */       stmt.setPaging(LQLBuilder.makePaging(BigInteger.ZERO, BigInteger.valueOf(config.getAlertConfig().getMaxNumber())));
/* 261:    */     } else {
/* 262:284 */       stmt.setPaging(LQLBuilder.makePaging(BigInteger.ZERO, BigInteger.ONE));
/* 263:    */     }
/* 264:    */   }
/* 265:    */   
/* 266:    */   private static void makeWhere(SemanticView view, Statement stmt, SummarizationConfig config)
/* 267:    */   {
/* 268:289 */     ExpressionBase filter = null;
/* 269:290 */     if (SummarizationBarUtils.isDimensionSummarizationType(config.getType()))
/* 270:    */     {
/* 271:291 */       ItemConfig itemConfig = config.getItemConfig();
/* 272:292 */       filter = LQLBuilder.makeIsNullOrEmpty(view, itemConfig.getDimensionKey(), false);
/* 273:    */     }
/* 274:293 */     else if (SummarizationBarUtils.isAlertSummarizationType(config.getType()))
/* 275:    */     {
/* 276:294 */       AlertConfig alertConfig = config.getAlertConfig();
/* 277:295 */       for (DimensionEntity dimension : alertConfig.getDimensionsList()) {
/* 278:296 */         if (filter != null) {
/* 279:297 */           filter = LQLBuilder.makeAnd(filter, LQLBuilder.makeIsNullOrEmpty(view, dimension.getKey(), false));
/* 280:    */         } else {
/* 281:299 */           filter = LQLBuilder.makeIsNullOrEmpty(view, dimension.getKey(), false);
/* 282:    */         }
/* 283:    */       }
/* 284:    */     }
/* 285:303 */     if (filter != null) {
/* 286:304 */       stmt.setWhere(LQLBuilder.makeWhere(filter));
/* 287:    */     }
/* 288:    */   }
/* 289:    */   
/* 290:    */   private static void makeSelect(SemanticView view, Statement stmt, SummarizationConfig config, Locale locale)
/* 291:    */   {
/* 292:310 */     if (SummarizationBarUtils.isAlertSummarizationType(config.getType())) {
/* 293:311 */       makeAlertsSelect(view, stmt, config, locale);
/* 294:    */     } else {
/* 295:313 */       makeMetricSelect(view, stmt, config, locale);
/* 296:    */     }
/* 297:    */   }
/* 298:    */   
/* 299:    */   private static void makeMetricSelect(SemanticView view, Statement stmt, SummarizationConfig config, Locale locale)
/* 300:    */   {
/* 301:318 */     ItemConfig itemConfig = config.getItemConfig();
/* 302:319 */     String prefix = "_$METRIC_";
/* 303:320 */     makeCommonSelect(view, stmt, prefix, itemConfig.getMetricKey(), itemConfig.getMetricAggre(), locale);
/* 304:    */     
/* 305:322 */     ConditionsConfig condition = config.getConditionsConfig();
/* 306:323 */     if ((condition != null) && (config.getConditionEnabled()) && (SummarizationBarEnum.ConditionType.ToAnotherMetric.equals(condition.getType()))) {
/* 307:324 */       makePartToAnotherMetricSelect(view, stmt, prefix, itemConfig.getMetricKey(), itemConfig.getMetricAggre(), condition.getTargetMetricKey(), condition.getTargetMetricAggre(), config.getType(), locale);
/* 308:    */     }
/* 309:    */   }
/* 310:    */   
/* 311:    */   private static void makeCommonSelect(SemanticView view, Statement stmt, String prefix, String key, LQLBuilder.Function aggre, Locale locale)
/* 312:    */   {
/* 313:331 */     PredefinedMetric predefinedMetric = (PredefinedMetric)view.getPredefinedMetrics().get(key);
/* 314:332 */     if (SummarizationBarUtils.isCountOne(key)) {
/* 315:333 */       makeCountOneSelect(stmt, prefix, view, locale);
/* 316:334 */     } else if (predefinedMetric != null) {
/* 317:335 */       makePredefineSelect(stmt, prefix, predefinedMetric, view, locale);
/* 318:    */     } else {
/* 319:337 */       makeNormalSelect(view, stmt, prefix, key, aggre, locale);
/* 320:    */     }
/* 321:    */   }
/* 322:    */   
/* 323:    */   private static void makeCountOneSelect(Statement stmt, String prefix, SemanticView view, Locale locale)
/* 324:    */   {
/* 325:342 */     stmt.getSelects().add(LQLBuilder.makeSelect(prefix + "system-metrics-count_1", makeCountOneExpr(), view, locale));
/* 326:    */   }
/* 327:    */   
/* 328:    */   private static void makeNormalSelect(SemanticView view, Statement stmt, String prefix, String key, LQLBuilder.Function aggre, Locale locale)
/* 329:    */   {
/* 330:346 */     FunctionCallExpression expr = LQLBuilder.makeAggregationFunctionCallExpression(aggre, LQLBuilder.makeAttributeRef(key, view, locale), SemanticViewUtils.isSingleAssign(view, key));
/* 331:347 */     stmt.getSelects().add(LQLBuilder.makeSelect(prefix + key, expr, view, locale));
/* 332:    */   }
/* 333:    */   
/* 334:    */   private static void makePredefineSelect(Statement stmt, String prefix, PredefinedMetric predefinedMetric, SemanticView view, Locale locale)
/* 335:    */   {
/* 336:351 */     stmt.getSelects().add(LQLBuilder.makeSelect(prefix + predefinedMetric.getKey(), predefinedMetric.getExpressionAST(), view, locale));
/* 337:    */   }
/* 338:    */   
/* 339:    */   private static void makePartToAnotherMetricSelect(SemanticView view, Statement stmt, String prefix, String key, LQLBuilder.Function aggre, String targetKey, LQLBuilder.Function targetAggre, SummarizationBarEnum.SummarizationType type, Locale locale)
/* 340:    */   {
/* 341:357 */     ExpressionBase dividendExpr = makeCommonExpr(view, key, aggre, locale);
/* 342:358 */     ExpressionBase divisorExpr = makeCommonExpr(view, targetKey, targetAggre, locale);
/* 343:    */     
/* 344:360 */     MultiplicativeExpression metricToMetricExpr = LQLBuilder.makeMultiplicativeExpression(dividendExpr, divisorExpr, LQLBuilder.MultiplicativeOp.DIV);
/* 345:361 */     MultiplicativeExpression metricToMetricWithPercentageExpr = LQLBuilder.makeMultiplicativeExpression(metricToMetricExpr, LQLBuilder.makeIntegerLiteral(BigInteger.valueOf(100L)), LQLBuilder.MultiplicativeOp.MULT);
/* 346:363 */     if (SummarizationBarUtils.isAlertSummarizationType(type))
/* 347:    */     {
/* 348:364 */       stmt.getSelects().add(LQLBuilder.makeSelect(prefix + key, metricToMetricExpr, view, locale));
/* 349:365 */       stmt.getSelects().add(LQLBuilder.makeSelect("_$CONDITION_VALUE" + prefix + key, metricToMetricWithPercentageExpr, view, locale));
/* 350:    */     }
/* 351:    */     else
/* 352:    */     {
/* 353:367 */       stmt.getSelects().add(LQLBuilder.makeSelect(prefix + "_$CONDITION_TARTET_VALUE", divisorExpr, view, locale));
/* 354:368 */       stmt.getSelects().add(LQLBuilder.makeSelect(prefix + "_$CONDITION_VALUE", metricToMetricWithPercentageExpr, view, locale));
/* 355:    */     }
/* 356:    */   }
/* 357:    */   
/* 358:    */   private static void makeAlertsSelect(SemanticView view, Statement stmt, SummarizationConfig config, Locale locale)
/* 359:    */   {
/* 360:373 */     AlertConfig alertConfig = config.getAlertConfig();
/* 361:374 */     for (AlertConditionEntity acEntity : alertConfig.getConditonsList()) {
/* 362:375 */       if (acEntity.isValid(acEntity.getType(), acEntity.getMetric(), acEntity.getTargetMetric()))
/* 363:    */       {
/* 364:376 */         MetricEntity metric = acEntity.getMetric();
/* 365:377 */         MetricEntity targetMetric = acEntity.getTargetMetric();
/* 366:378 */         String prefix = "_$METRIC_" + acEntity.getId();
/* 367:379 */         if (SummarizationBarEnum.ConditionType.ToAnotherMetric.equals(acEntity.getType())) {
/* 368:380 */           makePartToAnotherMetricSelect(view, stmt, prefix, metric.getKey(), metric.getAggregation(), targetMetric.getKey(), targetMetric.getAggregation(), config.getType(), locale);
/* 369:    */         } else {
/* 370:382 */           makeCommonSelect(view, stmt, prefix, metric.getKey(), metric.getAggregation(), locale);
/* 371:    */         }
/* 372:    */       }
/* 373:    */     }
/* 374:    */   }
/* 375:    */   
/* 376:    */   private static FunctionCallExpression makeCountOneExpr()
/* 377:    */   {
/* 378:389 */     IntegerLiteral one = LQLBuilder.makeIntegerLiteral(BigInteger.valueOf(1L));
/* 379:390 */     FunctionCallExpression countOneExpr = LQLBuilder.makeFunctionCallExpression(LQLBuilder.Function.COUNT, new ExpressionBase[] { one });
/* 380:391 */     return countOneExpr;
/* 381:    */   }
/* 382:    */   
/* 383:    */   private static ExpressionBase makeCommonExpr(SemanticView view, String key, LQLBuilder.Function aggre, Locale locale)
/* 384:    */   {
/* 385:395 */     ExpressionBase expression = null;
/* 386:396 */     PredefinedMetric predefinedMetric = (PredefinedMetric)view.getPredefinedMetrics().get(key);
/* 387:397 */     if (SummarizationBarUtils.isCountOne(key)) {
/* 388:398 */       expression = makeCountOneExpr();
/* 389:399 */     } else if (predefinedMetric != null) {
/* 390:400 */       expression = predefinedMetric.getExpressionAST();
/* 391:    */     } else {
/* 392:402 */       expression = LQLBuilder.makeFunctionCallExpression(aggre, new ExpressionBase[] { LQLBuilder.makeAttributeRef(key, view, locale) });
/* 393:    */     }
/* 394:404 */     return expression;
/* 395:    */   }
/* 396:    */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.service.EQLUtils
 * JD-Core Version:    0.7.0.1
 */