/*  1:   */ package com.endeca.portlet.summarizationbar;
/*  2:   */ 
/*  3:   */ import java.io.IOException;
/*  4:   */ import org.codehaus.jackson.map.ObjectMapper;
/*  5:   */ import org.codehaus.jackson.type.TypeReference;
/*  6:   */ 
/*  7:   */ public class SummarizationBarObjectMapper
/*  8:   */ {
/*  9:15 */   private static ObjectMapper mapper = new ObjectMapper();
/* 10:   */   
/* 11:   */   public static String toString(Object o)
/* 12:   */   {
/* 13:   */     try
/* 14:   */     {
/* 15:19 */       return mapper.writeValueAsString(o);
/* 16:   */     }
/* 17:   */     catch (IOException e) {}
/* 18:21 */     return null;
/* 19:   */   }
/* 20:   */   
/* 21:   */   public static <T> T fromString(String src, Class<T> type)
/* 22:   */   {
/* 23:   */     try
/* 24:   */     {
/* 25:27 */       return mapper.readValue(src, type);
/* 26:   */     }
/* 27:   */     catch (IOException e) {}
/* 28:29 */     return null;
/* 29:   */   }
/* 30:   */   
/* 31:   */   public static <T> T fromString(String src, TypeReference<?> type)
/* 32:   */   {
/* 33:   */     try
/* 34:   */     {
/* 35:35 */       return mapper.readValue(src, type);
/* 36:   */     }
/* 37:   */     catch (IOException e) {}
/* 38:37 */     return null;
/* 39:   */   }
/* 40:   */ }


/* Location:           C:\Users\realdecoy\Desktop\eid#endeca-summarization-bar-portlet\WEB-INF\classes\
 * Qualified Name:     com.endeca.portlet.summarizationbar.SummarizationBarObjectMapper
 * JD-Core Version:    0.7.0.1
 */