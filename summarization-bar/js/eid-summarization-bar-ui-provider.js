Ext.ns("Endeca.Portlets.SummarizationBarPortlet");
Endeca.Portlets.SummarizationBarPortlet.UIProvider = function(c, a, b) {
	this.portletId = c;
	this.constants = new Endeca.Portlets.SummarizationBarPortlet.Constants();
	this.isViewPage = a;
	this.resourceUrls = b.resourceUrls;
	this.formatterPanelResource = b.formatterPanelResource;
	this.utils = new Endeca.Portlets.SummarizationBarPortlet.Utils(c,
			b.resources);
	this.mediator = new Endeca.Portlets.SummarizationBarPortlet.Mediator(
			this.portletId, this.resourceUrls, b.resources);
	return this
};
Endeca.Portlets.SummarizationBarPortlet.UIProvider.prototype = {
	portletId : undefined,
	constants : undefined,
	mediator : undefined,
	summarizationNameEditor : undefined,
	isRevert : undefined,
	isViewPage : false,
	isExitListenerInit : false,
	createViewUI : function() {
		var a = this;
		return a.createSummarizationsDataViewPanel()
	},
	createEditUI : function() {
		var b = this;
		b.dataMgr = new Ext.ux.endeca.ViewDataManager({
			resourceUrl : b.resourceUrls.getViewDataUrl
		});
		b.utils.dataMgr = b.dataMgr;
		b.mediator.injectDataToMediator(b.dataMgr, b.editModel);
		b.mediator.lastSemanticKey = b.editModel.defaultCollectionKey;
		if (b.isRevert) {
			b.utils.displaySuccessMessage(b.utils
					.localize("df.revert-prefs-success"))
		}
		Ext.QuickTips.init();
		var a = new Ext.Panel({
			id : b.portletId + "edit_panel",
			autoHeight : true,
			border : false,
			cls : "edit-panel",
			items : [ b.getActionButtonsPanel(), b.getMainPanel(),
					b.getPreviewButtonPanel(), b.createPreviewPanel() ],
			listeners : {
				afterrender : function() {
					b.summarizationNameEditor = b
							.createSummarizationNameEditor();
					b.initExitListener()
				}
			}
		});
		return a
	},
	initExitListener : function() {
		var a = this;
		if (!a.isExitListenerInit) {
			Ext.select("a.portlet-icon-back").on("click", function(b) {
				a.mediator.handleExit(b, a.getExitConfirmationWin())
			});
			a.isExitListenerInit = true
		}
	},
	getExitConfirmationWin : function() {
		var b = this;
		var a = new Ext.ux.endeca.EndecaModalWindow(
				{
					title : b.utils
							.localize("summarizationbar.edit.window.exit-title"),
					html : b.utils
							.localize("summarizationbar.edit.window.exit-message"),
					cls : "eid-fdbk-modal-warn",
					buttonAlign : "right",
					autoDestroy : true,
					buttons : [
							{
								text : b.utils
										.localize("summarizationbar.edit.button.cancel"),
								handler : function() {
									a.close()
								}
							},
							{
								text : b.utils
										.localize("summarizationbar.edit.button.dont-save"),
								handler : function() {
									b.mediator.returnToFullPage()
								}
							},
							{
								text : b.utils
										.localize("summarizationbar.edit.button.save"),
								cls : "eid-btn-action",
								handler : function() {
									a.close();
									b.mediator.updateEditModel(true)
								}
							} ]
				});
		return a
	},
	createPreviewPanel : function() {
		var a = this;
		return {
			xtype : "panel",
			id : a.portletId + "preview_panel",
			cls : "eid-summarization-bar-view eid-configuration-preview-panel",
			border : false,
			autoWidth : true,
			collapsible : true,
			collapsed : true,
			hideCollapseTool : true,
			items : [ a.createSummarizationsDataViewPanel() ]
		}
	},
	createSummarizationsDataViewPanel : function() {
		var c = this;
		var a = new Ext.DataView({
			id : c.portletId + "metircs_list",
			store : c.getPreviewSummarizationBarStore(),
			deferEmptyText : false,
			tpl : c.getPreviewSummarizationBarTpl(),
			listeners : {
				beforerender : function() {
					if (c.isViewPage) {
						c.getSummarizations(true, false)
					}
				}
			}
		});
		var b = new Ext.Panel({
			id : c.portletId + "container_panel",
			cls : "eid-metric-panel",
			border : false,
			items : [ a ],
			listeners : {
				afterrender : function() {
					if (c.isViewPage) {
						c.utils.showConfigMessage(c.showConfigMessage,
								c.resourceUrls.updateShowConfigMessageUrl)
					}
				}
			}
		});
		return b
	},
	getSummarizations : function(a, c) {
		var e = this, d;
		if (a) {
			d = new Ext.LoadMask(Ext
					.get(e.portletId + "summarization_bar_view").parent(
							".portlet-content", false))
		} else {
			d = new Ext.LoadMask(Ext
					.get(e.portletId + "summarization_bar_edit").parent(
							".portlet-content", false))
		}
		d.show();
		var b = {
			url : e.resourceUrls.getSummarizationsUrl,
			success : function(g) {
				var f = Ext.decode(g.responseText);
				if (f.messageKey == "108") {
					e.utils.displayWarnMessage(f.message, false)
				}
				if (f.messageKey == "110") {
					e.utils.displayInfoMessage(f.message, false)
				}
				Ext.StoreMgr.get(e.portletId + "metrics_preview_store")
						.loadData(f.summarizations);
				d.hide()
			},
			failure : function(f) {
				e.utils.displayErrorMessage(f.responseText);
				d.hide()
			}
		};
		if (c) {
			e.mediator.checkEditModelConfigs(false);
			Ext.apply(b, {
				params : {
					editModel : Ext.encode(e.mediator.editModel),
					isUpdatePreview : true
				}
			})
		}
		Ext.Ajax.request(b)
	},
	getPreviewSummarizationBarTpl : function() {
		var a = this;
		return new Ext.XTemplate(
				'<tpl for=".">',
				'<tpl if="this.isDisplay(values)">',
				'<div class="eid-metric-context '
						+ a.portletId
						+ 'summarization" summarizationId="{values.id}" id="'
						+ a.portletId
						+ '{values.id}" {[this.getStyle(values)]}  {[this.clickAction(values)]}>',
				'<tpl if="this.isNameBelowValue()">',
				"{[this.showValueTemplate(values)]}",
				"{[this.showNameTemplate(values)]}",
				"</tpl>",
				'<tpl if="!this.isNameBelowValue()">',
				"{[this.showNameTemplate(values)]}",
				"{[this.showValueTemplate(values)]}",
				"</tpl>",
				"</div>",
				"</tpl>",
				"</tpl>",
				{
					compiled : true,
					provider : a,
					showNameTemplate : function(b) {
						return a.getNameTemplate().apply(b)
					},
					showValueTemplate : function(b) {
						if (b.type == a.constants.SUMMARIZATION_TYPE_ALERT) {
							return a.getAlertValueTemplate().apply(b)
						} else {
							return a.getNonAlertValueTemplate().apply(b)
						}
					},
					isNameBelowValue : function() {
						if (a.isViewPage) {
							return a.viewModel.settingsModel.namePosition == a.constants.GENERAL_SETTINGS_DISPLAY_NAME_POSITION_BELOW
						} else {
							return a.editModel.settingsModel.namePosition == a.constants.GENERAL_SETTINGS_DISPLAY_NAME_POSITION_BELOW
						}
					},
					isDisplay : function(b) {
						return b.isDisplay
					},
					getStyle : function(b) {
						if (a.showHyperlink(b)) {
							return 'style="max-width:' + b.width
									+ 'px; cursor: pointer;"'
						} else {
							if (b.type == a.constants.SUMMARIZATION_TYPE_ALERT) {
								return 'style="max-width:'
										+ b.width
										+ 'px; cursor: pointer; text-align: left;"'
							} else {
								return 'style="max-width:' + b.width
										+ 'px; cursor: default;"'
							}
						}
					},
					clickAction : function(c) {
						if (c.type == a.constants.SUMMARIZATION_TYPE_DIMENSION_SINGLE
								&& !a.showHyperlink(c)) {
							return ' onclick="JavaScript:e'
									+ a.portletId
									+ "summarizationBarController.uiProvider.handleRefinementAction('"
									+ c.id + "')\""
						}
						if (a.showHyperlink(c)) {
							var b = a.getRealUrl(c);
							return ' onclick="JavaScript:e'
									+ a.portletId
									+ "summarizationBarController.uiProvider.gotoHyperlink('"
									+ c.id + "','" + b + "')\""
						} else {
							if (c.type == a.constants.SUMMARIZATION_TYPE_ALERT) {
								return ' onclick="JavaScript:e'
										+ a.portletId
										+ "summarizationBarController.uiProvider.showAlertViewWindow('"
										+ c.id + "')\""
							}
						}
					}
				})
	},
	getNameTemplate : function(a) {
		var b = this;
		if (!b.nameTemplate) {
			b.nameTemplate = new Ext.XTemplate(
					'<div class="eid-summarization-name" {[this.getNameTextStyle(values)]}>{[this.getEncodedVal(values.name)]}</div>',
					'<tpl if="this.showDescriptionIcon(values)">',
					'<span id="'
							+ b.portletId
							+ '{values.id}" class="eid-action-icon eid-icon-info" {[this.getDescriptionTip(values.id)]}"></span>',
					"</tpl>",
					{
						getEncodedVal : function(c) {
							return Ext.util.Format.htmlEncode(c)
						},
						model : b.isViewPage ? b.viewModel : b.editModel,
						getNameTextStyle : function(c) {
							return 'style="max-width:' + c.width
									+ "px; font-size:"
									+ this.model.settingsModel.labelTextSize
									+ 'px;"'
						},
						showDescriptionIcon : function(c) {
							return !Ext.isEmpty(c.infoList)
									&& c.infoList.length > 0
						},
						getDescriptionTip : function(c) {
							return ' onclick="JavaScript:e'
									+ b.portletId
									+ "summarizationBarController.uiProvider.showInfoWindow('"
									+ c + "')\""
						}
					});
			b.nameTemplate.compile()
		}
		return b.nameTemplate
	},
	getNonAlertValueTemplate : function(a) {
		var b = this;
		if (!b.nonAlertValueTemplate) {
			b.nonAlertValueTemplate = new Ext.XTemplate(
					'<div class="eid-summarization-value" {[this.getValueTextStyle(values)]} >{formattedValue}',
					'<tpl if="this.showConditionalIcon(values)">',
					" <div {[this.getConditionalStyle(values)]}></div>",
					"</tpl>",
					"</div>",
					{
						model : b.isViewPage ? b.viewModel : b.editModel,
						getValueTextStyle : function(c) {
							if (this.showConditionalIcon(c)
									&& !c.isMetricTextBlack) {
								return 'style="max-width:'
										+ c.width
										+ "px; font-size:"
										+ this.model.settingsModel.valueTextSize
										+ "px; color:"
										+ b.conditionalStyleMap[c.styleGroupId
												+ "_" + Ext.num(c.styleId, 0)]
										+ '"'
							} else {
								return 'style="max-width:'
										+ c.width
										+ "px; font-size:"
										+ this.model.settingsModel.valueTextSize
										+ 'px;"'
							}
						},
						showConditionalIcon : function(c) {
							return b.showConditionalIcon(c, false)
						},
						getConditionalStyle : function(d) {
							var c = "condition-style-" + d.styleGroupId;
							return "class = " + c
									+ ' style="background-position: -'
									+ Ext.num(d.styleId, 0) * 30
									+ 'px center; vertical-align: middle;"'
						}
					});
			b.nonAlertValueTemplate.compile()
		}
		return b.nonAlertValueTemplate
	},
	getAlertValueTemplate : function(a) {
		var b = this;
		if (!b.alertValueTemplate) {
			b.alertValueTemplate = new Ext.XTemplate(
					'<span id="'
							+ b.portletId
							+ 'alert_{values.id}" class="eid-alert-value-text" {[this.getAlertValueTextStyle(values)]} >',
					"{values.formattedValue}",
					"</span>",
					{
						model : b.isViewPage ? b.viewModel : b.editModel,
						isNameBelowValue : function() {
							if (b.isViewPage) {
								return b.viewModel.settingsModel.namePosition == b.constants.GENERAL_SETTINGS_DISPLAY_NAME_POSITION_BELOW
							} else {
								return b.editModel.settingsModel.namePosition == b.constants.GENERAL_SETTINGS_DISPLAY_NAME_POSITION_BELOW
							}
						},
						getAlertValueTextStyle : function(c) {
							return 'style="background: none repeat scroll 0 0 '
									+ c.alertColor
									+ "; font-size:"
									+ (this.model.settingsModel.valueTextSize - 5)
									+ "px; "
									+ (this.isNameBelowValue() ? "margin-bottom:"
											: "margin-top:") + '5px;"'
						}
					});
			b.alertValueTemplate.compile()
		}
		return b.alertValueTemplate
	},
	showAlertViewWindow : function(d) {
		var e = this;
		var a = Ext.StoreMgr.get(e.portletId + "metrics_preview_store");
		var c = a.getAt(a.findExact("id", d.toString())).data;
		var f = c.alertDimensionColumns.length * 200
				+ c.alertConditionColumns.length * 120 + 19;
		var b = new Ext.ux.endeca.EndecaModalWindow(
				{
					id : e.portletId + "alert_view_window_" + c.id,
					title : Ext.util.Format.htmlEncode(c.name),
					cls : "eid-alert-view-window",
					width : f + 38,
					renderTo : Ext.get(e.portletId + "metircs_list")
							.findParent(".portlet-boundary"),
					modal : true,
					autoDestroy : true,
					items : [
							{
								xtype : "box",
								autoHeight : true,
								width : f,
								cls : "eid-alert-view-description",
								html : Ext.util.Format
										.htmlEncode(c.description),
								id : e.portletId + "alert_view_description_"
										+ c.id
							},
							{
								xtype : "grid",
								cls : "eid-alert-view-grid-panel",
								width : f,
								columnLines : true,
								autoHeight : true,
								enableHdMenu : false,
								enableColumnMove : false,
								enableColumnResize : true,
								store : e.mediator.getAlertGridStore(d),
								colModel : e.mediator
										.getAlertGridColumnModel(d),
								listeners : {
									viewready : function(h) {
										if (h.getHeight() < 330) {
											var i = Ext.getCmp(e.portletId
													+ "alert_view_description_"
													+ c.id);
											var g = Ext.getCmp(e.portletId
													+ "alert_view_window_"
													+ c.id);
											i.setWidth(f - 20);
											h.setWidth(f - 20);
											g.setWidth(f + 18);
											g.doLayout()
										}
									},
									cellclick : function(h, m, i, j) {
										if (j.getTarget(".eid-refinement-link",
												2, true)) {
											var g = h.getStore().getAt(m);
											var l = h.getColumnModel()
													.getDataIndex(i);
											var k = g.get(l);
											e.handleRefinementAction(c.id, k,
													true)
										}
									}
								}
							},
							{
								xtype : "box",
								cls : "eid-alert-view-total-message",
								hidden : c.alertDimensionValues.length == c.value,
								html : e.utils
										.localize(
												"summarizationbar.view.alert.display-of-count",
												c.alertDimensionValues.length,
												c.formattedValue)
							} ]
				});
		b.show()
	},
	closeTooltip : function(a) {
		Ext.getCmp(a).hide()
	},
	createToolTip : function(b, c) {
		var d = this;
		var a = Ext.getCmp(d.portletId + b + "tooltip");
		if (a) {
			a.update(c)
		} else {
			a = new Ext.ToolTip(
					{
						target : d.portletId + b,
						id : d.portletId + b + "tooltip",
						anchorToTarget : true,
						floating : true,
						shadow : false,
						width : c.type == d.constants.SUMMARIZATION_TYPE_ALERT ? 150
								: 250,
						autoHide : false,
						anchor : "bottom",
						anchorOffset : 5,
						data : c,
						cls : "eid-info-tooltip",
						onDocMouseDown : function(f) {
							if (this.autoHide !== true && !this.closable
									&& !f.within(this.el.dom)
									&& !f.within(Ext.get(d.portletId + b).dom)) {
								this.disable();
								this.doEnable.defer(100, this)
							}
						},
						tpl : new Ext.XTemplate(
								'<tpl if="this.isAlertType(values)">',
								'<tpl if="this.showTitle(values)">',
								'<div class="eid-info-tooltip-header">{values.name}</div>',
								'<div class="eid-info-tooltip-line"></div>',
								"</tpl>",
								"<div>"
										+ d.utils
												.localize("summarizationbar.view.flag.qtip.click-to-view-flags")
										+ "</div>",
								"</tpl>",
								'<tpl if="!this.isAlertType(values)">',
								'<tpl if="this.showTitle(values) || this.showValue(values)">',
								'<div class="eid-info-tooltip-header">{values.name}<br/>{values.formattedValue}</div>',
								'<tpl if="this.showDescription(values) || this.showConditionalIcon(values) || this.showRefinement(values)">',
								'<div class="eid-info-tooltip-line"></div>',
								"</tpl>",
								"</tpl>",
								'<tpl if="this.showDescription(values)">',
								'<div class="eid-info-tooltip-description">{[this.getDescription(values)]}</div>',
								'<tpl if="this.showConditionalIcon(values) || this.showRefinement(values)">',
								'<div class="eid-info-tooltip-line"></div>',
								"</tpl>",
								"</tpl>",
								'<tpl if="this.showConditionalIcon(values)">',
								'<div class="eid-info-tooltip-condition">',
								"<div {[this.getConditionalStyle(values)]}></div>",
								'<div class="eid-info-tooltip-condition-text">{[this.getConditionalTooltip(values)]} </div>',
								"</div>",
								'<tpl if="this.showHyperlink(values) || this.showRefinement(values)">',
								'<div class="eid-info-tooltip-line"></div>',
								"</tpl>",
								"</tpl>",
								'<tpl if="this.showRefinement(values)">',
								'<div class="eid-info-tooltip-refinement">'
										+ d.utils
												.localize("summarizationbar.view.tooltip.do-refinement")
										+ "</div>",
								"</tpl>",
								'<tpl if="this.showHyperlink(values)">',
								'<div class="eid-info-tooltip-description">{[this.generateRealUrlForLinkAction(values)]}</div>',
								"</tpl>",
								"</tpl>",
								{
									isAlertType : function(e) {
										return d.constants.SUMMARIZATION_TYPE_ALERT == e.type
									},
									showValue : function(e) {
										return d.showFullText(b, e.width,
												e.formattedValue, true)
									},
									showTitle : function(e) {
										return d.showFullText(b, e.width,
												e.name, false)
									},
									showDescription : function(e) {
										return d.showDescription(e)
									},
									getDescription : function(f) {
										var e = d.constants.METRIC_VALUE_TOKEN;
										return d.utils
												.replacePlaceHolderToValue(
														Ext.util.Format
																.htmlEncode(f.description),
														e,
														f.formattedMetricValue)
									},
									showConditionalIcon : function(e) {
										return d.showConditionalIcon(e, true)
									},
									showRefinement : function(e) {
										return d.showRefinement(e)
									},
									showHyperlink : function(e) {
										return d.showHyperlink(e)
									},
									getConditionalStyle : function(f) {
										var e = "condition-style-"
												+ f.styleGroupId;
										return "class = "
												+ e
												+ ' style="vertical-align: middle;background-position: -'
												+ Ext.num(f.styleId, 0) * 30
												+ 'px center;"'
									},
									getConditionalTooltip : function(f) {
										var g = f.conditionTooltip;
										if (f.conditionType == d.constants.CONDITION_TYPE_TO_ANOTHER_METRIC) {
											var e = d.constants.TARGET_METRIC_VALUE_TOKEN;
											g = d.utils
													.replacePlaceHolderToValue(
															g,
															e,
															f.conditionTargetValue)
										} else {
											if (f.conditionType == d.constants.CONDITION_TYPE_PART_TO_WHOLE) {
												var e = d.constants.WHOLE_METRIC_VALUE_TOKEN;
												g = d.utils
														.replacePlaceHolderToValue(
																g,
																e,
																f.conditionTargetValue)
											}
										}
										return Ext.util.Format.htmlEncode(g)
									},
									generateRealUrlForLinkAction : function(g) {
										var f = d.getRealUrl(g);
										var e = Ext.util.Format.htmlEncode(f);
										return d.utils
												.localize(
														"summarizationbar.view.tooltip.go-to",
														e)
									}
								}),
						listeners : {
							afterrender : function(e) {
								e.el
										.on(
												"mouseleave",
												function(g, f) {
													(function() {
														if (!g
																.within(Ext
																		.get(d.portletId
																				+ b).dom)
																&& !g
																		.within(e.el.dom)) {
															e.hide()
														}
													}).defer(100)
												})
							}
						}
					})
		}
		return a
	},
	getRealUrl : function(a) {
		return a.action.actionConfig.url.replace(/\{?\{(\d+)\}\}?/g, function(
				b, d) {
			var c = a.action.actionConfig.parameters[d].name;
			var e = c == a.dimensionKey ? a.value : a.metricValue;
			if (/^\{\{/.test(b) && /\}\}$/.test(b)) {
				return e
			}
			if (/^\{\{/.test(b) && /\}$/.test(b)) {
				return "{" + encodeURIComponent(e)
			}
			if (/^\{/.test(b) && /\}\}$/.test(b)) {
				return encodeURIComponent(e) + "}"
			}
			if (/^\{/.test(b) && /\}$/.test(b)) {
				return encodeURIComponent(e)
			}
		})
	},
	gotoHyperlink : function(b, a) {
		var c = this;
		if (!c.isViewPage) {
			return
		}
		Ext.Ajax.request({
			url : c.resourceUrls.hyperlinkActionUrl,
			success : function(e) {
				var d = Ext.decode(e.responseText);
				if (d.popupUrl) {
					window.open(d.popupUrl)
				} else {
					if (d.redirectUrl) {
						window.top.location = d.redirectUrl
					}
				}
			},
			failure : function(d) {
			},
			params : {
				summarizationId : b,
				hyperLinkURL : a
			}
		})
	},
	handleRefinementAction : function(f, h, b) {
		var g = this;
		if (!g.isViewPage) {
			return
		}
		var a = [], c = [];
		if (b) {
			Ext.each(h.attributeKeys, function(i) {
				a.push(i)
			});
			Ext.each(h.values, function(i) {
				c.push(i)
			})
		} else {
			var d = Ext.StoreMgr.get(g.portletId + "metrics_preview_store");
			var e = d.getAt(d.findExact("id", f.toString())).data;
			a.push(e.dimensionKey);
			c.push(e.value)
		}
		Ext.Ajax.request({
			url : g.resourceUrls.refinementActionUrl,
			success : function(j) {
				var i = Ext.decode(j.responseText);
				if (i.redirectUrl) {
					window.top.location = i.redirectUrl
				} else {
					if (b) {
						Ext.getCmp(g.portletId + "alert_view_window_" + f)
								.close()
					} else {
						Ext.getCmp(g.portletId + f + "tooltip").hide()
					}
					Ext.each(i.triggeredEvents, function(k) {
						Endeca.Async.triggerEvent(k)
					})
				}
			},
			failure : function(i) {
			},
			params : {
				summarizationId : f,
				dimensionKeys : Ext.util.JSON.encode(a),
				dimensionValues : Ext.util.JSON.encode(c)
			}
		})
	},
	showInfoWindow : function(d, c) {
		var e = this;
		var a = Ext.StoreMgr.get(e.portletId + "metrics_preview_store");
		var b = a.getAt(a.findExact("id", d.toString()));
		if (!Ext.isEmpty(b) && e.validateTooltip(d.toString(), b.data)) {
			e.createToolTip(d.toString(), b.data).show()
		}
	},
	validateTooltip : function(b, a) {
		var c = this;
		return a.type == c.constants.SUMMARIZATION_TYPE_ALERT
				|| c.showFullText(b, a.width, a.formattedValue, true)
				|| c.showFullText(b, a.width, a.name, false)
				|| c.showDescription(a) || c.showConditionalIcon(a, true)
				|| c.showRefinement(a) || c.showHyperlink(a)
	},
	showFullText : function(d, b, h, c) {
		var g = this;
		var f;
		if (c) {
			f = Ext.get(g.portletId + d).child(".eid-summarization-value")
		} else {
			f = Ext.get(g.portletId + d).child(".eid-summarization-name")
		}
		var a = Ext.util.TextMetrics.createInstance(f);
		var e = a.getWidth(h);
		return (e >= b)
	},
	showDescription : function(a) {
		return !Ext.isEmpty(a.description)
	},
	showConditionalIcon : function(b, a) {
		if (a) {
			return !Ext.isEmpty(b.styleId) && !Ext.isEmpty(b.conditionTooltip)
		} else {
			return !Ext.isEmpty(b.styleId)
		}
	},
	showRefinement : function(a) {
		return !Ext.isEmpty(a.action)
				&& a.action.actionType == Ext.ux.endeca.ActionsConfigurationEnums.actionTypeEnum.ACTION_REFINEMENT
	},
	showHyperlink : function(a) {
		return !Ext.isEmpty(a.action)
				&& a.action.actionType == Ext.ux.endeca.ActionsConfigurationEnums.actionTypeEnum.ACTION_PASS_PARAMETER
				&& !Ext.isEmpty(a.action.actionConfig.url)
	},
	getPreviewSummarizationBarStore : function() {
		var a = this;
		return new Ext.data.JsonStore(
				{
					autoDestroy : true,
					storeId : a.portletId + "metrics_preview_store",
					fields : [ "id", "name", "value", "metricValue",
							"formattedMetricValue", "formattedValue",
							"dimensionKey", "width", "description",
							"infoTitle", "infoList", "styleGroupId", "styleId",
							"isMetricTextBlack", "isDisplay", "conditionType",
							"conditionTooltip", "conditionTargetValue",
							"conditionCompareValue", "action", "type",
							"alertColor", "alertDimensionColumns",
							"alertDimensionValues", "alertConditionColumns",
							"alertConditionValues" ],
					listeners : {
						load : function(c, b) {
							if (!Ext.isEmpty(b)) {
								var d = Ext.query("." + a.portletId
										+ "summarization", Ext.fly(a.portletId
										+ "metircs_list").dom);
								Ext
										.each(
												d,
												function(f) {
													var e = Ext.get(f);
													var h = e
															.getAttribute("summarizationId");
													var g = c
															.getAt(c
																	.findExact(
																			"id",
																			h
																					.toString())).data;
													e
															.on(
																	"mouseenter",
																	function() {
																		a
																				.showInfoWindow(
																						h,
																						e)
																	});
													e
															.on(
																	"mouseleave",
																	function(k,
																			i) {
																		var j = Ext
																				.getCmp(a.portletId
																						+ h
																						+ "tooltip");
																		if (!Ext
																				.isEmpty(j)) {
																			(function() {
																				if (!k
																						.within(f)
																						&& !k
																								.within(j.el.dom)) {
																					j
																							.hide()
																				}
																			})
																					.defer(100)
																		}
																	})
												})
							}
						}
					}
				})
	},
	getPreviewButtonPanel : function() {
		var a = this;
		return {
			id : a.portletId + "preview_buttons_panel",
			xtype : "container",
			autoWidth : true,
			cls : "eid-configuration-page-bbar",
			items : [
					{
						id : a.portletId + "show_or_hide_preview_btn",
						xtype : "button",
						cls : "eid-btn-small",
						text : a.utils
								.localize("summarizationbar.edit.button.show-preview"),
						handler : function(b) {
							a.mediator.togglePreviewPanel(b);
							if (b.getText() === a.utils
									.localize("summarizationbar.edit.button.hide-preview")) {
								a.getSummarizations(false, true)
							}
						},
						listeners : {
							afterrender : function(b) {
								if (a.mediator.isEditModelConfigEmpty()) {
									b.disable()
								}
							}
						}
					},
					{
						id : a.portletId + "update_preview_btn",
						xtype : "button",
						cls : "eid-btn-small",
						text : a.utils
								.localize("summarizationbar.edit.button.update-preview"),
						hidden : true,
						hideMode : "visibility",
						handler : function() {
							a.getSummarizations(false, true)
						},
						listeners : {
							afterrender : function(b) {
								if (a.mediator.isEditModelConfigEmpty()) {
									b.disable()
								}
							}
						}
					} ]
		}
	},
	getActionButtonsPanel : function() {
		var a = this;
		return {
			xtype : "container",
			id : a.portletId + "action_buttons_panel",
			cls : "eid-configuration-page-tbar",
			border : false,
			autoWidth : true,
			items : [
					{
						text : a.utils.localize("df.button-save-preferences"),
						xtype : "button",
						cls : "eid-btn-action",
						handler : function() {
							a.mediator.updateEditModel(false)
						}
					},
					{
						text : a.utils.localize("df.button-revert-preferences"),
						xtype : "button",
						handler : function() {
							a.mediator.showRevertPrefsConfirmWin(a
									.getRevertPrefsConfirmWin())
						}
					} ]
		}
	},
	getRevertPrefsConfirmWin : function() {
		var b = this;
		var a = new Ext.ux.endeca.EndecaModalWindow({
			title : b.utils
					.localize("summarizationbar.edit.window.revert-title"),
			buttonAlign : "right",
			autoDestroy : true,
			html : b.utils
					.localize("summarizationbar.edit.window.revert-message"),
			buttons : [ {
				text : b.utils.localize("summarizationbar.edit.button.no"),
				handler : function() {
					a.close()
				}
			}, {
				text : b.utils.localize("summarizationbar.edit.button.yes"),
				cls : "eid-btn-action",
				handler : function() {
					b.mediator.revertEditModel();
					a.close()
				}
			} ]
		});
		return a
	},
	getMainPanel : function() {
		var b = this;
		var a = new Ext.Container({
			id : b.portletId + "main_panel",
			autoHeight : true,
			autoWidth : true,
			cls : "eid-configuration-main-panel",
			items : [ b.getLeftPanel(), b.getRightPanel() ]
		});
		return a
	},
	getRightPanel : function() {
		var b = this;
		var a = new Ext.Container({
			id : b.portletId + "right_panel",
			border : false,
			autoHeight : true,
			autoWidth : true,
			cls : "eid-right-panel eid-configuration-right-panel",
			layout : {
				type : "card",
				deferredRender : true
			},
			activeItem : 0,
			items : [ b.getSummaryItemsCard(), b.getGeneralSettingsPanel() ]
		});
		return a
	},
	getSummaryItemsCard : function() {
		var b = this;
		var a = new Ext.Container(
				{
					id : b.portletId + "summary_items_panel",
					autoWidth : true,
					autoHeight : true,
					border : false,
					bodyCssClass : "summary-items-panel-body",
					items : [ {
						xtype : "container",
						border : false,
						cls : "eid-right-panel-header",
						items : [
								{
									xtype : "displayfield",
									html : "<h3>"
											+ b.utils
													.localize("summarizationbar.edit.panel.summary-items-title")
											+ "</h3>"
								},
								{
									xtype : "panel",
									layout : "hbox",
									cls : "eid-instruction-text",
									border : false,
									items : [
											{
												xtype : "displayfield",
												cls : "eid-summarization-item-description-label",
												value : b.utils
														.localize("summarizationbar.edit.panel.summary-items-description")
											},
											{
												xtype : "button",
												cls : "x-btn-text-icon eid-btn-add",
												text : b.utils
														.localize("summarizationbar.edit.button.add-item"),
												listeners : {
													click : function() {
														var c = b
																.getNewSummarizationPanel();
														b.mediator
																.addSummarization(c);
														b.mediator
																.startEditingSummarizationName(
																		b.summarizationNameEditor,
																		c.el
																				.child(
																						".summarization-name",
																						false))
													}
												}
											} ]
								},
								{
									xtype : "container",
									id : b.portletId + "summarizations_panel",
									cls : "eid-summarizations-panel",
									border : false,
									width : 660,
									items : [ b
											.getSummarizationPanels(b.editModel.configs) ],
									listeners : {
										afterrender : function() {
											b.mediator.registerSummaryItemsDD()
										}
									}
								} ]
					} ]
				});
		return a
	},
	getSummarizationPanelsHeader : function() {
		var b = this;
		var a = new Ext.Container(
				{
					border : false,
					width : 660,
					cls : "eid-summarization-bar-column-header-container",
					items : [
							{
								xtype : "container",
								width : 15,
								cls : "eid-summarization-bar-drag-column-header"
							},
							{
								xtype : "container",
								width : 583,
								cls : "eid-summarization-bar-name-column-header",
								items : [ {
									xtype : "displayfield",
									value : b.utils
											.localize("summarizationbar.edit.displayfield.summary-name")
								} ]
							},
							{
								xtype : "container",
								width : 60,
								cls : "eid-summarization-bar-icons-column-header"
							} ]
				});
		return a
	},
	getSummarizationPanels : function(b) {
		var c = this;
		var a = [];
		Ext.each(b, function(d) {
			a.push(c.getNewSummarizationPanel(d))
		});
		return a
	},
	getEmptySummarizationPanel : function() {
		var a = this;
		return new Ext.Container(
				{
					cls : "eid-summarization-bar-row-container empty-summary-panel",
					id : a.portletId + "empty_summary_panel",
					border : false,
					width : 660,
					html : a.utils
							.localize("summarizationbar.edit.displayfield.empty-summary-items")
				})
	},
	getNewSummarizationPanel : function(a) {
		var f = this;
		var d, e, c;
		if (a) {
			d = a.id;
			e = a.name
		} else {
			d = Ext.id();
			e = f.mediator.getUniqueDefaultSummarizationName()
		}
		var b = new Ext.Container(
				{
					cls : "eid-summarization-bar-row-container dd-item",
					id : f.portletId + "summarization_" + d,
					border : false,
					width : 660,
					summarizationId : d,
					summarizationName : e,
					items : [
							{
								xtype : "container",
								cls : "eid-summarization-bar-drag-column",
								items : [ {
									xtype : "box",
									cls : "eid-icon eid-icon-row-draghandle",
									html : f.utils
											.localize("summarizationbar.iconic.drag-icon-text")
								} ]
							},
							{
								xtype : "container",
								cls : "eid-summarization-bar-name-column",
								items : [ {
									xtype : "box",
									html : '<span class="summarization-name" summarizationId="'
											+ d
											+ '" ext:qtip="'
											+ f.utils
													.localize("summarizationbar.edit.tooltip.edit-sumarization-name")
											+ '">'
											+ Ext.util.Format.htmlEncode(e)
											+ "</span>",
									ref : "../../summarizationNameField",
									listeners : {
										afterrender : function(g) {
											g.el
													.on(
															"click",
															function(i, h) {
																f.mediator
																		.startEditingSummarizationName(
																				f.summarizationNameEditor,
																				h)
															},
															null,
															{
																delegate : ".summarization-name"
															})
										}
									}
								} ]
							},
							{
								xtype : "container",
								cls : "eid-summarization-bar-edit-column",
								items : [ {
									xtype : "box",
									autoEl : {
										html : f.utils
												.localize("summarizationbar.iconic.edit-icon-text"),
										"ext:qtip" : f.utils
												.localize("summarizationbar.edit.tooltip.edit")
									},
									cls : "eid-summarization-bar-action eid-action-icon eid-icon-edit",
									listeners : {
										afterrender : function(g) {
											g.el
													.on(
															"click",
															function(i, h) {
																if (f.summarizationNameEditor.editing) {
																	f.summarizationNameEditor
																			.completeEdit(false)
																}
																f.mediator
																		.switchToSummarizationPanelById(d);
																f
																		.switchToTabPanel()
															})
										}
									}
								} ]
							},
							{
								xtype : "container",
								cls : "eid-summarization-bar-remove-column",
								items : [ {
									xtype : "box",
									autoEl : {
										html : f.utils
												.localize("summarizationbar.iconic.remove-icon-text"),
										"ext:qtip" : f.utils
												.localize("summarizationbar.edit.tooltip.remove")
									},
									cls : "eid-summarization-bar-action eid-action-icon eid-icon-remove",
									listeners : {
										afterrender : function(g) {
											g.el
													.on(
															"click",
															function(j, i) {
																var h = new Ext.ux.endeca.EndecaModalWindow(
																		{
																			title : f.utils
																					.localize("summarizationbar.edit.window.remove-item-title"),
																			cls : "eid-fdbk-modal-warn",
																			panel : {
																				border : false,
																				width : 400,
																				cls : "warning-content",
																				html : f.utils
																						.localize("summarizationbar.edit.window.remove-item-content")
																			},
																			rightButtons : [
																					{
																						text : f.utils
																								.localize("summarizationbar.edit.window.button.no"),
																						handler : function() {
																							h
																									.close()
																						}
																					},
																					{
																						cls : "eid-btn-action",
																						text : f.utils
																								.localize("summarizationbar.edit.window.button.yes"),
																						handler : function() {
																							f.mediator
																									.deleteSummarizationBar(d);
																							h
																									.close()
																						}
																					} ]
																		});
																h.show()
															})
										}
									}
								} ]
							} ],
					listeners : {
						afterrender : function(g) {
							if (f.mediator.summaryItemsDD) {
								f.mediator.summaryItemsDD
										.registerItem(g.el.dom)
							}
							g.el.on("mousedown", function(i, h) {
								if (f.summarizationNameEditor.editing) {
									f.summarizationNameEditor
											.completeEdit(false)
								}
							})
						}
					}
				});
		return b
	},
	switchToTabPanel : function() {
		var f = this;
		var b = f.getCurrentConfig();
		Ext.getCmp(f.portletId + "summary_item_menu").removeClass(
				"eid-menu-selected");
		Ext.getCmp(f.portletId + "general_settings_menu").removeClass(
				"eid-menu-selected");
		if (b.isCompleted) {
			var c = f.getCurrentConfig().getType();
			var e = f.portletId + "step_define_item";
			if (c == f.constants.SUMMARIZATION_TYPE_ALERT_VALUE) {
				e = f.portletId + "step_define_alert"
			}
			var d = new Ext.LoadMask(Ext.getCmp(f.portletId + "right_panel").el);
			d.show();
			var a = f.utils.getAttributes(b.dataSourceId, b.semanticKey);
			if (Ext.isEmpty(a)) {
				f.dataMgr.fetchData(b.dataSourceId, b.semanticKey || "Base", {
					fn : function(g) {
						f.mediator.lastSemanticKey = b.semanticKey;
						f.mediator.lastDataSourceId = b.dataSourceId;
						f.mediator.switchRightPanel(2, f.getTabPanelCard(e));
						d.hide()
					}
				})
			} else {
				d.hide();
				f.mediator.switchRightPanel(2, f.getTabPanelCard(e))
			}
		} else {
			f.mediator.switchRightPanel(2, f.getTabPanelCard(0))
		}
	},
	createSummarizationNameEditor : function() {
		var b = this;
		var a = Ext.getCmp(b.portletId + "summarization_name_editor");
		if (!a) {
			a = new Ext.Editor(
					{
						id : b.portletId + "summarization_name_editor",
						revertInvalid : true,
						shadow : false,
						completeOnEnter : true,
						cancelOnEsc : true,
						updateEl : false,
						ignoreNoChange : true,
						offsets : [ 280, 0 ],
						alignment : "c-l?",
						field : {
							width : 580,
							allowBlank : false,
							xtype : "textfield",
							cls : "eid-summarization-name-editor",
							selectOnFocus : true,
							validator : function(c) {
								if (c.trim() === "") {
									return false
								}
								return true
							}
						},
						listeners : {
							complete : function(c, f, d) {
								var e = c.boundEl.dom
										.getAttribute("summarizationId");
								b.mediator.getSummarizationConfig(e).name = f;
								if (b.mediator.getSummarizationConfig(e).isNew) {
									b.mediator.getSummarizationConfig(e).isChangeName = (d != f)
								}
								b.mediator.updateSummarizationNameInStore(e, f);
								Ext.get(c.boundEl).dom.innerHTML = Ext.util.Format
										.htmlEncode(f)
							},
							afterrender : function(c) {
								c.el
										.first("input")
										.set(
												{
													title : b.utils
															.localize("summarizationbar.edit.input.title.edit-item-name")
												})
							}
						}
					})
		}
		return a
	},
	getGeneralSettingsPanel : function() {
		var b = this;
		var a = new Ext.Container(
				{
					id : b.portletId + "genaral_settings_panel",
					autoWidth : true,
					autoHeight : true,
					border : false,
					items : [ {
						xtype : "container",
						border : false,
						cls : "eid-right-panel-header",
						items : [
								{
									xtype : "displayfield",
									html : "<h3>"
											+ b.utils
													.localize("summarizationbar.edit.panel.general-settings-title")
											+ "</h3>"
								},
								{
									xtype : "displayfield",
									cls : "eid-instruction-text",
									value : b.utils
											.localize("summarizationbar.edit.panel.general-settings-description")
								},
								{
									xtype : "container",
									items : [ {
										border : false,
										xtype : "panel",
										layout : "form",
										cls : "general-settings-panel-items",
										defaults : {
											labelStyle : "width:160px;"
										},
										items : [
												{
													xtype : "radiogroup",
													ctCls : "general-settings-panel-radiogroup",
													width : 230,
													fieldLabel : b.utils
															.localize("summarizationbar.edit.label.display-name-position"),
													items : [
															{
																name : "display_name_position",
																boxLabel : b.utils
																		.localize("summarizationbar.edit.label.display-name-position-below"),
																checked : b.editModel.settingsModel.namePosition != b.constants.GENERAL_SETTINGS_DISPLAY_NAME_POSITION_ABOVE,
																value : b.constants.GENERAL_SETTINGS_DISPLAY_NAME_POSITION_BELOW
															},
															{
																name : "display_name_position",
																boxLabel : b.utils
																		.localize("summarizationbar.edit.label.display-name-position-above"),
																checked : b.editModel.settingsModel.namePosition == b.constants.GENERAL_SETTINGS_DISPLAY_NAME_POSITION_ABOVE,
																value : b.constants.GENERAL_SETTINGS_DISPLAY_NAME_POSITION_ABOVE
															} ],
													listeners : {
														change : function(c, d) {
															b.mediator
																	.handleDisplayNamePositionChange(d.value)
														}
													}
												},
												{
													border : false,
													layout : "column",
													fieldLabel : b.utils
															.localize("summarizationbar.edit.label.value-text-size"),
													items : [
															{
																xtype : "box",
																columnWidth : 0.1,
																html : b.utils
																		.localize("summarizationbar.edit.label.text-size-smaller")
															},
															{
																xtype : "slider",
																cls : "text-size-slider",
																columnWidth : 0.5,
																decimalPrecision : 1,
																value : b.editModel.settingsModel.valueTextSize,
																minValue : b.editModel.settingsModel.valueTextMinSize,
																maxValue : b.editModel.settingsModel.valueTextMaxSize,
																increment : b.editModel.settingsModel.valueTextSizeIncrement,
																plugins : new Ext.slider.Tip(),
																listeners : {
																	change : function(
																			d,
																			e,
																			c) {
																		b.mediator
																				.handleValueTextSizeChange(e)
																	}
																}
															},
															{
																xtype : "box",
																columnWidth : 0.12,
																html : b.utils
																		.localize("summarizationbar.edit.label.text-size-larger")
															} ]
												},
												{
													border : false,
													layout : "column",
													fieldLabel : b.utils
															.localize("summarizationbar.edit.label.label-text-size"),
													items : [
															{
																xtype : "box",
																columnWidth : 0.1,
																html : b.utils
																		.localize("summarizationbar.edit.label.text-size-smaller")
															},
															{
																xtype : "slider",
																cls : "text-size-slider",
																columnWidth : 0.5,
																decimalPrecision : 1,
																value : b.editModel.settingsModel.labelTextSize,
																minValue : b.editModel.settingsModel.labelTextMinSize,
																maxValue : b.editModel.settingsModel.labelTextMaxSize,
																increment : b.editModel.settingsModel.labelTextSizeIncrement,
																plugins : new Ext.slider.Tip(),
																listeners : {
																	change : function(
																			d,
																			e,
																			c) {
																		b.mediator
																				.handleLabelTextSizeChange(e)
																	}
																}
															},
															{
																xtype : "box",
																columnWidth : 0.12,
																html : b.utils
																		.localize("summarizationbar.edit.label.text-size-larger")
															} ]
												} ]
									} ]
								} ]
					} ]
				});
		return a
	},
	getTabPanelCard : function(d) {
		var c = this;
		var b = c.createStepTabPanel(d);
		var a = new Ext.Container({
			id : c.portletId + "top_tab_panel",
			deferredRender : false,
			cls : "top-tab-panel",
			plain : true,
			enableTabScroll : true,
			autoHeight : true,
			items : [ b, c.createWizard() ]
		});
		return a
	},
	createStepTabPanel : function(c) {
		var h = this;
		var e = h.getCurrentConfig();
		var g = e.getType();
		var b = h.getSelectDataPanel();
		var j = h.getSelectTypePanel();
		var a = h.getItemContainerPanel();
		var d = h.getDefineConditionPanel();
		var i = h.getAlertContainerTabPanel();
		var k = {};
		k[h.portletId + "step_select_data"] = b;
		k[h.portletId + "step_select_type"] = j;
		k[h.portletId + "step_define_item"] = a;
		k[h.portletId + "step_define_conditions"] = d;
		k[h.portletId + "step_define_alert"] = i;
		var f = new Ext.TabPanel(
				{
					id : h.portletId + "step_tab_panel",
					cls : "step-tab-panel eid-configuration-tab",
					activeItem : c,
					autoWidth : true,
					autoHeight : true,
					border : true,
					hidden : true,
					items : [
							{
								itemId : h.portletId + "step_select_data",
								autoHeight : true,
								title : h.utils
										.localize("summarizationbar.edit.tabpanel.select-data"),
								items : b
							},
							{
								itemId : h.portletId + "step_select_type",
								title : h.utils
										.localize("summarizationbar.edit.tabpanel.select-item-type"),
								items : j
							},
							{
								itemId : h.portletId + "step_define_item",
								autoHeight : true,
								title : h.utils
										.localize("summarizationbar.edit.tabpanel.define-item"),
								items : a
							},
							{
								itemId : h.portletId + "step_define_conditions",
								autoHeight : true,
								title : h.utils
										.localize("summarizationbar.edit.tabpanel.define-conditions"),
								items : d
							},
							{
								itemId : h.portletId + "step_define_alert",
								autoHeight : true,
								title : h.utils
										.localize("summarizationbar.edit.tabpanel.define-alert"),
								items : i
							} ],
					listeners : {
						afterrender : function(l) {
							l.el.select("li > a[class!=x-tab-strip-close]")
									.addClass("async-ignore");
							h.mediator.handleStepTabDisplayHide(g)
						},
						tabchange : function(m, l) {
							h.mediator.handleTabChange(m, l, k)
						}
					}
				});
		return f
	},
	createWizard : function(a) {
		var b = this;
		return new Ext.Container(
				{
					id : b.portletId + "_wizard",
					layout : "column",
					cls : "eid-summarization-wizard-container",
					border : false,
					items : [
							{
								xtype : "box",
								id : b.portletId + "_prev_btn",
								columnWidth : 0.5,
								cls : "eid-action-icon eid-icon-wizard-previous",
								disabled : true,
								html : b.utils
										.localize("summarizationbar.iconic.previous-icon-text"),
								listeners : {
									afterrender : function(d) {
										d.el.on("click", function(f, c, g) {
											if (!d.disabled) {
												b.navigationHandler(true)
											}
										})
									}
								}
							},
							{
								xtype : "box",
								id : b.portletId + "_next_btn",
								columnWidth : 0.5,
								cls : "eid-action-icon eid-icon-wizard-next",
								html : b.utils
										.localize("summarizationbar.iconic.next-icon-text"),
								listeners : {
									afterrender : function(d) {
										d.el.on("click", function(f, c, g) {
											if (!d.disabled) {
												b.navigationHandler(false)
											}
										})
									}
								}
							} ]
				})
	},
	navigationHandler : function(d) {
		var f = this;
		var e = Ext.getCmp(f.portletId + "step_tab_panel");
		var a = e.getActiveTab();
		var b;
		var c = f.getCurrentConfig().getType();
		if (c == f.constants.SUMMARIZATION_TYPE_ALERT_VALUE) {
			if (a.getItemId() == f.portletId + "step_select_type" && !d) {
				b = e.getItem(f.portletId + "step_define_alert")
			} else {
				if (a.getItemId() == f.portletId + "step_define_alert" && d) {
					b = e.getItem(f.portletId + "step_select_type")
				}
			}
		}
		if (b == undefined) {
			b = d ? a.previousSibling() : a.nextSibling()
		}
		if (b) {
			e.activate(b)
		}
	},
	getLeftPanel : function() {
		var b = this;
		var a = new Ext.Container(
				{
					id : b.portletId + "left_panel",
					autoHeight : true,
					border : false,
					cls : "eid-configuration-navigation",
					items : [
							{
								xtype : "container",
								id : b.portletId + "summary_item_menu",
								html : '<span class="eid-action-icon eid-icon-white-arrow-open"></span>'
										+ b.utils
												.localize("summarizationbar.edit.displayfield.summary-items"),
								cls : "eid-first-level-menu",
								listeners : {
									afterrender : function(c) {
										c.el.addClass("eid-menu-selected");
										c.el
												.on(
														"click",
														function(g, d) {
															if (d.tagName
																	.toLowerCase() == "span") {
																var f = Ext
																		.get(Ext
																				.query(
																						"span",
																						c.el.dom)[0]);
																if (f
																		.hasClass("eid-icon-white-arrow-open")) {
																	f
																			.addClass("eid-icon-white-arrow-closed");
																	f
																			.removeClass("eid-icon-white-arrow-open");
																	Ext
																			.getCmp(
																					b.portletId
																							+ "summarization_grid")
																			.collapse(
																					true)
																} else {
																	f
																			.addClass("eid-icon-white-arrow-open");
																	f
																			.removeClass("eid-icon-white-arrow-closed");
																	Ext
																			.getCmp(
																					b.portletId
																							+ "summarization_grid")
																			.expand(
																					true)
																}
															} else {
																Ext
																		.getCmp(
																				b.portletId
																						+ "general_settings_menu")
																		.removeClass(
																				"eid-menu-selected");
																if (!c.el
																		.hasClass("eid-menu-selected")) {
																	c.el
																			.addClass("eid-menu-selected")
																}
																b.mediator
																		.switchRightPanel(0)
															}
														})
									}
								}
							},
							{
								xtype : "grid",
								id : b.portletId + "summarization_grid",
								cls : "eid-summarization-grid",
								store : new Ext.data.JsonStore({
									storeId : b.portletId
											+ "summarization_store",
									fields : [ {
										name : "id"
									}, {
										name : "name"
									}, {
										name : "itemConfig"
									}, {
										name : "dataSourceId"
									}, {
										name : "semanticKey"
									}, {
										name : "type"
									}, {
										name : "isCompleted"
									}, {
										name : "alertConfig"
									} ],
									root : "configs",
									data : b.editModel
								}),
								width : 165,
								autoHeight : true,
								border : false,
								hideHeaders : true,
								viewConfig : {
									getRowClass : function(c, f, d, e) {
										return "eid-second-level-menu"
									}
								},
								columns : [ {
									width : 165,
									renderer : function(f, e, d) {
										var c = "";
										if (d.get("isCompleted") === false) {
											c = "<span class='eid-icon-warning eid-icon'></span>"
										}
										c += Ext.util.Format.htmlEncode(d
												.get("name"));
										return c
									}
								} ],
								listeners : {
									rowclick : function(d, c, f) {
										if (b.mediator.summarizationIndex != c) {
											b.mediator.summarizationIndex = c;
											b.mediator
													.switchToSummarizationPanelByIndex(c);
											b.switchToTabPanel()
										}
									},
									afterrender : function(f) {
										var d = f.getStore();
										var c = f.getView();
										f.tip = new Ext.ToolTip(
												{
													target : c.mainBody,
													delegate : ".x-grid3-row",
													trackMouse : true,
													renderTo : document.body,
													listeners : {
														beforeshow : function e(
																g) {
															var h = c
																	.findRowIndex(g.triggerElement);
															g.body.dom.innerHTML = Ext.util.Format
																	.htmlEncode(b.editModel.configs[h]["name"])
														}
													}
												})
									},
									viewready : function(c) {
										if (c.getStore().getCount() == 0) {
											Ext.select(".x-grid3-body", false,
													c.body.dom).update("")
										}
									}
								}
							},
							{
								xtype : "container",
								id : b.portletId + "general_settings_menu",
								html : b.utils
										.localize("summarizationbar.edit.displayfield.general-settings"),
								cls : "eid-first-level-menu eid-navigation-menu-last",
								listeners : {
									afterrender : function(c) {
										c.el
												.on(
														"click",
														function() {
															Ext
																	.getCmp(
																			b.portletId
																					+ "summary_item_menu")
																	.removeClass(
																			"eid-menu-selected");
															if (!c.el
																	.hasClass("eid-menu-selected")) {
																c.el
																		.addClass("eid-menu-selected")
															}
															b.mediator
																	.switchRightPanel(1)
														})
									}
								}
							} ]
				});
		return a
	},
	getAlertContainerTabPanel : function() {
		var c = this;
		var b = c.getCurrentConfig();
		var a = new Ext.Panel(
				{
					xtype : "panel",
					cls : "eid-define-alert-tab-panel",
					id : c.portletId + "alert_tab_panel",
					border : false,
					items : [
							c
									.getCommonHeaderPanel(
											"summarizationbar.edit.panel.define-alert-tab.description",
											"step_define_alert"),
							c.getAlertDisplayNamePanel(),
							c.getAlertDimensionsPanel(), c.getAlertCFPanel(),
							c.getAlertDescriptionPanel(),
							c.getAlertDisplayOptionsPanel(),
							c.getAlertActionPanel() ],
					loadDataToUI : function() {
						c.mediator.loadDataToDefineAlertTabPanel()
					}
				});
		return a
	},
	getAlertDisplayNamePanel : function() {
		var b = this;
		var a = b.getCurrentConfig();
		return {
			border : false,
			layout : "form",
			items : [
					{
						fieldLabel : b.utils
								.localize("summarizationbar.edit.label.display-name"),
						xtype : "textfield",
						cls : "eid-alert-display-name",
						labelStyle : "margin-top: 2px;",
						allowBlank : false,
						id : b.portletId + "alert_display_name",
						value : a.alertConfig.displayName,
						listeners : {
							change : function(d, e, c) {
								b.mediator.handleAlertDisplayNameChange(e, c)
							}
						}
					},
					{
						xtype : "combo",
						editable : false,
						forceSelection : true,
						selectOnFocus : true,
						lazyInit : false,
						listWidth : 45,
						mode : "local",
						fieldClass : "eid-combo-text",
						ctCls : "eid-alert-color-radio",
						store : b.getAlertColorStore(),
						tpl : b.getAlertColorCobTpl(),
						fieldLabel : b.utils
								.localize("summarizationbar.edit.panel.define-alert-tab.alert-color.label"),
						listeners : {
							select : function(e, c, d) {
								b.mediator.handleAlertColorSelect(e,
										c.data.color)
							},
							afterrender : function(c) {
								b.mediator.handleAlertColorAfterRender(c)
							}
						}
					} ]
		}
	},
	getAlertColorCobTpl : function() {
		return '<tpl for="."><div class="x-combo-list-item" style="height: 14px; background: none repeat scroll 0 0 {color};"></div></tpl>'
	},
	getAlertColorStore : function() {
		var a = this;
		if (!a.alertColorStore) {
			a.alertColorStore = new Ext.data.ArrayStore({
				fields : [ "color" ],
				data : [ [ "#FF0000" ], [ "#E6AD02" ], [ "#65C406" ],
						[ "#56BCC1" ], [ "#666666" ] ]
			})
		}
		return a.alertColorStore
	},
	getAlertDimensionsPanel : function() {
		var b = this;
		var a = b.getCurrentConfig();
		return {
			border : false,
			cls : "eid-dimensions-panel  eid-item-panel",
			id : b.portletId + "alert_dimensions_panel",
			items : [
					{
						xtype : "displayfield",
						cls : "eid-item-title",
						value : b.utils
								.localize("summarizationbar.edit.panel.define-alert-tab.dimension-panel.title")
					},
					{
						border : false,
						cls : "eid-item-instruction",
						html : b.utils
								.localize("summarizationbar.edit.panel.define-alert-tab.dimension-panel.description")
					},
					{
						xtype : "container",
						id : b.portletId + "alert_dimensions_container",
						cls : "eid-alert-dimensions-container",
						border : false,
						autoHeight : true,
						width : 400,
						items : [ b.getAlertDimensionPanels() ],
						listeners : {
							afterrender : function() {
								b.mediator.registerAlertDimensionsDD()
							}
						}
					},
					{
						xtype : "button",
						cls : "x-btn-text-icon eid-btn-add",
						text : b.utils
								.localize("summarizationbar.edit.panel.define-alert-tab.dimension-panel.button.add-dimension"),
						handler : function() {
							var c = b.getAlertDimensionPanel({});
							b.mediator.handleAlertDimensionAdd(c);
							b.mediator.registerAlertDimensionsDD()
						}
					} ]
		}
	},
	getAlertDimensionPanels : function() {
		var b = this;
		var a = [];
		Ext.each(b.getCurrentConfig().alertConfig.dimensionsList, function(c) {
			a.push(b.getAlertDimensionPanel(c))
		});
		return a
	},
	getAlertDimensionPanel : function(b) {
		var c = this;
		var a = b.key || undefined;
		return new Ext.ux.endeca.AttributeSelectField(
				{
					attributeType : Ext.ux.endeca.AttributeSelectConstants.ATTRIBUTE_TYPE.ALL_DIMENSION,
					buttonTextType : Ext.ux.endeca.AttributeSelectConstants.BUTTON_TEXT_TYPE.DIMENSION,
					enableSystemMetrics : false,
					draggable : true,
					boxMinWidth : 300,
					dataSourceId : c.getCurrentConfig().dataSourceId,
					semanticKey : c.getCurrentConfig().semanticKey,
					dataMgr : c.dataMgr,
					attrData : {
						attributeKey : b.key,
						formatter : b.formatter,
						datePartCombo : b.datePartCombo
					},
					configWinConfig : {
						hasFormatter : true,
						hasAction : false,
						hasDatePart : true
					},
					resources : c.utils.resources,
					formatterPreviewUrl : c.resourceUrls.formatterPreviewUrl,
					formatterGetCurrencyListUrl : c.resourceUrls.formatterGetCurrencyListUrl,
					formatterPanelResource : c.formatterPanelResource,
					listeners : {
						beforeapply : function() {
							c.mediator
									.handleAlertDimensionBeforeChange(a, this)
						},
						attrchange : function(d) {
							c.mediator.handleAlertDimensionChange(a, this, d);
							a = d.attributeKey
						},
						"delete" : function() {
							c.mediator.handleAlertDimensionDelete(a, this)
						},
						winclose : function() {
							c.mediator.handleAlertDimensionWindowClose(a, this)
						}
					}
				})
	},
	getAlertCFPanel : function() {
		var b = this;
		var a = b.getCurrentConfig();
		return {
			bodyBorder : false,
			cls : "eid-item-panel eid-conditions-panel",
			id : b.portletId + "alert_conditions_panel",
			items : [
					{
						xtype : "displayfield",
						cls : "eid-item-title",
						value : b.utils
								.localize("summarizationbar.edit.panel.define-alert-tab.condition-panel.title")
					},
					{
						border : false,
						xtype : "compositefield",
						cls : "eid-alert-instruction-btn-compositefield",
						items : [
								{
									border : false,
									cls : "eid-item-instruction eid-alert-cf-instruction",
									html : b.getAlertCFPanelIntroHTML()
								},
								{
									xtype : "button",
									cls : "x-btn-text-icon eid-btn-add eid-alert-add-condition-btn",
									text : b.utils
											.localize("conditional-formattingv2.button.add-condition"),
									handler : function() {
										var c = Ext.getCmp(b.portletId
												+ "alert_cf_panel");
										if (!c) {
											return
										} else {
											c.addCondition()
										}
									}
								} ]
					},
					{
						id : b.portletId + "alert_cf_panel",
						bodyBorder : false,
						xtype : "conditionalFormatterPanelV2",
						columnConditionRuleTitle : "conditional-formattingv2.column.title.alert-rule",
						autoExpandColumn : "conditionRule",
						showCoditionStyleSelection : false,
						isShowPercentageSymbol : true,
						resources : this.utils.resources,
						conditionColumns : [
								"dragHandler",
								{
									header : b.utils
											.localize("summarizationbar.edit.panel.define-alert-tab.condition-panel.column.title.type"),
									dataIndex : "type",
									width : 200,
									fixed : true,
									renderer : function(g, e, d) {
										var c = b.portletId + d.id
												+ "_condition_type";
										var f = c + "_container";
										function h() {
											new Ext.form.ComboBox(
													{
														xtype : "combo",
														id : c,
														fieldClass : "eid-combo-text",
														editable : false,
														width : 180,
														value : g,
														valueField : "type",
														displayField : "typeDsp",
														store : new Ext.data.ArrayStore(
																{
																	fields : [
																			{
																				name : "type",
																				type : "String"
																			},
																			{
																				name : "typeDsp",
																				type : "String"
																			} ],
																	data : [
																			[
																					b.constants.CONDITION_TYPE_SELF_VALUE,
																					b.utils
																							.localize("summarizationbar.edit.label.condition-checkbox-label-metric") ],
																			[
																					b.constants.CONDITION_TYPE_TO_ANOTHER_METRIC,
																					b.utils
																							.localize("summarizationbar.edit.label.condition-checkbox-label-metric-to-metric") ] ]
																}),
														forceSelection : true,
														mode : "local",
														selectOnFocus : true,
														triggerAction : "all",
														listeners : {
															afterrender : function(
																	i) {
																i.el
																		.set({
																			title : b.utils
																					.localize("summarizationbar.edit.panel.define-alert-tab.condition-panel.column.title.type")
																		})
															},
															select : function(
																	o, l, j) {
																var i = b.portletId
																		+ d.id
																		+ "_metric";
																var m = b.portletId
																		+ d.id
																		+ "_target_metric";
																var n = b.portletId
																		+ d.id
																		+ "_divide_symbol";
																var k = b.portletId
																		+ d.id
																		+ "_ratio_formatter";
																var p = d.id
																		+ "_condition_rule_percentage_symbol";
																Ext
																		.getCmp(
																				b.portletId
																						+ "alert_cf_panel")
																		.updateGridRecord(
																				d,
																				"type",
																				l.data.type);
																if (l.data.type == b.constants.CONDITION_TYPE_SELF_VALUE) {
																	Ext
																			.getCmp(i).configWinConfig.hasFormatter = true;
																	Ext
																			.getCmp(
																					m)
																			.hide();
																	Ext
																			.getCmp(
																					n)
																			.hide();
																	Ext
																			.getCmp(
																					k)
																			.hide();
																	Ext
																			.getCmp(
																					p)
																			.hide()
																} else {
																	if (l.data.type == b.constants.CONDITION_TYPE_TO_ANOTHER_METRIC) {
																		Ext
																				.getCmp(i).configWinConfig.hasFormatter = false;
																		Ext
																				.getCmp(
																						m)
																				.show();
																		Ext
																				.getCmp(
																						n)
																				.show();
																		Ext
																				.getCmp(
																						k)
																				.show();
																		Ext
																				.getCmp(
																						p)
																				.show()
																	}
																}
															}
														}
													}).render(f);
											new Ext.form.Label(
													{
														width : 10,
														hidden : true,
														hideMode : "offsets",
														forId : c,
														html : b.utils
																.localize("summarizationbar.edit.panel.define-alert-tab.condition-panel.column.title.type")
													}).render(f)
										}
										h.defer(100, this);
										return '<div id="' + f + '"></div>'
									}
								},
								{
									header : b.utils
											.localize("summarizationbar.edit.panel.define-alert-tab.condition-panel.column.title.metric"),
									renderer : function(c, h, d) {
										var e = b.portletId + d.id + "_metric";
										var i = b.portletId + d.id
												+ "_target_metric";
										var k = b.portletId + d.id
												+ "_divide_symbol";
										var f = b.portletId + d.id
												+ "_ratio_formatter";
										var g = i + "_container";
										function j() {
											new Ext.Container(
													{
														cls : "conditional-metric-column-container",
														bodyBorder : false,
														layout : "column",
														bodyStyle : "background: none;",
														defaults : {
															bodyBorder : false
														},
														items : [
																new Ext.ux.endeca.AttributeSelectField(
																		{
																			attributeType : Ext.ux.endeca.AttributeSelectConstants.ATTRIBUTE_TYPE.NUMERIC_METRIC,
																			boxMinWidth : 100,
																			height : 25,
																			id : e,
																			dataSourceId : b
																					.getCurrentConfig().dataSourceId,
																			semanticKey : b
																					.getCurrentConfig().semanticKey,
																			dataMgr : b.dataMgr,
																			attrData : d.data.metric ? {
																				attributeKey : d.data.metric.key,
																				aggregation : d.data.metric.aggregation,
																				formatter : d.data.metric.formatter
																			}
																					: null,
																			configWinConfig : {
																				hasFormatter : d.data.type != b.constants.CONDITION_TYPE_TO_ANOTHER_METRIC,
																				hasAction : false
																			},
																			resources : b.utils.resources,
																			formatterPreviewUrl : b.resourceUrls.formatterPreviewUrl,
																			formatterGetCurrencyListUrl : b.resourceUrls.formatterGetCurrencyListUrl,
																			formatterPanelResource : b.formatterPanelResource,
																			listeners : {
																				attrchange : function(
																						m) {
																					var l = {
																						key : m.attributeKey,
																						aggregation : m.aggregation,
																						formatter : m.formatter
																					};
																					Ext
																							.getCmp(
																									b.portletId
																											+ "alert_cf_panel")
																							.updateGridRecord(
																									d,
																									"metric",
																									l)
																				},
																				cancel : function() {
																				},
																				"delete" : function() {
																					Ext
																							.getCmp(
																									b.portletId
																											+ "alert_cf_panel")
																							.updateGridRecord(
																									d,
																									"metric",
																									null)
																				}
																			}
																		}),
																{
																	xtype : "box",
																	id : k,
																	cls : "eid-label-divide",
																	html : b.utils
																			.localize("summarizationbar.edit.label.divide"),
																	hidden : d.data.type != b.constants.CONDITION_TYPE_TO_ANOTHER_METRIC
																},
																new Ext.ux.endeca.AttributeSelectField(
																		{
																			id : i,
																			hidden : d.data.type != b.constants.CONDITION_TYPE_TO_ANOTHER_METRIC,
																			attributeType : Ext.ux.endeca.AttributeSelectConstants.ATTRIBUTE_TYPE.NUMERIC_METRIC,
																			height : 25,
																			dataSourceId : b
																					.getCurrentConfig().dataSourceId,
																			semanticKey : b
																					.getCurrentConfig().semanticKey,
																			dataMgr : b.dataMgr,
																			attrData : d.data.targetMetric ? {
																				attributeKey : d.data.targetMetric.key,
																				aggregation : d.data.targetMetric.aggregation
																			}
																					: null,
																			configWinConfig : {
																				hasFormatter : false,
																				hasAction : false
																			},
																			resources : b.utils.resources,
																			formatterPreviewUrl : b.resourceUrls.formatterPreviewUrl,
																			formatterGetCurrencyListUrl : b.resourceUrls.formatterGetCurrencyListUrl,
																			formatterPanelResource : b.formatterPanelResource,
																			listeners : {
																				attrchange : function(
																						m) {
																					var l = {
																						key : m.attributeKey,
																						aggregation : m.aggregation,
																						formatter : m.formatter
																					};
																					Ext
																							.getCmp(
																									b.portletId
																											+ "alert_cf_panel")
																							.updateGridRecord(
																									d,
																									"targetMetric",
																									l)
																				},
																				cancel : function() {
																				},
																				"delete" : function() {
																					Ext
																							.getCmp(
																									b.portletId
																											+ "alert_cf_panel")
																							.updateGridRecord(
																									d,
																									"targetMetric",
																									null)
																				}
																			}
																		}),
																{
																	xtype : "box",
																	id : f,
																	cls : "eid-icon-edit eid-action-icon eid-condition-ratio-formatter",
																	hidden : d.data.type != b.constants.CONDITION_TYPE_TO_ANOTHER_METRIC,
																	listeners : {
																		afterrender : function(
																				l) {
																			l.el
																					.on(
																							"click",
																							function(
																									n,
																									o,
																									m) {
																								b
																										.getAlertCFRatioFormatWindow(
																												m)
																										.show()
																							},
																							this,
																							d.data)
																		}
																	}
																} ]
													}).render(g)
										}
										j.defer(100, this);
										return '<div id="'
												+ g
												+ '" class = "condition-metrics-column"></div>'
									}
								}, "conditionRule", "action" ],
						cfGridStoreField : [ "id", "type", "metric",
								"targetMetric", "ratioFormatter", "operator",
								"value1", "value2" ],
						cfDefaultGridStoreRecord : b.editModel.defaultConfigUtils
								.getDefaultAlertConditionsConfig(),
						portletId : this.portletId,
						conditionData : {
							conditions : a.alertConfig.conditonsList
						},
						afterCollect : function(c) {
							a.alertConfig.conditonsList = c.conditions
						},
						afterRecordUpdate : function(c, d) {
							b.mediator.syncAlertDisplayColumn(false, c.data, d,
									b);
							b.mediator.syncAlertIsCompleted()
						},
						afterConditionsOrderChange : function() {
							b.mediator.syncAlertDisplayColumnsList();
							b.mediator.updateAlertDisplayColumnsUI(b
									.getAlertConditionColumnsPanel())
						}
					} ]
		}
	},
	getAlertCFRatioFormatWindow : function(a) {
		var c = this;
		var b = new Ext.ux.endeca.EndecaModalWindow(
				{
					title : c.utils
							.localize("summarizationbar.edit.panel.define-alert-tab.condition-panel.ratio-format-win.title"),
					cls : "eid-ratio-condition-formatter-configuration-window",
					autoDestroy : true,
					items : [ new Ext.ux.endeca.AttributeFormatterPanelV2(
							{
								id : a.id + "_ratioFormatterPanel",
								cls : "endeca-attribute-formatter-v2",
								attrType : c.constants.MDEX_DATA_TYPE_DOUBLE,
								previewUrl : c.resourceUrls.formatterPreviewUrl,
								currencyListUrl : c.resourceUrls.formatterGetCurrencyListUrl,
								extraResources : c.formatterPanelResource,
								messages : c.utils.resources,
								formatData : Ext.isEmpty(a.ratioFormatter) ? undefined
										: Ext.decode(a.ratioFormatter),
								parentFormatData : Ext
										.decode(c.constants.PERCENTAGE_FORMAT),
								initCollapsed : false,
								enablePreview : true,
								autoHeight : true
							}) ],
					buttons : [
							"->",
							{
								text : c.utils
										.localize("summarizationbar.edit.button.cancel"),
								handler : function() {
									b.close();
									b.destroy()
								}
							},
							{
								cls : "eid-btn-action",
								text : c.utils
										.localize("summarizationbar.edit.button.apply"),
								handler : function() {
									a.ratioFormatter = Ext.encode(Ext.getCmp(
											a.id + "_ratioFormatterPanel")
											.generateFormatterObject());
									b.close();
									b.destroy()
								}
							} ]
				});
		return b
	},
	getAlertCFPanelIntroHTML : function() {
		var a = this;
		return a.utils
				.localize("summarizationbar.edit.panel.define-alert-tab.condition-panel.description")
				+ '<span class="eid-action-icon eid-icon-help eid-condition-intro-qtip" ext:qprofile="info" ext:qtip="'
				+ a.utils
						.localize("summarizationbar.edit.panel.define-alert-tab.condition-panel.qtip")
				+ '">'
	},
	getAlertDescriptionPanel : function() {
		var b = this;
		var a = b.getCurrentConfig();
		return {
			border : false,
			cls : "eid-alert-description-panel",
			items : [
					{
						xtype : "label",
						cls : "eid-item-title",
						html : b.utils
								.localize("summarizationbar.edit.panel.define-alert-tab.description-panel.title")
					},
					{
						border : false,
						layout : "form",
						defaults : {
							labelStyle : "width: 170px; margin-top : 3px;"
						},
						items : [
								{
									xtype : "numberfield",
									allowNegative : false,
									allowBlank : false,
									id : b.portletId + "alert_max_alert_number",
									fieldLabel : b.utils
											.localize("summarizationbar.edit.panel.define-alert-tab.description-panel.max-alert-number.label"),
									value : a.alertConfig.maxNumber,
									listeners : {
										change : function(d, e, c) {
											b.mediator
													.handleAlertMaxNumberChange(
															e, c)
										}
									}
								},
								{
									xtype : "textarea",
									cls : "eid-alert-description-textarea",
									id : b.portletId + "alert_description",
									fieldLabel : b.utils
											.localize("summarizationbar.edit.panel.define-alert-tab.description-panel.description.label"),
									value : a.alertConfig.description,
									width : 500,
									listeners : {
										change : function(c, e, d) {
											b.mediator
													.handleAlertDescriptionChange(e)
										}
									}
								} ]
					} ]
		}
	},
	getAlertDisplayOptionsPanel : function() {
		var b = this;
		var a = this.getCurrentConfig();
		return new Ext.Panel(
				{
					border : false,
					id : b.portletId + "alert_display_options_panel",
					cls : "eid-alert-display-option-panel eid-collapsible-panel",
					title : b.utils
							.localize("summarizationbar.edit.panel.define-alert-tab.display-options-panel.title")
							+ '<span class="eid-showhide-text eid-showhide-text-opened">'
							+ b.utils
									.localize("summarizationbar.iconic.showhide-opened")
							+ '</span><span class="eid-showhide-text eid-showhide-text-closed">'
							+ b.utils
									.localize("summarizationbar.iconic.showhide-closed")
							+ "</span>",
					collapsible : true,
					collapsed : true,
					titleCollapse : true,
					animCollapse : false,
					headerCssClass : "eid-collapsible-panel-header",
					bodyCssClass : "eid-collapsible-panel-body",
					layout : "form",
					defaults : {
						labelStyle : "width: 170px;"
					},
					items : [ b.getAlertWidthPanel(),
							b.getAlertDimensionColumnPanel(),
							b.getAlertConditionColumnsPanel() ]
				})
	},
	getAlertWidthPanel : function() {
		var b = this;
		var a = this.getCurrentConfig();
		return {
			fieldLabel : b.utils
					.localize("summarizationbar.edit.panel.define-alert-tab.display-options-panel.width.label"),
			border : false,
			items : [
					{
						xtype : "box",
						cls : "eid-label-instruction",
						html : b.utils
								.localize("summarizationbar.edit.label.display-description")
					},
					{
						xtype : "container",
						cls : "eid-define-item-max-width-container",
						layout : "column",
						items : [
								{
									xtype : "radio",
									fieldClass : "eid-field-group-to-right",
									boxLabel : b.utils
											.localize("summarizationbar.edit.radio.label.default-max-width"),
									id : b.portletId
											+ "alert_default_max_width",
									name : b.portletId + "alert_max_width",
									checked : !a.alertConfig.isCustomWidth
								},
								{
									xtype : "radio",
									boxLabel : b.utils
											.localize("summarizationbar.edit.radio.label.custom-max-width"),
									id : b.portletId + "alert_max_width",
									name : b.portletId + "alert_max_width",
									checked : a.alertConfig.isCustomWidth,
									listeners : {
										check : function(c, d) {
											b.mediator
													.handleAlertIsCustomWidthCheck(d)
										}
									}
								},
								{
									xtype : "numberfield",
									allowNegative : false,
									allowBlank : false,
									id : b.portletId + "alert_custom_max_width",
									width : 50,
									value : a.alertConfig.customWidth,
									disabled : !a.alertConfig.isCustomWidth,
									listeners : {
										change : function(d, e, c) {
											b.mediator
													.handleAlertCustomWidthChange(
															e, c)
										}
									}
								},
								{
									xtype : "label",
									forId : b.portletId
											+ "alert_custom_max_width",
									html : b.utils
											.localize("summarizationbar.edit.label.pixel")
								} ]
					} ]
		}
	},
	getAlertDimensionColumnPanel : function() {
		var b = this;
		var a = this.getCurrentConfig();
		return {
			border : false,
			cls : "eid-alert-dimension-column-panel",
			id : b.portletId + "alert_dimension_column_panel",
			fieldLabel : b.utils
					.localize("summarizationbar.edit.panel.define-alert-tab.display-options-panel.dimension-column.label"),
			items : [ b.getAlertDisplayColumnItemPanel(
					a.alertConfig.displayColumnsList[0], false) ]
		}
	},
	getAlertConditionColumnsPanel : function() {
		var c = this;
		var a = this.getCurrentConfig();
		var b = {
			border : false,
			id : c.portletId + "alert_condition_columns_panel",
			hidden : a.alertConfig.displayColumnsList.length == 1,
			fieldLabel : c.utils
					.localize("summarizationbar.edit.panel.define-alert-tab.display-options-panel.conditions-column.label"),
			items : []
		};
		Ext.each(a.alertConfig.displayColumnsList, function(e, d) {
			if (d != 0) {
				b.items.push(c.getAlertDisplayColumnItemPanel(e, true))
			}
		});
		return b
	},
	getAlertDisplayColumnItemPanel : function(a, c) {
		var d = this;
		var b = [];
		b.push({
			xtype : "textfield",
			allowBlank : false,
			hideLabel : true,
			width : c ? 400 : 425,
			disabled : !a.isDisplay || !a.isCustomName,
			id : d.portletId + "alert_display_column_name_" + a.id,
			value : a.isCustomName ? a.customName : a.autoGenName,
			listeners : {
				change : function(f, g, e) {
					d.mediator.handleAlertDisplayColumnNameChange(g, e, a.id)
				}
			}
		});
		b
				.push({
					xtype : "label",
					hidden : true,
					hideMode : "visibility",
					forId : d.portletId + "alert_display_column_name_" + a.id,
					html : d.utils
							.localize("summarizationbar.edit.panel.define-alert-tab.display-options-panel.columns-name.title")
				});
		if (c) {
			b.unshift({
				xtype : "checkbox",
				width : 20,
				boxLabel : undefined,
				hideLabel : true,
				id : d.portletId + "alert_display_column_display_" + a.id,
				checked : a.isDisplay,
				listeners : {
					check : function(f, e) {
						d.mediator.handleAlertDisplayColumnCheck(e, a.id)
					}
				}
			});
			b
					.push({
						xtype : "label",
						hidden : true,
						hideMode : "visibility",
						forId : d.portletId + "alert_display_column_display_"
								+ a.id,
						html : d.utils
								.localize("summarizationbar.edit.panel.define-alert-tab.display-options-panel.display-columns.title")
					})
		}
		return {
			border : false,
			id : d.portletId + "alert_display_column_panel_" + a.id,
			items : [
					{
						border : false,
						xtype : "compositefield",
						cls : "eid-alert-column-compositefield-item",
						items : b
					},
					{
						xtype : "container",
						layout : "column",
						cls : c ? "eid-alert-column-radio-group" : "",
						id : d.portletId
								+ "alert_display_column_auto_or_custom_radio_group"
								+ a.id,
						items : [
								{
									xtype : "radio",
									fieldClass : "eid-field-group-to-right",
									boxLabel : d.utils
											.localize("summarizationbar.edit.panel.define-alert-tab.display-options-panel.display-columns.radio.label.auto-name"),
									id : d.portletId
											+ "alert_display_column_auto_name_"
											+ a.id,
									name : d.portletId
											+ "alert_display_column_auto_or_custom_"
											+ a.id,
									disabled : !a.isDisplay,
									checked : !a.isCustomName
								},
								{
									xtype : "radio",
									boxLabel : d.utils
											.localize("summarizationbar.edit.panel.define-alert-tab.display-options-panel.display-columns.radio.label.custom-name"),
									id : d.portletId
											+ "alert_display_column_custom_name_"
											+ a.id,
									name : d.portletId
											+ "alert_display_column_auto_or_custom_"
											+ a.id,
									disabled : !a.isDisplay,
									checked : a.isCustomName,
									listeners : {
										check : function(e, f) {
											d.mediator
													.handleAlertDisplayColumnNameCustomCheck(
															f, a.id)
										}
									}
								} ]
					} ]
		}
	},
	getAlertActionPanel : function() {
		var b = this;
		var a = this.getCurrentConfig().alertConfig;
		return new Ext.Panel(
				{
					border : false,
					id : b.portletId + "alert_action_panel",
					cls : "eid-alert-action-panel eid-collapsible-panel",
					title : b.utils
							.localize("summarizationbar.edit.panel.define-alert-tab.action-panel.title")
							+ '<span class="eid-showhide-text eid-showhide-text-opened">'
							+ b.utils
									.localize("summarizationbar.iconic.showhide-opened")
							+ '</span><span class="eid-showhide-text eid-showhide-text-closed">'
							+ b.utils
									.localize("summarizationbar.iconic.showhide-closed")
							+ "</span>",
					collapsible : true,
					collapsed : true,
					titleCollapse : true,
					animCollapse : false,
					headerCssClass : "eid-collapsible-panel-header",
					bodyCssClass : "eid-collapsible-panel-body",
					layout : "form",
					items : [
							{
								xtype : "box",
								labelStyle : "width: 0px;",
								cls : "eid-item-instruction",
								html : b.utils
										.localize("summarizationbar.edit.panel.define-alert-tab.action-panel.instruction-text")
							},
							{
								border : false,
								labelStyle : "width: 170px;",
								fieldLabel : b.utils
										.localize("summarizationbar.edit.panel.define-alert-tab.action-panel.refinement.current-page.radio.filed-label"),
								items : [
										{
											border : false,
											layout : "column",
											items : [ {
												xtype : "radio",
												width : 100,
												boxLabel : b.utils
														.localize("summarizationbar.edit.panel.define-alert-tab.action-panel.refinement.current-page.radio.box-label"),
												name : b.portletId
														+ "alert_refinement_action_radio",
												checked : !a.isOtherPage
											} ]
										},
										{
											border : false,
											layout : "column",
											items : [
													{
														xtype : "radio",
														width : 75,
														boxLabel : b.utils
																.localize("summarizationbar.edit.panel.define-alert-tab.action-panel.refinement.other-page.radio.box-label"),
														name : b.portletId
																+ "alert_refinement_action_radio",
														checked : a.isOtherPage,
														listeners : {
															check : function(c,
																	d) {
																b.mediator
																		.handleAlertOtherPageCheck(d)
															}
														}
													},
													{
														xtype : "textfield",
														allowBlank : false,
														hideLabel : true,
														width : 340,
														disabled : !a.isOtherPage,
														id : b.portletId
																+ "alert_refinement_action_target_page",
														value : a.targetPage,
														listeners : {
															change : function(
																	d, e, c) {
																b.mediator
																		.handleAlertTargetPageChange(
																				e,
																				c)
															}
														}
													} ]
										},
										{
											xtype : "label",
											disabled : true,
											forId : b.portletId
													+ "alert_refinement_action_target_page",
											cls : "eid-alert-target-page-instruction-text",
											html : b.utils
													.localize("summarizationbar.edit.panel.define-alert-tab.action-panel.refinement.target-page.instruction-text")
										} ]
							} ]
				})
	},
	getSelectDataPanel : function() {
		var c = this;
		var b = c.getCurrentConfig();
		var a = {
			xtype : "panel",
			cls : "eid-select-data-panel",
			autoWidth : true,
			autoHeight : true,
			border : false,
			items : [
					c
							.getCommonHeaderPanel(
									"summarizationbar.edit.view-select-panel.description",
									"step_select_data"),
					c
							.getDataSelectionPanel(b.dataSourceId,
									b.semanticKey, b.id) ],
			loadDataToUI : function() {
			}
		};
		return a
	},
	getDataSelectionPanel : function(d, e, c) {
		var f = this;
		var a = f.getCurrentConfig();
		var b = new Ext.ux.endeca.DataSelectionPanel(
				{
					id : f.portletId + "data_selection_panel",
					resources : f.utils.resources,
					dataMgr : f.dataMgr,
					resourceUrl : f.resourceUrls.getViewDataUrl,
					loadMask : f.portletId + "top_tab_panel",
					dataSourceData : f.dataSourceData,
					dataSource : d,
					viewKey : e,
					autoHeight : true,
					autoWidth : true,
					showErrorMsg : function(g) {
						f.utils.displayErrorMessage(g)
					},
					listeners : {
						beforechangeview : function(g, i, j, h) {
							f.mediator.lastSemanticKey = h;
							f.mediator.lastDataSourceId = j;
							return f.mediator
									.checkSummarizationhasConfiguratedAttribute()
						},
						afterchangeview : function(g, h) {
							a.dataSourceId = g;
							a.semanticKey = h
						},
						warningcancel : function(h, i, g) {
							if (b.combo) {
								b.combo.setValue(b.gridPanel.dataSourceId)
							}
						},
						warningclear : function(h, i, g) {
							f.mediator.clearSummarizationConfig(i, g);
							f.mediator.reLoadDefineAlertTab(f
									.getAlertDimensionsPanel(), f
									.getAlertCFPanel(), f
									.getAlertDisplayOptionsPanel());
							f.mediator.lastSemanticKey = g;
							f.mediator.lastDataSourceId = i
						},
						warningmaintain : function(h, i, g) {
							f.mediator.maintainSummarizationConfig(i, g
									|| f.editModel.defaultCollectionKey);
							f.mediator.reLoadDefineAlertTab(f
									.getAlertDimensionsPanel(), f
									.getAlertCFPanel(), f
									.getAlertDisplayOptionsPanel());
							f.mediator.lastSemanticKey = g
									|| f.editModel.defaultCollectionKey;
							f.mediator.lastDataSourceId = i
						},
						afterrender : function(g) {
							(function() {
								if (!Ext.isEmpty(g.findByType("combo"))) {
									var h = g.findByType("combo")[0];
									h.purgeListeners();
									h
											.on({
												beforeselect : function(m, i,
														j, k, l) {
													g
															.handleComboSelect(
																	m,
																	i ? i
																			.get(g.comboConfig.idProperty)
																			: "",
																	f.editModel.defaultCollectionKey,
																	Ext
																			.isEmpty(f
																					.getCurrentConfig().itemConfig.metricKey))
												}
											})
								}
							}).defer(100)
						}
					}
				});
		return b
	},
	getSelectTypePanel : function() {
		var c = this;
		var b = c.getCurrentConfig();
		var a = {
			xtype : "panel",
			cls : "eid-summarization-type-select-panel",
			border : false,
			items : [
					c
							.getCommonHeaderPanel(
									"summarizationbar.edit.panel.select-type-description",
									"step_select_type"),
					c.getTypeSelectPanel(b.getType()) ],
			loadDataToUI : function() {
				c.mediator.loadDataToSelectTypePanel()
			}
		};
		return a
	},
	getTypeSelectPanel : function(a) {
		var e = this;
		var c = this
				.createTypePanel({
					type : e.constants.SUMMARIZATION_TYPE_METRIC_VALUE,
					id : "type_metric_value",
					css : "eid-metric-value",
					text : e.utils
							.localize("summarizationbar.edit.panel.metric-value"),
					introduction : e.utils
							.localize("summarizationbar.edit.panel.metric-value-description"),
					isSelected : a == e.constants.SUMMARIZATION_TYPE_METRIC_VALUE
				});
		var d = this
				.createTypePanel({
					type : e.constants.SUMMARIZATION_TYPE_DIMENSION_VALUE,
					id : "type_dimension_value",
					css : "eid-dimension-value",
					text : e.utils
							.localize("summarizationbar.edit.panel.dimension-value"),
					introduction : e.utils
							.localize("summarizationbar.edit.panel.dimension-value-description"),
					isSelected : a == e.constants.SUMMARIZATION_TYPE_DIMENSION_VALUE
				});
		var b = this
				.createTypePanel({
					type : e.constants.SUMMARIZATION_TYPE_ALERT_VALUE,
					id : "type_alert_value",
					css : "eid-alert-value",
					text : e.utils
							.localize("summarizationbar.edit.panel.alert-value"),
					introduction : e.utils
							.localize("summarizationbar.edit.panel.alert-value-description"),
					isSelected : a == e.constants.SUMMARIZATION_TYPE_ALERT_VALUE
				});
		return {
			xtype : "panel",
			cls : "eid-summarization-types-panel",
			border : false,
			items : [ c, d, b ]
		}
	},
	createTypePanel : function(b) {
		var c = this;
		var a = new Ext.Panel(
				{
					id : c.portletId + b.id,
					cls : b.css
							+ (b.isSelected ? " eid-type-image-container-selected"
									: ""),
					border : false,
					items : [ {
						xtype : "box",
						data : b,
						tpl : '<div class="eid-type-image-container"><span class="eid-type-symbol"></span><span class="eid-type-text">{text}</span></div><div class="eid-type-introduction-text">{introduction}</div>',
						listeners : {
							afterrender : function(d) {
								c.mediator.bindTypeSelectPanelEvents(a, d, b)
							}
						}
					} ]
				});
		return a
	},
	getSubTypeSelectPanel : function(a) {
		var e = this;
		var f = this
				.createSubTypePanel({
					id : "single_metric",
					css : "eid-single-metric",
					text : e.utils
							.localize("summarizationbar.edit.panel.single-metric"),
					introduction : e.utils
							.localize("summarizationbar.edit.panel.single-metric-description"),
					isSelected : a == e.constants.SUMMARIZATION_SUBTYPE_SINGLE_METRIC,
					value : e.constants.SUMMARIZATION_SUBTYPE_SINGLE_METRIC
				});
		var d = this
				.createSubTypePanel({
					id : "metric_ratio",
					css : "eid-metrics-ratio",
					text : e.utils
							.localize("summarizationbar.edit.panel.metrics-ratio"),
					introduction : e.utils
							.localize("summarizationbar.edit.panel.metrics-ratio-description"),
					isSelected : a == e.constants.SUMMARIZATION_SUBTYPE_METRICS_RATIO,
					value : e.constants.SUMMARIZATION_SUBTYPE_METRICS_RATIO
				});
		var c = this
				.createSubTypePanel({
					id : "part2whole",
					css : "eid-part-to-whole-ratio",
					text : e.utils
							.localize("summarizationbar.edit.panel.part-whole-ratio"),
					introduction : e.utils
							.localize("summarizationbar.edit.panel.part-whole-ratio-description"),
					isSelected : a == e.constants.SUMMARIZATION_SUBTYPE_PART_WHOLE_RATIO,
					value : e.constants.SUMMARIZATION_SUBTYPE_PART_WHOLE_RATIO
				});
		var b = {
			xtype : "panel",
			id : e.portletId + "subtype_details_panel",
			cls : "eid-subtype-details-panel",
			border : false
		};
		return {
			xtype : "panel",
			cls : "eid-summarization-subtype-panel",
			border : false,
			items : [ {
				xtype : "panel",
				border : false,
				items : [ f ]
			}, b ]
		}
	},
	createSubTypePanel : function(a) {
		var b = this;
		return {
			xtype : "panel",
			id : b.portletId + a.id,
			cls : "eid-subtype-panel " + a.css,
			border : false,
			items : [
					{
						xtype : "radio",
						id : b.portletId + a.id + "_radio",
						cls : "eid-subtype-radio",
						name : b.portletId + "summarizationSubtype",
						checked : a.isSelected,
						listeners : {
							check : function(c, d) {
								b.mediator.handleSubTypeSelect(d, a.value)
							}
						}
					},
					{
						xtype : "box",
						cls : "eid-subtype-content",
						html : '<div class="eid-subtype-image"></div><div>'
								+ a.text + "</div>"
					} ],
			listeners : {
				afterrender : function(c) {
					b.mediator.bindSubtypePanelEvents(c, a)
				}
			}
		}
	},
	getItemContainerPanel : function() {
		var d = this;
		var a = d.getMetricDescData();
		var b = d.getCurrentConfig().getType();
		var c = new Ext.Panel(
				{
					id : d.portletId + "define_item_panel",
					border : false,
					cls : "eid-define-item",
					items : [
							d
									.getCommonHeaderPanel(
											b == d.constants.SUMMARIZATION_TYPE_METRIC_VALUE ? "summarizationbar.edit.label.metric-description"
													: "summarizationbar.edit.label.dimval-description",
											"step_define_item"),
							d.getItemSelectDimesionPanel(),
							d.getItemSelectMetricPanel(),
							d.getItemDisplayNamePanel(),
							d.getItemDisplayOptionsPanel(),
							d.getItemActionsPanel() ],
					loadDataToUI : function() {
						d.mediator.itemUpdateLayout(a, d)
					}
				});
		return c
	},
	rerenderActionPanel : function() {
		var a = this;
		Ext.getCmp(a.portletId + "define_item_panel").remove(
				Ext.getCmp(a.portletId + "action_parent_panel"));
		Ext.getCmp(a.portletId + "define_item_panel").add(
				a.getItemActionsPanel());
		Ext.getCmp(a.portletId + "define_item_panel").doLayout();
		if (a.getCurrentConfig().getType() == a.constants.SUMMARIZATION_TYPE_METRIC_VALUE) {
			Ext.getCmp(a.portletId + "action_panel").parameters = a.mediator
					.getParameters()
		}
	},
	getItemActionsPanel : function() {
		var a = this;
		return new Ext.Panel(
				{
					border : false,
					cls : "eid-define-item-actions-panel eid-collapsible-panel",
					title : a.utils
							.localize("summarizationbar.edit.label.actions")
							+ '<span class="eid-showhide-text eid-showhide-text-opened">'
							+ a.utils
									.localize("summarizationbar.iconic.showhide-opened")
							+ '</span><span class="eid-showhide-text eid-showhide-text-closed">'
							+ a.utils
									.localize("summarizationbar.iconic.showhide-closed")
							+ "</span>",
					collapsible : true,
					collapsed : true,
					titleCollapse : true,
					animCollapse : false,
					headerCssClass : "eid-collapsible-panel-header",
					bodyCssClass : "eid-collapsible-panel-body",
					id : a.portletId + "action_parent_panel",
					items : a.getActionPanel()
				})
	},
	getActionPanel : function() {
		var c = this;
		var a = c.mediator.getCurrentConfig();
		if (a.getType() == c.constants.SUMMARIZATION_TYPE_METRIC_VALUE) {
			return [
					{
						xtype : "container",
						layout : "column",
						items : [
								{
									xtype : "box",
									cls : "eid-form-item-label",
									html : c.utils
											.localize("summarizationbar.edit.label.hyperlink")
								},
								{
									xtype : "box",
									cls : "eid-label-instruction",
									html : c.utils
											.localize("summarizationbar.edit.label.hyperlink-info")
								} ]
					},
					new Ext.ux.endeca.PassParametersActionPanel(
							{
								id : c.portletId + "action_panel",
								portletId : c.portletId,
								resources : c.utils.resources,
								width : 510,
								cls : "endeca-actions-configuration-window eid-form-item-indent",
								actionLevel : Ext.ux.endeca.ActionsConfigurationEnums.actionLevelEnum.RECORD,
								actionConfig : Ext.isEmpty(a.itemConfig.action)
										|| a.itemConfig.action["class"] != Ext.ux.endeca.ActionsConfigurationEnums.actionClassEnum.ACTION_HYPERLINK ? {}
										: a.itemConfig.action.actionConfig,
								listeners : {
									afteredit : function() {
										c.mediator
												.updateSummarizationAction({
													actionConfig : this
															.getConfig(),
													"class" : Ext.ux.endeca.ActionsConfigurationEnums.actionClassEnum.ACTION_HYPERLINK
												})
									},
									afterrender : function(d) {
										Ext
												.getCmp(
														c.portletId
																+ "_pass-param-action-panel")
												.remove(
														c.portletId
																+ "_pass-param-action-name-panel",
														true)
									}
								}
							}) ]
		} else {
			var b = [
					Ext.ux.endeca.ActionsConfigurationEnums.actionTypeEnum.ACTION_NONE,
					Ext.ux.endeca.ActionsConfigurationEnums.actionTypeEnum.ACTION_REFINEMENT,
					Ext.ux.endeca.ActionsConfigurationEnums.actionTypeEnum.ACTION_PASS_PARAMETER ];
			return [ new Ext.ux.endeca.AttributeActionsPanel(
					{
						id : c.portletId + "action_panel",
						portletId : c.portletId,
						resources : c.utils.resources,
						actionLevel : Ext.ux.endeca.ActionsConfigurationEnums.actionLevelEnum.ATTRIBUTE,
						action : Ext.isEmpty(a.itemConfig.action) ? undefined
								: a.itemConfig.action,
						comboboxLabel : c.utils
								.localize("summarizationbar.edit.panel.label.actions"),
						comboboxLabelWidth : 61,
						attributeActionsConfig : {
							actionTypes : b,
							allAttributes : c.mediator.getParameters()
						},
						listeners : {
							afteredit : function() {
								var d = this.getConfig();
								if (!Ext.isEmpty(d)) {
									c.mediator.updateSummarizationAction(d)
								} else {
									c.mediator.updateSummarizationAction()
								}
							},
							afterrender : function() {
								Ext
										.getCmp(
												c.portletId
														+ "_pass-param-show-description-context")
										.hide()
							}
						}
					}) ]
		}
	},
	getItemSelectDimesionPanel : function() {
		var b = this;
		var a = b.getCurrentConfig();
		return {
			layout : "form",
			border : false,
			items : [
					{
						type : "compositefield",
						id : b.portletId
								+ "define_item_select_dimension_button_container",
						fieldLabel : b.utils
								.localize("summarizationbar.edit.label.dimension"),
						labelSeparator : "",
						border : false,
						items : [ new Ext.ux.endeca.AttributeSelectField(
								{
									id : b.portletId
											+ "define_item_dimension_config_field",
									enableSystemMetrics : false,
									attributeType : Ext.ux.endeca.AttributeSelectConstants.ATTRIBUTE_TYPE.ALL_DIMENSION,
									buttonTextType : Ext.ux.endeca.AttributeSelectConstants.BUTTON_TEXT_TYPE.DIMENSION,
									dataMgr : b.dataMgr,
									dataSourceId : a.dataSourceId,
									semanticKey : a.semanticKey,
									attrData : {
										attributeKey : a.itemConfig.dimensionKey,
										datePartCombo : a.itemConfig.datePartCombo
									},
									configWinConfig : {
										hasFormatter : true,
										hasAction : false,
										hasDatePart : true
									},
									resources : b.utils.resources,
									formatterPreviewUrl : b.resourceUrls.formatterPreviewUrl,
									formatterGetCurrencyListUrl : b.resourceUrls.formatterGetCurrencyListUrl,
									formatterPanelResource : b.formatterPanelResource,
									listeners : {
										"delete" : function() {
											b.mediator
													.itemDeleteDimensionEditField();
											b.mediator.updateActionPanel()
										},
										attrchange : function(c) {
											b.mediator.itemSelectDimension(c);
											b.mediator.updateActionPanel()
										}
									}
								}) ]
					},
					{
						xtype : "compositefield",
						id : b.portletId + "define_item_top_buttom_radio",
						ctCls : "eid-define-item-top-buttom-compositefield",
						hidden : a.getType() == b.constants.SUMMARIZATION_TYPE_METRIC_VALUE,
						items : [
								{
									xtype : "radio",
									boxLabel : b.utils
											.localize("summarizationbar.edit.radio.label.top-value"),
									id : b.portletId + "top_value",
									name : b.portletId + "top_value",
									inputValue : false,
									checked : a.itemConfig.isTopValue,
									listeners : {
										check : function(c, d) {
											b.mediator.itemUpdateTopValue(d)
										}
									}
								},
								{
									xtype : "radio",
									boxLabel : b.utils
											.localize("summarizationbar.edit.radio.label.bottom-value"),
									id : b.portletId + "bottom_value",
									name : b.portletId + "top_value",
									inputValue : false,
									checked : !a.itemConfig.isTopValue
								} ]
					} ]
		}
	},
	getItemSelectMetricPanel : function() {
		var b = this;
		var a = b.getCurrentConfig();
		return {
			layout : "form",
			border : false,
			id : b.portletId + "define_item_select_metric_panel",
			items : [
					{
						xtype : "displayfield",
						id : b.portletId
								+ "define_item_metric_ratio_description_field",
						fieldLabel : b.utils
								.localize("summarizationbar.edit.label.ratio"),
						cls : "eid-define-item-select-metric-button-description",
						value : b.utils
								.localize("summarizationbar.edit.label.metrics-ratio-description")
					},
					{
						xtype : "displayfield",
						id : b.portletId
								+ "define_item_part_to_whole_description_field",
						fieldLabel : b.utils
								.localize("summarizationbar.edit.label.ratio"),
						cls : "eid-define-item-select-metric-button-description",
						value : b.utils
								.localize("summarizationbar.edit.label.part-to-whole-description")
					},
					{
						type : "compositefield",
						id : b.portletId
								+ "define_item_selected_metric_container",
						fieldLabel : b.utils
								.localize("summarizationbar.edit.label.metric"),
						labelSeparator : "",
						cls : "eid-define-item-select-metric-container",
						border : false,
						items : [
								new Ext.ux.endeca.AttributeSelectField(
										{
											id : b.portletId
													+ "define_item_metric_attr_config_field",
											attributeType : Ext.ux.endeca.AttributeSelectConstants.ATTRIBUTE_TYPE.ALL_METRIC,
											dataMgr : b.dataMgr,
											dataSourceId : a.dataSourceId,
											semanticKey : a.semanticKey,
											attrData : {
												attributeKey : a.itemConfig.metricKey,
												aggregation : a.itemConfig.metricAggre,
												formatter : a.itemConfig.metricFormatter
											},
											configWinConfig : {
												hasFormatter : true,
												hasAction : false
											},
											resources : b.utils.resources,
											formatterPreviewUrl : b.resourceUrls.formatterPreviewUrl,
											formatterGetCurrencyListUrl : b.resourceUrls.formatterGetCurrencyListUrl,
											formatterPanelResource : b.formatterPanelResource,
											listeners : {
												"delete" : function(d, c) {
													b.mediator
															.itemDeleteMetricField();
													b.mediator
															.itemClearActionPanel();
													b.mediator
															.itemClearDescription(
																	d, c);
													b.mediator
															.updateActionPanel()
												},
												attrchange : function(c) {
													b.mediator
															.itemSelectMetric(c);
													b.mediator
															.updateActionPanel()
												}
											}
										}),
								{
									xtype : "label",
									id : b.portletId
											+ "define_item_selected_slash_displayfield",
									html : '<span class="eid-define-item-icon-slash"></span>'
								},
								new Ext.ux.endeca.AttributeSelectField(
										{
											id : b.portletId
													+ "define_item_target_metric_attr_config_field",
											attributeType : Ext.ux.endeca.AttributeSelectConstants.ATTRIBUTE_TYPE.ALL_METRIC,
											dataSourceId : a.dataSourceId,
											semanticKey : a.semanticKey,
											dataMgr : b.dataMgr,
											attrData : {
												attributeKey : a.itemConfig.targetMetricKey,
												aggregation : a.itemConfig.targetMetricAggre,
												formatter : a.itemConfig.targetMetricFormatter
											},
											configWinConfig : {
												hasFormatter : true,
												hasAction : false
											},
											resources : b.utils.resources,
											formatterPreviewUrl : b.resourceUrls.formatterPreviewUrl,
											formatterGetCurrencyListUrl : b.resourceUrls.formatterGetCurrencyListUrl,
											formatterPanelResource : b.formatterPanelResource,
											listeners : {
												"delete" : function() {
													b.mediator
															.itemDeteleTargetMetricField()
												},
												attrchange : function(c) {
													b.mediator
															.itemSelectTargetMetric(c)
												}
											}
										}),
								{
									xtype : "textfield",
									id : b.portletId
											+ "define_item_selected_target_aggre_textfield",
									cls : "eid-define-item-aggre-textfield",
									disabled : true,
									value : a.itemConfig.metricKey
											+ b.utils
													.localize("summarizationbar.edit.label.sum")
								} ]
					} ]
		}
	},
	getItemDisplayNamePanel : function() {
		var c = this;
		var a = c.getCurrentConfig();
		var b = a.getType();
		return {
			border : false,
			layout : "form",
			items : [
					{
						fieldLabel : c.utils
								.localize("summarizationbar.edit.label.display-name"),
						xtype : "radio",
						boxLabel : b == c.constants.SUMMARIZATION_TYPE_METRIC_VALUE ? c.utils
								.localize("summarizationbar.edit.label.use-metric-name")
								: c.utils
										.localize("summarizationbar.edit.label.use-dimension-metric-name"),
						id : c.portletId + "default_display_name",
						name : c.portletId + "display_name",
						inputValue : true,
						checked : !a.itemConfig.isCustomDisplayName,
						listeners : {
							check : function(d, e) {
								c.mediator.itemSelectDisplayName(e)
							}
						}
					},
					{
						xtype : "displayfield",
						id : c.portletId + "default_display_name_field",
						cls : "eid-define-item-default-dispplay-name-context"
					},
					{
						xtype : "radio",
						boxLabel : c.utils
								.localize("summarizationbar.edit.label.custom-name"),
						id : c.portletId + "custom_display_name",
						name : c.portletId + "display_name",
						inputValue : false,
						checked : a.itemConfig.isCustomDisplayName
					},
					{
						xtype : "textfield",
						id : c.portletId + "custom_display_name_field",
						width : 200,
						allowBlank : false,
						ctCls : "eid-define-item-custom-dispplay-name-context",
						value : a.itemConfig.customDisplayName,
						fieldLabel : c.utils
								.localize("summarizationbar.edit.label.custom-name"),
						labelStyle : "visibility:hidden",
						listeners : {
							change : function(e, f, d) {
								if (Ext.isEmpty(f)) {
									e.setValue(d);
									return
								}
								c.mediator.itemHandleCustomNameChange(f, d)
							}
						}
					} ]
		}
	},
	getItemDisplayOptionsPanel : function() {
		var b = this;
		var a = this.getCurrentConfig();
		return new Ext.Panel(
				{
					border : false,
					cls : "eid-define-item-display-option-panel eid-collapsible-panel",
					title : b.utils
							.localize("summarizationbar.edit.label.display-option")
							+ '<span class="eid-showhide-text eid-showhide-text-opened">'
							+ b.utils
									.localize("summarizationbar.iconic.showhide-opened")
							+ '</span><span class="eid-showhide-text eid-showhide-text-closed">'
							+ b.utils
									.localize("summarizationbar.iconic.showhide-closed")
							+ "</span>",
					collapsible : true,
					collapsed : true,
					titleCollapse : true,
					animCollapse : false,
					headerCssClass : "eid-collapsible-panel-header",
					bodyCssClass : "eid-collapsible-panel-body",
					items : [ {
						xtype : "container",
						border : false,
						id : b.portletId + "display_option_panel",
						cls : "eid-define-item-display-option-panel-context",
						items : [
								b.getItemTooltipPanel(),
								{
									border : false,
									items : [
											{
												xtype : "container",
												cls : "eid-field-block-to-top",
												layout : "column",
												items : [
														{
															xtype : "box",
															cls : "eid-form-item-label",
															html : b.utils
																	.localize("summarizationbar.edit.radio.label.width")
														},
														{
															xtype : "box",
															cls : "eid-label-instruction",
															html : b.utils
																	.localize("summarizationbar.edit.label.display-description")
														} ]
											},
											{
												xtype : "container",
												cls : "eid-define-item-max-width-container eid-form-item-indent",
												layout : "column",
												items : [
														{
															xtype : "radio",
															boxLabel : b.utils
																	.localize("summarizationbar.edit.radio.label.default-max-width"),
															id : b.portletId
																	+ "default_max_width",
															name : b.portletId
																	+ "max_width",
															fieldClass : "eid-field-group-to-right",
															inputValue : false,
															checked : !a.itemConfig.isCustomWidth,
															listeners : {
																check : function(
																		c, d) {
																	b.mediator
																			.itemUpdateMaxWidth(d)
																}
															}
														},
														{
															xtype : "radio",
															boxLabel : b.utils
																	.localize("summarizationbar.edit.radio.label.custom-max-width"),
															id : b.portletId
																	+ "max_width",
															name : b.portletId
																	+ "max_width",
															inputValue : a.itemConfig.isCustomWidth,
															checked : false
														},
														{
															xtype : "numberfield",
															id : b.portletId
																	+ "custom_max_width",
															width : 50,
															value : a.itemConfig.customWidth,
															listeners : {
																blur : function() {
																	b.mediator
																			.itemUpdateMaxWidthValue(this)
																},
																afterrender : function(
																		c) {
																	c.el
																			.set({
																				title : b.utils
																						.localize("summarizationbar.edit.radio.label.custom-max-width")
																			})
																}
															}
														},
														{
															xtype : "label",
															forId : b.portletId
																	+ "custom_max_width",
															html : b.utils
																	.localize("summarizationbar.edit.label.pixel")
														} ]
											} ]
								} ]
					} ]
				})
	},
	getItemTooltipPanel : function() {
		var f = this;
		var b = f.getCurrentConfig();
		var e = b.getType();
		var a = b.getSubtype();
		var d = f.getMetricDescData();
		var c = d[b.type];
		return {
			xtype : "container",
			items : [
					{
						xtype : "container",
						layout : "column",
						items : [
								{
									xtype : "box",
									cls : "eid-form-item-label",
									html : f.utils
											.localize("summarizationbar.edit.label.metric-desc")
								},
								{
									xtype : "box",
									id : f.portletId
											+ "metric_description_introduction",
									cls : "eid-label-instruction",
									data : {
										qtipText : c.qtipText,
										type : e
									},
									tpl : new Ext.XTemplate(
											"{[this.getIntroductionText(values)]}",
											'<tpl if="this.showTooltip(values)">',
											'<span class="eid-action-icon eid-icon-help" ext:qprofile="info" ext:qtip="{[this.getQtipText(values)]}">'
													+ f.utils
															.localize("summarizationbar.iconic.info-icon-text")
													+ "</span>",
											"</tpl>",
											{
												getIntroductionText : function(
														g) {
													if (g.type == f.constants.SUMMARIZATION_TYPE_METRIC_VALUE) {
														return f.utils
																.localize("summarizationbar.edit.panel.metric-desc-introduction")
													} else {
														return f.utils
																.localize("summarizationbar.edit.panel.dimension-desc-introduction")
													}
												},
												showTooltip : function(g) {
													return !Ext
															.isEmpty(g.qtipText)
												},
												getQtipText : function(g) {
													return f.utils
															.localize("summarizationbar.edit.panel.metric-desc-tooltip-shared")
															+ "<br/><br/>"
															+ g.qtipText
												}
											})
								} ]
					},
					{
						xtype : "container",
						cls : "eid-metric-description-textarea-container eid-form-item-indent",
						items : [
								{
									xtype : "textarea",
									cls : "eid-metric-description-textarea",
									id : f.portletId + "metric_description",
									value : Ext
											.isDefined(b.itemConfig.description) ? b.itemConfig.description
											: c.defaultDesc,
									width : 500,
									listeners : {
										change : function(g, i, h) {
											f.mediator.itemChangeMetricDesc(i)
										}
									}
								},
								{
									xtype : "label",
									forId : f.portletId + "metric_description",
									html : f.utils
											.localize("summarizationbar.edit.label.metric-desc"),
									hideMode : "visibility",
									hidden : true
								} ]
					},
					{
						xtype : "box",
						cls : "eid-metric-description-example-field eid-label-instruction eid-form-item-indent",
						id : f.portletId + "metric_descritpion_example",
						tpl : new Ext.XTemplate(
								"{[this.getExampleText(values)]}",
								{
									getExampleText : function(g) {
										return Ext.isEmpty(g.text) ? ""
												: f.utils
														.localize("summarizationbar.edit.label.metric-desc-example")
														+ g.text
									}
								}),
						data : {
							text : c.exampleText
						}
					} ]
		}
	},
	getDefineConditionPanel : function() {
		var b = this;
		var a = b.getCurrentConfig();
		return {
			xtype : "panel",
			border : false,
			cls : "eid-conditional-formatting-container",
			items : [
					b
							.getCommonHeaderPanel(
									a.isCompleted ? "summarizationbar.edit.define-conditions-panel.description"
											: "summarizationbar.edit.define-conditions-panel.incomplete-description",
									"step_define_conditions"),
					{
						xtype : "panel",
						cls : "eid-conditional-formatting",
						id : b.portletId + "coditional_format_panel",
						disabled : !a.isCompleted,
						border : false,
						bodyBorder : false,
						defaults : {
							bodyBorder : false
						},
						items : [
								{
									id : b.portletId
											+ "codition_base_selection_panel",
									cls : "eid-codition_base_selection_panel",
									hidden : a.getSubtype() != b.constants.SUMMARIZATION_SUBTYPE_SINGLE_METRIC,
									items : [
											{
												bodyBorder : false,
												data : {
													text : b.utils
															.localize("summarizationbar.edit.label.condition-base-selection"),
													qtipText : b.utils
															.localize("summarizationbar.edit.tooltip.condition-base-selection")
												},
												tpl : new Ext.XTemplate(
														"{[this.getLabelText(values)]}",
														{
															getLabelText : function(
																	c) {
																return c.text
																		+ '<span class="eid-action-icon eid-icon-help eid-action-icon-eol" ext:qprofile="info" ext:qtip="'
																		+ c.qtipText
																		+ '">'
																		+ b.utils
																				.localize("summarizationbar.iconic.info-icon-text")
																		+ "</span>"
															}
														})
											},
											{
												xtype : "compositefield",
												height : 40,
												items : [
														{
															xtype : "radio",
															name : "metric_type",
															id : b.portletId
																	+ "condition_type_self_value",
															cls : "eid-condition-type-radio",
															autoWidth : true,
															boxLabel : b.utils
																	.localize("summarizationbar.edit.label.condition-checkbox-label-metric"),
															value : b.constants.CONDITION_TYPE_SELF_VALUE,
															listeners : {
																check : function(
																		c, d) {
																	if (d) {
																		b.mediator
																				.handlCFBaseSelectionChange(c.value)
																	}
																}
															}
														},
														{
															xtype : "radio",
															name : "metric_type",
															id : b.portletId
																	+ "condition_type_to_another_metric",
															cls : "eid-condition-type-radio",
															autoWidth : true,
															boxLabel : b.utils
																	.localize("summarizationbar.edit.label.condition-checkbox-label-metric-to-metric"),
															value : b.constants.CONDITION_TYPE_TO_ANOTHER_METRIC,
															listeners : {
																check : function(
																		c, d) {
																	if (d) {
																		b.mediator
																				.handlCFBaseSelectionChange(c.value)
																	}
																}
															}
														} ]
											},
											{
												id : b.portletId
														+ "cf_metric_select_panel",
												layout : "table",
												bodyBorder : false,
												height : 60,
												defaults : {
													bodyBorder : false
												},
												layoutConfig : {
													tableAttrs : {
														role : "presentation",
														summary : ""
													},
													columns : 3
												},
												items : [
														{
															xtype : "label",
															html : b.utils
																	.localize("summarizationbar.edit.label.actual"),
															forId : b.portletId
																	+ "cf_metric_displayfield",
															colspan : 2
														},
														{
															xtype : "label",
															forId : b.portletId
																	+ "cf_target_metric_attr_config_field",
															html : b.utils
																	.localize("summarizationbar.edit.label.target")
														},
														{
															xtype : "textfield",
															cls : "eid-cf-metric-field",
															id : b.portletId
																	+ "cf_metric_displayfield",
															readOnly : true,
															listeners : {
																focus : function(
																		c) {
																	c.el.dom
																			.blur()
																}
															}
														},
														{
															xtype : "box",
															cls : "eid-cf-metric-separater",
															html : b.utils
																	.localize("summarizationbar.edit.label.divide")
														},
														{
															xtype : "panel",
															border : false,
															items : [ new Ext.ux.endeca.AttributeSelectField(
																	{
																		id : b.portletId
																				+ "cf_target_metric_attr_config_field",
																		attributeType : Ext.ux.endeca.AttributeSelectConstants.ATTRIBUTE_TYPE.NUMERIC_METRIC,
																		cls : "eid-attribute-select-field eid-cf-target-metric-field",
																		dataSourceId : a.dataSourceId,
																		semanticKey : a.semanticKey,
																		dataMgr : b.dataMgr,
																		attrData : {
																			attributeKey : a.conditionsConfig.targetMetricKey,
																			aggregation : a.conditionsConfig.targetMetricAggre,
																			formatter : a.conditionsConfig.targetMetricFormatter
																		},
																		configWinConfig : {
																			hasFormatter : true,
																			hasAction : false
																		},
																		resources : b.utils.resources,
																		formatterPreviewUrl : b.resourceUrls.formatterPreviewUrl,
																		formatterGetCurrencyListUrl : b.resourceUrls.formatterGetCurrencyListUrl,
																		formatterPanelResource : b.formatterPanelResource,
																		listeners : {
																			"delete" : function() {
																				b.mediator
																						.conditionDeleteTargetMetric()
																			},
																			attrchange : function(
																					c) {
																				b.mediator
																						.conditionSelectTargetMetric(c)
																			}
																		}
																	}) ]
														} ]
											} ]
								},
								{
									id : b.portletId + "cf_panel",
									xtype : "conditionalFormatterPanelV2",
									resources : b.utils.resources,
									conditionData : a.conditionsConfig,
									tokenLink : b.mediator.getCFTokenLink(),
									isShowPercentageSymbol : a.conditionsConfig.type == b.constants.CONDITION_TYPE_TO_ANOTHER_METRIC
											|| a.conditionsConfig.type == b.constants.CONDITION_TYPE_PART_TO_WHOLE
								},
								b.getCFDisplayOptionsPanel(a.conditionsConfig) ]
					} ],
			loadDataToUI : function() {
				b.mediator.handleCFPanelLoadDataToUI();
				b.mediator.handlCFBaseSelectionAfterrender()
			}
		}
	},
	getCFDisplayOptionsPanel : function(b) {
		var a = this;
		return new Ext.Panel(
				{
					border : false,
					cls : "eid-define-item-display-option-panel eid-collapsible-panel",
					title : a.utils
							.localize("summarizationbar.edit.label.display-option")
							+ '<span class="eid-showhide-text eid-showhide-text-opened">'
							+ a.utils
									.localize("summarizationbar.iconic.showhide-opened")
							+ '</span><span class="eid-showhide-text eid-showhide-text-closed">'
							+ a.utils
									.localize("summarizationbar.iconic.showhide-closed")
							+ "</span>",
					collapsible : true,
					collapsed : true,
					titleCollapse : true,
					animCollapse : false,
					headerCssClass : "eid-collapsible-panel-header",
					bodyCssClass : "eid-collapsible-panel-body",
					items : [ {
						id : a.portletId + "_cf_display_option_panel",
						border : false,
						items : [
								{
									xtype : "checkbox",
									boxLabel : a.utils
											.localize("summarizationbar.edit.label.set-metric-text-black"),
									checked : b.isMetricTextBlack,
									listeners : {
										check : function(d, c) {
											b.isMetricTextBlack = c
										}
									}
								},
								{
									layout : "column",
									bodyBorder : false,
									items : [
											{
												xtype : "checkbox",
												boxLabel : a.utils
														.localize("summarizationbar.edit.label.display-meets-only"),
												checked : b.isMeetDispOnly,
												listeners : {
													check : function(d, c) {
														b.isMeetDispOnly = c
													}
												}
											},
											{
												xtype : "box",
												cls : "eid-action-icon eid-icon-help eid-cf-display-option-helper",
												autoEl : {
													"ext:qprofile" : "info",
													"ext:qtip" : a.utils
															.localize("summarizationbar.edit.label.display-meets-only-help")
												},
												html : a.utils
														.localize("summarizationbar.iconic.info-icon-text")
											} ]
								} ]
					} ]
				})
	},
	getDetailedInfoConfigPanel : function() {
		var f = this;
		var b = f.getCurrentConfig();
		var e = b.getType();
		var a = b.getSubtype();
		var d = {};
		d[f.constants.SUMMARIZATION_TYPE_METRIC_SINGLE] = {
			qtipText : f.utils
					.localize(
							"summarizationbar.edit.panel.metric-desc-tooltip-metric-n-single",
							f.constants.METRIC_VALUE_TOKEN_SPACE)
		};
		d[f.constants.SUMMARIZATION_TYPE_METRIC_RATIO] = {
			qtipText : f.utils
					.localize("summarizationbar.edit.panel.metric-desc-tooltip-metric-n-metric-ratio"),
			defaultDesc : f.utils
					.localize("summarizationbar.edit.textarea.default-desc-metric-n-metric-ratio"),
			exampleText : f.utils
					.localize("summarizationbar.edit.label.example-metric-n-metric-ratio")
		};
		d[f.constants.SUMMARIZATION_TYPE_METRIC_PART_TO_WHOLE] = {
			qtipText : f.utils
					.localize("summarizationbar.edit.panel.metric-desc-tooltip-metric-n-part-whole-ratio"),
			defaultDesc : f.utils
					.localize("summarizationbar.edit.textarea.default-desc-metric-n-part-whole-ratio"),
			exampleText : f.utils
					.localize("summarizationbar.edit.label.example-metric-n-part-whole-ratio")
		};
		d[f.constants.SUMMARIZATION_TYPE_DIMENSION_SINGLE] = {
			qtipText : f.utils
					.localize(
							"summarizationbar.edit.panel.metric-desc-tooltip-dimension-n-single",
							f.constants.METRIC_VALUE_TOKEN_SPACE),
			defaultDesc : f.constants.METRIC_VALUE_TOKEN,
			exampleText : f.utils
					.localize("summarizationbar.edit.label.example-dimension-n-single")
		};
		d[f.constants.SUMMARIZATION_TYPE_DIMENSION_RATIO] = {
			qtipText : f.utils
					.localize("summarizationbar.edit.panel.metric-desc-tooltip-dimension-n-metric-ratio"),
			defaultDesc : f.utils
					.localize("summarizationbar.edit.textarea.default-desc-dimension-n-metric-ratio"),
			exampleText : f.utils
					.localize("summarizationbar.edit.label.example-dimension-n-metric-ratio")
		};
		var c = d[b.type];
		return {
			xtype : "panel",
			id : f.portletId + "detailed_info_config_panel",
			border : false,
			cls : "eid-detailed-info-config-panel",
			items : [
					f
							.getCommonHeaderPanel(
									"summarizationbar.edit.panel.detailed-info-config-panel-description",
									"step_detailed_info_panel"),
					{
						xtype : "panel",
						border : false,
						layout : "column",
						items : [
								{
									xtype : "panel",
									border : false,
									columnWidth : 0.8,
									items : [ {
										xtype : "panel",
										layout : "form",
										border : false,
										items : [ {
											xtype : "checkbox",
											id : f.portletId
													+ "display_info_panel",
											fieldLabel : f.utils
													.localize("summarizationbar.edit.label.display-info-panel"),
											checked : b.infoConfig.isDisplay,
											listeners : {
												check : function(g, h) {
													f.mediator
															.changeIsDetailedInfoPanelDisplay(h)
												}
											}
										} ]
									} ]
								},
								{
									xtype : "panel",
									id : f.portletId
											+ "detailed_info_preview_panel_container",
									border : false,
									columnWidth : 0.2,
									items : f.getDetailedInfoPreviewPanel()
								} ]
					}, f.getRelatedDimensionValuesPanel(b, e, a) ],
			loadDataToUI : function() {
				f.mediator.loadDataToDetailedInfoConfigPanel(d)
			}
		}
	},
	getDetailedInfoPreviewPanel : function() {
		var a = this;
		return {
			xtype : "panel",
			cls : "eid-detailed-info-preview-panel",
			border : false,
			items : [
					{
						xtype : "panel",
						border : false,
						items : {
							xtype : "displayfield",
							cls : "eid-detailed-info-preview-title",
							value : a.utils
									.localize("summarizationbar.edit.panel.preview-info-panel-title")
						}
					},
					{
						xtype : "panel",
						id : a.portletId + "detailed_info_preview_panel",
						cls : "eid-detailed-info-preview-content-panel",
						border : false,
						data : {
							description : a.utils
									.localize("summarizationbar.edit.panel.preview-info-panel-default-text")
						},
						tpl : new Ext.XTemplate(
								'<div class="close-button"></div>',
								'<tpl if="this.hasDataInfo(values.dataInfo)">',
								'<div class="upper-info-panel">',
								'<div class="data-info-title">{[this.getTitle(values.dataInfo)]}</div>',
								'<tpl for="values.dataInfo.values">',
								'<div class="data-info-row {[this.getValueZeroClass(values.value)]}">',
								'<span class="data-info-name">{values.name}</span>',
								'<span class="data-info-value">{values.value}</span>',
								"</div>",
								"</tpl>",
								"</div>",
								"</tpl>",
								{
									hasDataInfo : function(b) {
										return !Ext.isEmpty(b)
									},
									getTitle : function(b) {
										if (b.isCustomTitle) {
											return b.title
										} else {
											return a.utils
													.localize("summarizationbar.edit.label.by")
													+ " " + b.title
										}
									},
									getValueZeroClass : function(b) {
										if (b === 0) {
											return "data-info-zero"
										} else {
											return ""
										}
									},
									getDescriptionCss : function(b) {
										if (Ext.isEmpty(b.description)) {
											return ""
										} else {
											if (Ext.isEmpty(b.dataInfo)) {
												return "upper-info-panel"
											} else {
												return "lower-info-panel"
											}
										}
									}
								}),
						listeners : {
							afterrender : function() {
								a.mediator.updateDetailedInfoPreviewPanel()
							}
						}
					} ]
		}
	},
	getRelatedDimensionValuesPanel : function(b, c, a) {
		var d = this;
		return {
			xtype : "panel",
			border : false,
			items : [
					{
						xtype : "panel",
						border : false,
						cls : "eid-tab-title",
						html : d.utils
								.localize("summarizationbar.edit.panel.related-dimension-values-title")
					},
					{
						xtype : "panel",
						border : false,
						items : [
								{
									xtype : "displayfield",
									cls : "eid-related-dimension-values-description",
									data : {
										text : d.utils
												.localize("summarizationbar.edit.panel.related-dimension-values-description"),
										qtipText : d.utils
												.localize("summarizationbar.edit.panel.related-dimension-values-tooltip")
									},
									tpl : '<span>{text}</span><span class="eid-action-icon eid-icon-help eid-action-icon-eol" ext:qprofile="info" ext:qtip="{qtipText}">'
											+ d.utils
													.localize("summarizationbar.iconic.info-icon-text")
											+ "</span>"
								},
								{
									xtype : "compositefield",
									cls : "eid-related-dimension-values-description",
									items : [
											{
												xtype : "displayfield",
												value : d.utils
														.localize("summarizationbar.edit.panel.related-dimension-values-metric-name-info")
											},
											{
												xtype : "displayfield",
												cls : "eid-related-dimension-values-description-metric-name",
												id : d.portletId
														+ "related_dimension_metric_name",
												value : Ext.util.Format
														.htmlEncode(b.name)
											} ]
								}, d.getDimensionConfigPanel(b, c, a) ]
					} ]
		}
	},
	getDimensionConfigPanel : function(b, c, a) {
		var d = this;
		return {
			xtype : "panel",
			layout : "form",
			border : false,
			items : [
					{
						xtype : "button",
						id : d.portletId + "detailed_info_select_dimension_btn",
						text : d.utils
								.localize("summarizationbar.edit.button.related-dimension-values-select-dimension"),
						fieldLabel : d.utils
								.localize("summarizationbar.edit.label.related-dimension-values-dimension"),
						cls : "eid-detailed-info-select-dimension-btn",
						hidden : c != d.constants.SUMMARIZATION_TYPE_METRIC_VALUE
								|| !Ext.isEmpty(b.infoConfig.dimensionKey),
						listeners : {
							click : function() {
								var e = d
										.getDetailedInfoSelectDimensionWindow(d.mediator.saveDetailedInfoSelectedDimension);
								d.mediator.showSelectAttributeWindow(e)
							}
						}
					},
					{
						border : false,
						layout : "form",
						id : d.portletId
								+ "detailed_info_dimension_selected_panel",
						hidden : c != d.constants.SUMMARIZATION_TYPE_METRIC_VALUE
								|| Ext.isEmpty(b.infoConfig.dimensionKey),
						items : [
								new Endeca.Portlets.SummarizationBarPortlet.EditField(
										{
											fieldLabel : d.utils
													.localize("summarizationbar.edit.label.related-dimension-values-dimension"),
											id : d.portletId
													+ "detailed_info_dimension_field",
											value : d.utils
													.getAttributeDisplayName(
															b.dataSourceId,
															b.semanticKey,
															b.infoConfig.dimensionKey),
											editable : false,
											listeners : {
												"delete" : function() {
													d.mediator
															.deleteDetailedInfoDimension()
												},
												edit : function() {
													d
															.getConfigureDimensionWindow()
															.show()
												}
											}
										}),
								{
									xtype : "compositefield",
									items : [
											{
												xtype : "radio",
												boxLabel : d.utils
														.localize("summarizationbar.edit.radio.top-values"),
												name : d.portletId
														+ "use-top-value",
												checked : b.infoConfig.isTopValue,
												listeners : {
													check : function(e, f) {
														d.mediator
																.changeIsTopValue(f)
													}
												}
											},
											{
												xtype : "radio",
												boxLabel : d.utils
														.localize("summarizationbar.edit.radio.bottom-values"),
												name : d.portletId
														+ "use-top-value",
												checked : !b.infoConfig.isTopValue
											} ]
								} ]
					},
					{
						border : false,
						hidden : c != d.constants.SUMMARIZATION_TYPE_DIMENSION_VALUE,
						id : d.portletId + "show_more_values",
						items : {
							xtype : "compositefield",
							items : [
									{
										xtype : "checkbox",
										id : d.portletId
												+ "show_more_value_checkbox",
										cls : "eid-show-more-value-field",
										checked : b.infoConfig.isShowMore,
										boxLabel : d.utils
												.localize("summarizationbar.edit.checkbox.show-more-values"),
										listeners : {
											check : function(f, e) {
												d.mediator.changeIsShowMore(e)
											}
										}
									},
									{
										xtype : "displayfield",
										id : d.portletId
												+ "show_more_value_dimension_name",
										cls : "eid-show-more-value-dimension-name",
										value : d.utils
												.getAttributeDisplayName(
														b.dataSourceId,
														b.semanticKey,
														b.infoConfig.dimensionKey)
									} ]
						}
					},
					{
						border : false,
						layout : "form",
						items : [
								{
									xtype : "compositefield",
									fieldLabel : d.utils
											.localize("summarizationbar.edit.label.title"),
									items : [
											{
												xtype : "radio",
												id : d.portletId
														+ "use_dimension_name_radio",
												boxLabel : d.utils
														.localize("summarizationbar.edit.radio.use-dimension-name"),
												name : "use-custom-name",
												checked : !b.infoConfig.isCustomTitle,
												listeners : {
													check : function(e, f) {
														d.mediator
																.changeIsCustomTitle(!f)
													}
												}
											},
											{
												xtype : "displayfield",
												id : d.portletId
														+ "dimension_name_field",
												cls : "dimension_name_display_field",
												tpl : new Ext.XTemplate(
														"{[this.getContent(values.value)]}",
														{
															getContent : function(
																	e) {
																if (Ext
																		.isEmpty(e)) {
																	return "<span></span>"
																} else {
																	return "<span>"
																			+ d.utils
																					.localize("summarizationbar.edit.label.by")
																			+ e
																			+ "</span>"
																}
															}
														}),
												data : {
													value : d.utils
															.getAttributeDisplayName(
																	b.dataSourceId,
																	b.semanticKey,
																	b.infoConfig.dimensionKey)
												}
											} ]
								},
								{
									xtype : "compositefield",
									items : [
											{
												xtype : "radio",
												boxLabel : d.utils
														.localize("summarizationbar.edit.label.custom-name"),
												name : "use-custom-name",
												checked : b.infoConfig.isCustomTitle
											},
											{
												xtype : "textfield",
												id : d.portletId
														+ "detailed_info_title",
												value : b.infoConfig.customTitle,
												disabled : !b.infoConfig.isCustomTitle,
												allowBlank : false,
												listeners : {
													change : function(g, f, e) {
														d.mediator
																.changeCustomTitle(
																		g, f)
													}
												}
											} ]
								} ]
					},
					{
						xtype : "numberfield",
						id : d.portletId + "number_of_values",
						fieldLabel : d.utils
								.localize("summarizationbar.edit.textfield.number-of-values"),
						value : b.infoConfig.maxNumber,
						listeners : {
							change : function(g, f, e) {
								d.mediator.changeNumberOfValues(f)
							}
						}
					} ]
		}
	},
	getDetailedInfoSelectDimensionWindow : function(b) {
		var a = this;
		return a.getSelectAttributeWindow(b, true)
	},
	getCurrentConfig : function() {
		return this.mediator.getCurrentConfig()
	},
	getCommonHeaderPanel : function(b, d) {
		var c = this;
		var a = c.getCurrentConfig();
		return {
			xtype : "container",
			cls : "eid-right-panel-header",
			border : false,
			items : [
					{
						xtype : "container",
						id : c.portletId + d + "_header",
						border : false,
						data : {
							summarizationName : a.type == c.constants.SUMMARIZATION_TYPE_ALERT ? a.alertConfig.displayName
									: a.name
						},
						autoEl : "h3",
						tpl : new Ext.XTemplate(
								"{[ Ext.util.Format.htmlEncode(values.summarizationName)]}",
								{
									compiled : true
								})
					}, {
						xtype : "container",
						border : false,
						id : c.portletId + d + "_description",
						cls : "eid-instruction-text",
						autoEl : "div",
						html : c.utils.localize(b)
					} ]
		}
	},
	getMetricDescData : function() {
		var b = this;
		var a = {};
		a[b.constants.SUMMARIZATION_TYPE_METRIC_SINGLE] = {
			qtipText : undefined
		};
		a[b.constants.SUMMARIZATION_TYPE_METRIC_RATIO] = {
			qtipText : b.utils
					.localize("summarizationbar.edit.panel.metric-desc-tooltip-metric-n-metric-ratio"),
			defaultDesc : b.utils
					.localize("summarizationbar.edit.textarea.default-desc-metric-n-metric-ratio"),
			exampleText : b.utils
					.localize("summarizationbar.edit.label.example-metric-n-metric-ratio")
		};
		a[b.constants.SUMMARIZATION_TYPE_METRIC_PART_TO_WHOLE] = {
			qtipText : b.utils
					.localize("summarizationbar.edit.panel.metric-desc-tooltip-metric-n-part-whole-ratio"),
			defaultDesc : b.utils
					.localize("summarizationbar.edit.textarea.default-desc-metric-n-part-whole-ratio"),
			exampleText : b.utils
					.localize("summarizationbar.edit.label.example-metric-n-part-whole-ratio")
		};
		a[b.constants.SUMMARIZATION_TYPE_DIMENSION_SINGLE] = {
			qtipText : b.utils
					.localize(
							"summarizationbar.edit.panel.metric-desc-tooltip-dimension-n-single",
							b.constants.METRIC_VALUE_TOKEN_SPACE),
			defaultDesc : b.constants.METRIC_VALUE_TOKEN,
			exampleText : b.utils
					.localize("summarizationbar.edit.label.example-dimension-n-single")
		};
		a[b.constants.SUMMARIZATION_TYPE_DIMENSION_RATIO] = {
			qtipText : b.utils
					.localize("summarizationbar.edit.panel.metric-desc-tooltip-dimension-n-metric-ratio"),
			defaultDesc : b.utils
					.localize("summarizationbar.edit.textarea.default-desc-dimension-n-metric-ratio"),
			exampleText : b.utils
					.localize("summarizationbar.edit.label.example-dimension-n-metric-ratio")
		};
		a[b.constants.SUMMARIZATION_TYPE_ALERT] = {
			qtipText : undefined
		};
		return a
	}
};