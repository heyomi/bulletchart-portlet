Ext.ns("Endeca.Portlets.SummarizationBarPortlet");
Endeca.Portlets.SummarizationBarPortlet.Mediator = function(a, c, b) {
	this.portletId = a;
	this.constants = new Endeca.Portlets.SummarizationBarPortlet.Constants();
	this.utils = new Endeca.Portlets.SummarizationBarPortlet.Utils(a, b);
	this.resourceUrls = c;
	return this
};
Endeca.Portlets.SummarizationBarPortlet.Mediator.prototype = {
	editModel : undefined,
	portletId : undefined,
	constants : undefined,
	summarizationIndex : -1,
	dataMgr : undefined,
	resourceUrls : undefined,
	initEditModel : undefined,
	lastSemanticKey : undefined,
	lastDataSourceId : undefined,
	injectDataToMediator : function(a, c) {
		var b = this;
		b.utils.dataMgr = a;
		b.dataMgr = a;
		if (!Ext.isEmpty(c)) {
			b.editModel = c;
			b.initEditModel = Ext.decode(Ext.encode(c))
		}
	},
	getAlertGridColumnModel : function(e) {
		var d = this;
		var a = Ext.StoreMgr.get(d.portletId + "metrics_preview_store");
		var c = a.getAt(a.findExact("id", e.toString())).data;
		var b = [];
		Ext
				.each(
						c.alertDimensionColumns,
						function(f) {
							var g = Ext.util.Format.htmlEncode(f.name);
							b
									.push({
										header : '<span ext:qtip="'
												+ Ext.util.Format.htmlEncode(g)
												+ '">' + g + "</span>",
										dataIndex : f.id,
										width : 200,
										renderer : function(m, l, j) {
											var h = "";
											for (var k = 0; k < m.attributeKeys.length; k++) {
												h = h + m.formattedValues[k]
														+ "<br>"
											}
											h = h.substring(0, h.length - 4);
											return '<span class="eid-refinement-link" ext:qtip="'
													+ d.utils
															.localize(
																	"summarizationbar.view.tooltip.do-refinement-by",
																	h)
													+ '">'
													+ h + "</span>"
										}
									})
						});
		Ext.each(c.alertConditionColumns, function(f) {
			var g = Ext.util.Format.htmlEncode(f.name);
			b.push({
				width : 120,
				header : '<span ext:qtip="' + Ext.util.Format.htmlEncode(g)
						+ '">' + g + "</span>",
				dataIndex : f.id,
				renderer : function(j, i, h) {
					return '<span ext:qtip="' + j + '">' + j + "</span>"
				}
			})
		});
		return new Ext.grid.ColumnModel(b)
	},
	getAlertGridStore : function(d) {
		var a = this;
		var h = Ext.StoreMgr.get(a.portletId + "metrics_preview_store");
		var f = h.getAt(h.findExact("id", d.toString())).data;
		var b = [];
		Ext.each(f.alertDimensionColumns, function(i) {
			b.push(i.id)
		});
		Ext.each(f.alertConditionColumns, function(i) {
			b.push(i.id)
		});
		var k = [];
		for (var e = 0; e < f.alertConditionValues.length; e++) {
			var g = {};
			g[b[0]] = f.alertDimensionValues[e];
			for (var c = 0; c < f.alertConditionValues[e].formattedValues.length; c++) {
				g[b[c + 1]] = f.alertConditionValues[e].formattedValues[c]
			}
			k.push(g)
		}
		return new Ext.data.JsonStore({
			autoLoad : true,
			autoDestroy : true,
			fields : b,
			data : k
		})
	},
	switchRightPanel : function(d, h) {
		var b = this;
		var f = Ext.getCmp(b.portletId + "right_panel");
		var a = Ext.getCmp(b.portletId + "summarization_grid");
		var e = Ext.getCmp(b.portletId + "top_tab_panel");
		if (!Ext.isEmpty(e)) {
			f.remove(e, true)
		}
		if (d == 0) {
			a.getSelectionModel().clearSelections();
			b.summarizationIndex = -1;
			var i = Ext.query(".summarization-name");
			var j = Ext.query(".summarization-view-name");
			var g = Ext.StoreMgr.get(b.portletId + "summarization_store");
			var c = g.getRange();
			Ext.each(c, function(l, m) {
				var k = l.get("name");
				if (k != Ext.util.Format.htmlDecode(i[m].innerHTML)) {
					Ext.fly(i[m]).update(Ext.util.Format.htmlEncode(k))
				}
			})
		} else {
			if (d == 1) {
				a.getSelectionModel().clearSelections();
				b.summarizationIndex = -1
			} else {
				a.getSelectionModel().selectRow(b.summarizationIndex);
				f.add(h)
			}
		}
		f.getLayout().setActiveItem(d);
		if (h) {
			h.items.get(0).show()
		}
	},
	checkSummarizationhasConfiguratedAttribute : function() {
		var b = this;
		var a = b.getCurrentConfig();
		return !Ext.isEmpty(a.itemConfig.metricKey)
				|| !Ext.isEmpty(a.itemConfig.targetMetricKey)
				|| !Ext.isEmpty(a.itemConfig.dimensionKey)
				|| !a.alertConfig.dimensionsList.length == 0
				|| !a.alertConfig.conditonsList.length == 0
	},
	clearSummarizationConfig : function(e, b) {
		var d = this;
		var c = d.editModel.getDefaultSummarizationConfig();
		var a = d.editModel.configs[d.summarizationIndex];
		c.id = a.id;
		c.isCompleted = false;
		c.dataSourceId = e;
		c.semanticKey = b;
		c.name = a.name;
		c.itemConfig.customDisplayName = a.itemConfig.customDisplayName;
		c.alertConfig.displayName = a.alertConfig.displayName;
		d.editModel.configs[d.summarizationIndex] = c;
		Ext.StoreMgr.get(this.portletId + "summarization_store").loadData(
				d.editModel);
		return true
	},
	maintainSummarizationConfig : function(d, a) {
		var c = this;
		var b = c.getCurrentConfig();
		b.dataSourceId = d;
		b.semanticKey = a;
		c.maintainMetricConfigurated(b);
		c.maintainAlertsConfigurated(b);
		Ext.applyIf(b, c.editModel.getDefaultSummarizationConfig());
		c.editModel.configs[c.summarizationIndex] = b;
		Ext.StoreMgr.get(this.portletId + "summarization_store").loadData(
				c.editModel);
		return true
	},
	maintainMetricConfigurated : function(c) {
		var a = this;
		var i = c.itemConfig.metricKey;
		if (!Ext.isEmpty(i) && i != a.constants.COUNT_ONE_KEY) {
			var d = a.utils.getMetricByKey(c.dataSourceId, c.semanticKey, i);
			if (Ext.isEmpty(d)) {
				c.isCompleted = false;
				c.itemConfig.metricKey = undefined;
				c.itemConfig.metricAggre = undefined;
				c.itemConfig.metricFormatter = undefined
			}
		}
		var b = c.itemConfig.targetMetricKey;
		if (!Ext.isEmpty(b) && b != a.constants.COUNT_ONE_KEY) {
			var d = a.utils.getMetricByKey(c.dataSourceId, c.semanticKey, b);
			if (Ext.isEmpty(d)) {
				c.itemConfig.targetMetricKey = undefined;
				c.itemConfig.targetMetricAggre = undefined;
				c.itemConfig.targetMetricFormatter = undefined
			}
		}
		var h = c.conditionsConfig.targetMetricKey;
		if (!Ext.isEmpty(h) && h != a.constants.COUNT_ONE_KEY) {
			var d = a.utils.getMetricByKey(c.dataSourceId, c.semanticKey, h);
			if (Ext.isEmpty(d)) {
				c.conditionsConfig.targetMetricKey = undefined;
				c.conditionsConfig.targetMetricAggre = undefined;
				c.conditionsConfig.targetMetricFormatter = undefined
			}
		}
		var j = c.itemConfig.dimensionKey;
		if (!Ext.isEmpty(j)) {
			var f = a.utils.getDimensionByKey(c.dataSourceId, c.semanticKey, j);
			if (Ext.isEmpty(f)) {
				c.itemConfig.dimensionKey = undefined;
				c.itemConfig.dimensionFormatter = undefined;
				c.itemConfig.isTopValue = true
			}
		}
		var g = c.infoConfig.dimensionKey;
		if (!Ext.isEmpty(g)) {
			var e = a.utils.getDimensionByKey(c.dataSourceId, c.semanticKey, g);
			if (Ext.isEmpty(e)) {
				c.infoConfig.dimensionKey = undefined;
				c.infoConfig.formatter = undefined;
				c.infoConfig.isTopValue = true
			}
		}
	},
	maintainAlertsConfigurated : function(a) {
		var b = this;
		b.maintainAlertsConditionConfigurated(a);
		b.maintainAlertsDimensionConfigurated(a)
	},
	maintainAlertsConditionConfigurated : function(f) {
		var a = this;
		var r = f.alertConfig.conditonsList;
		if (Ext.isEmpty(r)) {
			return
		}
		var c = [];
		for (var k = 0; k < r.length; k++) {
			var d = r[k];
			var m;
			if (!Ext.isEmpty(d.metric) && !Ext.isEmpty(d.metric.key)) {
				if (d.metric.key == a.constants.COUNT_ONE_KEY) {
					m = Ext.decode(Ext.encode(d.metric))
				} else {
					var g = a.utils.getMetricByKey(f.dataSourceId,
							f.semanticKey, d.metric.key);
					if (!Ext.isEmpty(g)) {
						m = Ext.decode(Ext.encode(d.metric))
					}
				}
			}
			var e;
			if (!Ext.isEmpty(d.targetMetric)
					&& !Ext.isEmpty(d.targetMetric.key)) {
				if (d.targetMetric.key == a.constants.COUNT_ONE_KEY) {
					e = Ext.decode(Ext.encode(d.targetMetric))
				} else {
					var q = a.utils.getMetricByKey(f.dataSourceId,
							f.semanticKey, d.targetMetric.key);
					if (!Ext.isEmpty(q)) {
						e = Ext.decode(Ext.encode(d.targetMetric))
					}
				}
			}
			var b;
			if (!Ext.isEmpty(m)
					|| (!Ext.isEmpty(e) && d.type == a.constants.CONDITION_TYPE_TO_ANOTHER_METRIC)) {
				b = Ext.decode(Ext.encode(d));
				b.metric = m;
				b.targetMetric = e;
				c.push(b)
			}
		}
		f.alertConfig.conditonsList = c;
		var o = [];
		var n = f.alertConfig.displayColumnsList[0];
		n.autoGenName = a.getAlertDimensionsDisplayColumnName();
		o.push(n);
		for (var l = 0; l < f.alertConfig.conditonsList.length; l++) {
			var p, d = f.alertConfig.conditonsList[l];
			for (var k = 1; k < f.alertConfig.displayColumnsList.length; k++) {
				var h = f.alertConfig.displayColumnsList[k];
				if (h.id == d.id && a.validateCondition(d)) {
					p = Ext.decode(Ext.encode(h));
					p.autoGenName = a.getAlertConditionDisplayColumnName(d,
							f.dataSourceId, f.semanticKey);
					break
				}
			}
			if (p) {
				o.push(p)
			}
		}
		f.alertConfig.displayColumnsList = o
	},
	maintainAlertsDimensionConfigurated : function(c) {
		var e = this;
		if (Ext.isEmpty(c.alertConfig.dimensionsList)) {
			return
		}
		var d = [];
		for (var a = 0; a < c.alertConfig.dimensionsList.length; a++) {
			var b = c.alertConfig.dimensionsList[a];
			if (e.utils.getDimensionByKey(c.dataSourceId, c.semanticKey, b.key)) {
				d.push(b)
			}
		}
		c.alertConfig.dimensionsList = d
	},
	reLoadDefineAlertTab : function(d, b, e) {
		var c = this;
		var a = Ext.getCmp(c.portletId + "alert_tab_panel");
		a.remove(c.portletId + "alert_dimensions_panel");
		a.remove(c.portletId + "alert_conditions_panel");
		a.remove(c.portletId + "alert_display_options_panel");
		a.insert(2, d);
		a.insert(3, b);
		a.add(e);
		a.doLayout()
	},
	bindSubtypePanelEvents : function(a, c) {
		var b = this;
		a.el.on("mouseenter", function() {
			Ext.getCmp(b.portletId + "subtype_details_panel").update(
					c.introduction)
		});
		a.el.on("mouseleave", function() {
			Ext.getCmp(b.portletId + "subtype_details_panel").update("")
		})
	},
	bindTypeSelectPanelEvents : function(e, a, d) {
		var c = this;
		var b = a.el.select(".eid-type-image-container");
		b.on("mouseenter", function(g, f, h) {
			b.addClass("eid-type-image-container-hover")
		});
		b.on("mouseleave", function(g, f, h) {
			b.removeClass("eid-type-image-container-hover")
		});
		b.on("click", function(j, h, k) {
			var f = c.getCurrentConfig();
			if (f.getType() == d.type) {
				return
			} else {
				var g = c.utils.getSummarizationName(f);
				f.setType(d.type);
				var i = c.utils.getSummarizationName(f);
				if (d.type == c.constants.SUMMARIZATION_TYPE_DIMENSION_VALUE) {
					f.itemConfig.action = c.utils.getDefaultRefinementAction()
				} else {
					delete f.itemConfig.action
				}
			}
			a.el.findParent(".eid-summarization-types-panel", null, true)
					.select(".eid-type-image-container-selected").removeClass(
							"eid-type-image-container-selected");
			e.addClass("eid-type-image-container-selected");
			switch (d.type) {
			case c.constants.SUMMARIZATION_TYPE_DIMENSION_VALUE:
			case c.constants.SUMMARIZATION_TYPE_METRIC_VALUE:
				if (f.itemConfig.isCustomDisplayName) {
					if (Ext.isEmpty(i)) {
						i = g
					}
					f.itemConfig.customDisplayName = i
				} else {
					f.name = i;
					c.updateSummarizationNameInStore(f.id, i)
				}
				break;
			case c.constants.SUMMARIZATION_TYPE_ALERT_VALUE:
				f.alertConfig.displayName = g;
				break
			}
			c.updateIncompleteIcon(c.checkConfigCompleted(f));
			c.handleStepTabDisplayHide(d.type);
			if (d.type == c.constants.SUMMARIZATION_TYPE_METRIC_VALUE) {
				if (!Ext.isEmpty(f.itemConfig.metricKey)) {
					c.itemClearDescription(f.itemConfig.metricKey,
							f.itemConfig.metricAggre)
				}
			} else {
				c.itemUpdateDescription(false)
			}
		})
	},
	handleSubTypeSelect : function(c, b) {
		var d = this;
		var a = d.getCurrentConfig();
		if (c && a.getSubtype() != b) {
			a.setSubtype(b)
		}
	},
	loadDataToSelectTypePanel : function() {
		var c = this;
		var a = c.getCurrentConfig().getType();
		var d = Ext.getCmp(c.portletId + "type_metric_value");
		var b = Ext.getCmp(c.portletId + "type_alert_value");
		if (a == c.constants.SUMMARIZATION_TYPE_METRIC_VALUE) {
			d.addClass("eid-type-image-container-selected");
			b.removeClass("eid-type-image-container-selected")
		} else {
			if (a == c.constants.SUMMARIZATION_TYPE_DIMENSION_VALUE) {
				d.removeClass("eid-type-image-container-selected");
				b.removeClass("eid-type-image-container-selected")
			} else {
				if (a == c.constants.SUMMARIZATION_TYPE_ALERT_VALUE) {
					d.removeClass("eid-type-image-container-selected");
					b.addClass("eid-type-image-container-selected")
				}
			}
		}
		c.handleStepTabDisplayHide(a)
	},
	handleStepTabDisplayHide : function(a) {
		var b = this;
		if (a == b.constants.SUMMARIZATION_TYPE_ALERT_VALUE) {
			Ext.getCmp(b.portletId + "step_tab_panel").hideTabStripItem(
					b.portletId + "step_define_item");
			Ext.getCmp(b.portletId + "step_tab_panel").hideTabStripItem(
					b.portletId + "step_define_conditions");
			Ext.getCmp(b.portletId + "step_tab_panel").unhideTabStripItem(
					b.portletId + "step_define_alert")
		} else {
			Ext.getCmp(b.portletId + "step_tab_panel").unhideTabStripItem(
					b.portletId + "step_define_item");
			Ext.getCmp(b.portletId + "step_tab_panel").unhideTabStripItem(
					b.portletId + "step_define_conditions");
			Ext.getCmp(b.portletId + "step_tab_panel").hideTabStripItem(
					b.portletId + "step_define_alert")
		}
	},
	itemUpdateLayout : function(b, l) {
		var a = this;
		var c = a.getCurrentConfig();
		var h = c.itemConfig;
		var j = c.getType();
		var g = c.getSubtype();
		var i = Ext.getCmp(a.portletId + "step_define_item_description");
		var f = Ext.getCmp(a.portletId
				+ "define_item_select_dimension_button_container");
		var k = Ext.getCmp(a.portletId + "define_item_top_buttom_radio");
		var e = Ext.getCmp(a.portletId + "default_display_name").wrap
				.first("label");
		if (j == a.constants.SUMMARIZATION_TYPE_METRIC_VALUE) {
			i
					.update(a.utils
							.localize("summarizationbar.edit.label.metric-description"));
			e.update(a.utils
					.localize("summarizationbar.edit.label.use-metric-name"));
			f.hide();
			k.hide()
		} else {
			i
					.update(a.utils
							.localize("summarizationbar.edit.label.dimval-description"));
			e
					.update(a.utils
							.localize("summarizationbar.edit.label.use-dimension-metric-name"));
			f.show();
			k.show();
			a.itemUpdateDimensionEditField({
				attributeKey : h.dimensionKey,
				formatter : h.dimensionFormatter,
				datePartCombo : h.datePartCombo
			})
		}
		if (g != a.constants.SUMMARIZATION_SUBTYPE_SINGLE_METRIC) {
			Ext.getCmp(a.portletId + "define_item_selected_metric_container").label
					.hide()
		} else {
			Ext.getCmp(a.portletId + "define_item_selected_metric_container").label
					.show()
		}
		a.itemUpdateMetricEditField({
			attributeKey : h.metricKey,
			aggregation : h.metricAggre,
			formatter : h.metricFormatter
		});
		if (g == a.constants.SUMMARIZATION_SUBTYPE_METRICS_RATIO) {
			a.itemHideTargetAggreContainer()
		} else {
			if (g == a.constants.SUMMARIZATION_SUBTYPE_PART_WHOLE_RATIO) {
				a.itemHideTargetMetricContainer()
			} else {
				a.itemHideTargetMetricContainer();
				a.itemHideTargetAggreContainer()
			}
		}
		var m = Ext.getCmp(a.portletId + "metric_description_introduction");
		if (m.rendered) {
			m.update({
				qtipText : b[c.type].qtipText,
				type : j
			})
		} else {
			m.data = {
				qtipText : b[c.type].qtipText,
				type : j
			}
		}
		Ext.getCmp(a.portletId + "metric_description").setValue(h.description);
		var d = Ext.getCmp(a.portletId + "metric_descritpion_example");
		if (d.rendered) {
			d.update({
				text : b[c.type].exampleText
			})
		} else {
			d.data = {
				text : b[c.type].exampleText
			}
		}
		l.rerenderActionPanel();
		a.itemUpdateDisplayName();
		a.itemSyncDisplayNameSelect();
		a.itemUpdateMaxWidth(!h.isCustomWidth);
		a.updateIncompleteIcon(a.checkConfigCompleted(c))
	},
	updateAttributeSelectData : function(a) {
		var b = this;
		a.dataSourceId = b.lastDataSourceId;
		a.editField.dataSourceId = b.lastDataSourceId;
		a.semanticKey = b.lastSemanticKey;
		a.editField.semanticKey = b.lastSemanticKey
	},
	itemHideTargetMetricContainer : function() {
		Ext.getCmp(
				this.portletId + "define_item_metric_ratio_description_field")
				.hide();
		Ext.getCmp(this.portletId + "define_item_selected_slash_displayfield")
				.hide();
		Ext.getCmp(
				this.portletId + "define_item_target_metric_attr_config_field")
				.hide()
	},
	itemHideTargetAggreContainer : function() {
		Ext.getCmp(
				this.portletId + "define_item_part_to_whole_description_field")
				.hide();
		Ext.getCmp(this.portletId + "define_item_selected_slash_displayfield")
				.hide();
		Ext.getCmp(
				this.portletId + "define_item_selected_target_aggre_textfield")
				.hide()
	},
	itemHideTargetAggreField : function() {
		Ext.getCmp(
				this.portletId + "define_item_part_to_whole_description_field")
				.show();
		Ext.getCmp(this.portletId + "define_item_selected_slash_displayfield")
				.hide();
		Ext.getCmp(
				this.portletId + "define_item_selected_target_aggre_textfield")
				.hide()
	},
	itemShowTargetAggreField : function() {
		Ext.getCmp(
				this.portletId + "define_item_part_to_whole_description_field")
				.show();
		Ext.getCmp(this.portletId + "define_item_selected_slash_displayfield")
				.show();
		Ext.getCmp(
				this.portletId + "define_item_selected_target_aggre_textfield")
				.show()
	},
	itemSelectDimension : function(d) {
		var c = this;
		var b = c.getCurrentConfig();
		var a = b.itemConfig;
		a.dimensionKey = d.attributeKey;
		a.dimensionFormatter = d.formatter;
		a.datePartCombo = d.datePartCombo;
		Ext.getCmp(c.portletId + "define_item_top_buttom_radio").show();
		c.updateIncompleteIcon(c.checkConfigCompleted(b));
		c.itemUpdateDisplayName()
	},
	itemDeleteDimensionEditField : function() {
		var b = this;
		var a = b.getCurrentConfig().itemConfig;
		a.dimensionKey = undefined;
		a.dimensionFormatter = undefined;
		Ext.getCmp(b.portletId + "define_item_top_buttom_radio").hide();
		b.updateIncompleteIcon(b.checkConfigCompleted(b.getCurrentConfig()));
		b.itemUpdateDisplayName()
	},
	itemUpdateDimensionEditField : function(c) {
		var d = this;
		var a = d.getCurrentConfig();
		var b = Ext.getCmp(d.portletId + "define_item_dimension_config_field");
		d.updateAttributeSelectData(b);
		b.setAttributeData(c);
		if (!Ext.isEmpty(a.itemConfig.dimensionKey)) {
			Ext.getCmp(d.portletId + "define_item_top_buttom_radio").show()
		} else {
			b.editField.onTrigger2Click()
		}
	},
	checkCFEnabled : function() {
		var c = this.getCurrentConfig();
		var a = c.itemConfig;
		if (!a.metricKey) {
			c.conditionEnabled = true;
			Ext.getCmp(this.portletId + "coditional_format_panel").setDisabled(
					false);
			if (Ext.get(this.portletId + "step_define_conditions_description")) {
				Ext
						.get(
								this.portletId
										+ "step_define_conditions_description")
						.update(
								this.utils
										.localize("summarizationbar.edit.define-conditions-panel.description"))
			}
		}
		var b = this.utils.getAttributeAggregatedType(a.metricKey,
				a.metricAggre, c);
		if (this.constants.MDEX_DATA_TYPE_LONG == b
				|| this.constants.MDEX_DATA_TYPE_INT == b
				|| this.constants.MDEX_DATA_TYPE_DOUBLE == b) {
			c.conditionEnabled = true;
			if (!c.conditionsConfig
					|| c.conditionsConfig.type == this.constants.CONDITION_TYPE_TO_ANOTHER_METRIC) {
				this.checkCFTargetValid()
			}
			Ext.getCmp(this.portletId + "coditional_format_panel").setDisabled(
					false);
			if (Ext.get(this.portletId + "step_define_conditions_description")) {
				Ext
						.get(
								this.portletId
										+ "step_define_conditions_description")
						.update(
								this.utils
										.localize("summarizationbar.edit.define-conditions-panel.description"))
			}
		} else {
			c.conditionEnabled = false;
			Ext.getCmp(this.portletId + "coditional_format_panel").setDisabled(
					true);
			if (Ext.get(this.portletId + "step_define_conditions_description")) {
				Ext
						.get(
								this.portletId
										+ "step_define_conditions_description")
						.update(
								this.utils
										.localize("summarizationbar.edit.define-conditions-panel.incomplete-description"))
			}
		}
	},
	getDefaultFormatter : function(b) {
		var f = this;
		var e = f.getCurrentConfig();
		var g = b.attributeKey;
		if (g == f.constants.COUNT_ONE_KEY
				|| b.aggregation == f.constants.AGGREGATION_FUNCTION_COUNT
				|| b.aggregation == f.constants.AGGREGATION_FUNCTION_COUNTDISTINCT) {
			return Ext.ux.endeca.FormattingConstants.Maps.getDefaultFormatter(
					f.constants.MDEX_DATA_TYPE_LONG, undefined)
		} else {
			var d = b.aggregation;
			var c = f.utils.getAttributeByKey(e.dataSourceId, e.semanticKey, g);
			var a = Ext.ux.endeca.FormattingConstants.Maps
					.getAggregatedDataType(c.datatype, d);
			return Ext.ux.endeca.FormattingConstants.Maps.getDefaultFormatter(
					a, Ext.decode(c.formatSettings))
		}
	},
	updateActionPanel : function() {
		var b = this;
		var a = Ext.getCmp(b.portletId + "_hyperlink-action-on-attribute");
		if (a) {
			Ext.getCmp(b.portletId + "_hyperlink-action-on-attribute").parameters = b
					.getParameters()
		}
	},
	itemSelectMetric : function(c) {
		var e = this;
		var d = e.getCurrentConfig();
		var b = d.itemConfig;
		var a = b.metricKey;
		var f = b.metricAggre;
		b.metricKey = c.attributeKey;
		b.metricAggre = c.aggregation;
		b.metricFormatter = c.formatter;
		if (Ext.isEmpty(b.metricFormatter)) {
			b.metricFormatter = Ext.encode(e.getDefaultFormatter(c))
		}
		e.itemUpdateDisplayName();
		if (d.getType() == e.constants.SUMMARIZATION_TYPE_METRIC_VALUE) {
			Ext.getCmp(e.portletId + "action_panel").parameters = e
					.getParameters()
		}
		if (d.getType() == e.constants.SUMMARIZATION_TYPE_DIMENSION_VALUE
				&& !d.isCustomerizedDesc) {
			if (!Ext.isEmpty(a)) {
				e.itemClearDescription(a, f)
			}
			e.itemUpdateDescription(true)
		}
		e.updateIncompleteIcon(e.checkConfigCompleted(d));
		e.checkCFEnabled()
	},
	itemUpdateDescription : function(h) {
		var f = this;
		var e = f.getCurrentConfig();
		var c = e.itemConfig;
		if (!Ext.isEmpty(c.metricKey)) {
			var b = Ext.getCmp(f.portletId + "metric_description");
			var g = f.utils.getFieldValueFromMetric(e.dataSourceId,
					e.semanticKey, c.metricKey, c.metricAggre, true);
			var d = g + ": " + f.constants.METRIC_VALUE_TOKEN_SPACE;
			var a = b.getValue() || "";
			if (Ext.isEmpty(a) || (h && a.indexOf(d) == -1)) {
				a = a + d;
				b.setValue(a);
				b.fireEvent("change", b, a)
			}
		}
	},
	updateIncompleteIcon : function(f) {
		var e = this;
		var d = e.getCurrentConfig();
		var a = Ext.StoreMgr.get(e.portletId + "summarization_store");
		var c = a.find("id", d.id);
		if (c != -1) {
			var b = a.getAt(c);
			b.beginEdit();
			b.set("isCompleted", f);
			b.endEdit();
			b.commit();
			d.isCompleted = f
		}
	},
	itemDeleteMetricField : function() {
		var c = this.getCurrentConfig();
		var b = c.itemConfig;
		b.metricKey = undefined;
		b.metricAggre = undefined;
		b.metricFormatter = undefined;
		c.formattedValue = undefined;
		var a = this.getCurrentConfig().getSubtype();
		if (a == this.constants.SUMMARIZATION_SUBTYPE_PART_WHOLE_RATIO) {
			this.itemHideTargetAggreField()
		}
		this.itemUpdateDisplayName();
		this.updateIncompleteIcon(this.checkConfigCompleted(c));
		this.checkCFEnabled()
	},
	itemUpdateMetricEditField : function(a) {
		var b = this;
		var c = Ext
				.getCmp(b.portletId + "define_item_metric_attr_config_field");
		b.updateAttributeSelectData(c);
		c.setAttributeData(a)
	},
	itemSelectTargetMetric : function(b) {
		this.itemHideTargetMetricButton();
		var a = this.getCurrentConfig().itemConfig;
		a.targetMetricKey = b.attributeKey;
		a.targetMetricAggre = b.aggregation;
		a.targetMetricFormatter = b.formatter;
		if (Ext.isEmpty(a.targetMetricFormatter)) {
			a.targetMetricFormatter = Ext.encode(this.getDefaultFormatter(b))
		}
		this.itemUpdateDisplayName()
	},
	itemDeteleTargetMetricField : function(b) {
		this.itemShowTargetMetricButton();
		var a = this.getCurrentConfig().itemConfig;
		a.targetMetricKey = undefined;
		a.targetMetricAggre = undefined;
		a.targetMetricFormatter = undefined;
		this.itemUpdateDisplayName()
	},
	itemUpdateTargetMetricEditField : function(a) {
		var b = this;
		var c = Ext.getCmp(b.portletId
				+ "define_item_target_metric_attr_config_field");
		c.setAttributeData(a)
	},
	itemSyncDisplayNameSelect : function() {
		var b = this.getCurrentConfig();
		var c = Ext.getCmp(this.portletId + "custom_display_name");
		var a = Ext.getCmp(this.portletId + "default_display_name");
		if (b.isNew && !b.isChangeName) {
			c.setDisabled(false);
			a.setDisabled(false);
			a.setValue(true);
			b.itemConfig.isCustomDisplayName = false
		}
	},
	itemUpdateDisplayName : function() {
		var b = this.getCurrentConfig();
		var e = b.itemConfig;
		var d = Ext.getCmp(this.portletId + "default_display_name_field");
		var i = Ext.getCmp(this.portletId + "custom_display_name_field");
		var f = Ext.getCmp(this.portletId + "default_display_name");
		var c = Ext.getCmp(this.portletId + "custom_display_name");
		d.setValue(this.utils.getAutoSummarizationName(b));
		var g = this.utils.getSummarizationName(b);
		var a = e.isCustomDisplayName;
		if (a) {
			i.setValue(g)
		}
		var h = false;
		if ((b.getType() == this.constants.SUMMARIZATION_TYPE_METRIC_VALUE && Ext
				.isEmpty(e.metricKey))
				|| (b.getType() == this.constants.SUMMARIZATION_TYPE_DIMENSION_VALUE
						&& Ext.isEmpty(e.dimensionKey) && Ext
						.isEmpty(e.metricKey))) {
			d.setValue("");
			a = true;
			h = true
		}
		this.updateSummarizationNameInStore(b.id, g);
		Ext.getCmp(this.portletId + "step_define_item_header").update({
			summarizationName : g
		});
		i.setDisabled(!a);
		c.setValue(a);
		f.setDisabled(h);
		d.setDisabled(h)
	},
	itemChangeMetricDesc : function(e) {
		var d = this;
		var c = d.getCurrentConfig();
		var a = c.itemConfig;
		var f = d.utils.getFieldValueFromMetric(c.dataSourceId, c.semanticKey,
				a.metricKey, a.metricAggre, true);
		var b = f + ": " + d.constants.METRIC_VALUE_TOKEN_SPACE;
		if (!Ext.isEmpty(e) && e != b) {
			c.isCustomerizedDesc = true
		} else {
			c.isCustomerizedDesc = false
		}
		c.itemConfig.description = e
	},
	itemUpdateMaxWidth : function(a) {
		Ext.getCmp(this.portletId + "custom_max_width").setDisabled(a);
		Ext.getCmp(this.portletId + "default_max_width").setValue(a);
		Ext.getCmp(this.portletId + "max_width").setValue(!a);
		var b = this.getCurrentConfig();
		b.itemConfig.isCustomWidth = !a
	},
	itemUpdateMaxWidthValue : function(a) {
		var b = this.getCurrentConfig();
		b.itemConfig.customWidth = Ext.getCmp(this.portletId
				+ "custom_max_width").value
	},
	itemUpdateTopValue : function(a) {
		Ext.getCmp(this.portletId + "top_value").setValue(a);
		var b = this.getCurrentConfig();
		b.itemConfig.isTopValue = a;
		this.itemUpdateDisplayName()
	},
	itemSelectDisplayName : function(c) {
		var b = this.getCurrentConfig();
		if (b.isNew && !b.isChangeName) {
			delete b.isNew;
			delete b.isChangeName;
			return
		}
		var a = b.itemConfig;
		if (c) {
			a.isCustomDisplayName = false;
			Ext.getCmp(this.portletId + "default_display_name_field").enable()
		} else {
			a.isCustomDisplayName = true;
			Ext.getCmp(this.portletId + "default_display_name_field").disable()
		}
		this.itemUpdateDisplayName()
	},
	itemHandleCustomNameChange : function(d, b) {
		if (d == b) {
			return
		}
		var a = this.getCurrentConfig();
		a.name = d;
		a.itemConfig.customDisplayName = d;
		var c = {
			summarizationName : a.name
		};
		Ext.getCmp(this.portletId + "step_define_item_header").update(c);
		this.updateSummarizationNameInStore(a.id, d)
	},
	handlCFBaseSelectionChange : function(c) {
		var d = this;
		var b = d.getCurrentConfig();
		b.conditionsConfig.type = c;
		var e = Ext.getCmp(d.portletId + "cf_metric_select_panel");
		var a = Ext.getCmp(d.portletId + "cf_panel");
		if (c == d.constants.CONDITION_TYPE_TO_ANOTHER_METRIC) {
			e.show();
			a.showAddTokenLink(d.getCFTokenLink());
			a.showPercentageSymbol();
			d.conditionUpdateMetricDisplayField();
			d.conditionUpdateTargetMetricField();
			d.checkCFTargetValid()
		} else {
			if (c == d.constants.CONDITION_TYPE_PART_TO_WHOLE) {
				e.hide();
				a.showAddTokenLink(d.getCFTokenLink());
				a
						.showPercentageSymbol("conditional-formattingv2.column.label.symbol.percentage.with.full.value");
				b.conditionEnabled = true
			} else {
				e.hide();
				a.hideAddTokenLink();
				a.hidePercentageSymbol();
				b.conditionEnabled = true
			}
		}
	},
	getCFTokenLink : function() {
		var c;
		var b = this;
		var a = b.getCurrentConfig();
		if (a.conditionsConfig.type == b.constants.CONDITION_TYPE_TO_ANOTHER_METRIC) {
			c = {
				linkText : b.utils
						.localize("conditional-formattingv2.column.title.target-metric-value-hyperlink"),
				linkTooltip : b.utils
						.localize(
								"summarizationbar.edit.tooltip.condition-target-metric-value",
								b.constants.TARGET_METRIC_VALUE_TOKEN_SPACE),
				token : b.constants.TARGET_METRIC_VALUE_TOKEN
			}
		} else {
			if (a.conditionsConfig.type == b.constants.CONDITION_TYPE_PART_TO_WHOLE) {
				c = {
					linkText : b.utils
							.localize("conditional-formattingv2.column.title.full-metric-value-hyperlink"),
					linkTooltip : b.utils
							.localize(
									"summarizationbar.edit.tooltip.condition-full-metric-value",
									b.constants.WHOLE_METRIC_VALUE_TOKEN_SPACE),
					token : b.constants.WHOLE_METRIC_VALUE_TOKEN
				}
			}
		}
		return c
	},
	handlCFBaseSelectionAfterrender : function() {
		var c = this;
		var a = c.getCurrentConfig();
		var b = a.conditionsConfig.type;
		if (b == c.constants.CONDITION_TYPE_SELF_VALUE) {
			Ext.getCmp(c.portletId + "condition_type_self_value")
					.setValue(true);
			a.conditionEnabled = true
		} else {
			if (b == c.constants.CONDITION_TYPE_TO_ANOTHER_METRIC) {
				Ext.getCmp(c.portletId + "condition_type_to_another_metric")
						.setValue(true)
			} else {
				Ext.getCmp(c.portletId + "condition_type_part_to_whole")
						.setValue(true);
				a.conditionEnabled = true
			}
		}
	},
	handleCFPanelLoadDataToUI : function() {
		var d = this;
		var b = d.getCurrentConfig();
		var e = Ext.getCmp(d.portletId + "codition_base_selection_panel");
		var a = Ext.getCmp(d.portletId + "cf_panel");
		if (b.getSubtype() == d.constants.SUMMARIZATION_SUBTYPE_SINGLE_METRIC) {
			e.show();
			var c = b.conditionsConfig.type;
			if (c == d.constants.CONDITION_TYPE_PART_TO_WHOLE) {
				a.showAddTokenLink(d.getCFTokenLink())
			} else {
				if (c == d.constants.CONDITION_TYPE_TO_ANOTHER_METRIC) {
					a.showAddTokenLink(d.getCFTokenLink());
					d.conditionUpdateMetricDisplayField();
					d.conditionUpdateTargetMetricField();
					d.checkCFTargetValid()
				} else {
					a.hideAddTokenLink()
				}
			}
		} else {
			e.hide();
			a.hideAddTokenLink()
		}
	},
	conditionUpdateMetricDisplayField : function() {
		var d = this;
		var a = d.getCurrentConfig();
		var c = a.itemConfig.metricKey;
		var g = a.itemConfig.metricAggre;
		var e = Ext.getCmp(d.portletId + "cf_metric_displayfield");
		e.setValue(d.utils.getFieldValueFromMetric(a.dataSourceId,
				a.semanticKey, c, g, true));
		var b = Ext.util.TextMetrics.createInstance(e.el);
		var f = b.getWidth(e.getValue());
		e.setWidth(f + 30)
	},
	conditionSelectTargetMetric : function(b) {
		var a = this.getCurrentConfig().conditionsConfig;
		a.targetMetricKey = b.attributeKey;
		a.targetMetricAggre = b.aggregation;
		a.targetMetricFormatter = b.formatter;
		if (Ext.isEmpty(a.targetMetricFormatter)) {
			a.targetMetricFormatter = Ext.encode(this.getDefaultFormatter(b))
		}
		this.checkCFTargetValid()
	},
	checkCFTargetValid : function() {
		var e = this.getCurrentConfig();
		var a = e.conditionsConfig;
		var d = e.conditionsConfig.type;
		if (!a.targetMetricKey
				&& d == this.constants.CONDITION_TYPE_TO_ANOTHER_METRIC) {
			e.conditionEnabled = false;
			return
		}
		var c = this.utils.getAttributeAggregatedType(a.targetMetricKey,
				a.targetMetricAggre, e);
		var b = Ext.getCmp(this.portletId
				+ "cf_target_metric_attr_config_field").editField;
		if (this.constants.MDEX_DATA_TYPE_LONG == c
				|| this.constants.MDEX_DATA_TYPE_INT == c
				|| this.constants.MDEX_DATA_TYPE_DOUBLE == c) {
			e.conditionEnabled = true;
			b.clearInvalid()
		} else {
			e.conditionEnabled = false;
			b.markInvalid()
		}
	},
	conditionDeleteTargetMetric : function() {
		var b = this;
		var a = b.getCurrentConfig().conditionsConfig;
		if (a.type != b.constants.CONDITION_TYPE_PART_TO_WHOLE) {
			a.targetMetricKey = undefined;
			a.targetMetricAggre = undefined;
			a.targetMetricFormatter = undefined
		}
		b.checkCFTargetValid()
	},
	conditionUpdateTargetMetricField : function() {
		var b = this;
		var a = b.getCurrentConfig().conditionsConfig;
		var c = Ext.getCmp(b.portletId + "cf_target_metric_attr_config_field");
		b.updateAttributeSelectData(c);
		c.setAttributeData({
			attributeKey : a.targetMetricKey,
			aggregation : a.targetMetricAggre,
			formatter : a.targetMetricFormatter
		})
	},
	loadDataToDetailedInfoConfigPanel : function(b) {
		var c = this;
		var a = c.getCurrentConfig();
		var d = a.infoConfig;
		Ext.getCmp(c.portletId + "display_info_panel").setValue(
				a.infoConfig.isDisplay);
		Ext.getCmp(c.portletId + "detailed_info_preview_panel_container")
				.setVisible(a.infoConfig.isDisplay);
		c.updateDetailedInfoPreviewPanel();
		c.loadDataToRelatedDimensionValuesPanel()
	},
	loadDataToRelatedDimensionValuesPanel : function() {
		var e = this;
		var a = e.getCurrentConfig();
		var f = a.infoConfig;
		Ext.getCmp(e.portletId + "related_dimension_metric_name").setValue(
				Ext.util.Format.htmlEncode(a.name));
		Ext.getCmp(e.portletId + "use_dimension_name_radio").setValue(
				!f.isCustomTitle);
		Ext.getCmp(e.portletId + "detailed_info_title").setValue(f.customTitle);
		Ext.getCmp(e.portletId + "number_of_values").setValue(f.maxNumber);
		var c = e.utils.getAttributeDisplayName(a.dataSourceId, a.semanticKey,
				f.dimensionKey);
		Ext.getCmp(e.portletId + "dimension_name_field").update({
			value : c
		});
		if (a.getType() == e.constants.SUMMARIZATION_TYPE_METRIC_VALUE) {
			var b = !Ext.isEmpty(f.dimensionKey);
			Ext.getCmp(e.portletId + "detailed_info_dimension_selected_panel")
					.setVisible(b);
			Ext.getCmp(e.portletId + "detailed_info_select_dimension_btn")
					.setVisible(!b);
			Ext.getCmp(e.portletId + "show_more_values").hide();
			if (b) {
				var d = Ext.getCmp(e.portletId
						+ "detailed_info_dimension_field");
				d.setValue(c)
			}
		} else {
			Ext.getCmp(e.portletId + "detailed_info_dimension_selected_panel")
					.hide();
			Ext.getCmp(e.portletId + "detailed_info_select_dimension_btn")
					.hide();
			Ext.getCmp(e.portletId + "show_more_values").show();
			Ext.getCmp(e.portletId + "show_more_value_checkbox").setValue(
					f.isShowMore);
			Ext.getCmp(e.portletId + "show_more_value_dimension_name")
					.setValue(c)
		}
	},
	changeIsDetailedInfoPanelDisplay : function(a) {
		var b = this;
		b.getCurrentConfig().infoConfig.isDisplay = a;
		Ext.getCmp(b.portletId + "detailed_info_preview_panel_container")
				.setVisible(a)
	},
	changeIsTopValue : function(a) {
		var b = this;
		b.getCurrentConfig().infoConfig.isTopValue = a;
		b.updateDetailedInfoPreviewPanel()
	},
	changeIsShowMore : function(b) {
		var a = this;
		a.getCurrentConfig().infoConfig.isShowMore = b
	},
	changeIsCustomTitle : function(b) {
		var c = this;
		c.getCurrentConfig().infoConfig.isCustomTitle = b;
		var a = Ext.getCmp(c.portletId + "detailed_info_title");
		if (!b) {
			a.clearInvalid()
		} else {
			a.validateValue()
		}
		a.setDisabled(!b);
		if (b && !a.isValid()) {
			return
		}
		c.updateDetailedInfoPreviewPanel()
	},
	changeCustomTitle : function(c, b) {
		var a = this;
		a.getCurrentConfig().infoConfig.customTitle = b;
		if (c.isValid()) {
			a.updateDetailedInfoPreviewPanel()
		}
	},
	changeNumberOfValues : function(b) {
		var a = this;
		a.getCurrentConfig().infoConfig.maxNumber = b;
		a.updateDetailedInfoPreviewPanel()
	},
	saveDetailedInfoSelectedDimension : function(c) {
		var b = this;
		var a = b.getCurrentConfig().infoConfig;
		a.dimensionKey = c.key;
		a.dimensionDispName = c.displayName;
		a.dataType = c.dataType;
		a.formatter = c.formatSettings;
		b.loadDataToRelatedDimensionValuesPanel();
		b.updateDetailedInfoPreviewPanel()
	},
	deleteDetailedInfoDimension : function() {
		var b = this;
		var a = b.getCurrentConfig().infoConfig;
		a.dimensionKey = undefined;
		a.dimensionDispName = undefined;
		a.dataType = undefined;
		a.formatter = undefined;
		a.action = undefined;
		b.loadDataToRelatedDimensionValuesPanel();
		b.updateDetailedInfoPreviewPanel()
	},
	updateDetailedInfoPreviewPanel : function() {
		var c = this;
		var b = c.getCurrentConfig();
		if (Ext.isEmpty(b)) {
			return
		}
		var d = b.infoConfig;
		var a = c.getDetailedInfoPreviewData(b);
		Ext.getCmp(c.portletId + "detailed_info_preview_panel").update(a)
	},
	getDetailedInfoPreviewData : function(c) {
		var f = this;
		var b = {};
		var g = c.detailedInfo;
		var a = summarizationBarEditModel.detailedInfoPreviewData;
		if (!Ext.isEmpty(g.description)) {
			b.description = a.description
		}
		if (!Ext.isEmpty(g.dimensionKey)) {
			var e = [];
			b.dataInfo = {};
			for (var d = 0; d < g.maxNumber; d++) {
				e.push({
					name : "Name " + (d + 1),
					value : g.maxNumber - d - 1
				})
			}
			b.dataInfo.values = e;
			if (g.isCustomTitle) {
				b.dataInfo.title = g.customTitle
			} else {
				b.dataInfo.title = f.utils.getAttributeDisplayName(
						c.dataSourceId, c.semanticKey, g.dimensionKey)
			}
			b.dataInfo.isCustomTitle = g.isCustomTitle
		}
		if (Ext.isEmpty(b.dataInfo) && Ext.isEmpty(b.description)) {
			b.description = f.utils
					.localize("summarizationbar.edit.no-effective-config-description")
		}
		return b
	},
	loadDataToDefineAlertTabPanel : function() {
		var b = this;
		var a = b.getCurrentConfig();
		Ext.getCmp(b.portletId + "alert_display_name").setValue(
				Ext.util.Format.htmlDecode(a.alertConfig.displayName))
	},
	handleAlertDisplayNameChange : function(d, b) {
		var c = this;
		if (Ext.isEmpty(d)) {
			Ext.getCmp(c.portletId + "alert_display_name").setValue(b);
			return
		}
		var a = c.getCurrentConfig();
		a.alertConfig.displayName = d;
		a.name = d;
		c.updateSummarizationNameInStore(a.id, d);
		Ext.getCmp(this.portletId + "step_define_alert_header").update({
			summarizationName : d
		})
	},
	handleAlertDimensionAdd : function(c) {
		var b = this;
		var a = b.getCurrentConfig().alertConfig.dimensionsList;
		Ext.getCmp(b.portletId + "alert_dimensions_container").add(c);
		c.getSelectAttributeWindow().show();
		c.hide()
	},
	handleAlertDimensionBeforeChange : function(a, c) {
		var b = this;
		if (a == undefined) {
			c.show();
			Ext.getCmp(b.portletId + "alert_dimensions_container").doLayout();
			b.registerAlertDimensionsDD()
		}
	},
	handleAlertDimensionChange : function(b, g, f) {
		var e = this;
		var a = e.getCurrentConfig();
		var d = a.alertConfig.dimensionsList;
		for (var c = 0; c < d.length; c++) {
			if (d[c].key == b) {
				break
			}
		}
		d[c] = {
			key : f.attributeKey,
			formatter : f.formatter,
			datePartCombo : f.datePartCombo
		};
		e.syncAlertDisplayColumn(true);
		if (b == undefined && d.length == 1) {
			e.syncAlertIsCompleted()
		}
	},
	handleAlertDimensionDelete : function(a, f) {
		var e = this;
		var b = e.getCurrentConfig();
		var d = b.alertConfig.dimensionsList;
		for (var c = 0; c < d.length; c++) {
			if (d[c].key == a) {
				d.splice(c, 1);
				e.syncAlertDisplayColumn(true);
				break
			}
		}
		Ext.getCmp(e.portletId + "alert_dimensions_container").remove(f);
		if (d.length == 0) {
			e.syncAlertIsCompleted()
		}
	},
	handleAlertDimensionWindowClose : function(a, e) {
		var d = this;
		var b = d.getCurrentConfig();
		var c = b.alertConfig.dimensionsList;
		if (Ext.isEmpty(a)) {
			Ext.getCmp(d.portletId + "alert_dimensions_container").remove(e)
		}
	},
	registerAlertDimensionsDD : function() {
		var b = this;
		if (b.alertDimensionsDD) {
			b.alertDimensionsDD.destroy()
		}
		var a = Ext.get(b.portletId + "alert_dimensions_container").dom;
		b.alertDimensionsDD = new Endeca.Portlets.SummarizationBarPortlet.DDCore(
				{
					container : a,
					itemSelectors : [ ".eid-attribute-select-field" ],
					dragGroups : [ b.portletId + "alert_dimensions_container" ],
					selfDragOnly : true,
					listeners : {
						afterdrop : function(c, g, f, e, d) {
							b.sortAlertsDimensions(g, f);
							b.syncAlertDisplayColumn(true)
						}
					}
				})
	},
	sortAlertsDimensions : function(h, c) {
		var e = this;
		var a = e.getCurrentConfig();
		var d = a.alertConfig.dimensionsList;
		if (c < d.length) {
			var g = d[h];
			d.splice(h, 1);
			var b, f;
			b = d.slice(0, c);
			f = d.slice(c);
			b.push(g);
			a.alertConfig.dimensionsList = b.concat(f)
		}
	},
	syncAlertDisplayColumn : function(m, e, a, k) {
		var b = this;
		var h = "";
		var g = b.getCurrentConfig();
		if (m) {
			h = b.getAlertDimensionsDisplayColumnName();
			g.alertConfig.displayColumnsList[0].autoGenName = h;
			if (!g.alertConfig.displayColumnsList[0].isCustomName) {
				Ext
						.getCmp(
								b.portletId
										+ "alert_display_column_name_dimensions")
						.setValue(h)
			}
		} else {
			var c = false;
			for (var f = 0; f < g.alertConfig.displayColumnsList.length; f++) {
				if (g.alertConfig.displayColumnsList[f].id == e.id) {
					c = true;
					break
				}
			}
			var d = b.validateCondition(e);
			var l = Ext.getCmp(k.portletId + "alert_condition_columns_panel");
			h = b.getAlertConditionDisplayColumnName(e, g.dataSourceId,
					g.semanticKey);
			if (!c && d) {
				var j = {
					id : e.id,
					isDisplay : true,
					isCustomName : false,
					autoGenName : h,
					customName : h
				};
				g.alertConfig.displayColumnsList.push(j);
				l.add(k.getAlertDisplayColumnItemPanel(j, true));
				l.doLayout();
				if (g.alertConfig.displayColumnsList.length == 2) {
					l.show()
				}
			}
			if (c && d) {
				g.alertConfig.displayColumnsList[f].autoGenName = h;
				if (!g.alertConfig.displayColumnsList[f].isCustomName) {
					Ext.getCmp(
							k.portletId + "alert_display_column_name_" + e.id)
							.setValue(h)
				}
			}
			if (c && (a || !d)) {
				g.alertConfig.displayColumnsList.splice(f, 1);
				l.remove(Ext.getCmp(k.portletId + "alert_display_column_panel_"
						+ e.id));
				if (g.alertConfig.displayColumnsList.length == 1) {
					l.hide()
				}
			}
		}
	},
	syncAlertDisplayColumnsList : function() {
		var f = this;
		var b = f.getCurrentConfig().alertConfig;
		var g = b.conditonsList;
		var d = b.displayColumnsList;
		var a = [ b.displayColumnsList[0] ];
		for (var e = 0; e < g.length; e++) {
			for (var c = 1; c < d.length; c++) {
				if (d[c].id == g[e].id) {
					a.push(d[c]);
					break
				}
			}
		}
		b.displayColumnsList = a
	},
	updateAlertDisplayColumnsUI : function(c) {
		var b = this;
		var a = Ext.getCmp(b.portletId + "alert_display_options_panel");
		a.remove(b.portletId + "alert_condition_columns_panel");
		a.add(c);
		a.doLayout()
	},
	syncAlertIsCompleted : function() {
		var b = this;
		var a = b.getCurrentConfig();
		b.updateIncompleteIcon(b.checkConfigCompleted(a))
	},
	checkAlertHasValidConditon : function(a) {
		var c = this;
		var d = false;
		for (var b = 0; !d && b < a.conditonsList.length; b++) {
			d = c.validateCondition(a.conditonsList[b])
		}
		return d
	},
	validateCondition : function(d) {
		var c = this;
		if (d.type == c.constants.CONDITION_TYPE_TO_ANOTHER_METRIC) {
			if (Ext.isEmpty(d.targetMetric)) {
				return false
			}
		} else {
			if (Ext.isEmpty(d.metric)) {
				return false
			}
		}
		var b = d.value1, a = d.value2;
		if (d.operator == c.constants.CONDITION_OPERATOR_BTWN) {
		}
		switch (d.operator) {
		case c.constants.CONDITION_OPERATOR_EQ:
		case c.constants.CONDITION_OPERATOR_GT:
		case c.constants.CONDITION_OPERATOR_GTEQ:
			if (!Ext.isEmpty(d.value1)) {
				return true
			} else {
				return false
			}
		case c.constants.CONDITION_OPERATOR_LT:
		case c.constants.CONDITION_OPERATOR_LTEQ:
			if (!Ext.isEmpty(d.value2)) {
				return true
			} else {
				return false
			}
		case c.constants.CONDITION_OPERATOR_BTWN:
			if (!Ext.isEmpty(d.value1) && !Ext.isEmpty(d.value2)) {
				return parseFloat(d.value2) >= parseFloat(d.value1)
			} else {
				return false
			}
		}
	},
	getAlertDimensionsDisplayColumnName : function() {
		var g = this;
		var b = g.getCurrentConfig();
		var f = "";
		for (var e = 0; e < b.alertConfig.dimensionsList.length; e++) {
			var c = g.utils.getAttributeDisplayName(b.dataSourceId,
					b.semanticKey, b.alertConfig.dimensionsList[e].key);
			if (!Ext.isEmpty(b.alertConfig.dimensionsList[e].datePartCombo)) {
				var a = "", h = b.alertConfig.dimensionsList[e].datePartCombo;
				for (var d = 0; d < h.length; d++) {
					a = a + h[d]
				}
				c = c + " ("
						+ EIDJSMessages.getMessage("datetime.combonames." + a)
						+ ")"
			}
			if (e == 0) {
				f = c
			} else {
				f = g.utils
						.localize(
								"summarizationbar.edit.panel.define-alert-tab.display-options-panel.display-columns.dimension-name",
								f, c)
			}
		}
		return f
	},
	getAlertConditionDisplayColumnName : function(a, c, d) {
		var e = this;
		var f = "", b = "";
		if (!Ext.isEmpty(a.metric)) {
			f = e.utils.getFieldValueFromMetric(c, d, a.metric.key,
					a.metric.aggregation, true)
		}
		if (!Ext.isEmpty(a.targetMetric)) {
			b = e.utils.getFieldValueFromMetric(c, d, a.targetMetric.key,
					a.targetMetric.aggregation, true)
		}
		switch (a.type) {
		case e.constants.CONDITION_TYPE_SELF_VALUE:
			return f;
		case e.constants.CONDITION_TYPE_TO_ANOTHER_METRIC:
			return e.utils
					.localize(
							"summarizationbar.edit.panel.define-alert-tab.display-options-panel.display-columns.to-another-condition-name",
							f, b);
		case e.constants.CONDITION_TYPE_PART_TO_WHOLE:
			return e.utils
					.localize(
							"summarizationbar.edit.panel.define-alert-tab.display-options-panel.display-columns.part-to-whole-condition-name",
							f)
		}
	},
	handleAlertDescriptionChange : function(c) {
		var b = this;
		var a = b.getCurrentConfig();
		a.alertConfig.description = c
	},
	handleAlertColorSelect : function(c, d) {
		var b = this;
		var a = b.getCurrentConfig();
		c.el.applyStyles({
			background : "none repeat scroll 0 0 " + d
		});
		a.alertConfig.color = d
	},
	handleAlertColorAfterRender : function(c) {
		var b = this;
		var a = b.getCurrentConfig().alertConfig.color;
		c.el.applyStyles({
			background : "none repeat scroll 0 0 " + a
		})
	},
	handleAlertIsCustomWidthCheck : function(c) {
		var d = this;
		var a = d.getCurrentConfig();
		a.alertConfig.isCustomWidth = c;
		var b = Ext.getCmp(d.portletId + "alert_custom_max_width");
		if (c) {
			b.enable()
		} else {
			b.disable()
		}
	},
	handleAlertCustomWidthChange : function(d, b) {
		var c = this;
		var a = c.getCurrentConfig();
		if (Ext.isEmpty(d)) {
			Ext.getCmp(c.portletId + "alert_custom_max_width").setValue(b)
		} else {
			a.alertConfig.customWidth = d
		}
	},
	handleAlertMaxNumberChange : function(d, b) {
		var c = this;
		var a = c.getCurrentConfig();
		if (Ext.isEmpty(d)) {
			Ext.getCmp(c.portletId + "alert_max_alert_number").setValue(b)
		} else {
			a.alertConfig.maxNumber = d
		}
	},
	handleAlertDisplayColumnCheck : function(e, h) {
		var g = this;
		var b = g.getCurrentConfig().alertConfig.displayColumnsList;
		var d = Ext.getCmp(g.portletId + "alert_display_column_name_" + h);
		var f = Ext.getCmp(g.portletId + "alert_display_column_auto_name_" + h);
		var a = Ext.getCmp(g.portletId + "alert_display_column_custom_name_"
				+ h);
		if (e) {
			f.enable();
			a.enable()
		} else {
			f.disable();
			a.disable()
		}
		for (var c = 0; c < b.length; c++) {
			if (b[c].id == h) {
				b[c].isDisplay = e;
				if (e && b[c].isCustomName) {
					d.enable()
				} else {
					d.disable()
				}
				break
			}
		}
	},
	handleAlertDisplayColumnNameChange : function(f, b, e) {
		var d = this;
		var a = d.getCurrentConfig().alertConfig.displayColumnsList;
		for (var c = 0; c < a.length; c++) {
			if (a[c].id == e) {
				if (a[c].isCustomName) {
					if (Ext.isEmpty(f)) {
						b = b ? b : a[c].autoGenName;
						Ext.getCmp(
								d.portletId + "alert_display_column_name_" + e)
								.setValue(b)
					} else {
						a[c].customName = f
					}
				} else {
					a[c].autoGenName = f
				}
				break
			}
		}
	},
	handleAlertDisplayColumnNameCustomCheck : function(e, g) {
		var f = this;
		var b = f.getCurrentConfig().alertConfig.displayColumnsList;
		var d = Ext.getCmp(f.portletId + "alert_display_column_name_" + g);
		for (var c = 0; c < b.length; c++) {
			if (b[c].id == g) {
				b[c].isCustomName = e;
				if (e) {
					d.enable();
					var a = b[c].customName ? Ext.util.Format
							.htmlDecode(b[c].customName) : b[c].autoGenName;
					d.setValue(a);
					b[c].customName = a
				} else {
					d.disable();
					d.setValue(b[c].autoGenName)
				}
				break
			}
		}
	},
	handleAlertOtherPageCheck : function(a) {
		var b = this;
		b.getCurrentConfig().alertConfig.isOtherPage = a;
		var c = Ext.getCmp(b.portletId + "alert_refinement_action_target_page");
		if (a) {
			c.enable();
			c.validate()
		} else {
			c.clearInvalid();
			c.disable()
		}
	},
	handleAlertTargetPageChange : function(c, a) {
		var b = this;
		if (Ext.isEmpty(c)) {
			Ext.getCmp(b.portletId + "alert_refinement_action_target_page")
					.setValue(a);
			return
		}
		b.getCurrentConfig().alertConfig.targetPage = c
	},
	getCurrentConfig : function() {
		return this.editModel.configs[this.summarizationIndex]
	},
	getSummarizationConfig : function(c) {
		var b = this;
		for (var a = 0; a < b.editModel.configs.length; a++) {
			if (b.editModel.configs[a].id == c) {
				return b.editModel.configs[a]
			}
		}
	},
	updateSummarizationNameInStore : function(g, d) {
		var f = this;
		var a = Ext.StoreMgr.get(f.portletId + "summarization_store");
		var e = this.getCurrentConfig();
		var c = a.find("id", g);
		if (c != -1) {
			var b = a.getAt(c);
			if (f.summarizationIndex == -1) {
				if (b.get("type") == f.constants.SUMMARIZATION_TYPE_ALERT) {
					b.get("alertConfig")["displayName"] = d
				} else {
					b.get("itemConfig")["isCustomDisplayName"] = true;
					b.get("itemConfig")["customDisplayName"] = d
				}
			}
			b.set("name", d);
			b.commit()
		}
	},
	checkConfigCompleted : function(c, b) {
		var d = this;
		var a = c.infoConfig && c.infoConfig.isDisplay
				&& !c.infoConfig.dimensionKey;
		if (a) {
			return false
		}
		if (b === undefined && !d.isCurrentConfigSaved(c)) {
			return true
		}
		if (b) {
			delete c.isNew;
			delete c.isChangeName
		}
		switch (c.type) {
		case d.constants.SUMMARIZATION_TYPE_METRIC_SINGLE:
			if (b) {
				delete c.itemConfig.dimensionKey;
				delete c.itemConfig.dimensionFormatter;
				delete c.itemConfig.targetMetricKey;
				delete c.itemConfig.targetMetricAggre;
				delete c.itemConfig.targetMetricFormatter;
				c.clearAlertConfig()
			}
			return !Ext.isEmpty(c.itemConfig.metricKey);
		case d.constants.SUMMARIZATION_TYPE_METRIC_RATIO:
			if (b) {
				delete c.itemConfig.dimensionKey;
				delete c.itemConfig.dimensionFormatter;
				c.clearAlertConfig()
			}
			return !Ext.isEmpty(c.itemConfig.metricKey)
					&& !Ext.isEmpty(c.itemConfig.targetMetricKey);
		case d.constants.SUMMARIZATION_TYPE_METRIC_PART_TO_WHOLE:
			if (b) {
				delete c.itemConfig.dimensionKey;
				delete c.itemConfig.dimensionFormatter;
				delete c.itemConfig.targetMetricKey;
				delete c.itemConfig.targetMetricAggre;
				delete c.itemConfig.targetMetricFormatter;
				c.clearAlertConfig()
			}
			return !Ext.isEmpty(c.itemConfig.metricKey);
		case d.constants.SUMMARIZATION_TYPE_DIMENSION_SINGLE:
			if (b) {
				delete c.itemConfig.targetMetricKey;
				delete c.itemConfig.targetMetricAggre;
				delete c.itemConfig.targetMetricFormatter;
				c.clearAlertConfig()
			}
			return !Ext.isEmpty(c.itemConfig.metricKey)
					&& !Ext.isEmpty(c.itemConfig.dimensionKey);
		case d.constants.SUMMARIZATION_TYPE_DIMENSION_RATIO:
			if (b) {
				c.clearAlertConfig()
			}
			return !Ext.isEmpty(c.itemConfig.metricKey)
					&& !Ext.isEmpty(c.itemConfig.targetMetricKey)
					&& !Ext.isEmpty(c.itemConfig.dimensionKey);
		case d.constants.SUMMARIZATION_TYPE_ALERT:
			if (b) {
				c.clearItemConfig();
				c.clearConditionsConfig()
			}
			return c.alertConfig.dimensionsList.length != 0
					&& d.checkAlertHasValidConditon(c.alertConfig)
		}
	},
	handleTabChange : function(h, g, d) {
		var e = this;
		if (!g) {
			return false
		}
		if (!Ext.isEmpty(Ext.getCmp(e.portletId + "_prev_btn"))) {
			if (g.itemId == e.portletId + "step_select_data") {
				Ext.getCmp(e.portletId + "_prev_btn").setDisabled(true);
				Ext.getCmp(e.portletId + "_next_btn").setDisabled(false)
			} else {
				if (g.itemId == e.portletId + "step_define_conditions"
						|| g.itemId == e.portletId + "step_define_alert") {
					Ext.getCmp(e.portletId + "_prev_btn").setDisabled(false);
					Ext.getCmp(e.portletId + "_next_btn").setDisabled(true)
				} else {
					Ext.getCmp(e.portletId + "_prev_btn").setDisabled(false);
					Ext.getCmp(e.portletId + "_next_btn").setDisabled(false)
				}
			}
		}
		if (d[g.itemId].rendered !== false) {
			d[g.itemId].loadDataToUI()
		}
		var b = e.getCurrentConfig();
		var c, f;
		if (b.type == this.constants.SUMMARIZATION_TYPE_ALERT) {
			c = {
				summarizationName : b.alertConfig.displayName
			}
		} else {
			c = {
				summarizationName : b.name
			}
		}
		if (Ext.getCmp(g.itemId + "_header").rendered) {
			Ext.getCmp(g.itemId + "_header").update(c)
		}
		if (g.itemId == e.portletId + "step_define_conditions") {
			e.checkCFEnabled();
			var a = Ext.getCmp(e.portletId + "condition_type_self_value")
					.getValue();
			if (a) {
				b.conditionEnabled = true
			}
		}
	},
	getUniqueDefaultSummarizationName : function() {
		var d = this;
		var e = d.utils.localize("summarizationbar.edit.button.new-item-name")
				+ " ";
		var a;
		for (var c = 0; c < d.editModel.configs.length; c++) {
			var b = d.editModel.configs[c];
			a = e + (c + 1);
			if (d.isValidSummarizationName(a)) {
				return a
			}
		}
		return e
	},
	isValidSummarizationName : function(d) {
		var c = this;
		for (var b = 0; b < c.editModel.configs.length; b++) {
			var a = c.editModel.configs[b];
			if (d == a.name) {
				return false
			}
		}
		return true
	},
	addSummarization : function(g) {
		var e = this;
		var a = Ext.StoreMgr.get(e.portletId + "summarization_store");
		var d = a.recordType;
		var c = e.editModel.getDefaultSummarizationConfig();
		c.id = g.summarizationId;
		c.name = g.summarizationName;
		c.itemConfig.customDisplayName = g.summarizationName;
		c.alertConfig.displayName = g.summarizationName;
		c.semanticKey = e.lastSemanticKey;
		c.dataSourceId = e.lastDataSourceId;
		c.isNew = true;
		var f = new d(c);
		a.add(f);
		var b = Ext.getCmp(e.portletId + "summarizations_panel");
		b.add(g);
		b.doLayout();
		e.editModel.configs.push(c);
		if (!Ext.isEmpty(e.editModel.configs)) {
			Ext.getCmp(e.portletId + "show_or_hide_preview_btn").enable();
			Ext.getCmp(e.portletId + "update_preview_btn").enable()
		}
	},
	switchToSummarizationPanelByIndex : function(a) {
		this.summarizationIndex = a
	},
	switchToSummarizationPanelById : function(c) {
		var a = Ext.StoreMgr.get(this.portletId + "summarization_store");
		var b = a.find("id", c);
		this.summarizationIndex = b
	},
	startEditingSummarizationName : function(a, b) {
		if (!a.editing) {
			a
					.startEdit(b, Ext.util.Format
							.htmlDecode(Ext.get(b).dom.innerHTML))
		}
	},
	deleteSummarizationBar : function(f) {
		var e = this;
		var a = Ext.StoreMgr.get(e.portletId + "summarization_store");
		var c = a.find("id", f);
		a.removeAt(c);
		e.editModel.configs.splice(c, 1);
		var b = Ext.getCmp(e.portletId + "summarizations_panel");
		b.remove(e.portletId + "summarization_" + f);
		if (Ext.isEmpty(e.editModel.configs)) {
			var d = Ext.getCmp(e.portletId + "show_or_hide_preview_btn");
			d.disable();
			Ext.getCmp(e.portletId + "update_preview_btn").disable();
			if (d.getText() !== e.utils
					.localize("summarizationbar.edit.button.show-preview")) {
				e.togglePreviewPanel(d)
			}
		}
	},
	registerSummaryItemsDD : function() {
		var b = this;
		if (b.summaryItemsDD) {
			b.summaryItemsDD.destroy()
		}
		var a = Ext.get(b.portletId + "summarizations_panel").dom;
		b.summaryItemsDD = new Endeca.Portlets.SummarizationBarPortlet.DDCore({
			container : a,
			itemSelectors : [ ".dd-item" ],
			dragGroups : [ b.portletId + "summarizations_panel" ],
			selfDragOnly : true,
			listeners : {
				afterdrop : function(c, g, f, e, d) {
					b.sortSummarizationStore(g, f)
				}
			}
		})
	},
	sortSummarizationStore : function(g, h) {
		var f = Ext.StoreMgr.get(this.portletId + "summarization_store");
		var b = f.getCount();
		if (h < b) {
			var i = f.getAt(g);
			f.removeAt(g);
			f.insert(h, i);
			var e = this.editModel.configs;
			var c = e[g];
			e.splice(g, 1);
			var a, d;
			a = e.slice(0, h);
			d = e.slice(h);
			a.push(c);
			this.editModel.configs = a.concat(d)
		}
	},
	handleExit : function(c, a) {
		var b = this;
		c.stopEvent();
		var d = Ext.decode(Ext.encode(b.editModel));
		if (b.isEditModelChanged(d, b.initEditModel)) {
			a.show()
		} else {
			a.close();
			b.returnToFullPage()
		}
	},
	togglePreviewPanel : function(b) {
		var c = this;
		var d = Ext.getCmp(c.portletId + "update_preview_btn");
		var a = Ext.getCmp(c.portletId + "preview_panel");
		if (b.getText() === c.utils
				.localize("summarizationbar.edit.button.hide-preview")) {
			d.hide();
			a.collapse(false)
		} else {
			d.show();
			d.ownerCt.doLayout();
			a.show();
			a.expand(false)
		}
		b.setText(b.getText().toggle(
				c.utils.localize("summarizationbar.edit.button.show-preview"),
				c.utils.localize("summarizationbar.edit.button.hide-preview")))
	},
	returnToFullPage : function() {
		window.location = Ext.select("a.portlet-icon-back").first()
				.getAttribute("href")
	},
	handleDisplayNamePositionChange : function(a) {
		var b = this;
		b.editModel.settingsModel.namePosition = a
	},
	handleValueTextSizeChange : function(b) {
		var a = this;
		a.editModel.settingsModel.valueTextSize = b
	},
	handleLabelTextSizeChange : function(b) {
		var a = this;
		a.editModel.settingsModel.labelTextSize = b
	},
	isEditModelChanged : function(b, a) {
		return !(this.compareTwoObject(b, a) && this.compareTwoObject(a, b))
	},
	compareTwoObject : function(b, a) {
		if (b != null && a != null && typeof (a) == "object"
				&& typeof (b) == "object") {
			for ( var c in b) {
				if (typeof (b[c]) == "function") {
					continue
				}
				if (b.hasOwnProperty(c) && a.hasOwnProperty(c)) {
					if (!this.compareTwoObject(b[c], a[c])) {
						return false
					}
				} else {
					return false
				}
			}
			return true
		} else {
			return b == a
		}
	},
	updateEditModel : function(a) {
		var b = this;
		b.checkEditModelConfigs(true);
		Ext.Ajax.request({
			url : b.resourceUrls.postEditModel,
			success : function(c) {
				b.utils.displaySuccessMessage(c.responseText);
				b.initEditModel = Ext.decode(Ext.encode(b.editModel));
				Ext.getCmp(b.portletId + "summarization_grid").getView()
						.refresh();
				if (a) {
					b.returnToFullPage()
				}
			},
			failure : function(c) {
				b.utils.displayErrorMessage(c.responseText)
			},
			params : {
				editModel : Ext.encode(b.editModel)
			}
		})
	},
	checkEditModelConfigs : function(a) {
		var d = this;
		for (var c = 0; c < d.editModel.configs.length; c++) {
			var b = d.editModel.configs[c];
			b.isCompleted = d.checkConfigCompleted(b, a)
		}
	},
	isCurrentConfigSaved : function(a) {
		var c = this;
		for (var b = 0; b < c.initEditModel.configs.length; b++) {
			if (a.id == c.initEditModel.configs[b].id) {
				return true
			}
		}
	},
	isEditModelConfigEmpty : function() {
		var a = this;
		return Ext.isEmpty(a.editModel.configs)
	},
	revertEditModel : function() {
		var a = this;
		a.asyncObject.doLoad(a.resourceUrls.revertEditView)
	},
	showRevertPrefsConfirmWin : function(a) {
		a.show()
	},
	updateSummarizationAction : function(c) {
		var b = this;
		var a = b.getCurrentConfig().itemConfig;
		a.action = c
	},
	itemClearDescription : function(b, c) {
		var a = this;
		var g = a.getCurrentConfig();
		if (g.isCustomerizedDesc) {
			return
		}
		var h = g.getType();
		var i = Ext.getCmp(a.portletId + "metric_description");
		var e = i.getValue() || "";
		var f = a.utils.getFieldValueFromMetric(g.dataSourceId, g.semanticKey,
				b, c, true);
		var d = f + ": " + a.constants.METRIC_VALUE_TOKEN_SPACE;
		if (e.indexOf(d) > -1) {
			e = e.replace(d, "");
			i.setValue(e);
			i.fireEvent("change", i, e)
		}
	},
	itemClearActionPanel : function() {
		var d = this;
		var a = d.getCurrentConfig();
		var c = a.getType();
		if (c == d.constants.SUMMARIZATION_TYPE_METRIC_VALUE) {
			d.updateSummarizationAction(undefined);
			var b = Ext.getCmp(d.portletId + "action_panel");
			Ext
					.each(
							b.parameters,
							function(e) {
								b
										.removeRecordFromArray(
												e,
												Ext.ux.endeca.ActionsConfigurationEnums.parameterType.URL)
							});
			delete b.parameters;
			b.setConfigValue("parameters", undefined);
			if (b.rendered) {
				b.addParamGrid()
			}
		}
	},
	getAllAttributes : function() {
		var d = this;
		var a = d.getCurrentConfig();
		var c = a.itemConfig;
		var b = d.utils.getAttributes(a.dataSourceId, a.semanticKey);
		Ext.each(b, function(e) {
			Ext.apply(e, {
				name : e.dn,
				displayName : e.dn
			})
		});
		return b
	},
	getParameters : function() {
		var f = this;
		var a = [];
		var b = f.getCurrentConfig();
		var c = b.itemConfig;
		var d = {};
		if (c.metricKey == f.constants.COUNT_ONE_KEY) {
			Ext.apply(d, {
				attributeKey : f.constants.COUNT_ONE_KEY,
				dn : f.utils
						.localize("summarizationbar.edit.label.record-count"),
				datatype : f.constants.MDEX_DATA_TYPE_LONG,
				aggreMethods : f.constants.AGGREGATION_FUNCTION_RECORD_COUNT,
				description : "",
				name : f.constants.AGGREGATION_FUNCTION_RECORD_COUNT,
				displayName : f.utils
						.localize("summarizationbar.edit.label.record-count")
			});
			a.push(d)
		} else {
			d = f.utils.getAttributeByKey(b.dataSourceId, b.semanticKey,
					c.metricKey);
			if (d) {
				if (!Ext.isEmpty(c.metricAggre)) {
					Ext.apply(d, {
						name : c.metricKey + "_" + c.metricAggre,
						displayName : c.metricKey + "("
								+ c.metricAggre.toLowerCase() + ")"
					})
				} else {
					Ext.apply(d, {
						name : b.name,
						displayName : b.name
					})
				}
				a.push(d)
			}
		}
		var e = b.getType();
		if (e == f.constants.SUMMARIZATION_TYPE_DIMENSION_VALUE) {
			var g = f.utils.getAttributeByKey(b.dataSourceId, b.semanticKey,
					c.dimensionKey);
			if (g) {
				Ext.apply(g, {
					name : c.dimensionKey,
					displayName : c.dimensionKey
				});
				a.push(g)
			}
		}
		return a
	}
};