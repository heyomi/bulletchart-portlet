<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%-- TODO REVIEW remove temptation of c/fmt/fn? --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://endeca.com/discovery" prefix="edisc"%>

<portlet:defineObjects /><%-- establish the renderRequest, renderResponse, and portletConfig for use in the JSP --%>

<%-- We can't assume that liferay-display.xml imported our JavaScript successfully - call import-javascript-dependencies as a backup --%>
<%@ include file="import-view-js-dependencies.jspf" %>

<%-- Static properties --%>
<edisc:getStaticProperty var="getSummarizations" className="com.endeca.portlet.summarizationbar.SummarizationBarConstants" property="RESOURCE_GET_SUMMARIZATIONS"/>
<edisc:getStaticProperty var="refinementActionName" className="com.endeca.portlet.summarizationbar.SummarizationBarConstants" property="ACTION_REFINEMENT"/>
<edisc:getStaticProperty var="hyperlinkActionName" className="com.endeca.portlet.summarizationbar.SummarizationBarConstants" property="ACTION_HYPERLINK"/>
<edisc:getStaticProperty var="updateShowConfigMessage" className="com.endeca.portlet.summarizationbar.SummarizationBarConstants" property="RESOURCE_UPDATE_SHOW_CONFIG_MESSAGE"/>

<!-- This is the view page where render to -->
<div id="<portlet:namespace />Error" class="eid-feedback-within-component-fail" style="display:none"></div>
<div id="<portlet:namespace />Warning" class="eid-feedback-within-component-warning" style="display:none"></div>
<div id="<portlet:namespace />Success" class="eid-feedback-within-component-success" style="display:none"></div>
<div id="<portlet:namespace />Info" class="eid-feedback-within-component-info" style="display:none"></div>
<div id="<portlet:namespace/>summarization_bar_view" class="eid-summarization-bar-view"></div>

<script type="text/javascript" language="javascript">
	<%-- load the resource strings and urls --%>  
	var e<portlet:namespace/>summarizationBarController;
	if(!e<portlet:namespace/>summarizationBarController) {
		e<portlet:namespace/>summarizationBarController = new Endeca.Portlets.SummarizationBarPortlet.Controller('<portlet:namespace/>');
		e<portlet:namespace/>summarizationBarController.showConfigMessage = ${showConfigMessage};
		e<portlet:namespace/>summarizationBarController.viewModel = new Endeca.Portlets.SummarizationBarPortlet.ViewModel(${viewModel});
		e<portlet:namespace/>summarizationBarController.resources = <edisc:initJSLanguageUtils/>;
		e<portlet:namespace/>summarizationBarController.resourceUrls = {
				getSummarizationsUrl: '<portlet:resourceURL escapeXml="false" id="${getSummarizations}"/>',
				refinementActionUrl : '<portlet:actionURL name="${refinementActionName}" windowState="EXCLUSIVE"/>',
				hyperlinkActionUrl : '<portlet:actionURL name="${hyperlinkActionName}" windowState="EXCLUSIVE"/>',
				updateShowConfigMessageUrl : '<portlet:resourceURL escapeXml="false" id="${updateShowConfigMessage}"/>',
		};
		e<portlet:namespace/>summarizationBarController.conditionalStyleMap = ${conditionalStyle};
	}
	
	<%-- tell Ext to call initializeView() when the portlet is ready --%>
	Ext.onReady(function() {
		if(e<portlet:namespace/>summarizationBarController) {
			e<portlet:namespace/>summarizationBarController.initializeView();
		}
	});
</script>
