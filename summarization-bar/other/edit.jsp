<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://endeca.com/discovery" prefix="edisc"%>
<portlet:defineObjects />

<%-- We can't assume that liferay-display.xml imported our JavaScript successfully - call import-javascript-dependencies as a backup --%>
<%@ include file="import-edit-js-dependencies.jspf" %>

<%-- Static properties --%>
<edisc:getStaticProperty var="postEditModelName" className="com.endeca.portlet.summarizationbar.SummarizationBarConstants" property="RESOURCE_SAVE_PREFS"/>
<edisc:getStaticProperty var="revertEditViewName" className="com.endeca.portlet.summarizationbar.SummarizationBarConstants" property="ACTION_REVERT_PREFS"/>
<edisc:getStaticProperty var="getEditModel" className="com.endeca.portlet.summarizationbar.SummarizationBarConstants" property="RESOURCE_GET_EDIT_MODEL" />
<edisc:getStaticProperty var="getSummarizations" className="com.endeca.portlet.summarizationbar.SummarizationBarConstants" property="RESOURCE_GET_SUMMARIZATIONS"/>
<edisc:getStaticProperty var="refinementActionName" className="com.endeca.portlet.summarizationbar.SummarizationBarConstants" property="ACTION_REFINEMENT"/>
<edisc:getStaticProperty var="hyperlinkActionName" className="com.endeca.portlet.summarizationbar.SummarizationBarConstants" property="ACTION_HYPERLINK"/>

<edisc:getStaticProperty var="getViewData" className="com.endeca.portlet.util.Constants" property="RESOURCE_GET_SEMANTIC_VIEW_DATA" />
<edisc:getStaticProperty var="formattingPreview" className="com.endeca.portlet.util.Constants" property="RESOURCE_FORMATTING_PREVIEW" />
<edisc:getStaticProperty var="formattingGetCurrencyList" className="com.endeca.portlet.util.Constants" property="RESOURCE_FORMATTING_GET_CURRENCY_LIST" />

<!-- This is the edit page where render to -->
<div id="<portlet:namespace />Warning" class="eid-feedback-within-component-warning" style="display:none"></div>
<div id="<portlet:namespace />Success" class="eid-feedback-within-component-success" style="display:none"></div>
<div id="<portlet:namespace />Error" class="eid-feedback-within-component-fail" style="display:none"></div>
<div id="<portlet:namespace />Info" class="eid-feedback-within-component-info" style="display:none"></div>
<div id="<portlet:namespace/>summarization_bar_edit" class="eid-summarization-bar-edit eid-configuration-edit"></div>

<script type="text/javascript">
	<%-- instantiate the javascript object  --%>

	<%-- load the resource strings and urls --%>  
	var e<portlet:namespace/>summarizationBarController;
	if(!e<portlet:namespace/>summarizationBarController) {
		e<portlet:namespace/>summarizationBarController = new Endeca.Portlets.SummarizationBarPortlet.Controller('<portlet:namespace/>');
		e<portlet:namespace/>summarizationBarController.resources = <edisc:initJSLanguageUtils/>;
		e<portlet:namespace/>summarizationBarController.formatterPanelResource = <%=com.realdecoy.portal.format.FormattingUtils.formatterPanelResources(renderRequest)%>;
		e<portlet:namespace/>summarizationBarController.resourceUrls = {
				postEditModel: '<portlet:resourceURL escapeXml="false" id="${postEditModelName}" />',
				revertEditView : '<portlet:actionURL name="${revertEditViewName}" windowState="EXCLUSIVE"/>',
				getViewDataUrl : '<portlet:resourceURL escapeXml="false" id="${getViewData}"/>',
				getSummarizationsUrl: '<portlet:resourceURL escapeXml="false" id="${getSummarizations}"/>',
				formatterPreviewUrl : '<portlet:resourceURL id="${formattingPreview}" />',
				formatterGetCurrencyListUrl : '<portlet:resourceURL id="${formattingGetCurrencyList}" />',
				getEditModelUrl : '<portlet:resourceURL id="${getEditModel}" />',
				refinementActionUrl : '<portlet:actionURL name="${refinementActionName}" windowState="EXCLUSIVE"/>',
				hyperlinkActionUrl : '<portlet:actionURL name="${hyperlinkActionName}" windowState="EXCLUSIVE"/>'
		};
		e<portlet:namespace/>summarizationBarController.conditionalStyleMap = ${conditionalStyle};
	}
		
	<%-- tell Ext to call initializeEdit() when the portlet is ready --%>
	Ext.onReady(function() {
		if (e<portlet:namespace/>summarizationBarController) {
			e<portlet:namespace/>summarizationBarController.dataSourceData = ${dataSourceData};
			e<portlet:namespace/>summarizationBarController.initializeEdit(${isRevert});
		}
	});
	
	function postRender<portlet:namespace/>(asyncObject) {
		asyncObject.initAsync(); 

		/* set the async object onto the main object */ 
		e<portlet:namespace/>summarizationBarController.asyncObject = asyncObject; 
   	}
</script>
